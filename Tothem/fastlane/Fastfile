
fastlane_version "2.123.0"

default_platform :ios

platform :ios do

  before_all do
    #xcode_select "/Applications/Xcode.app"
  end

  lane :setup_app do
    sh "cp ../../Resources/App-Info.plist ../TOTHEM/Info.plist"
    sh "cp -r ../totem-ios-env/" + ENV["ENVIRONMENT"] + "/* ../TOTHEM"
    instabug
  end

  lane :setup_env do
    git_submodule_update
    sh "cp -r ../totem-ios-env/.env* ../fastlane"
    sh "cp -r ../totem-ios-env/actions ../fastlane"
  end

  desc "Submit a new Beta Build to Apple TestFlight"
  desc "This will also make sure the profile is up to date"
  lane :beta do
    setup_app
    cocoapods
    build_rc
  end

  desc "Runs all the tests"
  lane :test do
    setup_app
    cocoapods
    scan(scheme: 'TOTHEM', configuration: 'Testing', devices: 'iPhone 8', skip_build: true)
  end

  ##################################################
  desc "Setup instabug"
  lane :instabug do
    env = ENV["ENVIRONMENT"]
    plist_file = ENV["PLIST"]
    set_info_plist_value(path: plist_file, key: "InstabugToken", value: ENV["INSTABUG_TOKEN"])
  end

  desc "Do initial things for the build"
  lane:prepare do
    env = ENV["ENVIRONMENT"]
    plist_file = ENV["PLIST"]
    $build_version = get_info_plist_value(path: ENV["PLIST"], key: 'CFBundleShortVersionString')

    git_pull(only_tags: true)
    $latest_tag = sh "git tag | grep ^" + env + " | sort -V | tail -1"
    $latest_tag = $latest_tag.gsub("\n", "")

    build_number = $latest_tag.split('.')[2].to_i + 1
    $build_number = build_number.to_s

    $current_version = env + "-" + $build_version + "." + $build_number

    puts "Current version is: " + $current_version
    puts "Build Version is: " + $build_version
    puts "Build Number is: " + $build_number
    puts "Latest Tag is: " + $latest_tag
    set_info_plist_value(path: plist_file, key: 'CFBundleVersion', value: $build_number)
    set_info_plist_value(path: plist_file, key: "CFBundleName", value: ENV["BUNDLE_NAME"])
    set_info_plist_value(path: plist_file, key: "CFBundleDisplayName", value: ENV["BUNDLE_NAME"])

    update_project_settings(
        project: ENV["PROJ_PATH"],
        bundle_id: ENV["BUNDLE_ID"],
        config: ENV["CONFIGURATION"],
        profile: ENV["PROVISIONING_PROFILE"],
        appicon_asset_name: ENV["APPICON_ASSET_NAME"]
    )
  end # end init lane
  ##################################################
  desc "Build RC"
  lane :build_rc do |options|
    prepare

    add_git_tag(tag: $current_version)
    push_git_tags

    #telegram_main(text: "#{$current_version} build started")

    match(type: "appstore", app_identifier: ENV["BUNDLE_ID"])
    gym(export_method: "app-store", configuration: ENV["CONFIGURATION"])
    pilot(skip_waiting_for_build_processing: true)

    #telegram_main(text: "#{$current_version} successfully deployed to Testflight")
    #$branch_name = git_branch
    #telegram_main(text: "#{$current_version} branch: #{$branch_name}")
  end # end build_rc lane

  ##################################################
  # You can define as many lanes as you want
  ##################################################
  desc "Execute match"
  lane :build_match do |options|
    match(type: "appstore", app_identifier: ENV["BUNDLE_ID"])
  end # end build_rc lane
  ##################################################
  desc "Set provisioning profiles"
  lane :update_project_settings do |params|
    proj = params[:project]
    prov_name = params[:profile]
    bundle_id = params[:bundle_id]
    configuration=params[:config]
    appicon_asset_name=params[:appicon_asset_name]
    target_filter="application"

    require 'xcodeproj'

    project = Xcodeproj::Project.open(proj)
    project.targets.each do |target|
      if (target.respond_to?(:product_type) && target.product_type.match(target_filter))
        UI.success("Updating target #{target.product_name}...")
      else
        UI.important("Skipping target #{target.product_name} as it doesn't match the filter '#{target_filter}'")
        next
      end

      target.build_configuration_list.build_configurations.each do |build_configuration|
        config_name = build_configuration.name
        if !configuration || config_name.match(configuration)
          UI.success("Updating configuration #{config_name}...")
        else
          UI.important("Skipping configuration #{config_name} as it doesn't match the filter '#{configuration}'")
          next
        end

        build_configuration.build_settings['PROVISIONING_PROFILE_SPECIFIER'] = prov_name
        build_configuration.build_settings['PRODUCT_BUNDLE_IDENTIFIER'] = bundle_id
        build_configuration.build_settings['ASSETCATALOG_COMPILER_APPICON_NAME'] = appicon_asset_name
      end
    end
    project.save
  end
  ##################################################
  after_all do |lane|
    # This block is called, only if the executed lane was successful
  end

  error do |lane, exception|
    #telegram_main(text: "#{$current_version} build failed")
    #telegram(text: "Build failure #{exception.message}")
  end
end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://docs.fastlane.tools/actions

# fastlane reports which actions are used. No personal data is recorded.
# Learn more at https://github.com/fastlane/fastlane#metrics
