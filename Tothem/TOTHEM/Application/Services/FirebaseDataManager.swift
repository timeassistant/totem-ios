//
//  FirebaseDataManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import RxSwift

struct UserModel {
    var uid: String
    var email: String?
}

final class FirebaseDataManager {
    static let shared = FirebaseDataManager()
    let reference: DatabaseReference
    let storage: StorageReference
    
    var currentUser: UserModel? {
        return map(Auth.auth().currentUser)
    }
    
    private init() {
        FirebaseApp.configure()
        let database = Database.database()
        database.isPersistenceEnabled = true
        reference = database.reference()
        storage = Storage.storage().reference()
    }
    
    func signOut() throws {
        try Auth.auth().signOut()
    }
    
    private func map(_ user: User?) -> UserModel? {
        guard let model = user else { return nil}
        return UserModel(uid: model.uid, email: model.email)
    }
    
    func createUser(email: String, password: String) -> Observable<UserModel?> {
        return Auth.auth()
            .rx_createUserWithEmail(email: email, password: password)
            .map({ [weak self] in self?.map($0) })
    }
    
    func signin(email: String, password: String) -> Observable<UserModel?> {
        return Auth.auth()
            .rx_signinWithEmail(email: email, password: password)
            .map({ [weak self] in self?.map($0) })
    }
    
    class func configure(_ file: String, type: String) -> DataSource {
        if let path = Bundle.main.path(forResource: file, ofType: type),
            let options = FirebaseOptions(contentsOfFile: path) {
            FirebaseApp.configure(name: file, options: options)
        }
        
        if let app = FirebaseApp.app(name: file) {
            let paramsDatabase = Database.database(app: app)
            paramsDatabase.isPersistenceEnabled = true
            return paramsDatabase.reference()
        } else {
            fatalError("Parameters application is not set")
        }
    }
}
