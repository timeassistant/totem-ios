//
//  EngineService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class EngineTimelineService: TimelineServiceProtocol {
    let prossessor: Prossessor
    let stateService: StateTimelineService

    private var centerView: UIView {
        return prossessor.centerStack.controller.view
    }

    private var leftView: UIView {
        return prossessor.leftStack.controller.view
    }

    private var rightView: UIView {
        return prossessor.rightStack.controller.view
    }
    
    var isChangingPosition: Observable<ScrollShift> {
        return horizontalScroll.isChangingPosition.asObservable()
    }

    var positionBackgroundAlphaColor = BehaviorSubject<TimesetType>(value: .center)
	var backgroundColorShiftValue = BehaviorSubject<CGFloat>(value: 1)

    // Containers view
    private var timesetContainer = UIView()
    private var rightContainer = UIView()
    private var leftContainer = UIView()
    private var centerCoverView = UIButton()

    // Horizontal scroll view
    private var horizontalScroll: HorizontalTimesetScroll!

    //CONSTANTS
    var collectionCoverHeight: CGFloat = 0.2
    private var collectionCoverWidth: CGFloat {
        return ConfigurationFacade.shared
            .collectionViewLayout.shift()
    }

    var shiftDistance: CGFloat = 100

    init(prossessor: Prossessor, stateService: StateTimelineService) {
        self.prossessor = prossessor
        self.stateService = stateService
        self.start()
    }

    func set(bias: CGFloat) {
        horizontalScroll.horizontalBias = CGFloat(bias)
    }
    
    func scrollTo(offset: CGFloat) {
        horizontalScroll.scrollTo(offset, animated: false)
    }
    
    private func start() {
        self.setupContainers()
        self.setupCollections()
        self.setupHorizontalScroll()
        self.setupSideButtonViews()
        self.observePreviewMode()
    }

    private func setupSideButtonViews() {
        self.centerCoverView.frame = CGRect(x: UIScreen.main.bounds.width - self.collectionCoverWidth, y: 0, width: UIScreen.main.bounds.width + self.collectionCoverWidth*2, height: self.leftContainer.frame.height)
        let leftVerticalView = UIView(frame: CGRect(x: 0,
                                                    y: 0,
                                                    width: 3,
                                                    height: self.centerCoverView.frame.height))
        let rightVerticalView = UIView(frame: CGRect(x: self.centerCoverView.frame.width - 3,
                                                     y: 0,
                                                     width: 3,
                                                     height: self.centerCoverView.frame.height))
        [rightVerticalView, leftVerticalView].forEach {
            $0.backgroundColor = .gray
            self.centerCoverView.addSubview($0)
        }
        self.centerCoverView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        self.horizontalScroll.addSubview(self.centerCoverView)
        self.bindHorizontalScroll()
        self.centerCoverView.alpha = 0
    }

    private func bindHorizontalScroll() {
        self.horizontalScroll
            .isChangingPosition
            .asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.setState(with: $0)
                
                guard !self.stateService.currentPreview else { return }

                switch $0 {
                case .center(progress: let progress):
                    guard self.horizontalScroll.isLooped.value else { return }
                    UIView.animate(CATransaction.animationDuration(), animations: {
                        self.centerCoverView.alpha = progress == 1 ? 0 : 1
                    })
                case .left(progress: let progress):
                    self.setBackground(view: self.centerCoverView,
                                       for: .right)
                    guard self.horizontalScroll.isLooped.value else { return }
                    self.centerCoverView.alpha = self.centerCoverView.alpha == 1 ? 1 : progress
                case .right(progress: let progress):
                    self.setBackground(view: self.centerCoverView,
                                       for: .left)
                    guard self.horizontalScroll.isLooped.value else { return }
                    self.centerCoverView.alpha = self.centerCoverView.alpha == 1 ? 1 : progress
                }
            })
            .disposed(by: self.prossessor.dispose)
        
        self.horizontalScroll
            .isLooped
            .asObservable()
            .subscribe(onNext: { value in
                UIView.animate(CATransaction.animationDuration(), animations: {
                    self.centerCoverView.alpha = value ? (self.stateService.currentPreview ? 0 : 1) : 0
                })
            })
            .disposed(by: self.prossessor.dispose)

        self.centerCoverView.rx
            .controlEvent(.touchUpInside)
            .subscribe(onNext: { [weak self] _ in
                self?.horizontalScroll.set(side: .center)
                HapticEngine.shared.impactFeedback
                    .activateFeedback(with: .light)
            })
            .disposed(by: self.prossessor.dispose)
    }

    private func setState(with shift: ScrollShift) {
        switch shift {
        case .center(progress: let progress):
            if progress.equal(to: 1) {
                stateService.state.onNext(.center)
            }
        case .left(progress: let progress):
            if progress.equal(to: 1) {
                stateService.state.onNext(.left)
            }
        case .right(progress: let progress):
            if progress.equal(to: 1) {
                stateService.state.onNext(.right)
            }
        }
    }
    
    private func setBackground(view: UIView, for side: TimesetType) {
        ConfigurationFacade.shared
            .taskModuleConstants
            .getTaskBackgroundColorValue(side)
            .subscribe(onNext: { value in
                UIView.animate(CATransaction.animationDuration(), animations: {
                    view.backgroundColor = value.withAlphaComponent(0.5)
                })
            })
            .disposed(by: self.prossessor.dispose)
    }

    private func setupHorizontalScroll() {
        self.horizontalScroll = HorizontalTimesetScroll(frame: self.prossessor.frame, leftSide: self.leftContainer, rightSide: self.rightContainer, center: self.timesetContainer)
        horizontalScroll.fill(in: prossessor)
    }

    func horizontalScrollEnabled(isEnabled: Bool) {
        self.horizontalScroll.set(side: .center)
        self.horizontalScroll.isScrollEnabled = isEnabled
    }

    func setupContainers() {
        self.setupLeftContainer()
        self.setupRightContainer()
    }

    private func setupLeftContainer() {
        self.leftContainer.backgroundColor = .clear
    }

    private func setupRightContainer() {
        self.rightContainer.backgroundColor = .clear
    }

    private func setupCollectionCover() {
		self.setupLeftRightCollectionCover()
    }

	private func setupLeftRightCollectionCover() {
		self.prossessor.leftStack.cover.frame = CGRect(x: 0, y: 0, width: (self.prossessor.bounds.width - collectionCoverWidth), height: self.prossessor.bounds.height)

		self.prossessor.rightStack.cover.frame = CGRect(x: collectionCoverWidth, y: 0, width: (self.prossessor.bounds.width - collectionCoverWidth), height: self.prossessor.bounds.height)
	}

    private func setupCollections() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.centerView.fill(in: self.timesetContainer)
            self.leftView.fill(in: self.leftContainer)
            self.rightView.fill(in: self.rightContainer)
        }
    }

    private func observePreviewMode() {
        self.stateService
            .preview
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [unowned self] preview in
                self.horizontalScroll.isScrollEnabled = !preview
                UIView.animate(CATransaction.animationDuration(), animations: {
                    self.horizontalScroll.expandList(preview)
                    self.centerCoverView.alpha = preview ? 0 : (self.horizontalScroll.currentPosition == .center(progress: 0) || !self.horizontalScroll.isLooped.value ? 0 : 1)
                })
            })
            .disposed(by: self.prossessor.dispose)
    }
}
