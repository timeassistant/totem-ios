//
//  UsersDataService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import Firebase
import FirebaseDatabase

let firebasePositionsKey = "positions"
let firebaseTypeKey = "type"
let firebaseViewedKey = "viewed"
let firebaseIdKey = "id"

enum FirebaseManagerError: Error {
    case notFound
}

final class UsersDataService {
    static let shared = UsersDataService()
    private let dataManager = FirebaseDataManager.shared
    private init() { }

    private var currentUserDatabaseReference: DatabaseReference? {
        guard let currentUser = dataManager.currentUser else { return nil }
        return dataManager.reference
            .child("3_personalData/tasks")
            .child(currentUser.uid)
    }

    private var photosDatabaseReference: DatabaseReference? {
        guard let currentUser = dataManager.currentUser else { return nil }
        return allPhotosReference.child(currentUser.uid)
    }
    
    private var allPhotosReference: DatabaseReference {
        return dataManager.reference.child("3_personalData/photos")
    }

    var items: Observable<[TaskModel]> {
        guard let database = currentUserDatabaseReference else {
            return Observable.just([])
        }
        return UsersDataMigrationService.migrateTasks(database)
            .flatMap({ _ in database.rx_observe(eventType: .value) })
            .map { $0.children.compactMap({ $0 as? DataSnapshot })
            .map({ TaskModel(snapshot: $0) }) }
    }

    func move(task: TaskModel, to side: TimesetType) -> Completable {
        return Completable.create { event in
            let newPosition = TaskPositionModel(side: side, value: TaskModel.newItemPosition)
            if let reference = task.reference {
                var positions = task.positions.filter({ $0.side != side })
                positions.append(newPosition)
                reference.updateChildValues([
                    firebaseTypeKey: side.rawValue,
                    firebasePositionsKey: positions.map({ $0.dictionary }),
                    firebaseViewedKey: false as AnyObject
                ])
            }
            event(.completed)
            return Disposables.create()
        }
    }

    func remove(task: TaskModel) -> Observable<Void> {
        return Observable.create { observable in
            task.reference?.removeValue()
            observable.onNext(())
            observable.onCompleted()
            return Disposables.create()
        }
    }

    func update(_ items: [TaskModel]) -> Completable {
        guard let db = currentUserDatabaseReference else { return .empty() }
        
        return Completable.create { observer in
            db.runTransactionBlock({ mutableData -> TransactionResult in
                items.forEach({
                    guard let key = $0.reference?.key else { return }
                    let data = mutableData.childData(byAppendingPath: key)
                    data.value = $0.json
                })
                
                observer(.completed)
                return TransactionResult.success(withValue: mutableData)
            })
            return Disposables.create()
        }
    }

    func save(task: TaskModel) -> Observable<TaskModel> {
        task.uid = dataManager.currentUser?.uid
        return Observable.create { event in
            if let reference = task.reference {
                reference.updateChildValues(task.json)
            } else {
                if let reference = self.currentUserDatabaseReference?.child(task.id) {
                    reference.setValue(task.json)
                    task.reference = reference
                }
            }
            event.on(.next(task))
            event.on(.completed)
            return Disposables.create()
        }
    }

    func save(_ model: PhotoModel) -> Observable<Void> {
        guard !model.id.isEmpty &&
            !model.id.contains(".") &&
            !model.id.contains("#")
            else {
            return Observable.error(RxError.noElements)
        }
        
        return Observable.create { event in
            self.photosDatabaseReference?
                .child(model.id)
                .updateChildValues(model.json)
            event.onNext(())
            event.onCompleted()
            return Disposables.create()
        }
    }

    func photoFilename(uid: String, photoEntityValue: String) -> Observable<String> {
        guard let path = uid.hasVisibleCharacters ? uid : nil,
            !photoEntityValue.isEmpty &&
            !photoEntityValue.contains(".") &&
            !photoEntityValue.contains("#")
            else { return Observable.error(RxError.noElements) }
        
        return allPhotosReference
            .child(path)
            .child(photoEntityValue)
            .rx_observeSingleEventOfType(eventType: .value)
            .map ({ $0.value })
            .flatMap({ value -> Observable<String> in
                if let value = value as? JSONObject {
                    return Observable.just(PhotoModel(id: photoEntityValue, json: value).filename)
                } else {
                    return Observable.error(GenericError.notFound)
                }
            })
    }
    
    func task(uid: String? = nil, taskId: String) -> Observable<TaskModel?> {
        
        guard let userId = uid ?? dataManager.currentUser?.uid else { return .just(nil)
        }
        
        return self.dataManager
            .reference
            .child("3_personalData/tasks")
            .child(userId)
            .rx_observe(eventType: .value)
            .map { $0.children.compactMap({ $0 as? DataSnapshot })
            .map({ TaskModel(snapshot: $0) }) }
            .map({ array in array.first(where: { $0.id == taskId }) })
            .map({ $0?.uid == userId || $0?.public == true ? $0 : nil })
    }
    
    func networkTask(uid: String? = nil, taskId: String) -> Observable<JSONObject> {
        guard let userId = uid ?? dataManager.currentUser?.uid else {
            return Observable.error(GenericError.notAuthorized)
        }
        
        return NetworkDataSource().object(forEndpoint: Endpoint.UsersEndpoint.content(userId, taskId))
    }
}

extension Array {
    mutating func rearrange(from: Int, to: Int) {
        guard from != to, from < count, to < count else { return }
        insert(remove(at: from), at: to)
    }
}
