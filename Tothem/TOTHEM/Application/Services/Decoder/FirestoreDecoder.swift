//
//  FirestoreDecoder.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

public protocol FirestoreDecodable: Decodable {}
public protocol FirestoreEncodable: Encodable {}

public typealias DocumentReferenceType = FirestoreDecodable & FirestoreEncodable
public typealias FieldValueType = FirestoreEncodable

public protocol GeoPointType: FirestoreDecodable, FirestoreEncodable {
    var latitude: Double { get }
    var longitude: Double { get }
    init(latitude: Double, longitude: Double)
}

open class FirestoreDecoder {
    public init() {}

    open var userInfo: [CodingUserInfoKey: Any] = [:]

    open func decode<T: Decodable>(_ type: T.Type, from container: [String: Any]) throws -> T {
        let options = _FirebaseDecoder._Options(
            dateDecodingStrategy: nil,
            dataDecodingStrategy: nil,
            skipFirestoreTypes: true,
            userInfo: userInfo
        )
        let decoder = _FirebaseDecoder(referencing: container, options: options)
        guard let value = try decoder.unbox(container, as: T.self) else {
            throw DecodingError.valueNotFound(T.self, DecodingError.Context(codingPath: [], debugDescription: "The given dictionary was invalid"))
        }

        return value
    }
}

enum GeoPointKeys: CodingKey {
    case latitude, longitude
}

extension GeoPointType {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: GeoPointKeys.self)
        let latitude = try container.decode(Double.self, forKey: .latitude)
        let longitude = try container.decode(Double.self, forKey: .longitude)
        self.init(latitude: latitude, longitude: longitude)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: GeoPointKeys.self)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
    }
}

enum DocumentReferenceError: Error {
    case typeIsNotSupported
    case typeIsNotNSObject
}

extension FirestoreDecodable {
    public init(from decoder: Decoder) throws {
        throw DocumentReferenceError.typeIsNotSupported
    }
}

extension FirestoreEncodable {
    public func encode(to encoder: Encoder) throws {
        throw DocumentReferenceError.typeIsNotSupported
    }
}
