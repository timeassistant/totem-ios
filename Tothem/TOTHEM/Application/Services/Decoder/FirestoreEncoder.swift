//
//  CodableFirestore.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

open class FirestoreEncoder {
    public init() {}

    open var userInfo: [CodingUserInfoKey: Any] = [:]

    open func encode<Value: Encodable>(_ value: Value) throws -> [String: Any] {
        let topLevel = try encodeToTopLevelContainer(value)
        switch topLevel {
        case let top as [String: Any]:
            return top
        default:
            throw EncodingError.invalidValue(value,
                                             EncodingError.Context(codingPath: [],
                                                                   debugDescription: "Top-level \(Value.self) encoded not as dictionary."))
        }
    }

    internal func encodeToTopLevelContainer<Value: Encodable>(_ value: Value) throws -> Any {
        let options = _FirebaseEncoder._Options(
            dateEncodingStrategy: nil,
            dataEncodingStrategy: nil,
            skipFirestoreTypes: true,
            userInfo: userInfo
        )
        let encoder = _FirebaseEncoder(options: options)
        guard let topLevel = try encoder.box_(value) else {
            throw EncodingError.invalidValue(value,
                                             EncodingError.Context(codingPath: [],
                                                                   debugDescription: "Top-level \(Value.self) did not encode any values."))
        }

        return topLevel
    }
}
