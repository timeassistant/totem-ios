//
//  UsersDataMigration.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation
import RxSwift
import Firebase

final class UsersDataMigrationService {
    class func migrateTasks(_ dataSource: DataSource) -> Observable<Void> {
        guard !UserDefaults.standard.bool(forKey: "users_data_09_2019") else { return .just(()) }
        
        return dataSource.rx_object
            .flatMap({ obj -> Observable<DataSource> in
                var newObject: JSONObject = [:]
                for (_, value) in obj {
                    guard let value = value as? JSONObject,
                        let id = value["id"] as? String else { continue }
                    newObject[id] = value
                }
                return dataSource.rx_set(value: newObject)
            })
            .flatMap({ _ -> Observable<Void> in
                UserDefaults.standard.set(true, forKey: "users_data_09_2019")
                return Observable.just(())
            })
    }
}
