//
//  SuggestsService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct SuggestModel {
    var value: String
    var confidence: Float
    var user: String
}

struct SuggestsModel {
    var data: [TaskModel]
    init(_ json: JSONObject) {
        let data = json["data"] as? JSONArray ?? []
        self.data = data.map({ TaskModel($0) })
    }
}

final class SuggestsService {
    static let shared = SuggestsService()
    private let dataSource = NetworkDataSource()
    
    func develop(to task: TaskModel, side: TimesetType) -> Observable<[TaskModel]> {
        var content: String
        switch side {
        case .right: content = "actions"
        case .left: content = "note"
        default: return .error(GenericError.badParameters)
        }
        return execute(to: task, type: "development", content: content)
    }
    
    func addition(to task: TaskModel, side: TimesetType) -> Observable<[TaskModel]> {
        var content: String
        switch side {
        case .right: content = "note"
        case .left: content = "actions"
        default: return .error(GenericError.badParameters)
        }
        return execute(to: task, type: "addition", content: content)
    }
    
    private func execute(to task: TaskModel, type: String, content: String) -> Observable<[TaskModel]> {
        guard let user = try? AuthManager.shared.user.value() else {
            return .error(GenericError.badParameters)
        }
        
        let params: JSONObject = [
            "match_type": type,
            "content_type": content
        ]
        
        let endpoint = Endpoint.UsersEndpoint.similar(user.uid, task.id)
        return self.dataSource
            .object(forEndpoint: endpoint, parameters: params)
            .map ({ SuggestsModel($0).data })
    }
}
