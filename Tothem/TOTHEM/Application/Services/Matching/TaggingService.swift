//
//  TaggingService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation
import RxSwift

// 1. Существительное
// 2. Существительное + Прилагательное

final class TaggingService {
    private let taggingDataManager = TaggingDataManager()
    private let firebaseDataManager = FirebaseDataManager.shared
    private let textLabelingService = TextAnalysisService()
    
    // TODO: create tag with all entities labels
    func save(task: TaskModel) -> Observable<[String]> {
        let uid = task.uid ?? firebaseDataManager.currentUser?.uid ?? ""
        guard task.public, !uid.isEmpty else { return .just([]) }
        
        let priority = task.priority ?? .description
        
        var labelsDict: [String: AiLabelModel] = [:]
        task.items.map ({ $0.1.labels })
            .reduce([], +)
            .forEach({ labelsDict[$0.text] = $0 })
        
        guard labelsDict.count > 0 else { return .just([]) }
        
        let observables = labelsDict.map ({ dic in
            return self.taggingDataManager
                .save(priority: priority, uid: uid, taskId: task.id, label: dic.1)
                .map({ _ in dic.0 })
        })
        
        return Observable.zip(observables)
    }
    
    func content(for taskId: String, priority: TaskContentPriority, with label: AiLabelModel) -> Observable<[(String, String)]> {
        guard let uid = firebaseDataManager.currentUser?.uid else {
            return .just([])
        }
        
        return self.taggingDataManager
            .retrieve(priority: priority, label: label)
            .map({ array -> [(String, String)] in
                array.filter({ $0.0 != uid || $0.1 != taskId })
            })
    }
}
