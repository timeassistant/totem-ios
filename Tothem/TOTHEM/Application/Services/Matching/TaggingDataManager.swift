//
//  TaggingDataManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation
import RxSwift

final class TaggingDataManager {
    
    func save(priority: TaskContentPriority, uid: String, taskId: String, label: AiLabelModel) -> Observable<DataSource> {
        return FirebaseDataManager.shared
            .reference
            .child("1_matchingLabels")
            .child(priority.rawValue)
            .child(label.text)
            .child(uid)
            .child(taskId)
            .rx_setValue(value: label.confidence as AnyObject)
            .map({ $0 as DataSource })
    }
    
    func retrieve(priority: TaskContentPriority, label: AiLabelModel) -> Observable<[(String, String)]> {
        return FirebaseDataManager.shared
            .reference
            .child("1_matchingLabels")
            .child(priority.rawValue)
            .child(label.text)
            .rx_observe(eventType: .value)
            .map({ snapshot -> [(String, String)] in
                let items = snapshot.value as? JSONObject ?? [:]
                var array: [(String, String)] = []
                items.forEach({
                    guard let tasks = $0.1 as? JSONObject else { return }
                    let uid = $0.0
                    tasks.forEach({
                        array.append((uid, $0.0))
                    })
                })
                return array
            })
    }
}
