//
//  TextTranslationService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift
import Firebase

enum TranslationLanguage: String {
    case ru
    case en
}

final class TextTranslationService {
    private let dataSource = NetworkDataSource()
    
    func execute(text: String, from: TranslationLanguage, to: TranslationLanguage) -> Observable<String?> {
        let params = [
            "text": text,
            "src": from.rawValue,
            "dest": to.rawValue
        ]
        
        return self.dataSource
            .object(forEndpoint: Endpoint.ToolsEndpoint.translate, parameters: params)
            .map ({ obj in obj["text"] as? String })
    }
}
