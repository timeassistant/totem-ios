//
//  PhotoLabelService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Firebase

import RxSwift

final class PhotoLabelService {
    static let shared = PhotoLabelService()
    func execute(image: UIImage) -> Observable<[AiLabelModel]> {
        return Observable.create({ observable in
            DispatchQueue.global().async {
                let image = VisionImage(image: image)
                let labeler = Vision.vision().onDeviceImageLabeler()
                
                labeler.process(image) { labels, error in
                    if let error = error {
                        observable.onError(error)
                    }
                    
                    defer {
                        observable.onCompleted()
                    }
                    
                    guard let labels = labels else {
                        observable.onNext([])
                        return
                    }
                    
                    let lbls = labels.map({ lbl -> AiLabelModel in
                        return AiLabelModel(confidence: lbl.confidence?.floatValue ?? 0, text: lbl.text)
                    })
                    
                    observable.onNext(lbls)
                }
            }
            
            return Disposables.create()
        })
    }
}
