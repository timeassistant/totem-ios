//
//  TextAnalysisService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 
import UIKit
import Foundation
import RxSwift

private struct TextAnalysisLabelEntity {
    var data: [AiLabelModel]
    init(json: JSONObject) {
        let data = json["data"] as? JSONArray
        self.data = data?.compactMap({
            guard let text = $0["label"] as? String, text.hasVisibleCharacters,
                let confidence = $0["confidence"] as? Float else { return nil }
            return AiLabelModel(confidence: confidence, text: text)
        }) ?? []
    }
}

final class TextAnalysisService {
    private let dataSource = NetworkDataSource()
    func execute(text: String) -> Observable<[AiLabelModel]> {
        let params = ["text": text]
        return self.dataSource
            .object(forEndpoint: Endpoint.ToolsEndpoint.label, parameters: params)
            .map ({ TextAnalysisLabelEntity(json: $0) })
            .map({ $0.data })
    }
}
