//
//  ProfileSensorsModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation

struct ProfileSensorsModel {
    static let `default` = ProfileSensorsModel(lightning: 0, velocity: 0)
    
    var lightning: Double
    var velocity: Double
    
    var json: [String: AnyObject] {
        return [
            "lightning": lightning as AnyObject,
            "velocity": velocity as AnyObject
        ]
    }
    
    init(lightning: Double, velocity: Double) {
        self.lightning = lightning
        self.velocity = velocity
    }
    
    init(_ json: JSONObject) {
        lightning = json["lightning"] as? Double ?? 0
        velocity = json["velocity"] as? Double ?? 0
    }
}
