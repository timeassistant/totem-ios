//
//  UserClass.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

struct UserClass {
    private(set) var energy: Double
    private(set) var form: Double
    
    private(set) var subject: Double
    private(set) var unique: Double
    private(set) var creative: Double
    
    private(set) var choice: Double
    private(set) var envirinment: Double
    private(set) var customer: Double
    
    static let empty = UserClass(subject: 0, unique: 0, creative: 0)
    
    var json: JSONObject {
        return [
            "subject": subject,
            "unique": unique,
            "creative": creative
        ]
    }
    
    init(subject: Double, unique: Double, creative: Double) {
        self.subject = subject
        self.unique = unique
        self.creative = creative
        
        choice = -subject
        envirinment = -unique
        customer = -creative
        
        energy = (subject + unique /*+ creative*/) / 2 //3
        form = (choice + envirinment /*+ customer*/) / 2 //3
    }
}
