//
//  UserEnergySessionRepository.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation

final class UserEnergySessionRepository {
    private func key(for subject: UserSubject) -> String {
        switch subject {
        case .left: return "user_session_subject_left"
        case .right: return "user_session_subject_right"
        }
    }
    
    private func key(for unique: UserUnique) -> String {
        switch unique {
        case .alpha: return "user_session_unique_alpha"
        case .beta: return "user_session_unique_beta"
        }
    }
    
    private func key(for choice: UserChoice) -> String {
        switch choice {
        case .media: return "user_session_choice_media"
        case .bullet: return "user_session_choice_bullet"
        }
    }
    
    var sessionLength: Int {
        return UserDefaults.standard.integer(forKey: "user_session_initiated")
    }
    
    func setNewSessionStarted() {
        UserDefaults.standard.set(0, forKey: key(for: .left))
        UserDefaults.standard.set(0, forKey: key(for: .right))
        UserDefaults.standard.set(0, forKey: key(for: .alpha))
        UserDefaults.standard.set(0, forKey: key(for: .beta))
        UserDefaults.standard.set(0, forKey: key(for: .media))
        UserDefaults.standard.set(0, forKey: key(for: .bullet))
        UserDefaults.standard.set(0, forKey: "user_session_initiated")
    }
    
    func time(subject: UserSubject) -> Int {
        return UserDefaults.standard.integer(forKey: key(for: subject))
    }
    
    func time(unique: UserUnique) -> Int {
        return UserDefaults.standard.integer(forKey: key(for: unique))
    }
    
    func time(choice: UserChoice) -> Int {
        return UserDefaults.standard.integer(forKey: key(for: choice))
    }
    
    func addSession(time: Int) {
        let newTime = UserDefaults.standard.integer(forKey: "user_session_initiated") + time
        UserDefaults.standard.set(newTime, forKey: "user_session_initiated")
    }
    
    func add(choice: UserChoice, time: Int) {
        let newTime = self.time(choice: choice) + time
        UserDefaults.standard.set(newTime, forKey: key(for: choice))
    }
    
    func add(subject: UserSubject, time: Int) {
        let newTime = self.time(subject: subject) + time
        UserDefaults.standard.set(newTime, forKey: key(for: subject))
    }
    
    func add(unique: UserUnique, time: Int) {
        let newTime = self.time(unique: unique) + time
        UserDefaults.standard.set(newTime, forKey: key(for: unique))
    }
}
