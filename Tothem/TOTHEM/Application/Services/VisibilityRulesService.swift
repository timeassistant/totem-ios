//
//  VisibilityRulesService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

enum VisualElementHideable: String, Codable {

    case background
    case cell
    case checkList
    case coverLayer
    case createButton
    case scaleButton
    case sendButton
    case separator
    case text
    case title

    var title: String {
        switch self {
        case .background: return "Background"
        case .cell: return "Cell"
        case .checkList: return "CheckList"
        case .coverLayer: return "Cover layer"
        case .createButton: return "Create button"
        case .scaleButton: return "Scale button"
        case .sendButton: return "Send button"
        case .separator: return "Separator"
        case .text: return "Text"
        case .title: return "Title"
        }
    }

    static var all: [VisualElementHideable] {
        return [
            .background,
            .cell,
            .checkList,
            .coverLayer,
            .createButton,
            .scaleButton,
            .sendButton,
            .separator,
            .text,
            .title
        ]
    }
}

struct ElementsHiddenRule: Codable {
    var uid: String?
    var elements: [VisualElementHideable]
    
    var description: String {
        return elements
            .map { $0.title }
            .sorted()
            .joined(separator: ", ")
    }
}

class VisibilityRulesService {
    
    static let shared = VisibilityRulesService()
    
    private static let rulesEnabledKeyPath = "VisibilityRules.rulesAreEnabled"
    private static let rulesListKeyPath = "VisibilityRules.rules.list"
    private static let ruleKeyPathPrefix = "VisibilityRules.rules."
    
    private let defaultsUpdated: BehaviorSubject<Void>
    
    private init() {
        self.defaultsUpdated = BehaviorSubject<Void>(value: ())    
    }
    
    var areVisibilityRulesEnabled: Observable<Bool> {
        return self.defaultsUpdated
            .map { _ in return UserDefaults.standard.bool(forKey: VisibilityRulesService.rulesEnabledKeyPath) }
    }
    
    var rules: Observable<[ElementsHiddenRule]> {
        return self.defaultsUpdated
            .map { _ in UserDefaults.standard.stringArray(forKey: VisibilityRulesService.rulesListKeyPath) }
            .map { uids in
                guard let uids = uids else { return [ElementsHiddenRule]() }
                return uids
                    .compactMap { UserDefaults.standard.data(forKey: "\(VisibilityRulesService.ruleKeyPathPrefix)\($0)") }
                    .compactMap { try? JSONDecoder().decode(ElementsHiddenRule.self, from: $0) }
            }
    }
    
    var elementsCoveredByRules: Observable<[VisualElementHideable]> {
        return self.rules
            .map { $0.flatMap { $0.elements } }
            .map { Array(Set($0)) }
    }
    
    var hideableElements: [VisualElementHideable] {
        return VisualElementHideable.all
    }
    
    private var rulesUids: [String] {
        return UserDefaults.standard.stringArray(forKey: VisibilityRulesService.rulesListKeyPath) ?? [String]()
    }
    
    func setVisibilityRulesEnabled(to value: Bool) {
        UserDefaults.standard.set(value, forKey: VisibilityRulesService.rulesEnabledKeyPath)
        
        self.defaultsUpdated.onNext(())
    }
    
    func visibility(for element: VisualElementHideable) -> Observable<Bool> {
        return areVisibilityRulesEnabled
            .flatMapLatest { [weak self] rulesEnabled -> Observable<Bool> in
                guard let strongSelf = self else { return Observable.just(true) }
                return rulesEnabled
                    ? strongSelf.elementCoveredByRules(element).map { !$0 }
                    : Observable.just(true)
            }
    }
    
    private func elementCoveredByRules(_ element: VisualElementHideable) -> Observable<Bool> {
        return self.elementsCoveredByRules
            .map { $0.contains(element) }
    }
    
    func saveVisibilityRule(_ rule: ElementsHiddenRule) {
        var ruleMutable = rule
        if ruleMutable.uid == nil {
            ruleMutable.uid = UUID().uuidString
        }
        guard let encodedRule = try? JSONEncoder().encode(ruleMutable),
            let ruleUid = ruleMutable.uid else {
            print("Encoding of visibility rule failed on \(ruleMutable)")
            return
        }
        UserDefaults.standard.set(encodedRule, forKey: "\(VisibilityRulesService.ruleKeyPathPrefix)\(ruleUid)")

        appendToRulesList(ruleUid)

        self.defaultsUpdated.onNext(())
    }
    
    func deleteVisibilityRule(_ rule: ElementsHiddenRule) {
        guard let ruleUid = rule.uid else { return }
        UserDefaults.standard.removeObject(forKey: "\(VisibilityRulesService.ruleKeyPathPrefix)\(ruleUid)")
        deleteFromRulesList(ruleUid)

        self.defaultsUpdated.onNext(())
    }
    
    private func appendToRulesList(_ uid: String) {
        var currentList = rulesUids
        if !currentList.contains(uid) {
            currentList.append(uid)
        }
        UserDefaults.standard.set(currentList, forKey: VisibilityRulesService.rulesListKeyPath)
    }
    
    private func deleteFromRulesList(_ uid: String) {
        var currentList = rulesUids
        guard let uidPosition = currentList.firstIndex(of: uid) else { return }
        currentList.remove(at: uidPosition)
        UserDefaults.standard.set(currentList, forKey: VisibilityRulesService.rulesListKeyPath)
    }
}
