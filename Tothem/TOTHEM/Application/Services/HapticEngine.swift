//
//  HapticEngine.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit

class HapticEngine {

	static let shared = HapticEngine()

	lazy var impactFeedback = ImpactFeedback()
	lazy var selectionFeedback = SelectionFeedback()
	lazy var notificationFeedback = NotificationFeedback()
}

class ImpactFeedback {

	/**
	Activates UIImpactFeedback generator with UIImpactFeedbackStyle

	- parameters:
		-	light
		-	medium
		-	heavy

	- returns: Physical feedback from device
	*/
    func activateFeedback(with type: UIImpactFeedbackGenerator.FeedbackStyle) {
		if #available(iOS 10.0, *) {
			let generator = UIImpactFeedbackGenerator(style: type)
			generator.impactOccurred()

			print()
			print("👀👀👀👀👀👀👀👀👀👀👀")
			print("HAPTIC IMPACT FEEDBACK")
			print("👀👀👀👀👀👀👀👀👀👀👀")
			print()
		} else {
			// Fallback on earlier versions
		}
	}
}

class SelectionFeedback {

	/**
	Activates UISelectionFeedbackGenerator generator

	- returns: Physical feedback from device
	*/
	func activateFeedback() {
		if #available(iOS 10.0, *) {
			let generator = UISelectionFeedbackGenerator()
			generator.selectionChanged()

			print()
			print("👀👀👀👀👀👀👀👀👀👀👀")
			print("HAPTIC SELECTION FEEDBACK")
			print("👀👀👀👀👀👀👀👀👀👀👀")
			print()
		} else {
			// Fallback on earlier versions
		}
	}
}

class NotificationFeedback {

	/**
	Activates UINotificationFeedback generator with UINotificationFeedbackType

	- parameters:
		-	success
		-	warning
		-	error

	- returns: Physical feedback from device
	*/
    func activateFeedback(with type: UINotificationFeedbackGenerator.FeedbackType) {
		if #available(iOS 10.0, *) {
			let generator = UINotificationFeedbackGenerator()
			generator.notificationOccurred(type)

			print()
			print("👀👀👀👀👀👀👀👀👀👀👀")
			print("HAPTIC NOTIFICATION FEEDBACK")
			print("👀👀👀👀👀👀👀👀👀👀👀")
			print()
		} else {
			// Fallback on earlier versions
		}
	}
}
