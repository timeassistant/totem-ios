//
//  EventsDataManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

protocol Event {
    var key: String { get }
    var classify: Bool { get }
}

final class EventsDataManager {
    static let shared = EventsDataManager()
    private let disposeBag = DisposeBag()
    
    let dataSource: DataSource
    
    private var startDate: Date?
    private var events: String? = ""
    
    private init() {
        dataSource = FirebaseDataManager
            .configure("GoogleServiceEvents-Info", type: "plist")
    }
    
    func start() {
        startDate = Date()
    }
    
    func add(_ event: Event) {
        if let events = events {
            self.events = events + event.key
        } else {
            events = event.key
        }
    }
    
    func send() {
        defer {
            startDate = nil
            events = nil
        }
        
        guard let user = try? AuthManager.shared.user.value(),
            let events = events, !events.isEmpty,
            let date = startDate else { return }
        
        let key = "\(Int(Date().timeIntervalSince1970))"
        let value = events as AnyObject
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        
        self.dataSource
            .child(path: user.uid)
            .child(path: "\(components.year ?? 0)")
            .child(path: "\(components.month ?? 0)")
            .child(path: "\(components.day ?? 0)")
            .rx_set(value: [key: value])
            .subscribe()
            .disposed(by: disposeBag)
    }
}
