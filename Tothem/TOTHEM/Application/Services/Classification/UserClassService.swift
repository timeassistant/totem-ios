//
//  UserClassService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import Foundation
import RxSwift
import Firebase

final class UserClassService {
    static let shared = UserClassService()
    var userOnboarding = PublishSubject<UserOnboarding?>()
    
    var userEnergy = BehaviorSubject<Double>(value: 0)
    var userEnergyValue: Double {
        return (try? userEnergy.value()) ?? 0
    }
    
    var userForm = BehaviorSubject<Double>(value: 0)
    var userFormValue: Double {
        return (try? userForm.value()) ?? 0
    }
    
    var userClass = BehaviorSubject<UserClass>(value: .empty)
    var userClassValue: UserClass {
        return (try? userClass.value()) ?? UserClass(subject: userEnergyValue, unique: userEnergyValue, creative: userEnergyValue)
    }
    
    private var disposeBag = DisposeBag()
    private var uid: String?

//    private var uniqueness: DataSource?
//    private var sensorsModel: ProfileSensorsModel?

//    private var statistics: DataSource?
//    private var statisticsModel: ProfileStatisticsModel?
    
    private let dataSource = NetworkDataSource()
    private let repository = UserEnergySessionRepository()
    private var timerDisposeBag = DisposeBag()
    private let sensorsService = ProfileSensorsService()
    private let stateService = StateTimelineService.shared
    private var inForeground = true
    
    var choice: UserChoice?
    
    private init() {
        sensorsService.start()
        
        set(uid: try? AuthManager.shared.user.value()?.uid)

        AuthManager.shared.user
            .asObservable()
            .skipNil()
            .subscribe(onNext: { user in
                self.set(uid: user.uid)
            })
            .disposed(by: disposeBag)
        
        let time = 1
        
        Observable.merge([
            NotificationCenter.default.rx
                .notification(UIApplication.didEnterBackgroundNotification),
            NotificationCenter.default.rx
                .notification(UIApplication.willEnterForegroundNotification)])
            .subscribe(onNext: { [weak self] notification in
                switch notification.name {
                case UIApplication.didEnterBackgroundNotification:
                    self?.inForeground = false
                case UIApplication.willEnterForegroundNotification:
                    self?.inForeground = true
                default: break
                }
            })
            .disposed(by: disposeBag)
        
        Observable<Int>.interval(Double(time), scheduler: SerialDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { [weak self] _ in
                guard let self = self, self.inForeground else { return }
                
                var left = self.repository.time(subject: .left)
                var right = self.repository.time(subject: .right)
                var media = self.repository.time(choice: .media)
                var bullet = self.repository.time(choice: .bullet)
                
                switch self.stateService.currentState {
                case .left:
                    left += time
                    self.repository.add(subject: .left, time: time)
                    if self.stateService.currentPreview {
                        media += time
                        self.repository.add(choice: .media, time: time)
                    }
                case .right:
                    right += time
                    self.repository.add(subject: .right, time: time)
                    if self.stateService.currentPreview {
                        bullet += time
                        self.repository.add(choice: .bullet, time: time)
                    }
                default: break
                }
                
                var alpha = self.repository.time(unique: .alpha)
                var beta = self.repository.time(unique: .beta)
                
                switch self.sensorsService.currentState {
                case .some(.alpha):
                    alpha += time
                    self.repository.add(unique: .alpha, time: time)
                case .some(.beta):
                    beta += time
                    self.repository.add(unique: .beta, time: time)
                default: break
                }
                
                self.repository.addSession(time: time)
                
                let length = self.repository.sessionLength
                let subject = Double(right - left) / Double(length)
                let unique = Double(alpha - beta) / Double(length)
                
                let userClass = UserClass(subject: subject, unique: unique, creative: self.userEnergyValue)
                
                if length > 20, ((right == left && right > 0) ||
                    (alpha == beta && alpha > 0)) {
                    self.send(userClass: userClass)
                    self.repository.setNewSessionStarted()
                } else {
                    self.userClass.onNext(userClass)
                }
                
                self.updateChoice()
            })
            .disposed(by: timerDisposeBag)
    }

    private func updateChoice() {
        let media = repository.time(choice: .media)
        let bullet = repository.time(choice: .bullet)
        
        if media > bullet {
            self.choice = .media
        } else if bullet > media {
            self.choice = .bullet
        } else {
            self.choice = nil
        }
    }
    
    private func send(userClass: UserClass) {
        guard let uid = uid else { return }
        let parameters = ["data": userClass.json]

        let endpoint = Endpoint.UsersEndpoint.userClass(uid)
        self.dataSource
            .object(forEndpoint: endpoint, parameters: parameters)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func set(uid: String?) {
        self.uid = uid
//        self.uniqueness = nil
//        self.statistics = nil

        self.disposeBag = DisposeBag()

        guard let uid = uid else { return }

//        self.uniqueness = FirebaseDataManager.shared.reference
//            .child("4_usersData")
//            .child(uid)
//            .child("uniqueness")

//        self.statistics = FirebaseDataManager.shared.reference
//            .child("4_usersData")
//            .child(uid)
//            .child("statistics")
//
//        self.statistics?
//            .rx_object
//            .subscribe(onNext: {
//                let model = ProfileStatisticsModel($0)
//                self.statisticsModel = model
//            })
//            .disposed(by: disposeBag)

        FirebaseDataManager.shared.reference
            .child("4_usersData")
            .child(uid)
            .rx_object
            .subscribe(onNext: { object in
                let value = object["unique"] as? String ?? ""
                self.userOnboarding.on(.next(UserOnboarding(rawValue: value)))
                self.userEnergy.on(.next(object["energy"] as? Double ?? 0))
                self.userForm.on(.next(object["form"] as? Double ?? 0))
            })
            .disposed(by: disposeBag)
        
//        LocationSensorService.shared
//            .currentLocation
//            .flatMap({
//                self.set(latitude: $0.latitude, longitude: $0.longitude)
//            })
//            .subscribe()
//            .disposed(by: self.dispose)
    }

    func save(task: TaskModel) -> Observable<JSONObject> {
        guard let uid = uid else { return .error(GenericError.badParameters) }
        let parameters = ["content": task.json]

        let endpoint = Endpoint.UsersEndpoint.saveTask(uid)
        return self.dataSource
            .object(forEndpoint: endpoint, parameters: parameters)
    }

//    func set(sensors: ProfileSensorsModel?) {
//        self.sensorsModel = sensors
//
//        if let uid = self.uid,
//            let value = sensors,
//            (!value.lightning.equal(to: 0) || !value.velocity.equal(to: 0)) {
//                let endpoint = Endpoint.UsersEndpoint.uniqueClass(uid)
//                return self.dataSource
//                    .object(forEndpoint: endpoint, parameters: value.json)
//                    .subscribe()
//                    .disposed(by: disposeBag)
//        }
//    }

//    func save(preview task: String, time: Double) -> Observable<Void> {
//        guard let uid = uid else { return .error(GenericError.badParameters) }
//        let parameters = ["content": [ "value": time ] ]
//        let endpoint = Endpoint.UsersEndpoint.contentView(uid, task)
//        return self.dataSource
//            .object(forEndpoint: endpoint, parameters: parameters)
//            .map({ _ in () })
//    }

//    func save(side: String, time: Double) -> Observable<Void> {
//        guard let uid = uid else { return .error(GenericError.badParameters) }
//        let parameters = ["content": [ "value": time ] ]
//        let endpoint = Endpoint.UsersEndpoint.sideView(uid)
//        return self.dataSource
//            .object(forEndpoint: endpoint, parameters: parameters)
//            .map({ _ in () })
//    }

    func set(userOnboarding: UserOnboarding) -> Observable<UserOnboarding> {
        guard let uid = uid else { return .error(GenericError.badParameters) }
        let endpoint = Endpoint.UsersEndpoint.onboardingClass(uid)
        let params = [ "unique": userOnboarding.rawValue ]
        return self.dataSource
            .object(forEndpoint: endpoint, parameters: params)
            .map({ _ in return userOnboarding })
            .catchErrorJustReturn(userOnboarding)
    }

//    func set(userSubject: UserSubject, time: Int) -> Observable<UserSubject> {
//        guard let uid = uid else { return .error(GenericError.badParameters) }
//        let endpoint = Endpoint.UsersEndpoint.subjectClass(uid)
//        let params: [String: Any] = [
//            "subject": userSubject.rawValue,
//            "time": time
//        ]
//        return self.dataSource
//            .object(forEndpoint: endpoint, parameters: params)
//            .map({ _ in return userSubject })
//            .catchErrorJustReturn(userSubject)
//    }
    
//    func set(latitude: Double, longitude: Double) -> Observable<JSONObject> {
//        guard let uid = uid else { return .error(GenericError.badParameters) }
//        let endpoint = Endpoint.UsersEndpoint.evolutionClass(uid)
//        let params: [String: Any] = [
//            "latitude": latitude,
//            "longitude": longitude
//        ]
//
//        return self.dataSource
//            .object(forEndpoint: endpoint, parameters: params)
//    }
    
    static var objectDs: TimesetType {
        let values = UserDefaults.standard.dictionary(forKey: "UserClassValues")
        let priority: Int = (values?["priority"] as? Int) ?? 0
        if priority == 0 {
            return .center
        } else if priority > 0 {
            return .right
        } else {
            return .left
        }
    }
    
    static func addPriorityDs(_ value: TaskContentPriority) {
        var values = UserDefaults.standard.dictionary(forKey: "UserClassValues") ?? [:]
        var priority: Int = (values["priority"] as? Int) ?? 0
        switch value {
        case .description: priority -= 1
        case .action: priority += 1
        }
        values["priority"] = priority
        UserDefaults.standard.set(values, forKey: "UserClassValues")
    }
}
