//
//  ProfileSensorsService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation
import RxSwift

final class ProfileSensorsService {
    private var timerDisposeBag = DisposeBag()
    private let lightning = LightConfigurator.shared
    private let velocity = MovingConfigurator.shared
    
    private var initialModel = ProfileSensorsModel.default
    private var currentModel = ProfileSensorsModel.default
    private var amplitudeModel = ProfileSensorsModel.default
    
    var currentState: UserUnique? {
        if amplitudeModel.lightning > 0.3 || amplitudeModel.velocity > 0.3 {
            return .alpha
        } else if amplitudeModel.lightning < 0.2 || amplitudeModel.velocity < 0.2 {
            return .beta
        }
        
        return nil
    }
    
    func start() {
        initialModel = currentModel
        amplitudeModel = ProfileSensorsModel.default
        
        LightSensor.shared
            .lightBrightnessShared
            .subscribe(onNext: { [weak self] in
                self?.update(lightning: $0.asDouble())
            })
            .disposed(by: timerDisposeBag)
        
        self.velocity.normalizedValue
            .subscribe(onNext: { [weak self] in
                self?.update(velocity: $0)
            })
            .disposed(by: timerDisposeBag)
        
        Observable<Int>.interval(1, scheduler: SerialDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.amplitudeModel.lightning = max(0, self.amplitudeModel.lightning - 0.05)
                self.amplitudeModel.velocity = max(0, self.amplitudeModel.velocity - 0.05)
            })
            .disposed(by: timerDisposeBag)
    }
    
    private func update(lightning: Double) {
        initialModel.lightning = currentModel.lightning
        currentModel.lightning = lightning
        amplitudeModel.lightning += abs(initialModel.lightning - currentModel.lightning)
    }
    
    private func update(velocity: Double) {
        initialModel.velocity = currentModel.velocity
        currentModel.velocity = velocity
        amplitudeModel.velocity += abs(initialModel.velocity - currentModel.velocity)
    }
}
