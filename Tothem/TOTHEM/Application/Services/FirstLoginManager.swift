//
//  FirstLoginManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import UIKit
import RxSwift

final class FirstLoginManager {
    private let userRepository = UserRepository()
    private let picturesRepository = PicturesRepository()
    
    let disposeBag = DisposeBag()
    func handle() {
        Observable.zip(centerTask, leftTask, rightTask)
            .subscribe(onNext: {
                print(self)
                print($0)
            })
            .disposed(by: disposeBag)
    }
    
    private func task(_ side: TimesetType, mode: TaskModelMode, title: String, texts: [String], bullets: [String], photos: [String]) -> Observable<TaskModel> {
        guard let uid = userRepository.user?.uid else {
            return .error(GenericError.notAuthorized)
        }
        
        let task = TaskModel(side)
        task.mode = mode
        task.title = title
        
        var textItems: [EntityModel] = []
        texts.forEach {
            guard !$0.isEmpty else { return }
            textItems.append(EntityModel(.text, value: $0))
        }
        
        var checkItems: [EntityModel] = []
        bullets.forEach {
            guard !$0.isEmpty else { return }
            checkItems.append(EntityModel(.bullet, value: $0))
        }
        
        var photosItems: [EntityModel] = []
        var photosObs: [Observable<Void>] = []
        
        let ids: [String] = photos.map({ _ in EntityModel.newId() })
        for (index, item) in ids.enumerated() {
            photosItems.append(EntityModel(.photo, value: item))
            let file = photos[index]
            
            let url = Bundle.main.url(forResource: file, withExtension: "jpeg")!
            let name = "\(file).jpeg"
            
            let getImage: Observable<UIImage> = Observable.create { obs in
                DispatchQueue.global().async {
                    do {
                        let data = try Data(contentsOf: url)
                        let image = UIImage(data: data)
                        if let image = image {
                            obs.onNext(image)
                        } else {
                            obs.onError(GenericError.badData)
                        }
                    } catch {
                        obs.onError(GenericError.badParameters)
                    }
                    obs.onCompleted()
                }
                return Disposables.create()
            }
            
            let obs = getImage.flatMap({
                self.picturesRepository
                    .put(image: $0, path: uid, name: name)
                })
                .map({ state -> Int64? in
                    switch state {
                    case .finished(let size): return size
                    default: return nil
                    }
                })
                .skipNil()
                .flatMap({ size -> Observable<Void> in
                    UsersDataService.shared.save(PhotoModel(id: item, filename: name, size: size))
                })
            
            photosObs.append(obs)
        }
        
        task.setup(textItems, photosItems, checkItems)
        guard photosObs.count > 0 else {
            return UsersDataService.shared.save(task: task)
        }
        
        return Observable.zip(photosObs)
            .flatMap({ _ in UsersDataService.shared.save(task: task) })
    }
    
    private var centerTask: Observable<TaskModel> {
        return task(.center, mode: .combined, title: "FirstLaunch.Center.Title".localized, texts: ["FirstLaunch.Center.Text1".localized, "FirstLaunch.Center.Text2".localized], bullets: ["FirstLaunch.Center.Bullet1".localized, "FirstLaunch.Center.Bullet2".localized], photos: ["1_First_Center", "2_First_Center"])
    }
    
    var leftTask: Observable<TaskModel> {
        return task(.left, mode: .splitted, title: "FirstLaunch.Left.Title".localized, texts: ["FirstLaunch.Left.Text1".localized, "FirstLaunch.Left.Text2".localized], bullets: ["FirstLaunch.Left.Bullet1".localized, "FirstLaunch.Left.Bullet2".localized], photos: ["3_First_Left", "4_First_Left"])
    }
    
    var rightTask: Observable<TaskModel> {
        return task(.right, mode: .splitted, title: "FirstLaunch.Right.Title".localized, texts: ["FirstLaunch.Right.Text1".localized, "FirstLaunch.Right.Text2".localized], bullets: ["FirstLaunch.Right.Bullet1".localized, "FirstLaunch.Right.Bullet2".localized], photos: ["5_First_Right", "6_First_Right"])
    }
}
