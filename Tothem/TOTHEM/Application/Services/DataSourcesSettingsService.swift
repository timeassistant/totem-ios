//
//  DataSourcesSettingsService.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

struct DataSourceDynamic: Codable {
    var dataSource: ConnectorParameter
    var isDynamicModeEnabled: Bool
    var currentStaticValue: Double
}

class DataSourcesSettingsService {
    
    private static let dynamicDisabledKeyPath = "DataSources.isDynamicModeDisabled"

    private static let dataSourcesKeyPrefix = "DataSources."

    private static let isDisabledKeyPostfix = ".isDisabled"
    private static let staticValueKeyPostfix = ".staticValue"
    
    private static let geolocationSensorLastlyChanged = "DataSources.Geolocation.LastlyChanged"
    
    static let shared = DataSourcesSettingsService()
    
    private let defaultsUpdated: BehaviorSubject<Void>
    
    var isDynamicModeEnabled: Observable<Bool> {
        return self.defaultsUpdated
            .map { UserDefaults.standard.bool(forKey: DataSourcesSettingsService.dynamicDisabledKeyPath) }
            .map { !$0 }
    }
    
    var dataSources: Observable<[DataSourceDynamic]> {
        return self.defaultsUpdated
            .map { ConnectorParameter.dataSourcesList }
            .map { $0.compactMap { [weak self] parameterName in
                guard let connectorParameter = ConnectorParameter(rawValue: parameterName),
                    let isEnabled = self?.isEnabled(dataSource: connectorParameter),
                    let staticValue = self?.staticValue(dataSource: parameterName) else { return nil }
                return DataSourceDynamic(dataSource: connectorParameter,
                                         isDynamicModeEnabled: isEnabled,
                                         currentStaticValue: staticValue)
            }}
    }
    
    private init() {
        self.defaultsUpdated = BehaviorSubject<Void>(value: ())  
    }
    
    func observableIsDynamic(for dataSource: ConnectorParameter) -> Observable<Bool> {
        return self.defaultsUpdated
            .map { [weak self] _ in return self?.isEnabled(dataSource: dataSource) ?? true }
            .startWith(isEnabled(dataSource: dataSource))
    }
    
    func observableStaticValue(for dataSource: ConnectorParameter) -> Observable<Double?> {
        return self.defaultsUpdated
            .map { [weak self] _ in return self?.staticValue(dataSource: dataSource.rawValue) }
    }
    
    func observableGeolocationLastlyChanged() -> Observable<String> {
        return self.defaultsUpdated
            .map { [weak self] _ in return self?.geolocationLastlyChanged() ?? ConnectorParameter.walkingSpeed.rawValue }
    }
    
    func setDynamicDataSourcesEnabled(to value: Bool) {
        UserDefaults.standard.set(!value, forKey: DataSourcesSettingsService.dynamicDisabledKeyPath)
        self.defaultsUpdated.onNext(())
    }
    
    func setEnabled(dataSource dataSourceName: String, to value: Bool) {
        UserDefaults.standard.set(!value, forKey: isDisabledKey(for: dataSourceName))
        self.defaultsUpdated.onNext(())
    }
    
    func setStaticValue(dataSource dataSourceName: String, to value: Double) {
        UserDefaults.standard.set(value, forKey: staticValueKey(for: dataSourceName))
        // Manual changing of driving speed has to be accompanied with a resetting of walking speed
        if dataSourceName == ConnectorParameter.drivingSpeed.rawValue {
            UserDefaults.standard.set(0, forKey: staticValueKey(for: ConnectorParameter.walkingSpeed.rawValue))
            UserDefaults.standard.set(dataSourceName, forKey: DataSourcesSettingsService.geolocationSensorLastlyChanged)
        }
        // // Manual changing of walking speed has to be accompanied with a resetting of driving speed
        if dataSourceName == ConnectorParameter.walkingSpeed.rawValue {
            UserDefaults.standard.set(0, forKey: staticValueKey(for: ConnectorParameter.drivingSpeed.rawValue))
            UserDefaults.standard.set(dataSourceName, forKey: DataSourcesSettingsService.geolocationSensorLastlyChanged)
        }
        self.defaultsUpdated.onNext(())
    }
    
    func isEnabled(dataSource: ConnectorParameter) -> Bool {
        return !UserDefaults.standard.bool(forKey: isDisabledKey(for: dataSource.parameterDomain))
    }
    
    func staticValue(dataSource dataSourceName: String) -> Double {
        return UserDefaults.standard.double(forKey: staticValueKey(for: dataSourceName))
    }
    
    func geolocationLastlyChanged() -> String {
        return UserDefaults.standard.string(forKey: DataSourcesSettingsService.geolocationSensorLastlyChanged) ??
            ConnectorParameter.walkingSpeed.rawValue
    }
    
    func isDisabledKey(for dataSourceName: String) -> String {
        return DataSourcesSettingsService.dataSourcesKeyPrefix
            + dataSourceName
            + DataSourcesSettingsService.isDisabledKeyPostfix
    }
    
    func staticValueKey(for dataSourceName: String) -> String {
        return DataSourcesSettingsService.dataSourcesKeyPrefix
            + dataSourceName
            + DataSourcesSettingsService.staticValueKeyPostfix
    }
    
}
