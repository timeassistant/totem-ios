//
//  ParametersDataManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class ParametersDataManager {
    static let shared = ParametersDataManager()
    let dataSource: DataSource
    
    // For cases when deviation from anchor became lower
    static let delayMultiplier = 3.0
    private let disposableBag = DisposeBag()
    
    private init() {
        dataSource = FirebaseDataManager
            .configure("GoogleServiceParams-Info", type: "plist")
            .child(path: "1_parametersLibrary")
    }

	//----------------------------------
	func getParameters() {
        self.dataSource
            .rx_object
            .subscribe(onNext: { [weak self] value in
                if let raw = self?.getRawParametersList(bySnapshotValue: value),
                    let list = self?.getParametersList(byRawValues: raw) {
                    ConfigurationFacade.shared.parameters.onNext(list)
                }
                
                if let restrictions = self?.getParametersRestrictions(bySnapshotValue: value) {
                    ConfigurationFacade.shared.parametersRestrictions.value =
                    restrictions
                }
            })
            .disposed(by: disposableBag)
	}
    
    private func getRawParametersList(bySnapshotValue snapshotValue: [String: Any]) -> [RawParameter] {
        var result = [RawParameter]()
        
        if let rawParameter = RawParameter(bySnapshot: snapshotValue) {
            result.append(rawParameter)
        } else {
            for (_, value) in snapshotValue {
                guard let subdictionary = value as? [String: Any] else { continue }
                result += getRawParametersList(bySnapshotValue: subdictionary)
            }
        }
        
        return result
    }
    
    private func getParametersRestrictions(bySnapshotValue snapshotValue: [String: Any]) -> [ParameterRestriction] {
        var result = [ParameterRestriction]()
        
        if let parameterRestricton = ParameterRestriction(bySnapshot: snapshotValue) {
            result.append(parameterRestricton)
        } else {
            for (_, value) in snapshotValue {
                guard let subdictionary = value as? [String: Any] else { continue }
                result += getParametersRestrictions(bySnapshotValue: subdictionary)
            }
        }
        
        return result
    }
    
    private func getParametersList(byRawValues rawValues: [RawParameter]) -> [Parameter] {
        return rawValues.compactMap { Parameter(byRaw: $0) }
    }
}
