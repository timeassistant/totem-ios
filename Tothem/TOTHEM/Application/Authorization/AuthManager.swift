//
//  AuthManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

protocol CredentialsStorageProtocol {
    var data: AuthorizationData? { get set }
}

final class AuthManager {
    static let shared = AuthManager()
    private let dataManager = FirebaseDataManager.shared
    
    private var anonymousStorage: CredentialsStorageProtocol = CloudStorage(emailKey: "totem.email", passwordKey: "totem.password")
    private var customStorage: CredentialsStorageProtocol = CloudStorage(emailKey: "com.visera.totem.email", passwordKey: "com.visera.totem.password")

    private var authorizationData: AuthorizationData? {
        return customStorage.data == nil ? anonymousStorage.data : customStorage.data
    }
    
    var user: BehaviorSubject<UserModel?>
    private var disposeBag = DisposeBag()
    
    private init() {
        user = BehaviorSubject<UserModel?>(value: dataManager.currentUser)
        authorizeIfNeeded()
            .bind(to: user)
            .disposed(by: disposeBag)
    }
    
    func logout() -> Observable<UserModel?> {
        do {
            try dataManager.signOut()
            return authorizeIfNeeded()
        } catch _ {
            nilAuthorizationData()
            return authorizeIfNeeded()
        }
    }
    
    //reset current credentials storage
    private func nilAuthorizationData() {
        if customStorage.data != nil {
            customStorage.data = nil
        } else {
            anonymousStorage.data = nil
        }
    }
    
    private func handle(_ error: Error) -> Observable<UserModel?> {
        if (error as NSError).code == 17009 /*WRONG_PASSWORD*/ {
            nilAuthorizationData()
            return authorizeIfNeeded()
        } else if (error as NSError).code == 17011 /*USER_NOT_FOUND*/ {
            nilAuthorizationData()
            return authorizeIfNeeded()
        } else {
            return authorizeIfNeeded()
        }
    }
    
    func authorizeIfNeeded() -> Observable<UserModel?> {
        let data = self.authorizationData
        print(String(describing: data?.email))
        if let user = dataManager.currentUser {
            if data?.email != user.email {
                return logout()
            } else {
                return .just(user)
            }
        } else if let data = data {
            return dataManager.signin(email: data.email, password: data.password)
                    .catchError({ self.handle($0) })
        } else {
            return newUser.catchError({ self.handle($0) })
        }
    }
    
    private var newUser: Observable<UserModel?> {
        let data = AuthorizationData.create()
        self.anonymousStorage.data = data
        return self.dataManager
            .createUser(email: data.email, password: data.password)
            .do(onNext: { _ in
                FirstLoginManager().handle()
            })
    }

    func signIn(with email: String, password: String) {
        let data = AuthorizationData(email: email, password: password)
        customStorage.data = data
        dataManager.signin(email: data.email, password: data.password)
            .do(onNext: { model in
                print(String(describing: model))
            }, onError: { error in
                print(error)
            })
            .bind(to: user)
            .disposed(by: disposeBag)
    }

    func signUp(with email: String, password: String) {
        let data = AuthorizationData(email: email, password: password)
        customStorage.data = data
        dataManager.createUser(email: data.email, password: data.password)
            .bind(to: user)
            .disposed(by: disposeBag)
    }
}
