//
//  AuthorizationStorage.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

class CloudStorage: CredentialsStorageProtocol {
    private var emailKey: String
    private var passwordKey: String
    init(emailKey: String, passwordKey: String) {
        self.emailKey = emailKey
        self.passwordKey = passwordKey
    }

    var data: AuthorizationData? {
        get {
            #if DEBUG
            return AuthorizationData(email: "debug_ios@visera.com", password: "admin1234")
            #endif
            
            let store = NSUbiquitousKeyValueStore()
            store.synchronize()
            guard let email = store.string(forKey: emailKey)?.lowercased(), let password = store.string(forKey: passwordKey) else { return nil }
            return AuthorizationData(email: email, password: password)
        }
        set {
            let store = NSUbiquitousKeyValueStore()
            store.synchronize()
            if let data = newValue {
                store.set(data.email, forKey: emailKey)
                store.set(data.password, forKey: passwordKey)
            } else {
                store.removeObject(forKey: emailKey)
                store.removeObject(forKey: passwordKey)
            }
            store.synchronize()
        }
    }
}
