//
//  LocalSettings.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

enum LocalSettingsType {
    enum EntityStatistics: String, CaseIterable {
        case object = "entity_statistics_object"
        case subject = "entity_statistics_subject"
        case priority = "entity_statistics_priority"
        case choice = "entity_statistics_choice"
        case person = "entity_statistics_person"
        case unique = "entity_statistics_unique"
        case matching = "entity_statistics_matching"
        case agreement = "entity_statistics_agreement"
        case intuit = "entity_statistics_intuit"
        case creative = "entity_statistics_creative"
        case supplier = "entity_statistics_supplier"
        case customer = "entity_statistics_customer"
    }
}

final class LocalSettings {
    static func update() {
        var values: [String: Any] = [:]
        
        LocalSettingsType.EntityStatistics.allCases.forEach({
            values[$0.rawValue] = UserDefaults.standard.bool(forKey: $0.rawValue)
        })
        
        UserDefaults.standard.register(defaults: values)
    }
    
    static func entityStatistics(for type: LocalSettingsType.EntityStatistics) -> Bool {
        return UserDefaults.standard.bool(forKey: type.rawValue)
    }
}
