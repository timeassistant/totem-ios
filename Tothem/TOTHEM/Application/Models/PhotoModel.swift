//
//  PhotoModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 
import Foundation

struct PhotoModel {
    var id: String
    var filename: String
    var size: Int64
    
    var json: JSONObject {
        return [
            "size": size,
            "filename": filename
        ]
    }
    
    init(id: String, filename: String, size: Int64) {
        self.id = id
        self.filename = filename
        self.size = size
    }
    
    init(id: String, json: JSONObject) {
        self.id = id
        self.filename = json["filename"] as? String ?? ""
        self.size = json["size"] as? Int64 ?? 0
    }
}
