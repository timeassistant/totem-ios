//
//  ParametersModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum ParameterType: String, Codable {
	case color
	case number
	case blurType
	case statusBarStyle
	case textAlignment
	case font
	case hapticEngine
}

enum ParameterEvaluationType: String, Codable {
    case rawValue
    case constant
    case configurationUnit
}

enum ParameterName: String, Codable {
	case activeCreateButtonSize = "active_createButton_size"
	case activeCreateButtonPosition = "active_createButton_x_position"
	case activeCreateButtonColor = "active_createButton_color"
	case activeCreateButtonBorderColor = "active_createButton_border_color"
	case activeCreateButtonBorderWidth = "active_createButton_border_Width"

	case activeScaleButtonSize = "active_scaleButton_size"
	case activeScaleButtonPosition = "active_scaleButton_x_position"
	case activeScaleButtonColor = "active_scaleButton_color"
	case activeScaleButtonBorderColor = "active_scaleButton_border_color"
	case activeScaleButtonBorderWidth = "active_scaleButton_border_Width"

	case passiveCreateButtonSize = "passive_createButton_size"
	case passiveCreateButtonPosition = "passive_createButton_x_position"
	case passiveCreateButtonColor = "passive_createButton_color"
	case passiveCreateButtonBorderColor = "passive_createButton_border_color"
	case passiveCreateButtonBorderWidth = "passive_createButton_border_Width"

	case passiveScaleButtonSize = "passive_scaleButton_size"
	case passiveScaleButtonPosition = "passive_scaleButton_x_position"
	case passiveScaleButtonColor = "passive_scaleButton_color"
	case passiveScaleButtonBorderColor = "passive_scaleButton_border_color"
	case passiveScaleButtonBorderWidth = "passive_scaleButton_border_Width"

	case leftCreateButtonSize = "left_createButton_size"
	case leftCreateButtonColor = "left_createButton_color"
	case leftCreateButtonBorderColor = "left_createButton_border_color"
	case leftCreateButtonBorderWidth = "left_createButton_border_Width"

	case rightCreateButtonSize = "right_createButton_size"
	case rightCreateButtonColor = "right_createButton_color"
	case rightCreateButtonBorderColor = "right_createButton_border_color"
	case rightCreateButtonBorderWidth = "right_createButton_border_Width"

	case primaryBackgroundBlurType = "primary_background_blur_type"
	case primaryBackgroundBlurAlpha = "primary_background_blur_alpha"

	case activeContrastLayerColor = "active_contrast_layer_color"
	case passiveContrastLayerColor = "passive_contrast_layer_color"
	case leftContrastLayerColor = "left_contrast_layer_color"
	case rightContrastLayerColor = "right_contrast_layer_color"

	case activePannelBlurType = "active_pannel_blur_type"
	case passivePannelBlurType = "passive_pannel_blur_type"
	case leftPannelBlurType = "left_pannel_blur_type"
	case rightPannelBlurType = "right_pannel_blur_type"

	case activePannelBlurAlpha = "active_pannel_blur_alpha"
	case passivePannelBlurAlpha = "passive_pannel_blur_alpha"
	case leftPannelBlurAlpha = "left_pannel_blur_alpha"
	case rightPannelBlurAlpha = "right_pannel_blur_alpha"

	case activePannelContrastLayerColor = "active_pannel_contrast_layer_color"
	case passivePannelContrastLayerColor = "passive_pannel_contrast_layer_color"
	case leftPannelContrastLayerColor = "left_pannel_contrast_layer_color"
	case rightPannelContrastLayerColor = "right_pannel_contrast_layer_color"

	case statusBarStyle = "status_bar_style"

	case activeTaskBackgroundColor = "active_task_background_color"
	case passiveTaskBackgroundColor = "passive_task_background_color"
	case leftTaskBackgroundColor = "left_task_background_color"
	case rightTaskBackgroundColor = "right_task_background_color"

	case activeTaskTitleLabelColor = "active_task_title_label_color"
	case passiveTaskTitleLabelColor = "passive_task_title_label_color"
	case leftTaskTitleLabelColor = "left_task_title_label_color"
	case rightTaskTitleLabelColor = "right_task_title_label_color"

	case activeTaskChecklistLabelColor = "active_task_checklist_label_color"
	case passiveTaskChecklistLabelColor = "passive_task_checklist_label_color"
	case leftTaskChecklistLabelColor = "left_task_checklist_label_color"
	case rightTaskChecklistLabelColor = "right_task_checklist_label_color"

	case activeTaskDescriptionLabelColor = "active_task_description_label_color"
	case passiveTaskDescriptionLabelColor = "passive_task_description_label_color"
	case leftTaskDescriptionLabelColor = "left_task_description_label_color"
	case rightTaskDescriptionLabelColor = "right_task_description_label_color"

	case activeTaskSeapatorLineColor = "active_task_seapator_line_color"
	case passiveTaskSeapatorLineColor = "passive_task_seapator_line_color"
	case leftTaskSeapatorLineColor = "left_seapator_line_color"
	case rightTaskSeapatorLineColor = "right_task_seapator_line_color"

	case activeTaskSeapatorLineHeight = "active_task_seapator_line_height"
	case passiveTaskSeapatorLineHeight = "passive_task_seapator_line_height"
	case leftTaskSeapatorLineHeight = "left_task_seapator_line_height"
	case rightTaskSeapatorLineHeight = "right_task_seapator_line_height"

	case activeTaskSendButtonColor = "active_task_send_button_color"
	case passiveTaskSendButtonColor = "passive_task_send_button_color"
	case leftTaskSendButtonColor = "left_task_send_button_color"
	case rightTaskSendButtonColor = "right_task_send_button_color"

	case activeTaskSendButtonBorderColor  = "active_task_send_button_border_color"
	case passiveTaskSendButtonBorderColor  = "passive_task_send_button_border_color"
	case leftTaskSendButtonBorderColor  = "left_task_send_button_border_color"
	case rightTaskSendButtonBorderColor  = "right_task_send_button_border_color"

	case activeTaskSendButtonBorderWidth  = "active_task_send_button_border_width"
	case passiveTaskSendButtonBorderWidth  = "passive_task_send_button_border_width"
	case leftTaskSendButtonBorderWidth  = "left_task_send_button_border_width"
	case rightTaskSendButtonBorderWidth  = "right_task_send_button_border_width"

	case activeTaskTitleFont  = "active_task_title_font"
	case passiveTaskTitleFont = "passive_task_title_font"
	case leftTaskTitleFont  = "left_task_title_font"
	case rightTaskTitleFont  = "right_task_title_font"

	case activeTaskChecklistFont  = "active_checklist_title_font"
	case passiveTaskChecklistFont  = "passive_task_checklist_font"
	case leftTaskChecklistFont  = "left_task_checklist_font"
	case rightTaskChecklistFont  = "right_task_checklist_font"

	case activeTaskContentFont  = "active_task_content_font"
	case passiveTaskContentFont  = "passive_task_content_font"
	case leftTaskContentFont  = "left_task_content_font"
	case rightTaskContentFont  = "right_task_content_font"

	case activeTaskContentScale = "active_task_content_scale"
	case passiveTaskContentScale  = "passive_task_content_scale"
	case leftTaskContentScale  = "left_task_content_scale"
	case rightTaskContentScale  = "right_task_content_scale"

	case activeTaskAdditionalHeight  = "active_task_additional_height"
	case passiveTaskAdditionalHeight  = "passive_task_additional_height"
	case leftTaskAdditionalHeight  = "left_task_additional_height"
	case rightTaskAdditionalHeight  = "right_task_additional_height"

	case activeTaskCorner  = "active_task_corner"
	case passiveTaskCorner  = "passive_task_corner"
	case leftTaskCorner  = "left_task_corner"
	case rightTaskCorner  = "right_task_corner"

	case activeTaskTextAlignment  = "active_task_text_alignment"
	case passiveTaskTextAlignment  = "passive_task_text_alignment"
	case leftTaskTextAlignment  = "left_task_text_alignment"
	case rightTaskTextAlignment  = "right_task_text_alignment"

	case activeStatusbarColor  = "active_statusbar_color"
	case passiveStatusbarColor  = "passive_statusbar_color"
	case leftStatusbarColor  = "left_statusbar_color"
	case rightStatusbarColor  = "right_statusbar_color"

	case activeProfileButtonSize = "active_profileButton_size"
	case activeProfileButtonPosition = "active_profileButton_x_position"

	case passiveProfileButtonSize = "passive_profileButton_size"
	case passiveProfileButtonPosition = "passive_profileButton_x_position"

	case activeProfileButtonBorderColor = "active_profileButton_border_color"
	case activeProfileButtonBorderWidth = "active_profileButton_border_Width"

	case passiveProfileButtonBorderColor = "passive_profileButton_border_color"
	case passiveProfileButtonBorderWidth = "passive_profileButton_border_Width"

	case activeProfileButtonColor = "active_profileButton_color"
	case passiveProfileButtonColor = "passive_profileButton_color"

		//-------

	case activeCellHeightIdle = "active_cellHeight_idle"
	case passiveCellHeightIdle = "passive_cellHeight_idle"
	case leftCellHeightIdle = "left_cellHeight_idle"
	case rightCellHeightIdle = "right_cellHeight_idle"

	case activeCellHeightMoving = "active_cellHeight_moving"
	case passiveCellHeightMoving = "passive_cellHeight_moving"
	case leftCellHeightMoving = "left_cellHeight_moving"
	case rightCellHeightMoving = "right_cellHeight_moving"

	case activeCellLeftInsetIdle = "active_cellLeftInset_idle"
	case passiveCellLeftInsetIdle = "passive_cellLeftInset_idle"
	case leftCellLeftInsetIdle = "left_cellLeftInset_idle"
	case rightCellLeftInsetIdle = "right_cellLeftInset_idle"

	case activeCellLeftInsetIdleMoving = "active_cellLeftInset_moving"
	case passiveCellLeftInsetIdleMoving = "passive_cellLeftInset_moving"
	case leftCellLeftInsetIdleMoving = "left_cellLeftInset_moving"
	case rightCellLeftInsetIdleMoving = "right_cellLeftInset_moving"

	case activeCellRightInsetIdle = "active_cellRightInset_idle"
	case passiveCellRightInsetIdle = "passive_cellRightInset_idle"
	case leftCellRightInsetIdle = "left_cellRightInset_idle"
	case rightCellRightInsetIdle = "right_cellRightInset_idle"

	case activeCellRightInsetIdleMoving = "active_cellRightInset_moving"
	case passiveCellRightInsetIdleMoving = "passive_cellRightInset_moving"
	case leftCellRightInsetIdleMoving = "left_cellRightInset_moving"
	case rightCellRightInsetIdleMoving = "right_cellRightInset_moving"

	case activeInLineCellPadingIdle = "active_inLineCellPading_idle"
	case passiveInLineCellPadingIdle = "passive_inLineCellPading_idle"
	case leftInLineCellPadingIdle = "left_inLineCellPading_idle"
	case rightInLineCellPadingIdle = "right_inLineCellPading_idle"

	case activeInLineCellPadingMoving = "active_inLineCellPading_moving"
	case passiveInLineCellPadingMoving = "passive_inLineCellPading_moving"
	case leftInLineCellPadingMoving = "left_inLineCellPading_moving"
	case rightInLineCellPadingMoving = "right_inLineCellPading_moving"

	case activeInLineCellPadingPreview = "active_inLineCellPading_preview"
	case passiveInLineCellPadingPreview = "passive_inLineCellPading_preview"
	case leftInLineCellPadingPreview = "left_inLineCellPading_preview"
	case rightInLineCellPadingPreview = "right_inLineCellPading_preview"

	case activeCreateButtonHaptic = "active_createButton_haptic"
	case passiveCreateButtonHaptic = "passive_createButton_haptic"
	case leftCreateButtonHaptic = "left_createButton_haptic"
	case rightCreateButtonHaptic = "right_createButton_haptic"

	case activeScaleButtonHaptic = "active_scaleButton_haptic"
	case passiveScaleButtonHaptic = "passive_scaleButton_haptic"

	case activeProfileButtonHaptic = "active_profileButton_haptic"
	case passiveProfileButtonHaptic = "passive_profileButton_haptic"

	case hapticButton = "button_haptic"

    case testCreateButtonSize1 = "test_createButton_size1"
    case testCreateButtonSize2 = "test_createButton_size2"
    case testCreateButtonSize3 = "test_createButton_size3"
    case testCreateButtonSize4 = "test_createButton_size4"
    case testCreateButtonSize5 = "test_createButton_size5"
    case testCreateButtonSize6 = "test_createButton_size6"
    case testCreateButtonSize7 = "test_createButton_size7"
    case testCreateButtonSize8 = "test_createButton_size8"

    case activeCreateButtonSizeTest = "active_createButton_sizeTest"
}

struct RawParameter: Codable {
    var name: String
    var type: String
    var value: String
    var dynamic: Int
    var dynamicLink: String
    var constant: Int
    var constantLink: String

    init?(bySnapshot snapshot: [String: Any]) {
        guard let name = snapshot["1_name"] as? String,
            let type = snapshot["2_type"] as? String, let value = snapshot["7_value"] as? String,
            let dynamic = snapshot["3_dynamic"] as? Int, let dynamicLink = snapshot["4_dynamicLink"] as? String,
            let constant = snapshot["5_constant"] as? Int, let constantLink = snapshot["6_constantLink"] as? String else {
            return nil
        }

        self.name = name
        self.type = type
        self.value = value
        self.dynamic = dynamic
        self.dynamicLink = dynamicLink
        self.constant = constant
        self.constantLink = constantLink
    }
}

struct Parameter: Codable {
	var name: ParameterName = .activeContrastLayerColor
	var type: ParameterType = .number
    var evaluationType: ParameterEvaluationType = .rawValue
	var value: String = "0"

    init?(byRaw rawParameter: RawParameter) {
        guard let name = ParameterName(rawValue: rawParameter.name),
            let type = ParameterType(rawValue: rawParameter.type) else {
            return nil
        }

        let evaluationType: ParameterEvaluationType
        let value: String

        if rawParameter.dynamic == 1 {
            evaluationType = .configurationUnit
            value = rawParameter.dynamicLink
        } else if rawParameter.constant == 1 {
            evaluationType = .constant
            value = rawParameter.constantLink
        } else {
            evaluationType = .rawValue
            value = rawParameter.value
        }

        self.name = name
        self.type = type
        self.evaluationType = evaluationType
        self.value = value
    }
}

struct ParameterRestriction {
    var parameterName: ParameterName
    var min: String
    var max: String

    init?(bySnapshot snapshot: [String: Any]) {
        guard let name = snapshot["1_name"] as? String,
            let parameterName = ParameterName(rawValue: name),
            let min = snapshot["8_min"] as? String,
            let max = snapshot["9_max"] as? String else {
                return nil
        }

        self.parameterName = parameterName
        self.min = min
        self.max = max
    }
}
