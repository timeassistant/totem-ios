//
//  TaskModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import FirebaseDatabase

enum TaskModelMode: String {
    case splitted
    case combined
}

enum TaskContentPriority: String {
    case action
    case description
}

protocol EntityProtocol {
    var id: String { get set }
    var uid: String? { get set }
    var value: String { get set }
    var checked: Bool { get set }
    var child: [String] { get set }
    var json: JSONObject { get }
}

final class TaskModel {
    
    static var newId: () -> String = {
        return UUID().uuidString
    }

    static var newItemPosition: Double {
        return Date().timeIntervalSince1970
    }
    
    var side: TimesetType = .center
    var mode: TaskModelMode = .combined
    
    var id: String = ""
    var uid: String?
    var title: String = ""
    var checked: Bool = false
    var viewed: Bool = true
    var `public`: Bool = false
    
    var positions: [TaskPositionModel] = []

    var reference: DatabaseReference?

    var photos: [EntityModel] {
        let models = items.filter({ $0.1.type == .photo }).map({ $0.1 })
        return EditingTextHelper.ordered(models, order: order)
    }
    
    var texts: [EntityModel] {
        let models = items.filter({ $0.1.type == .text }).map({ $0.1 })
        return EditingTextHelper.ordered(models, order: order)
    }
    
    var bullets: [EntityModel] {
        let models = items.filter({ $0.1.type == .bullet }).map({ $0.1 })
        return EditingTextHelper.ordered(models, order: order)
    }

    var order: [String] = []

    var isEmpty: Bool {
        return isContentEmpty
    }

    var descriptionText: String {
        return EditingTextHelper.text(from: texts)
    }

    var checklistText: String {
        return EditingTextHelper.text(from: bullets)
    }

    var isContentEmpty: Bool {
        return title.isEmpty && items.reduce(true, { $0 && $1.1.value.isEmpty })
    }
    
    var isContainsMedia: Bool {
        return items.contains(where: { $0.1.type == .bullet || $0.1.type == .photo })
    }

    var isDraft: Bool {
        return side != .center
    }

    var items: [String: EntityModel] = [:]
    
    var json: JSONObject {
        get {
            return [
                firebaseTypeKey: side.rawValue,
                "title": title,
                "text_items": texts.map({ $0.json }),
                firebasePositionsKey: positions.map({ $0.dictionary }),
                firebaseViewedKey: viewed,
                "photos": photos.map { $0.json },
                firebaseIdKey: id,
                "checklist_items": bullets.map({ $0.json }),
                "checked": checked,
                "mode": mode.rawValue,
                "order": order,
                "public": `public`,
                "uid": uid ?? ""
            ]
        }
        set {
            id = newValue[firebaseIdKey] as? String ?? TaskModel.newId()
            uid = newValue["uid"] as? String
            side = TimesetType(rawValue: newValue[firebaseTypeKey] as? Int ?? 0) ?? .center
            title = newValue["title"] as? String ?? ""

            checked = newValue["checked"] as? Bool ?? false
            `public` = newValue["public"] as? Bool ?? false
            
            if let positions = newValue[firebasePositionsKey] as? JSONArray {
                self.positions = positions.map({ TaskPositionModel($0) })
            }

            viewed = newValue[firebaseViewedKey] as? Bool ?? true

            if let rawValue = newValue["mode"] as? String,
                let value = TaskModelMode(rawValue: rawValue) {
                mode = value
            } else {
                mode = side == .center ? .combined : .splitted
            }

            order = newValue["order"] as? [String] ?? []

            let texts: [EntityModel] = (newValue["text_items"] as? JSONArray ?? [])
                .map({ EntityModel(.text, mid: id, uid: uid, json: $0) })
                .filter({ !$0.value.isEmpty })

            let bullets: [EntityModel] = (newValue["checklist_items"] as? JSONArray ?? [])
                .map({ EntityModel(.bullet, mid: id, uid: uid, json: $0) })
                .filter({ !$0.value.isEmpty })

            let photos: [EntityModel] = (newValue["photos"] as? JSONArray ?? [])
                .map { EntityModel(.photo, mid: id, uid: uid, json: $0) }
                .filter({ !$0.value.isEmpty })

            createPositions()
            setup(texts, photos, bullets)
            
            if let rawValue = newValue["priority"] as? String {
                priority = TaskContentPriority(rawValue: rawValue)
            }
        }
    }

    init(_ json: JSONObject? = nil) {
        self.json = json ?? [:]
    }

    init(snapshot: DataSnapshot) {
        self.json = snapshot.value as? JSONObject ?? [:]
        self.reference = snapshot.ref
    }
    
    init(_ side: TimesetType) {
        self.side = side
        self.id = TaskModel.newId()
    }

    var copy: TaskModel {
        let task = TaskModel(json)
        task.reference = reference
        return task
    }

    var priority: TaskContentPriority?
    
    func setup(_ texts: [EntityModel], _ photos: [EntityModel], _ bullets: [EntityModel]) {
        texts.forEach({ items[$0.id] = $0 })
        photos.forEach({ items[$0.id] = $0 })
        bullets.forEach({ items[$0.id] = $0 })
        
        guard order.count == 0 else { return }
        switch side {
        case .center:
            order.append(contentsOf: texts.map({ $0.id }))
            order.append(contentsOf: photos.map({ $0.id }))
            order.append(contentsOf: bullets.map({ $0.id }))
        case .right:
            order.append(contentsOf: bullets.map({ $0.id }))
            order.append(contentsOf: texts.map({ $0.id }))
            order.append(contentsOf: photos.map({ $0.id }))
        case .left:
            order.append(contentsOf: photos.map({ $0.id }))
            order.append(contentsOf: texts.map({ $0.id }))
            order.append(contentsOf: bullets.map({ $0.id }))
        }
    }

    // MARK: - Editing
    private var lastCreatedEntityId: String?
    func update(bullets text: String, side: TimesetType? = nil) {
        let items = EditingTextHelper.updated(bullets, order: order, type: .bullet, text: text)
        
        var root = side == nil || side == .right || texts.count == 0 && side == .center && lastCreatedEntityId == nil
        
        if !root, let id = lastCreatedEntityId {
            root = self.items[id]?.type == .bullet
        }
        
        update(order: items, old: bullets, root: root)
        self.items = self.items.filter({ $0.1.type != .bullet })
        items.forEach({ self.items[$0.id] = $0 })
    }

    func update(photos items: [EntityModel], side: TimesetType? = nil) {
        var root = side == nil || side == .left || texts.count == 0 && side == .center && lastCreatedEntityId == nil
        if !root, let id = lastCreatedEntityId {
            root = self.items[id]?.type == .photo
        }
        
        update(order: items, old: photos, root: root)
        self.items = self.items.filter({ $0.1.type != .photo })
        items.forEach({ self.items[$0.id] = $0 })
    }

    func update(texts text: String, side: TimesetType? = nil) {
        let items = EditingTextHelper.updated(texts, order: order, type: .text, text: text)
        update(order: items, old: texts, root: side == nil || side == .center)
        self.items = self.items.filter({ $0.1.type != .text })
        items.forEach({ self.items[$0.id] = $0 })
    }

    private func update(order items: [EntityModel], old: [EntityModel], root: Bool) {
        let removed = old.filter({ existing in
            !items.contains(where: { $0.id == existing.id })
        })
        order = order.filter({ id in !removed.contains(where: { $0.id == id }) })

        for model in items where !old.contains(model) {
            insertOrder(items, model)
            if root {
                if model.type != .text {
                    lastCreatedEntityId = model.id
                }
            } else {
                if let id = lastCreatedEntityId, var parent = self.items[id] {
                    parent.child.append(model.id)
                    self.items[id] = parent
                }
            }
        }
    }

    private func insertOrder(_ items: [EntityModel], _ model: EntityModel) {
        var nextId: String = ""
        for item in items.reversed() where item.type == model.type {
            if item.id == model.id {
                break
            } else {
                nextId = item.id
            }
        }

        if let index = order.firstIndex(of: nextId) {
            order.insert(model.id, at: index)
        } else {
            order.append(model.id)
        }
    }
    
    // MARK: - Drag and Drop
    
    private func removeFromChild(_ entityId: String) {
        for (key, item) in items where item.child.contains(entityId) {
            items[key]?.child.removeAll(where: { $0 == entityId })
        }
    }
    
    func insert(_ entityId: String, after id: String?) {
        removeFromChild(entityId)
        
        if let index = order.firstIndex(of: entityId) {
            order.remove(at: index)
        }
        
        if let id = id, let index = order.firstIndex(of: id) {
            order.insert(entityId, at: index + 1)
        } else {
            order.insert(entityId, at: 0)
        }
    }
    
    func insertChild(_ entityId: String, after id: String) {
        removeFromChild(entityId)
        
        var child = items[entityId]?.child ?? []
        child.insert(entityId, at: 0)
        items[entityId]?.child = []

        for (key, item) in items where item.child.contains(id) {
            if let index = item.child.firstIndex(of: id),
                index < item.child.count - 1 {
                items[key]?.child.insert(contentsOf: child, at: index + 1)
            } else {
                items[key]?.child.append(contentsOf: child)
            }
            return
        }

        for (key, item) in items where item.id == id {
            items[key]?.child.insert(contentsOf: child, at: 0)
            return
        }
    }
    
    func createPositions(position: Double? = nil) {
        let newPosition = position ?? TaskModel.newItemPosition
        let sides = GetTaskSidesUseCase.shared
            .execute(self)

        positions = positions.filter({ sides.contains($0.side) })

        if position != nil {
            positions = positions.map({ TaskPositionModel(side: $0.side, value: newPosition) })
        }

        for side in sides where !positions.contains(where: { $0.side == side }) {
            positions.append(TaskPositionModel(side: side, value: newPosition))
        }
    }
}

extension TaskModel: Equatable {
    static func == (lhs: TaskModel, rhs: TaskModel) -> Bool {
        return lhs.id == rhs.id
    }
}
