//
//  SignUpModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct SignUpModel {

    enum SexType: Int {
        case male
        case female
        case unknown
    }

    enum FlowType {
        case signIn
        case signUp
    }

    enum TestType: Int {
        case alpha
        case beta
        case gamma
        case delta
        case unknown
    }

    enum Alpha: Int {
        case deonix
        case haggo
        case titan
        case viosity
        case unknown
    }

    enum Beta: Int {
        case alpha
        case beta
        case delta
        case omega
        case unknown
    }

    var currentStep = Variable<SignUpStepsGenerator.StepType>(.sex)

    var firstName =  Variable<String>("")
    var lastName =  Variable<String>("")
    var password =  Variable<String>("")
    var sex =  Variable<SexType>(.unknown)
    var surname =  Variable<String>("")
    var email =  Variable<String>("")

    var token =  Variable<String>("")
    var alpha =  Variable<Alpha>(.unknown)
    var beta =  Variable<Beta>(.unknown)
    let isValid: Observable<(Bool, String)>

    var dictionary: [String: AnyObject] {
        var dictionary: [String: AnyObject] = [:]
        dictionary["firstName"] = self.firstName.value as NSString
        dictionary["lastName"] = self.lastName.value as NSString
        dictionary["sex"] = NSNumber(value: self.sex.value.rawValue)
        dictionary["alpha"] = NSNumber(value: self.alpha.value.rawValue)
        dictionary["beta"] = NSNumber(value: self.beta.value.rawValue)
       return dictionary
    }

	init() {
		isValid = Observable.combineLatest(
			self.email.asObservable(),
			self.password.asObservable(),
			self.firstName.asObservable(),
			self.lastName.asObservable(),
			self.sex.asObservable(),
			self.alpha.asObservable(),
			self.beta.asObservable(),
			self.currentStep.asObservable()) { (email, password, firstName, lastName, gender, alpha, beta, step)  in

				switch step {
				case .email:
					return (email.isValidEmail, email.isValidEmail || email.isEmpty ? "" : NSLocalizedString("error.emailNotValid", comment: ""))
				case .sex:
					return  (gender != .unknown, "")
				case .alpha:
					return  (alpha != .unknown, "")
				case .beta:
					return  (beta != .unknown, "")
				case .password:
					return (password.isValidPassword, password.isValidPassword  || password.isEmpty ? "" : NSLocalizedString("error.passwordNotValid", comment: ""))
				case .firstName:
					return (firstName.count > 0, "")
				case .lastName:
					return (lastName.count > 0, "")
				case .finish:
					return (true, "")
				default:
					return (false, "")
				}
		}
	}
}
