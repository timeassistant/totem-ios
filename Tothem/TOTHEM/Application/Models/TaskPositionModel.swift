//
//  TaskPositionModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

struct TaskPositionModel: Equatable {
    var side: TimesetType = .center
    var value: Double = Double.greatestFiniteMagnitude
    var dictionary: [String: Any] {
        get {
            return [
                "side": side.rawValue,
                "value": value
            ]
        }
        set {
            if let side = TimesetType(rawValue: (newValue["side"] as? Int) ?? NSNotFound) {
                self.side = side
            }
            if let value = (newValue["value"] as? Double) {
                self.value = value
            } else if let value = (newValue["value"] as? Int) {
                self.value = Double(value)
            }
        }
    }

    init(_ dictionary: [String: Any]? = nil) {
        self.dictionary = dictionary ?? [:]
    }

    init(side: TimesetType, value: Double) {
        self.side = side
        self.value = value
    }
    
    static func == (lhs: TaskPositionModel, rhs: TaskPositionModel) -> Bool {
        return lhs.side == rhs.side && lhs.value == rhs.value
    }
}
