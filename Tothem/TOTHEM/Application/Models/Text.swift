//
//  Text.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct Text {
    static var animationDuration: Double = 3

    static func height(_ string: String, width: CGFloat, font: UIFont) -> CGFloat {
        return string.size(width: width, font: font).height + 1
    }

    static func height(_ strings: [String], width: CGFloat, font: UIFont) -> CGFloat {
        return strings.reduce(0) { Text.height($1, width: width, font: font) + $0 }
    }
}
