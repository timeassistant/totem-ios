//
//  TimesetType.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

enum TimesetType: Int {
    case center
    case left
    case right

    var path: String {
        switch self {
        case .center: return "center"
        case .right: return "right"
        case .left: return "left"
        }
    }
    
    var eventKey: String {
        switch self {
        case .center: return "c"
        case .right: return "r"
        case .left: return "l"
        }
    }
}

//Switch cuurent state state
extension TimesetType {
    var `switch`: TimesetType {
        switch self {
        case .center:
            return .center
        case .left:
            return .right
        case .right:
            return .left
        }
    }
}
