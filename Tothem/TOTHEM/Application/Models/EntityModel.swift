//
//  EntityModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

struct EntityModel: EntityProtocol, Equatable {
    static var newId: () -> String = {
        return UUID().uuidString
    }

    enum EntityType: String {
        case photo
        case bullet
        case text
    }
    
    var id: String = EntityModel.newId()
    var mid: String?
    var uid: String?
    var value: String = ""
    var checked: Bool = false
    var child: [String] = []
    var type: EntityType = .text
    
    var labels: [AiLabelModel] = []
    
    var json: JSONObject {
        var dict: JSONObject = [
            "id": id,
            "value": value,
            "checked": checked,
            "type": type.rawValue,
            "labels": labels.map({ $0.json })
        ]
        
        if let uid = uid {
            dict["uid"] = uid
        }
        
        if let mid = mid {
            dict["mid"] = mid
        }
        
        dict["child"] = child
        
        return dict
    }

    init(_ type: EntityType, mid: String?, uid: String?, json: JSONObject? = nil) {
        self.mid = (json?["mid"] as? String) ?? mid
        self.uid = (json?["uid"] as? String) ?? uid
        self.type = type
        set(json)
    }

    init(_ type: EntityType, value: String, labels: [AiLabelModel] = []) {
        self.type = type
        self.value = value
        self.labels = labels
    }

    private mutating func set(_ json: JSONObject?) {
        guard let newValue = json else { return }
        id = newValue["id"] as? String ?? EntityModel.newId()
        if let asset = newValue["asset_id"] as? String {
            value = asset
        } else {
            value = (newValue["value"] as? String) ?? ""
        }
        child = newValue["child"] as? [String] ?? []
        if let value = newValue["checked"] as? Bool {
            checked = value
        } else if let stringValue = newValue["checked"] as? String,
            let value = Bool(stringValue) {
            checked = value
        }
        
        if let array = json?["labels"] as? JSONArray {
            labels = array.map({ AiLabelModel(json: $0) })
        }
    }
    
    mutating func set(_ labels: [AiLabelModel]) {
        self.labels = labels
    }
    
    static func == (lhs: EntityModel, rhs: EntityModel) -> Bool {
        return lhs.id == rhs.id
    }
}
