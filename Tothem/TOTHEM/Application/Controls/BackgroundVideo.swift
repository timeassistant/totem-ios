//
//  BackgroundVideo.swft
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import AVFoundation

enum BackgroundVideoError: Error {
    case missingVideo
}

public class BackgroundVideo: UIView {

    private var player: AVPlayer?

    /**
     createBackgroundVideo(name: String, type: String) function:
     - name: String - take in the name of the video file
     - type: String - take in the file type of the video file
     */

    public func createBackgroundVideo(name: String, type: String) {
        do {
            try createBackground(name: name, type: type)
        } catch BackgroundVideoError.missingVideo {
            print("SwiftVideoBackground Error: Invalid Video URL. Please check the video name and type.")
        } catch {
            print("SwiftVideoBackground Fatal Error")
        }
    }

    /**
      createBackgroundVideo(name: String, type: String, alpha: CGFloat) function:
      - name: String - take in the name of the video file
      - type: String - take in the file type of the video file
      - alpha: CGFloat - take in the desired alpha/darkness of the background video
    */

    public func createBackgroundVideo(name: String, type: String, alpha: CGFloat) {
        createAlpha(alpha: alpha)

        do {
            try createBackground(name: name, type: type)
        } catch BackgroundVideoError.missingVideo {
            print("SwiftVideoBackground Error: Invalid Video URL. Please check the video name and type.")
        } catch {
            print("SwiftVideoBackground Fatal Error")
        }
    }

    private func createAlpha(alpha: CGFloat) {
        let overlayView = UIView(frame: UIScreen.main.bounds)
        overlayView.backgroundColor = UIColor.black
        overlayView.alpha = alpha
        self.addSubview(overlayView)
        self.sendSubviewToBack(overlayView)
    }

    private func createBackground(name: String, type: String) throws {
        guard let path = Bundle.main.path(forResource: name, ofType: type) else {
            throw BackgroundVideoError.missingVideo
        }

        player = AVPlayer(url: URL(fileURLWithPath: path))
        if let player = player {
            player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = UIScreen.main.bounds
            player.volume = 0
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.layer.insertSublayer(playerLayer, at: 0)

            // Set observer for when video ends and loop video infinitely
            NotificationCenter.default.addObserver(self,
												   selector: #selector(playerItemDidReachEnd),
												   name: Notification.Name.AVPlayerItemDidPlayToEndTime,
												   object: player.currentItem)
            player.seek(to: CMTime.zero)
            player.play()
        }
    }

    @objc private func playerItemDidReachEnd() {
        player?.seek(to: CMTime.zero)
    }
}
