//
//  AppearanceProxyHelper.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit

struct ApperanceProxyHelper {

    static func customizeSearchBar() {
        UIBarButtonItem.appearance(whenContainedInInstancesOf:
            [UISearchBar.self]).tintColor = .white
    }

    static func customizeNavigationBar() {
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = .white
        navigationBarAppearace.barTintColor = UIColor.black
        navigationBarAppearace.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor.white ]
    }

    static func drawBorderForView(view: UIView, color: UIColor, width: CGFloat) {
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = width
        view.clipsToBounds = true
    }
}

enum NavigationItems {
    case menu(Any, Selector)
    case back(Any, Selector)
    case logout(Any, Selector)
    case search(Any, Selector)

    func button() -> UIBarButtonItem {
        switch self {
        case .menu(let target, let selector):
            return UIBarButtonItem(image: UIImage(), style: .plain, target: target, action: selector)
        case .back(let target, let selector):
            return UIBarButtonItem(image: UIImage(), style: .plain, target: target, action: selector)
        case .logout(let target, let selector):
            return UIBarButtonItem(image: UIImage(), style: .plain, target: target, action: selector)
        case .search(let target, let selector):
            return UIBarButtonItem(image: UIImage(), style: .plain, target: target, action: selector)
        }
    }
}
