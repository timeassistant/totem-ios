//
//  JTMaterialSpinner.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

open class JTMaterialSpinner: UIView {
    let circleLayer = CAShapeLayer()
    open private(set) var isAnimating = false
    private var animationName: String = "pullToRefreshAnimation"
    private let dispose = DisposeBag()

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    open func commonInit() {
        self.layer.addSublayer(circleLayer)

        circleLayer.fillColor = nil
        circleLayer.lineCap = CAShapeLayerLineCap.round
        circleLayer.lineWidth = 3

        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.strokeStart = 0
        circleLayer.strokeEnd = 0
    }

    open override func layoutSubviews() {
        super.layoutSubviews()

        if self.circleLayer.frame != self.bounds {
            updateCircleLayer()
        }
    }

    open func updateCircleLayer() {
        let center = CGPoint(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
        let radius = (self.bounds.height - self.circleLayer.lineWidth) / 2.0

        let startAngle: CGFloat = 0.0
        let endAngle: CGFloat = 2.0 * CGFloat.pi

        let path = UIBezierPath(arcCenter: center,
                                radius: radius,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)

        self.circleLayer.path = path.cgPath
        self.circleLayer.frame = self.bounds
    }

    open func forceBeginRefreshing() {
        self.isAnimating = false
        self.beginRefreshing()
    }

    open func beginRefreshing() {
        guard !isAnimating else { return }
        self.isAnimating = true
        ViseraAnimator.shared
            .animate(circleLayer, with: .pullToRefresh, name: animationName)
            .do(onNext: { animationName in
                self.animationName = animationName
            }, onCompleted: {
                self.isAnimating = false
            })
            .subscribe()
            .disposed(by: dispose)
    }

    open func endRefreshing () {
        self.circleLayer.removeAnimation(forKey: animationName)
    }
}
