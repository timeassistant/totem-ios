//
//  NetworkDataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class NetworkDataSource {
    // MARK: - Public methods
    func object(forEndpoint endpoint: EndpointProtocol, headers: [String: String]? = nil, parameters: [String: Any]? = nil) -> Observable<JSONObject> {
        return makeAPICall(with: ApiRequest(endpoint: endpoint, headers: headers, parameters: parameters))
    }
    
    func array(forEndpoint endpoint: EndpointProtocol, headers: [String: String]? = nil, parameters: [String: Any]? = nil) -> Observable<JSONArray> {
        return makeAPICall(with: ApiRequest(endpoint: endpoint, headers: headers, parameters: parameters))
    }
    
    func makeMultiPartAPIRequest(forEndpoint endpoint: EndpointProtocol, headers: [String: String]? = nil, boundary: String, parameters: Data, delegate: URLSessionTaskDelegate? = nil) -> Observable<JSONObject> {
        return makeAPICall(with: ApiRequest(endpoint: endpoint, headers: headers, parameters: parameters, type: ApiRequestType.multiPart(boundary: boundary), delegate: delegate))
    }
    
    // MARK: - Private methods
    private func makeAPICall<T>(with apiRequest: ApiRequest) -> Observable<T> {
        return Observable.create({ observer in
            let request = NetworkDataSource.createUrlRequest(for: apiRequest)
            let session = URLSession(configuration: .default, delegate: apiRequest.delegate, delegateQueue: nil)
            
            let task = session.dataTask(with: request) { data, response, error in
                if let httpResponse = response as? HTTPURLResponse {
                    if let url = httpResponse.url {
                        print("\(httpResponse.statusCode): \(url)")
                    }
                    
                    switch httpResponse.statusCode {
                    case 200 ... 299:
                        if let mod: T = NetworkDataSource.treatSuccess(with: data) {
                            print("\(mod)")
                            observer.onNext(mod)
                        } else {
                            observer.onError(ErrorModel(value: nil))
                        }
                    default:
                        observer.onError(ErrorModel(value: error))
                    }
                } else {
                    observer.onError(ErrorModel(value: error))
                }
                observer.onCompleted()
            }
            task.resume()
            
            return Disposables.create()
        })
    }
    
    private class func createUrlRequest(for apiRequest: ApiRequest) -> URLRequest {
        let url = URL(string: "\(apiRequest.endpoint.host)/\(apiRequest.endpoint.value)")!
        var request = URLRequest(url: url)
        request.httpMethod = apiRequest.endpoint.method.rawValue
        request.setValue(apiRequest.type.headerValue, forHTTPHeaderField: "Content-Type")
        if let parameters = apiRequest.parameters {
            switch apiRequest.endpoint.method {
            case .POST, .PUT, .DELETE:
                if let data = parameters as? Data {
                    request.httpBody = data
                } else if let dict = parameters as? [String: Any] {
                    request.httpBody = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                }
            case .GET:
                if let dict = parameters as? [String: Any] {
                    request.url = URL(string: "\(apiRequest.endpoint.host)/\(apiRequest.endpoint.value)?\(dict.stringFromHttpParameters())")!
                }
            }
        }
        
        for (key, value) in apiRequest.headers ?? [:] {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        return request
    }
    
    private class func treatSuccess<T>(with data: Data?) -> T? {
        if let data = data,
            let dict = (try? JSONSerialization.jsonObject(with: data, options: [])) as? T {
            return dict
        } else if let emptyDict = [String: Any]() as? T {
            return emptyDict
        } else if let emptyArray = [[String: Any]]() as? T {
            return emptyArray
        } else {
            return nil
        }
    }
}

extension String {
    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
}

extension Dictionary {
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            if let key = key as? String {
                let percentEscapedKey = key.addingPercentEncodingForURLQueryValue()!
                let percentEscapedValue = String(describing: value).addingPercentEncodingForURLQueryValue()!
                return "\(percentEscapedKey)=\(percentEscapedValue)"
            }
            return ""
        }
        
        return parameterArray.joined(separator: "&")
    }
}
