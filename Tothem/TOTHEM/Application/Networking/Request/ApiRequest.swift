//
//  ApiRequest.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

enum ApiRequestType {
    case json
    case multiPart(boundary: String)
    
    var headerValue: String {
        switch self {
        case .json: return "application/json; charset=utf-8"
        case .multiPart(let boundary): return "multipart/form-data; boundary=\(boundary)"
        }
    }
}

struct ApiRequest {
    let endpoint: EndpointProtocol
    let parameters: Any?
    weak var delegate: URLSessionTaskDelegate?
    let headers: [String: String]?
    let type: ApiRequestType
    
    init(endpoint: EndpointProtocol, headers: [String: String]?, parameters: Any?, type: ApiRequestType = .json, delegate: URLSessionTaskDelegate? = nil) {
        self.endpoint = endpoint
        self.parameters = parameters
        self.delegate = delegate
        self.headers = headers
        self.type = type
    }
}
