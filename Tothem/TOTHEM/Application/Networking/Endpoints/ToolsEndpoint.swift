//
//  TextAnalysisEndpoint.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

extension Endpoint {
    enum ToolsEndpoint: EndpointProtocol {
        case label
        case translate
        
        var host: String {
            #if DEBUG
            return "https://visera-sandbox.herokuapp.com"
            #else
            return "https://visera-text-analysis.herokuapp.com"
            #endif
        }
        
        var value: String {
            switch self {
            case .label: return "tools/text/label"
            case .translate: return "tools/text/translate"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            default: return .POST
            }
        }
        
        var authenticated: Bool {
            switch self {
            default: return false
            }
        }
    }
}
