//
//  TextAnalysisEndpoint.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

extension Endpoint {
    enum UsersEndpoint: EndpointProtocol {
        case content(String, String)
        case similar(String, String)
        case saveTask(String)
        case classify(String)
        case contentView(String, String)
        case sideView(String)
        case onboardingClass(String)
        case subjectClass(String)
        case uniqueClass(String)
        case evolutionClass(String)
        case userClass(String)

        var host: String {
            #if DEBUG
            return "http://127.0.0.1:5000"
            #else
            return "https://visera-text-analysis.herokuapp.com"
            #endif
        }

        var value: String {
            switch self {
            case .content(let user, let task):
                return "users/\(user)/content/\(task)"
            case .similar(let user, let task):
                return "users/\(user)/content/\(task)/similar"
            case .saveTask(let user):
                return "users/\(user)/content/save"
            case .classify(let user):
                return "users/\(user)/classify"
            case .contentView(let user, let task):
                return "users/\(user)/content/\(task)/view"
            case .sideView(let user):
                return "users/\(user)/content"
            case .onboardingClass(let user):
                return "users/\(user)/onboarding"
            case .subjectClass(let user):
                return "users/\(user)/subject"
            case .uniqueClass(let user):
                return "users/\(user)/unique"
            case .evolutionClass(let user):
                return "users/\(user)/evolution"
            case .userClass(let user):
                return "users/\(user)/cn"
            }
        }

        var method: HTTPMethod {
            switch self {
            case .saveTask, .classify, .contentView, .sideView, .onboardingClass, .subjectClass, .uniqueClass, .evolutionClass, .userClass:
                return .POST
            default:
                return .GET
            }
        }

        var authenticated: Bool {
            switch self {
            default: return false
            }
        }
    }
}
