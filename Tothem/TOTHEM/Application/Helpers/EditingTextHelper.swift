//
//  EditingTextHelper.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

extension Sequence where Iterator.Element: EditingTextViewModel {
    var text: String {
        return reduce("") { res, model in
            guard let text = model.asString, model.active else { return res }
            return res.isEmpty ? text : res + Symbol.newLine.rawValue + text
        }
    }
}

struct EditingTextHelper {
    // MARK: - view models
    
    static func textViewModel(_ models: [EntityModel]) -> EditingTextViewModel {
        let viewModel = EditingTextViewModel(.text)
        let text = self.text(from: models)
        viewModel.text.on(.next(text))
        return viewModel
    }
    
    static func checklistViewModel(_ models: [EntityModel]) -> EditingTextViewModel {
        let viewModel = EditingTextViewModel(.checklist)
        let text = self.text(from: models)
        viewModel.text.on(.next(text))
        return viewModel
    }
    
    static func text(from models: [EntityModel]) -> String {
        return models.reduce("", { result, item in
            return result.isEmpty ? item.value : result + Symbol.newLine.rawValue + item.value
        })
    }

    static func ordered(_ models: [EntityModel], order: [String]) -> [EntityModel] {
        var newModels: [EntityModel] = []
        var dict: [String: EntityModel] = [:]
        for model in models {
            dict[model.id] = model
        }

        for id in order {
            guard let model = dict[id] else { continue }
            newModels.append(model)
            for child in model.child {
                guard let model = dict[child] else { continue }
                newModels.append(model)
            }
        }

        newModels.append(contentsOf: models.filter({
            !newModels.contains($0)
        }))

        return newModels
    }

    static func text(_ models: [EntityModel], order: [String]) -> String {
        return EditingTextHelper.text(from: ordered(models, order: order))
    }
    
    static func updated(_ models: [EntityModel], order: [String], type: EntityModel.EntityType, text: String) -> [EntityModel] {
        var orderedModels = ordered(models, order: order)
        let lines = text.components(separatedBy: Symbol.newLine.rawValue)
        let newLines = lines.filter({ line in
            !models.contains(where: { $0.value == line }) && !line.isEmpty
        })

        if newLines.count == 0 {
            if newLines.count < orderedModels.count {
                orderedModels = orderedModels.filter({ lines.contains($0.value) })
            }
        } else if newLines.count == 1 {
            if lines.count == orderedModels.count {
                if let index = lines.firstIndex(of: newLines[0]) {
                    orderedModels[index].value = newLines[0]
                }
            } else if lines.count > orderedModels.count {
                if let index = lines.firstIndex(of: newLines[0]) {
                    let model = EntityModel(type, value: newLines[0])
                    if index >= orderedModels.count {
                        orderedModels.append(model)
                    } else {
                        orderedModels.insert(model, at: index)
                    }
                }
            } else if lines.count < orderedModels.count {
                orderedModels = orderedModels.filter({ lines.contains($0.value) })
                if let index = lines.firstIndex(of: newLines[0]) {
                    orderedModels.insert(EntityModel(type, value: newLines[0]), at: index)
                }
            }
        } else {
            // FIXME: handle multiple line insertion
        }
        
        return orderedModels
    }

    static func makeOrder(_ models: [EntityModel]) -> [String] {
        var items: [String: EntityModel] = [:]
        var child: [String] = []

        models.forEach {
            child.append(contentsOf: $0.child)
            items[$0.id] = $0
        }

        var entities: [String] = models
            .filter({ !$0.checked && !child.contains($0.id) })
            .map({ $0.id })

        entities.append(contentsOf: models
            .filter({ $0.checked && !child.contains($0.id) })
            .map({ $0.id }))

        return entities
    }
}
