//
//  GyroscopeConnector.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import CoreMotion

class Gyroscope {
    static let shared = Gyroscope()

    var userAccelerationX: Observable<ParameterValue>!
    var userAccelerationY: Observable<ParameterValue>!
    var userAccelerationZ: Observable<ParameterValue>!

    var deviceUserAccelerationX: Observable<ParameterValue>!
    var deviceUserAccelerationY: Observable<ParameterValue>!
    var deviceUserAccelerationZ: Observable<ParameterValue>!

    var attitudeRoll: Observable<ParameterValue>!
    var attitudePitch: Observable<ParameterValue>!
    var attitudeYaw: Observable<ParameterValue>!

    var deviceAttitudeRoll: Observable<ParameterValue>!
    var deviceAttitudePitch: Observable<ParameterValue>!
    var deviceAttitudeYaw: Observable<ParameterValue>!
    
    private init() {
        let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "Gyroscope")
        
        let device = CMMotionManager.rx.manager()
            .observeOn(queue)
            .flatMapLatest({ $0.deviceMotion ?? Observable.empty() })

        let manualRoll = DataSourcesSettingsService.shared
            .observableStaticValue(for: .attitudeRoll)
            .observeOn(queue)
            .map { $0 ?? 0 }
        let manualPitch = DataSourcesSettingsService.shared
            .observableStaticValue(for: .attitudePitch)
            .observeOn(queue)
            .map { $0 ?? 0 }
        let manualYaw = DataSourcesSettingsService.shared
            .observableStaticValue(for: .attitudeYaw)
            .observeOn(queue)
            .map { $0 ?? 0 }

        let dataFlow = Observable.combineLatest(
            DataSourcesSettingsService.shared.isDynamicModeEnabled,
            DataSourcesSettingsService.shared.observableIsDynamic(for: .userAccelerationX)).observeOn(queue)
            .map { $0.0 || $0.1 }

        deviceUserAccelerationX = device.map({ .double($0.userAcceleration.x)
        })
        deviceUserAccelerationY = device.map({ .double($0.userAcceleration.y)
        })

        userAccelerationX = dataFlow
            .flatMap({ value -> Observable<ParameterValue> in
                if value {
                    return self.deviceUserAccelerationX
                } else {
                    return manualPitch.map({ .double($0) })
                }
            })
        userAccelerationY = dataFlow
            .flatMap({ value -> Observable<ParameterValue> in
                if value {
                    return self.deviceUserAccelerationY
                } else {
                    return manualPitch.map({ .double($0) })
                }
            })

        deviceAttitudeRoll = device.map({ .double($0.attitude.roll) })
        deviceAttitudePitch = device.map({ .double($0.attitude.pitch) })
        deviceAttitudeYaw = device.map({ .double($0.attitude.yaw) })

        attitudeRoll = dataFlow
            .flatMap({ value -> Observable<ParameterValue> in
                if value {
                    return self.deviceAttitudeRoll
                } else {
                    return manualRoll.map({ .double($0) })
                }
            })

        attitudePitch = dataFlow
            .flatMap({ value -> Observable<ParameterValue> in
                if value {
                    return self.deviceAttitudePitch
                } else {
                    return manualPitch.map({ .double($0) })
                }
            })

        attitudeYaw = dataFlow
            .flatMap({ value -> Observable<ParameterValue> in
                if value {
                    return self.deviceAttitudeYaw
                } else {
                    return manualYaw.map({ .double($0) })
                }
            })
    }
}
