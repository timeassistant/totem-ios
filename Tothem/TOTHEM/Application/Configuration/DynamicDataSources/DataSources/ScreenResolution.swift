//
//  ScreenResolution.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import UIKit

class ScreenResolution {

    static let shared = ScreenResolution()

    let screenWidthDeviceBased = Variable<ParameterValue>(Sensor.screenWidth.defaultValue)
    var screenWidthShared: Observable<ParameterValue>

    enum Sensor {
        case screenWidth

        var defaultValue: ParameterValue {
            switch self {
            case .screenWidth: return ParameterValue.int(320)
            }
        }

        var minimumDelta: Double {
            switch self {
            case .screenWidth: return 1
            }
        }
    }

    private init() {
        let dynamicScreenWidthShared = screenWidthDeviceBased.asObservable()
            .share(replay: 1, scope: SubjectLifetimeScope.forever)
        let staticScreenWidth = DataSourcesSettingsService.shared
            .observableStaticValue(for: .screenWidth)
            .map { $0 ?? Sensor.screenWidth.defaultValue.asDouble() }
        screenWidthShared = Observable.combineLatest(
            DataSourcesSettingsService.shared.isDynamicModeEnabled,
            DataSourcesSettingsService.shared.observableIsDynamic(for: .screenWidth))
            .map { $0.0 || $0.1 }
            .flatMap { value -> Observable<ParameterValue> in
                if value {
                    return dynamicScreenWidthShared
                } else {
                    return staticScreenWidth.map { ParameterValue.double($0) }
                }
        }
        updateScreenWidth()
    }

    private func updateScreenWidth() {
        let screenWidthAsDouble = Double(UIScreen.main.bounds.width)
        screenWidthDeviceBased.value = ParameterValue.double(screenWidthAsDouble)
    }

}
