//
//  LightSensor.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift
import AVFoundation

class LightSensor: NSObject {

    private let updatePeriod = 1.0 / 20.0

    static let shared = LightSensor()
    
    let captureSession: AVCaptureSession
    let captureDevice: AVCaptureDevice?
    let captureOutput: AVCaptureVideoDataOutput

    let lightBrightnessDeviceBased = Variable<ParameterValue>(Sensor.ambientLightBrightness.defaultValue)
    var lightBrightnessShared: Observable<ParameterValue>

    enum Sensor {
        case ambientLightBrightness

        var defaultValue: ParameterValue {
            switch self {
            case .ambientLightBrightness: return ParameterValue.double(0.5)
            }
        }
        var minimumDelta: Double {
            switch self {
            case .ambientLightBrightness: return 0.01
            }
        }
    }

    private override init() {

        let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "LightSensor")
        
        let dataSource = ConnectorParameter.ambientLightBrightness
        let isDynamicShared = DataSourcesSettingsService.shared.observableIsDynamic(for: dataSource)
            .share(replay: 1, scope: .forever)

        let dynamicLightBrightnessShared = Observable.combineLatest(isDynamicShared, lightBrightnessDeviceBased.asObservable()
            .distinctUntilChanged { $0.asDouble() == $1.asDouble() })
            .observeOn(queue)
            .filter { $0.0 }
            .map { $0.1 }
        
        let staticLightBrightnessShared = Observable.combineLatest(isDynamicShared, DataSourcesSettingsService.shared
            .observableStaticValue(for: dataSource))
            .observeOn(queue)
            .filter { !$0.0 }
            .map { $0.1 }
            .map { $0 ?? Sensor.ambientLightBrightness.defaultValue.asDouble() }
            .distinctUntilChanged()
            .map { ParameterValue.double($0) }
        lightBrightnessShared = Observable.from([dynamicLightBrightnessShared, staticLightBrightnessShared])
            .observeOn(queue)
            .merge()
            .share(replay: 1, scope: .forever)

        captureSession = AVCaptureSession()
        captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        captureOutput = AVCaptureVideoDataOutput()

        super.init()

        self.lightBrightnessDeviceBased.value = ParameterValue.double(Double(UIScreen.main.brightness))

        captureSession.beginConfiguration()
        captureSession.sessionPreset = .low
        guard let captureDevice = captureDevice,
            let cameraDeviceInput = try? AVCaptureDeviceInput(device: captureDevice),
            captureSession.canAddInput(cameraDeviceInput) else {
                setupLightSensorListeners()
                return
        }
        captureSession.addInput(cameraDeviceInput)
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global())
        guard captureSession.canAddOutput(captureOutput) else {
            setupLightSensorListeners()
            return
        }
        captureSession.addOutput(captureOutput)
        captureSession.commitConfiguration()

        captureSession.startRunning()
    }

    private func setupLightSensorListeners() {
        _ = NotificationCenter.default.addObserver(forName: UIScreen.brightnessDidChangeNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let screen = notification.object as? UIScreen else { return }
            self?.lightBrightnessDeviceBased.value = ParameterValue.double(Double(screen.brightness))
        }
    }
}

extension LightSensor: AVCaptureVideoDataOutputSampleBufferDelegate {

    //https://stackoverflow.com/questions/41921326/how-to-get-light-value-from-avfoundation
    //http://www.conservationphysics.org/lightmtr/luxmtr1.php
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //Retrieving EXIF data of camara frame buffer
        let rawMetadata = CMCopyDictionaryOfAttachments(allocator: nil, target: sampleBuffer, attachmentMode: CMAttachmentMode(kCMAttachmentMode_ShouldPropagate))
        let metadata = CFDictionaryCreateMutableCopy(nil, 0, rawMetadata) as NSMutableDictionary
        guard let exifData = metadata.value(forKey: "{Exif}") as? NSMutableDictionary else {
            return
        }
        guard let fNumber = exifData["FNumber"] as? Double,
            let exposureTime = exifData["ExposureTime"] as? Double,
            let ISOSpeedRatingsArray = exifData["ISOSpeedRatings"] as? NSArray,
            let ISOSpeedRatings = ISOSpeedRatingsArray[0] as? Double else {
                return
        }

        let calibrationConstant = 0.5
        
        //Calculating the luminosity
        let luminosity = (calibrationConstant * fNumber * fNumber ) / ( exposureTime * ISOSpeedRatings )
        lightBrightnessDeviceBased.value = ParameterValue.double(luminosity)
    }
}
