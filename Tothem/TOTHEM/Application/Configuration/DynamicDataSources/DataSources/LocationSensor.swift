//
//  LocationSensor.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import CoreLocation
import CoreMotion
import RxCoreMotion

public let constantG: Double = 9.8

enum LocationSensorActivityType: String {
    case walkingStroke, other, driving
}

class LocationSensor: NSObject {
    
    private let disposeBag = DisposeBag()

    private let updatePeriod = 1.0 / 20.0

    static let shared = LocationSensor()

    struct AccelerationComputationData {
        var newSpeed: Double
        var oldSpeed: Double
        var newAcceleration: Double
        var updateDate: Date
    }

    private let locationManager = CLLocationManager()
    private let activityManager = CMMotionActivityManager()
    private let motionManager = CMMotionManager.rx.manager()

    private var isInDrivingMode = false
    private var currentActivityType: LocationSensorActivityType = .other {
        didSet {
            print("\(currentActivityType.rawValue) started")
        }
    }

    let walkingSpeedDeviceBased = Variable<ParameterValue>(Sensor.walkingSpeed.defaultValue)
    let walkingSpeedShared: Observable<ParameterValue>

    let drivingSpeedDeviceBased = Variable<ParameterValue>(Sensor.drivingSpeed.defaultValue)
    let drivingSpeedShared: Observable<ParameterValue>
    
    let currentLocation = BehaviorSubject<(ParameterValue, ParameterValue)>(value: (.double(0), .double(0)))

    enum Sensor {
        case walkingSpeed
        case drivingSpeed

        var defaultValue: ParameterValue {
            switch self {
            case .walkingSpeed: return ParameterValue.double(0)
            case .drivingSpeed: return ParameterValue.double(0)
            }
        }

        var minimumDelta: Double {
            switch self {
            case .walkingSpeed: return 0.1
            case .drivingSpeed: return 1
            }
        }
    }

    private override init() {

        let walkingSpeedDataSource = ConnectorParameter.walkingSpeed
        let isDynamicShared = DataSourcesSettingsService.shared.observableIsDynamic(for: walkingSpeedDataSource)
            .share(replay: 1, scope: .forever)

        let dynamicWalkingSpeedShared = Observable.combineLatest(isDynamicShared, walkingSpeedDeviceBased.asObservable()
            .distinctUntilChanged { $0.asDouble() == $1.asDouble() })
            .filter { $0.0 }
            .map { $0.1 }
        let staticWalkingSpeedShared = Observable.combineLatest(isDynamicShared,
            DataSourcesSettingsService.shared.observableStaticValue(for: walkingSpeedDataSource))
            .filter { !$0.0 }
            .map { $0.1 }
            .map { $0 ?? Sensor.walkingSpeed.defaultValue.asDouble() }
            .distinctUntilChanged()
            .map { ParameterValue.double($0) }
        walkingSpeedShared = Observable.from([dynamicWalkingSpeedShared, staticWalkingSpeedShared])
            .merge()
            .share(replay: 1, scope: .forever)

        let drivingSpeedDataSource = ConnectorParameter.drivingSpeed
        let dynamicDrivingSpeedShared = Observable.combineLatest(isDynamicShared, drivingSpeedDeviceBased.asObservable()
            .distinctUntilChanged { $0.asDouble() == $1.asDouble() })
            .filter { $0.0 }
            .map { $0.1 }
        let staticDrivingSpeedShared = Observable.combineLatest(isDynamicShared,
            DataSourcesSettingsService.shared.observableStaticValue(for: drivingSpeedDataSource))
            .filter { !$0.0 }
            .map { $0.1 }
            .map { $0 ?? Sensor.drivingSpeed.defaultValue.asDouble() }
            .distinctUntilChanged()
            .map { ParameterValue.double($0) }
        drivingSpeedShared = Observable.from([dynamicDrivingSpeedShared, staticDrivingSpeedShared])
            .merge()
            .share(replay: 1, scope: .forever)
        
        super.init()
        setupMotionListener()
        setupWalkingSpeedDeviceBased()
    }

    private func setupMotionListener() {
        guard let currentQueue = OperationQueue.current else { return }
        activityManager.startActivityUpdates(to: currentQueue) { [weak self] (motionActivity) in
            guard let activity = motionActivity else { return }
            guard let strongSelf = self else { return }
            if activity.automotive || activity.cycling {
                strongSelf.setIfNeeded(activityType: .driving)
            } else if strongSelf.isInDrivingMode {
                strongSelf.isInDrivingMode = false
                strongSelf.setIfNeeded(activityType: .other)
            }
        }
        walkingSpeedDeviceBased.asObservable()
            .map { $0.asDouble() > 0.0 }
            .subscribe(onNext: { [weak self] speedHigherThanZero in
                guard let strongSelf = self, !strongSelf.isInDrivingMode else { return }
                if speedHigherThanZero {
                    strongSelf.setIfNeeded(activityType: .walkingStroke)
                } else {
                    strongSelf.setIfNeeded(activityType: .other)
                }
            })
            .disposed(by: disposeBag)

        locationManager.delegate = self
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            break
        case .authorizedWhenInUse, .authorizedAlways:
            startUpdating()
        @unknown default:
            break
        }
    }

    private func setupWalkingSpeedDeviceBased() {
        let deviceMotionObservable = motionManager
            .flatMapLatest { manager in
                manager.deviceMotion ?? Observable.empty()
            }
            .share(replay: 1, scope: .forever)

        let minimumAccelerationVertical = 0.02
        let returningToZeroTime: TimeInterval = 2.0

        let motionSpeedX = BehaviorSubject<Double>(value: 0.0)
        var speedLastUpdateX = Date()
        var lastAccelerationX: Double = 0.0

        let xAccelerationInG = deviceMotionObservable
            .map { $0.userAcceleration.x }
        normalizedAccelerationInG(xAccelerationInG)
            .withLatestFrom(motionSpeedX, resultSelector: combineAccelerationData)
            .map { (acceleration: $0.acceleration, currentSpeed: $0.currentSpeed,
                    lastAcceleration: lastAccelerationX, updatedAt: speedLastUpdateX) }
            .map(getNewSpeed)
            .withLatestFrom(deviceMotionObservable.map { $0.userAcceleration.z }, resultSelector: combineHorizontalAndZAcceleration)
            .subscribe(onNext: { data in
                lastAccelerationX = data.accelerationData.newAcceleration
                if data.accelerationData.newAcceleration == 0.0 && abs(data.zAcceleration) < minimumAccelerationVertical {
                    let coeffForDecreasingInSeconds = (returningToZeroTime
                        - (data.accelerationData.updateDate.timeIntervalSince(speedLastUpdateX)))
                        / returningToZeroTime
                    let normalizedCoeff = (coeffForDecreasingInSeconds > 0) ? coeffForDecreasingInSeconds : 0.0
                    motionSpeedX.onNext(data.accelerationData.newSpeed * normalizedCoeff)
                    return
                }
                speedLastUpdateX = data.accelerationData.updateDate
                motionSpeedX.onNext(data.accelerationData.newSpeed)
            })
            .disposed(by: disposeBag)

        let motionSpeedY = BehaviorSubject<Double>(value: 0.0)
        var speedLastUpdateY = Date()
        var lastAccelerationY: Double = 0.0

        let yAccelerationInG = deviceMotionObservable
            .map { $0.userAcceleration.y }
        normalizedAccelerationInG(yAccelerationInG)
            .withLatestFrom(motionSpeedY, resultSelector: combineAccelerationData)
            .map { (acceleration: $0.acceleration, currentSpeed: $0.currentSpeed,
                    lastAcceleration: lastAccelerationY, updatedAt: speedLastUpdateY) }
            .map(getNewSpeed)
            .withLatestFrom(deviceMotionObservable.map { $0.userAcceleration.z }, resultSelector: combineHorizontalAndZAcceleration)
            .subscribe(onNext: { data in
                lastAccelerationY = data.accelerationData.newAcceleration
                if data.accelerationData.newAcceleration == 0.0 && abs(data.zAcceleration) < minimumAccelerationVertical {
                    let coeffForDecreasingInSeconds = (returningToZeroTime
                        - (data.accelerationData.updateDate.timeIntervalSince(speedLastUpdateY)))
                        / returningToZeroTime
                    let normalizedCoeff = (coeffForDecreasingInSeconds > 0) ? coeffForDecreasingInSeconds : 0.0
                    motionSpeedY.onNext(data.accelerationData.newSpeed * normalizedCoeff)
                    return
                }
                speedLastUpdateY = data.accelerationData.updateDate
                motionSpeedY.onNext(data.accelerationData.newSpeed)
            })
            .disposed(by: disposeBag)

        Observable.combineLatest(motionSpeedX, motionSpeedY)
            .map { sqrt( ($0.0*$0.0) + ($0.1*$0.1) ) }
            .map { [weak self] newSpeed in
                if let activityType = self?.currentActivityType, activityType == .driving {
                    return 0.0
                }
                return newSpeed
            }
            .subscribe(onNext: { [weak self] speed in
                self?.walkingSpeedDeviceBased.value = ParameterValue.double(speed)
            })
            .disposed(by: disposeBag)
    }

    private func setIfNeeded(activityType: LocationSensorActivityType) {
        if currentActivityType != activityType {
            currentActivityType = activityType
            if activityType == .driving {
                isInDrivingMode = true
            }
        }
    }

    private func startUpdating() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 1
        locationManager.startUpdatingLocation()
    }

    private func stopUpdating() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationSensor: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.last?.coordinate {
            currentLocation.onNext((.double(coordinate.latitude), .double(coordinate.longitude)))
        }
        
        if let speedValue = locations.last?.speed, currentActivityType == .driving {
            if abs(drivingSpeedDeviceBased.value.asDouble() - speedValue) >= Sensor.drivingSpeed.minimumDelta {
                drivingSpeedDeviceBased.value = ParameterValue.double(speedValue * 3.6) /* Convert meters/second to kilometers/hour */
            }
        } else {
            drivingSpeedDeviceBased.value = ParameterValue.double(0)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error.localizedDescription)")
    }

    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            stopUpdating()
        case .authorizedWhenInUse:
            startUpdating()
        case .notDetermined, .authorizedAlways:
            break
        @unknown default:
            break
        }
    }
}

extension LocationSensor {

    private func normalizedAccelerationInG(_ observable: Observable<Double>) -> Observable<Double> {
        let minimumAccelerationHorizontal = 0.03
        let maximumAccelerationHorizontal = 0.8
        return observable
            .map { ($0 > minimumAccelerationHorizontal) ? $0 : 0.0 }
            .map { ($0 > maximumAccelerationHorizontal) ? maximumAccelerationHorizontal : $0 }
    }

    private func combineAccelerationData(acceleration: Double, speed: Double) -> (acceleration: Double, currentSpeed: Double) {
        return (acceleration: acceleration, currentSpeed: speed)
    }

    private func combineHorizontalAndZAcceleration(data: AccelerationComputationData, zAcceleration: Double)
        -> (accelerationData: AccelerationComputationData, zAcceleration: Double) {
        return (accelerationData: data, zAcceleration: zAcceleration)
    }

    private func getNewSpeed(_ data: (acceleration: Double, currentSpeed: Double, lastAcceleration: Double, updatedAt: Date)) -> AccelerationComputationData {
        let currentDate = Date()
        let dateDifference = currentDate.timeIntervalSince(data.updatedAt)
        let averageAcceleration = (data.acceleration + data.lastAcceleration) * constantG / 2
        let newSpeed = data.currentSpeed + (averageAcceleration * dateDifference)
        return AccelerationComputationData(newSpeed: newSpeed, oldSpeed: data.currentSpeed, newAcceleration: data.acceleration, updateDate: currentDate)
    }

}
