//
//  DataSourceManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

enum ConnectorParameter: String, Codable {

    static let dataSourcesList = [
        ConnectorParameter.screenWidth.rawValue,
        ConnectorParameter.ambientLightBrightness.rawValue,
        ConnectorParameter.walkingSpeed.rawValue,
        ConnectorParameter.drivingSpeed.rawValue,
        ConnectorParameter.userAccelerationX.rawValue,
        ConnectorParameter.userAccelerationY.rawValue,
        ConnectorParameter.userAccelerationZ.rawValue,
        ConnectorParameter.attitudeRoll.rawValue,
        ConnectorParameter.attitudePitch.rawValue,
        ConnectorParameter.attitudeYaw.rawValue
    ]

    case screenWidth = "Sc"
    case ambientLightBrightness = "Br"
    case walkingSpeed = "Sp"
    case drivingSpeed = "SpDr"
    case userAccelerationX = "UsAccX"
    case userAccelerationY = "UsAccY"
    case userAccelerationZ = "UsAccZ"
    case attitudeRoll = "AttRoll"
    case attitudePitch = "AttPitch"
    case attitudeYaw = "AttYaw"

    var description: String {
        switch self {
        case .screenWidth: return "Screen width in points"
        case .ambientLightBrightness: return "Light brightness"
        case .walkingSpeed: return "Speed of movement (kilometers/hour)"
        case .drivingSpeed: return "Speed of driving (kilometers/hour)"
        case .userAccelerationX: return "Shake acceleration by x - left and right"
        case .userAccelerationY: return "Shake acceleration by y - up and down"
        case .userAccelerationZ: return "Shake acceleration by z - back and forward"
        case .attitudeRoll: return "A roll is a rotation around a longitudinal (Y) axis that passes through the device from its top to bottom"
        case .attitudePitch: return "A pitch is a rotation around a lateral (X) axis that passes through the device from side to side"
        case .attitudeYaw: return "A yaw is a rotation around an axis (Z) that runs vertically through the device. It is perpendicular to the body of the device, with its origin at the center of gravity and directed toward the bottom of the device"
        }
    }

    var parameterDomain: String {
        switch self {
        case .walkingSpeed, .drivingSpeed: return "SIZE"
        case .ambientLightBrightness: return "COLOR"
        default: return ""
        }
    }

    var normalizedValueObservable: Observable<ParameterValue> {
        switch self {
        case .screenWidth:
            return ScreenResolutionNormalizer.normalizedScreenWidthObservable(
                ScreenResolution.shared.screenWidthShared)
        case .userAccelerationX:
            return Gyroscope.shared.userAccelerationX
        case .userAccelerationY:
            return Gyroscope.shared.userAccelerationX
        case .userAccelerationZ:
            return Gyroscope.shared.userAccelerationZ
        case .attitudeRoll:
            return Gyroscope.shared.attitudeRoll
        case .attitudePitch:
            return Gyroscope.shared.attitudePitch
        case .attitudeYaw:
            return Gyroscope.shared.attitudeYaw
        case .ambientLightBrightness:
            return LightSensorNormalizer.normalizedLightSensorObservable(
                LightSensor.shared.lightBrightnessShared)
        case .walkingSpeed:
            return LocationSensorNormalizer.normalizedWalkingSpeedObservable(
                LocationSensor.shared.walkingSpeedShared)
        case .drivingSpeed:
            return LocationSensorNormalizer.normalizedDrivingSpeedObservable(
                LocationSensor.shared.drivingSpeedShared)
        }
    }

    var normalizedDeviceBasedValueObservable: Observable<ParameterValue> {
        switch self {
        case .screenWidth:
            return ScreenResolutionNormalizer
                .normalizedScreenWidthObservable(ScreenResolution.shared
                    .screenWidthDeviceBased.asObservable())
        case .userAccelerationX:
            return GyroscopeNormalizer.normalized(
                Gyroscope.shared.deviceUserAccelerationX, .userAccelerationX)
        case .userAccelerationY:
            return GyroscopeNormalizer.normalized(
                Gyroscope.shared.deviceUserAccelerationY, .userAccelerationY)
        case .userAccelerationZ:
            return GyroscopeNormalizer.normalized(
                Gyroscope.shared.deviceUserAccelerationZ, .userAccelerationZ)
        case .attitudeRoll:
            return GyroscopeNormalizer.normalized(
                Gyroscope.shared.deviceAttitudeRoll, .attitudeRoll)
        case .attitudePitch:
            return GyroscopeNormalizer.normalized(
                Gyroscope.shared.deviceAttitudePitch, .attitudePitch)
        case .attitudeYaw:
            return GyroscopeNormalizer.normalized(
                Gyroscope.shared.deviceAttitudeYaw, .attitudeYaw)
        case .ambientLightBrightness:
            return LightSensorNormalizer.normalizedLightSensorObservable(
                LightSensor.shared.lightBrightnessDeviceBased.asObservable())
        case .walkingSpeed:
            return LocationSensorNormalizer.normalizedWalkingSpeedObservable(
                LocationSensor.shared.walkingSpeedDeviceBased.asObservable())
        case .drivingSpeed:
            return LocationSensorNormalizer.normalizedDrivingSpeedObservable(
                LocationSensor.shared.drivingSpeedDeviceBased.asObservable())
        }
    }
}

struct DataSourceInfo: Codable {
    var name: ConnectorParameter
    var description: String?
    var normalizerValuesForPlatforms: [NormalizerForPlatformInfo]

    init?(json: JSONObject) {
        
        guard let key = json["name"] as? String,
            let name = ConnectorParameter(rawValue: key) else {
            return nil
        }

        var normalizerInfoSets = [NormalizerForPlatformInfo]()
        if let json = json["normalizerValuesForPlatforms"] as? JSONObject {
            for (key, value) in json {
                guard let value = value as? JSONObject,
                    let normalizerInfoSet = NormalizerForPlatformInfo(name: key, json: value) else { continue }
                normalizerInfoSets.append(normalizerInfoSet)
            }
        }

        self.name = name
        self.description = json["description"] as? String
        self.normalizerValuesForPlatforms = normalizerInfoSets
    }

    func getNormalizer(parameter: NormalizerParameterName, forPlatform platform: NormalizerPlatform) -> Decimal? {
        return normalizerValuesForPlatforms
            .first(where: { $0.name == platform })?.values
            .first(where: { $0.name == parameter })?.value
    }
}

struct NormalizerForPlatformInfo: Codable {
    var name: NormalizerPlatform
    var values: [NormalizerParameter]

    init?(name: String, json: JSONObject) {
        guard let name = NormalizerPlatform(rawValue: name) else {
            return nil
        }

        var values = [NormalizerParameter]()
        for (key, value) in json {
            guard let value = NormalizerParameter(key: key, value: value) else {
                continue
            }
            values.append(value)
        }

        self.name = name
        self.values = values
    }
}

enum NormalizerPlatform: String, Codable {
    case ios
}

struct NormalizerParameter: Codable {
    var name: NormalizerParameterName
    var value: Decimal

    init?(key: String, value: Any) {
        guard let name = NormalizerParameterName(rawValue: key),
            let valueDouble = value as? Double else { return nil }

        self.name = name
        self.value = Decimal(valueDouble)
    }
}

enum NormalizerParameterName: String, Codable {
    case min
    case max
    case multiplier
}

enum NormalizerError: Error {
    case couldNotParse
}

class DataSourceManager {

    private let disposeBag = DisposeBag()

    private let sources = ParametersDataSource.sources

    static let shared = DataSourceManager()

    var dataSources = BehaviorSubject<[DataSourceInfo]>(value: [])
    var minValues = BehaviorSubject<[ConnectorParameter: ParameterValue]>(value: [:])
    var maxValues = BehaviorSubject<[ConnectorParameter: ParameterValue]>(value: [:])

    private init() {
        setupObservers()
    }

    private func setupObservers() {
        let dataObservable = sources.rx_array
            .map({ array -> [DataSourceInfo] in
                var dataSources = [DataSourceInfo]()
                for group in array {
                    for (_, value) in group {
                        guard let value = value as? JSONObject,
                            let info = DataSourceInfo(json: value) else { continue }
                        dataSources.append(info)
                    }
                }
                return dataSources
            })
            .filter { !$0.isEmpty }
            .share(replay: 1, scope: SubjectLifetimeScope.forever)

        // Observe data sources info
        dataObservable
            .bind(to: dataSources)
            .disposed(by: disposeBag)
        // Observe min values
        dataObservable
            .map { dataSources in
                dataSources.map { (parameter: $0.name, minValue: $0.getNormalizer(parameter: .min, forPlatform: .ios)) }
            }
            .map { parameters -> [ConnectorParameter: ParameterValue] in
                var resultDictionary = [ConnectorParameter: ParameterValue]()
                for parameter in parameters {
                    guard let value = parameter.minValue else { continue }
                    resultDictionary[parameter.parameter] = ParameterValue.double(Double(truncating: value as NSNumber))
                }
                return resultDictionary
            }
            .bind(to: minValues)
            .disposed(by: disposeBag)
        // Observe max values
        dataObservable
            .map { dataSources in
                dataSources.map { (parameter: $0.name, maxValue: $0.getNormalizer(parameter: .max, forPlatform: .ios)) }
            }
            .map { parameters -> [ConnectorParameter: ParameterValue] in
                var resultDictionary = [ConnectorParameter: ParameterValue]()
                for parameter in parameters {
                    guard let value = parameter.maxValue else { continue }
                    resultDictionary[parameter.parameter] = ParameterValue.double(Double(truncating: value as NSNumber))
                }
                return resultDictionary
            }
            .bind(to: maxValues)
            .disposed(by: disposeBag)
    }

    static func getObservable(forConnectors connectors: [ConnectorParameter]) -> Observable<[ConnectorParameter: ParameterValue]> {
        let connectorObservables = connectors.map { return $0.normalizedValueObservable }
        return Observable.combineLatest(connectorObservables) {
            return $0.toDictionary(withKeys: connectors)
        }.skipNil()
    }

}
