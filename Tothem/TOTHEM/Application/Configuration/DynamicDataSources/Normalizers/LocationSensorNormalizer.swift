//
//  LocationSensorNormalizer.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

class LocationSensorNormalizer: BaseNormalizer {

    static func normalizedWalkingSpeedObservable(_ observable: Observable<ParameterValue>) -> Observable<ParameterValue> {
        var anchor = 0.0
        return BaseNormalizer.normalizedObservable(observable, parameter: ConnectorParameter.walkingSpeed, valueTransformation: *)
            .do(onNext: { data in
                anchor = data.minValue.asDouble()
            })
            .map { $0.inscribedValue }
            .delay(startValue: ParameterValue.double(anchor), delayMultiplier: ParametersDataManager.delayMultiplier, directionRelativeToAnchor: { (previousValue, newValue) -> DeviationChange in
                    let previousDeviation = abs(previousValue.asDouble() - anchor)
                    let newDeviation = abs(newValue.asDouble() - anchor)
                    if newDeviation > previousDeviation {
                        return .increase
                    } else if newDeviation == previousDeviation {
                        return .withoutChange
                    } else {
                        return .decrease
                    }
                }, delayModifier: Observable<ParameterValue>.normalizeDelay)
    }

    static func normalizedDrivingSpeedObservable(_ observable: Observable<ParameterValue>) -> Observable<ParameterValue> {
        var anchor = 0.0
        return BaseNormalizer.normalizedObservable(observable, parameter: ConnectorParameter.drivingSpeed, valueTransformation: *)
            .do(onNext: { data in
                anchor = data.minValue.asDouble()
            })
            .map { $0.inscribedValue }
            .delay(startValue: ParameterValue.double(anchor), delayMultiplier: ParametersDataManager.delayMultiplier, directionRelativeToAnchor: { (previousValue, newValue) -> DeviationChange in
                    let previousDeviation = abs(previousValue.asDouble() - anchor)
                    let newDeviation = abs(newValue.asDouble() - anchor)
                    if newDeviation > previousDeviation {
                        return .increase
                    } else if newDeviation == previousDeviation {
                        return .withoutChange
                    } else {
                        return .decrease
                    }
                }, delayModifier: Observable<ParameterValue>.normalizeDelay)
    }
}
