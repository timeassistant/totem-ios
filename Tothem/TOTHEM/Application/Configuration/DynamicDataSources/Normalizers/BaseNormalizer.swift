//
//  BaseNormalizer.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

class BaseNormalizer {
    static let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "BaseNormalizer")
    static func normalizedObservable(_ observable: Observable<ParameterValue>, parameter: ConnectorParameter, valueTransformation: @escaping (_ value: Double, _ multiplier: Double) -> Double) -> Observable<(inscribedValue: ParameterValue, minValue: ParameterValue, maxValue: ParameterValue)> {
        return Observable.combineLatest(observable, DataSourceManager.shared.dataSources)
            .observeOn(queue)
            .map { parameterValue, dataSources -> (inscribedValue: ParameterValue,
                minValue: ParameterValue, maxValue: ParameterValue)? in
                guard let dataSourceInfo = dataSources.first(where: { $0.name == parameter }) else {
                    return nil
                }
                let min = dataSourceInfo.getNormalizer(parameter: .min, forPlatform: .ios)
                let max = dataSourceInfo.getNormalizer(parameter: .max, forPlatform: .ios)

                let mult = dataSourceInfo.getNormalizer(parameter: .multiplier, forPlatform: .ios)

                guard let minValue = min, let maxValue = max, let multiplier = mult else { return nil }

                let normalizedValue = valueTransformation(parameterValue.asDouble(), Double(truncating: multiplier as NSNumber))
                let minValueDouble = Double(truncating: minValue as NSNumber)
                let maxValueDouble = Double(truncating: maxValue as NSNumber)
                let inscribedValue = getInscribed(value: normalizedValue,
                                                  minValue: minValueDouble,
                                                  maxValue: maxValueDouble)
                return (inscribedValue: ParameterValue.double(inscribedValue),
                        minValue: ParameterValue.double(minValueDouble),
                        maxValue: ParameterValue.double(maxValueDouble))
            }
            .skipNil()
    }

}
