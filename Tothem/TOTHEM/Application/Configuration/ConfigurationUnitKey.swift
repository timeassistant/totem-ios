//
//  FormulaKey.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

enum ConfigurationUnitKey: String {
    case circleSize = "CircleSize"
}

extension ConfigurationUnitKey {
    var shortName: String {
        switch self {
        case .circleSize:
            return "CircleSize"
        }
    }
    static func getKey(byShortName shortName: String) -> ConfigurationUnitKey? {
        switch shortName {
        case "CircleSize": return .circleSize
        default: return nil
        }
    }

    var readableName: String {
        return self.rawValue.split(separator: "_")
            .map { return String($0).prefix(1).uppercased() + String($0).dropFirst() }
            .joined(separator: ".")
    }
    var previewElementType: FormulaPreviewVisualElement {
        switch self {
        case .circleSize: return .circleButton
        }
    }
    var previewElementParameter: FormulaPreviewVisualElementParameter {
        switch self {
        case .circleSize: return .size
        }
    }
}
