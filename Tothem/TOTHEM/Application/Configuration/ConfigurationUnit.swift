//
//  ConfigurationUnit.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct ComplexValue<T> {
    var minValue: T
    var value: T
    var maxValue: T
}

class ConfigurationUnit {

    private let key: ConfigurationUnitKey
    private let setIndex: Int

    private let debugMode: Bool

    let parametersSet: Observable<ConfigurationSet>
    let targetValue: Observable<ParameterValue>

    let parameterValues: Observable<FormulaParametersCollector>
    let minTargetValue: Observable<ParameterValue>?
    let maxTargetValue: Observable<ParameterValue>?

    static func getKeyAndIndex(byDescription description: String) -> (ConfigurationUnitKey, Int)? {
        let keyParts = description.split(separator: ".")
        guard keyParts.count == 2, let formulaKey = keyParts.first,
            let indexString = keyParts.last, let unitKey = ConfigurationUnitKey.getKey(byShortName: String(formulaKey)),
            let index = Int(String(indexString)) else {
                return nil
        }
        return (unitKey, index)
    }

    static func isIdentificatorValid(key: String) -> Bool {
        return (getKeyAndIndex(byDescription: key) != nil)
    }

    convenience init?(forDescription description: String, debugMode: Bool = false) {

        guard let (key, index) = ConfigurationUnit.getKeyAndIndex(byDescription: description) else {
            return nil
        }

        self.init(forKey: key, andIndex: index, debugMode: debugMode)
    }

    init(forKey key: ConfigurationUnitKey, andIndex setIndex: Int, debugMode: Bool = false) {

        self.key = key
        self.setIndex = setIndex

        self.debugMode = debugMode

        let metaDataObservable = FormulaEvaluationMetaDataManager.shared.data.asObservable()

        let formulaObservable = Formula.observable(forKey: key)
        let connectorsObservable = Observable.combineLatest(formulaObservable, metaDataObservable)
            .map { (formula, metaData) in
                return formula.getConnectors(withMetaData: metaData)
            }
            .flatMapLatest { DataSourceManager.getObservable(forConnectors: $0) }

        let sharedMainObservable = Observable.combineLatest(formulaObservable, metaDataObservable, connectorsObservable)
            .share(replay: 1, scope: SubjectLifetimeScope.forever)

        let parametersEvaluationObservable = sharedMainObservable
            .throttle(0.25, scheduler: MainScheduler.instance)
            .map { (formula, metaData, connectorValues) in
                return formula.evaluate(withMetaData: metaData, andConnectorValues: connectorValues)
            }
            .skipNil()
            .share()

        let parametersSetObservable = parametersEvaluationObservable
            .map { $0.configurationSet }
        self.parametersSet = parametersSetObservable

        let formulaUnitsObservable = parametersEvaluationObservable
            .map { $0.parametersCollector }
        self.parameterValues = formulaUnitsObservable

        let targetValueObservable = Observable.combineLatest(parametersSetObservable, Observable.just(self.setIndex))
            .map { (configurationSet, setIndex) in
                return configurationSet[setIndex]
            }
            .skipNil()
        self.targetValue = targetValueObservable

        if debugMode {
            self.minTargetValue = Observable.combineLatest(sharedMainObservable,
                                                                    DataSourceManager.shared.minValues,
                                                                    Observable.just(setIndex))
                .throttle(0.25, scheduler: MainScheduler.instance)
                .map { mainInfoTuple, minValues, setIndex -> ParameterValue? in
                    let formula = mainInfoTuple.0
                    let metaData = mainInfoTuple.1
                    if let (minParametersSet, _) =
                        formula.evaluate(withMetaData: metaData, andConnectorValues: minValues),
                        let minTargetValue = minParametersSet[setIndex] {
                        return minTargetValue
                    }
                    return nil
                }
                .skipNil()
            self.maxTargetValue = Observable.combineLatest(sharedMainObservable,
                                                                    DataSourceManager.shared.maxValues,
                                                                    Observable.just(setIndex))
                .throttle(0.25, scheduler: MainScheduler.instance)
                .map { mainInfoTuple, maxValues, setIndex -> ParameterValue? in
                    let formula = mainInfoTuple.0
                    let metaData = mainInfoTuple.1
                    if let (maxParametersSet, _) =
                        formula.evaluate(withMetaData: metaData, andConnectorValues: maxValues),
                        let maxTargetValue = maxParametersSet[setIndex] {
                        return maxTargetValue
                    }
                    return nil
                }
                .skipNil()
        } else {
            self.minTargetValue = nil
            self.maxTargetValue = nil
        }
    }

}
