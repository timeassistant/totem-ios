//
//  Formula.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct FormulaFirebaseInfo {

    var resultType: String
    var setLength: String
    var components: [String: String]
    var conditionalStatements: [FormulaComponentConditionalStatement]

    init?(with json: JSONObject) {
        guard let resultType = json["resultType"] as? String,
            let setLength = json["setLength"] as? String,
            let components = json["components"] as? [String: String] else {
            return nil
        }
        guard let statementsSnapshotValue = json["conditionalStatements"],
            let statements = try? FirebaseDecoder().decode([FormulaComponentConditionalStatement].self, from: statementsSnapshotValue) else {
            return nil
        }

        self.resultType = resultType
        self.setLength = setLength
        self.components = components
        self.conditionalStatements =  statements
    }
}

struct FormulaComponentConditionalStatement: Codable {
    var parameter: String
    var declaration: String
}

struct FormulaAlgorithm {
    var equation: String

    init?(with json: JSONObject) {
        guard let algorithm = json["value"] as? String else { return nil }
        self.equation = algorithm
    }
}

class FormulaParametersCollector {
    private var value: [String: ParameterValue]
    var parameters: [String: ParameterValue] { return value }
    init () { self.value = [String: ParameterValue]() }
    subscript(index: String) -> ParameterValue? {
        get { return self.value[index] }
        set(newValue) { self.value[index] = newValue }
    }
}

class Formula {

    static private let storage = ParametersDataSource.algorithms

    enum StructureType: String {
        case commonNormalizedProgression = "common_normalized_progression"
    }

    enum ResultType: String {

        case double
        case integer

        var initialValue: ParameterValue {
            switch self {
            case .double: return ParameterValue.double(0)
            case .integer: return ParameterValue.int(0)
            }
        }
    }

    private var algorithm: String
    private var resultType: Formula.ResultType
    private var setLength: String
    private var components: [String: String]
    private var conditionalStatements: [FormulaComponentConditionalStatement]

    private var initialValue: String? {
        return self.components[FormulaProcessor.initialValuePrefix]
    }

    func getConnectors(withMetaData metaData: FormulaEvaluationMetaData) -> [ConnectorParameter] {
        var connectorsList = [ConnectorParameter]()
        for (_, componentValue) in components {
            connectorsList += FormulaProcessor.getConnectorsList(forFormula: componentValue, metaData: metaData)
        }
        return connectorsList
    }

    static func observable(forKey key: ConfigurationUnitKey) -> Observable<Formula> {

        let formulaObservable = ConfigurationObservables.formulaFirebaseInfo(for: key)
        let typeObservable = ConfigurationObservables.type(for: key)
        let algorithmObservable = typeObservable
            .flatMap { ConfigurationObservables.algorithm(for: $0) }

        return Observable.combineLatest(formulaObservable, algorithmObservable)
            .map { (firebaseInfo, algorithm) in
                return Formula(fromFirebaseInfo: firebaseInfo, andAlgorithm: algorithm)
            }
            .skipNil()
    }

    private init?(fromFirebaseInfo data: FormulaFirebaseInfo, andAlgorithm algorithm: FormulaAlgorithm) {
        guard let resultType = ResultType.init(rawValue: data.resultType) else { return nil }
        self.algorithm = algorithm.equation
        self.resultType = resultType
        self.setLength = data.setLength
        self.components = data.components
        self.conditionalStatements = data.conditionalStatements
    }

    func evaluate(withMetaData metaData: FormulaEvaluationMetaData,
                  andConnectorValues connectorsValues: [ConnectorParameter: ParameterValue])
                    -> (configurationSet: ConfigurationSet, parametersCollector: FormulaParametersCollector)? {

        var parameterValues = FormulaParametersCollector()
        guard let setLength = evaluatedSetLength(withMetaData: metaData, parameterValuesCollector: &parameterValues),
            let initialValue = evaluateInitialValue(withMetaData: metaData, andConnectorValues: connectorsValues,
                                                    parameterValuesCollector: &parameterValues) else {
            return nil
        }

        var currentValue = initialValue
        var parametersSet = [
            1: currentValue
        ]

        guard let formulaDetailedAlgorithm =
            FormulaProcessor.convertToDetailed(formulaStructure: self.algorithm, withComponents: self.components) else {
                return nil
        }
        for currentIndex in 2...setLength {
            let nextValueInfo = FormulaEvaluationAdditionalInfo(metaData: metaData, connectorValues: connectorsValues,
                                                                currentValue: currentValue, resultType: self.resultType,
                                                                conditionalStatements: self.conditionalStatements)
            guard let evaluatedValue = FormulaProcessor.evaluate(formula: formulaDetailedAlgorithm,
                                                              info: nextValueInfo,
                                                              parameterValuesCollector: &parameterValues) else {
                print("Parameter set evaluation is unavailable")
                return nil
            }
            parametersSet[currentIndex] = evaluatedValue
            currentValue = evaluatedValue
        }

        return (configurationSet: parametersSet, parametersCollector: parameterValues)
    }

    private func evaluatedSetLength(withMetaData metaData: FormulaEvaluationMetaData,
                                    parameterValuesCollector: inout FormulaParametersCollector) -> Int? {
        let setLengthEvaluationInfo = FormulaEvaluationAdditionalInfo(metaData: metaData, connectorValues: [:],
                                                                      currentValue: nil, resultType: .integer,
                                                                      conditionalStatements: self.conditionalStatements)
        guard let setLength = FormulaProcessor.evaluate(formula: self.setLength,
                                                        info: setLengthEvaluationInfo,
                                                        parameterValuesCollector: &parameterValuesCollector) else {
                                                            return nil
        }
        return setLength.asInt()
    }

    private func evaluateInitialValue(withMetaData metaData: FormulaEvaluationMetaData,
                                      andConnectorValues connectorsValues: [ConnectorParameter: ParameterValue],
                                      parameterValuesCollector: inout FormulaParametersCollector) -> ParameterValue? {
        let initialValueEvaluationInfo = FormulaEvaluationAdditionalInfo(metaData: metaData, connectorValues: connectorsValues,
                                                                         currentValue: nil, resultType: self.resultType,
                                                                         conditionalStatements: self.conditionalStatements)
        guard let currentValueEquation = self.initialValue,
            let initialValue = FormulaProcessor.evaluate(formula: currentValueEquation,
                                                         info: initialValueEvaluationInfo,
                                                         parameterValuesCollector: &parameterValuesCollector) else {
                                                            return nil
        }
        return initialValue
    }

}
