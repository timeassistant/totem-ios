//
//  ConfigurationSet.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

typealias ConfigurationSet = [Int: ParameterValue]

private enum ComparisonOperator: Character {
    case lessThan = "<"
    case higherThan = ">"
    case equalTo = "="
    case lessThanOrEqualTo = "≤"
    case higherThanOrEqualTo = "≥"

    func compare<T: Comparable>(_ first: T, _ second: T) -> Bool {
        switch self {
        case .lessThan: return (first < second)
        case .higherThan: return (first > second)
        case .equalTo: return (first == second)
        case .lessThanOrEqualTo: return (first <= second)
        case .higherThanOrEqualTo: return (first >= second)
        }
    }
}

enum ParameterValue {
    case int(Int)
    case double(Double)
    case string(String)

    init?(json: JSONObject) {
        guard let value = json["value"] else { return nil }
        if let intValue = value as? Int {
            self = ParameterValue.int(intValue)
            return
        }
        if let numberValue = value as? NSNumber {
            self = ParameterValue.double(numberValue.doubleValue)
            return
        }
        if let stringValue = value as? String {
            self = ParameterValue.string(stringValue)
            return
        }
        return nil
    }

    init?(object: JSONObject) {
        guard let value = object["value"] else { return nil }
        if let intValue = value as? Int {
            self = ParameterValue.int(intValue)
            return
        }
        if let numberValue = value as? NSNumber {
            self = ParameterValue.double(numberValue.doubleValue)
            return
        }
        if let stringValue = value as? String {
            self = ParameterValue.string(stringValue)
            return
        }
        return nil
    }

    func rawValue<T>() -> T? {
        switch self {
        case .int(let intValue):
            return intValue as? T
        case .double(let doubleValue):
            return doubleValue as? T
        case .string(let stringValue):
            return stringValue as? T
        }
    }

    func asDouble() -> Double {
        switch self {
        case .int(let intValue):
            return Double(exactly: intValue) ?? 0
        case .double(let doubleValue):
            return doubleValue
        case .string(let stringValue):
            return Double(stringValue) ?? 0
        }
    }

    func asInt() -> Int {
        switch self {
        case .int(let intValue):
            return intValue
        case .double(let doubleValue):
            return Int(doubleValue)
        case .string(let stringValue):
            return Int(stringValue) ?? 0
        }
    }

    func asString() -> String {
        switch self {
        case .int(let intValue):
            return String(intValue)
        case .double(let doubleValue):
            return String(doubleValue.rounded(toPlaces: 2))
        case .string(let stringValue):
            return stringValue
        }
    }

    func perform(action: FormulaProcessor.Action, withOperand operand: ParameterValue) -> ParameterValue {
        switch action {
        case .binaryOperation(let binaryOperation):
            return self.perform(binaryOperation: binaryOperation, withOperand: operand)
        }
    }

    func isComparisonCorrect(comparisonSign: Character, secondValue: String) -> Bool {
        guard let comparisonOperator = ComparisonOperator(rawValue: comparisonSign) else {
            print("Can't recognize comparison operator: \(comparisonSign)")
            return false
        }
        switch self {
        case .int(let intValue):
            guard let secondIntValue = Int(secondValue) else {
                print("Can't recognize Int value: \(secondValue)")
                return false
            }
            return comparisonOperator.compare(intValue, secondIntValue)
        case .double(let doubleValue):
            guard let secondDoubleValue = Double(secondValue) else {
                print("Can't recognize Double value: \(secondValue)")
                return false
            }
            return comparisonOperator.compare(doubleValue, secondDoubleValue)
        case .string(let stringValue):
            return comparisonOperator.compare(stringValue, secondValue)
        }
    }

    func getClone(withValue newValue: String) -> ParameterValue {
        switch self {
        case .int:
            guard let newIntValue = Int(newValue) else {
                print("Can't clone Int value: \(newValue)")
                return self
            }
            return ParameterValue.int(newIntValue)
        case .double:
            guard let newDoubleValue = Double(newValue) else {
                print("Can't clone Double value: \(newValue)")
                return self
            }
            return ParameterValue.double(newDoubleValue)
        case .string:
            return ParameterValue.string(newValue)
        }
    }

    func perform(binaryOperation: FormulaProcessor.BinaryOperation, withOperand operand: ParameterValue) -> ParameterValue {
        switch binaryOperation {
        case .plus:
            switch self {
            case .double(let doubleValue):
                return ParameterValue.double(doubleValue + operand.asDouble())
            case .int(let intValue):
                return ParameterValue.int(intValue + operand.asInt())
            case .string(let stringValue):
                return ParameterValue.string(stringValue + operand.asString())
            }
        case .multiply:
            switch self {
            case .double(let doubleValue):
                return ParameterValue.double(doubleValue * operand.asDouble())
            case .int(let intValue):
                return ParameterValue.int(intValue * operand.asInt())
            case .string:
                return self
            }
        case .minus:
            switch self {
            case .double(let doubleValue):
                return ParameterValue.double(doubleValue - operand.asDouble())
            case .int(let intValue):
                return ParameterValue.int(intValue - operand.asInt())
            case .string:
                return self
            }
        case .divide:
            switch self {
            case .double(let doubleValue):
                return ParameterValue.double(doubleValue / operand.asDouble())
            case .int(let intValue):
                return ParameterValue.int(intValue / operand.asInt())
            case .string:
                return self
            }
        }
    }
}
