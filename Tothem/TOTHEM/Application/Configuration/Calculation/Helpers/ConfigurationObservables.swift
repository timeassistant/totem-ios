//
//  ConfigurationObservables.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

class ConfigurationObservables {

    static private let storage = ParametersDataSource.algorithms
    static private let designStorage = storage.child(path: "2_design")
    static private let matchingStorage = storage.child(path: "3_matching")
    static private let typesStorage = storage.child(path: "1_types")

    static func formulaFirebaseInfo(for key: ConfigurationUnitKey) -> Observable<FormulaFirebaseInfo> {
        return designStorage.child(path: key.rawValue).rx_object
            .map({ FormulaFirebaseInfo(with: $0) })
            .skipNil()
    }

    static func type(for key: ConfigurationUnitKey) -> Observable<Formula.StructureType> {
        return matchingStorage.rx_array
            .map({ array -> Formula.StructureType? in
                var type: Formula.StructureType?
                for item in array {
                    if let typeString = item[key.rawValue] as? String,
                        let castedType = Formula.StructureType(rawValue: typeString) {
                        type = castedType
                    }
                }
                return type
            })
            .skipNil()
    }

    static func algorithm(for type: Formula.StructureType) -> Observable<FormulaAlgorithm> {
        return typesStorage.rx_object
            .map({ FormulaAlgorithm(with: $0) })
            .skipNil()
    }
}
