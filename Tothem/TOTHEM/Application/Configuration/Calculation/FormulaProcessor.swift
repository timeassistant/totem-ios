//
//  FormulaParser.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

public typealias FormulaConstantKey = String
public typealias FormulaConnectorKey = String

struct FormulaEvaluationAdditionalInfo {
    var metaData: FormulaEvaluationMetaData
    var connectorValues: [ConnectorParameter: ParameterValue]
    var currentValue: ParameterValue?
    var resultType: Formula.ResultType
    var conditionalStatements: [FormulaComponentConditionalStatement]

    func statementDeclarations(for component: String) -> [String] {
        return conditionalStatements
            .filter { $0.parameter == component }
            .map { $0.declaration }
    }

    static func shortInstance(withConstants constants: [String: ParameterValue], resultType: Formula.ResultType) -> FormulaEvaluationAdditionalInfo {

        let metaData = FormulaEvaluationMetaData(constants: constants)
        return FormulaEvaluationAdditionalInfo(metaData: metaData,
                                               connectorValues: [ConnectorParameter: ParameterValue](),
                                               currentValue: nil,
                                               resultType: resultType,
                                               conditionalStatements: [FormulaComponentConditionalStatement]())
    }
}

class FormulaProcessor {

    enum FormulaUnit {
        case constant(String)
        case connectorValue(ConnectorParameter)
        case currentValue
        case binaryOperation(BinaryOperation)
        case subexpression(String)
        case complexComponent(String, String)
        case rawNumber(ParameterValue, String)

        var syntacticStructure: String {
            switch self {
            case .constant(let constName):
                return constName
            case .connectorValue(let connectorPath):
                return connectorPath.rawValue
            case .currentValue:
                return FormulaProcessor.currentValuePrefix
            case .binaryOperation(let binaryOperation):
                return binaryOperation.rawValue
            case .subexpression(let subexpression):
                return "(\(subexpression))"
            case .complexComponent(let componentKey, _):
                return componentKey
            case .rawNumber(_, let rawNumberText):
                return rawNumberText
            }
        }
    }

    enum StructureUnit {
        case currentValue
        case component(String)
        case binaryOperation(BinaryOperation)

        var syntacticStructure: String {
            switch self {
            case .component(let component):
                return component
            case .currentValue:
                return FormulaProcessor.currentValuePrefix
            case .binaryOperation(let binaryOperation):
                return binaryOperation.rawValue
            }
        }
    }

    // Update FormulaParser.getBinaryOperationPrefix when add new operation
    enum BinaryOperation: String {
        case plus = "+"
        case multiply = "*"
        case divide = "/"
        case minus = "-"
    }

    enum Action {
        case binaryOperation(BinaryOperation)
    }

    static let currentValuePrefix = "X(n-1)"
    static let initialValuePrefix = "X(1)"

    private static let numberRegExp = "[0-9\\.]+"
    private static let formulaComponentRegExp = "[A-Za-z\\.]+"
    private static let structureComponentRegExp = "[A-Za-z_]+"

    static func getConnectorsList(forFormula formula: String, metaData: FormulaEvaluationMetaData) -> [ConnectorParameter] {

        var connectors = [ConnectorParameter]()
        let components = FormulaProcessor.matches(for: FormulaProcessor.formulaComponentRegExp, in: formula)
        for component in components {
            if metaData.dataSources.contains(component) {
                connectors.append(ConnectorParameter(rawValue: component)!)
            }
            if let complexComponent = metaData.complexComponents[component] {
                connectors += FormulaProcessor.getConnectorsList(forFormula: complexComponent, metaData: metaData)
            }
        }
        return connectors
    }

    static func convertToDetailed(formulaStructure: String, withComponents components: [String: String]) -> String? {
        var formulaMutable = formulaStructure.replacingOccurrences(of: " ", with: "")
        let equationParts = formulaMutable.split(separator: "=")
        guard equationParts.count == 2 else { return nil }
        formulaMutable = String(equationParts.last!)
        var result = ""
        while !formulaMutable.isEmpty {
            guard let structureUnit = FormulaProcessor.getFirstStructureUnit(forFormula: formulaMutable) else {
                print("First unit for formula structure \(formulaMutable) couldn't be parsed. Probably the formula structure isn't valid")
                return nil
            }
            switch structureUnit {
            case .component(let componentKey):
                guard let componentDetailed = components[componentKey] else {
                    print("Detailed component description for \(componentKey) in formula \(formulaStructure) not found")
                    return nil
                }
                result += "(\(componentDetailed))"
            case .currentValue, .binaryOperation:
                result += structureUnit.syntacticStructure
            }
            formulaMutable = FormulaProcessor.removeFirst(unit: structureUnit, from: formulaMutable)
        }
        return result
    }

    static func evaluate(formula: String,
                         info: FormulaEvaluationAdditionalInfo,
                         parameterValuesCollector: inout FormulaParametersCollector) -> ParameterValue? {

        var formulaMutable = formula.replacingOccurrences(of: " ", with: "")

        var value = info.resultType.initialValue
        var action = Action.binaryOperation(.plus)

        while !formulaMutable.isEmpty {

            guard let formulaUnit = FormulaProcessor.getFirstUnit(forFormula: formulaMutable, metaData: info.metaData) else {
                print("First unit for formula state \(formulaMutable) couldn't be parsed. Probably the formula isn't valid")
                return nil
            }

            switch formulaUnit {
            case .binaryOperation(let binaryOperation):
                action = Action.binaryOperation(binaryOperation)
            case .subexpression(let subexpression):
                guard let subexpressionValue = FormulaProcessor.evaluate(formula: subexpression,
                                                                      info: info,
                                                                      parameterValuesCollector: &parameterValuesCollector) else {

                    print("Evaluation of subexpression \(subexpression) failed")
                    return nil
                }
                value = value.perform(action: action, withOperand: subexpressionValue)
            case .complexComponent(let formulaComponentId, let componentValue):
                guard var subexpressionValue = FormulaProcessor.evaluate(formula: componentValue,
                                                                      info: info,
                                                                      parameterValuesCollector: &parameterValuesCollector) else {
                                                                        print("Evaluation of complex component \(componentValue) failed")
                                                                        return nil
                }
                let conditionalDeclarations = info.statementDeclarations(for: formulaComponentId)
                for conditionalDeclaration in conditionalDeclarations {
                    subexpressionValue = FormulaProcessor.prepareValue(byConditionalStatement: conditionalDeclaration,
                                                                       forComponent: formulaComponentId,
                                                                       value: subexpressionValue,
                                                                       constants: info.metaData.constants)
                }
                value = value.perform(action: action, withOperand: subexpressionValue)
            default:
                guard let parameterValue = FormulaProcessor.getParameterValue(forUnit: formulaUnit,
                                                                           additionalInfo: info,
                                                                           andConnectorValues: info.connectorValues,
                                                                           basedOnCurrentValue: info.currentValue) else {
                    print("Parameter value not defiined for unit \(formulaUnit.syntacticStructure)")
                    return nil
                }
                switch formulaUnit {
                case .connectorValue, .constant:
                    parameterValuesCollector[formulaUnit.syntacticStructure] = parameterValue
                default:
                    break
                }
                value = value.perform(action: action, withOperand: parameterValue)
            }

            formulaMutable = FormulaProcessor.removeFirst(unit: formulaUnit, from: formulaMutable)
        }

        return value
    }

    static private func getFirstUnit(forFormula formula: String, metaData: FormulaEvaluationMetaData) -> FormulaUnit? {

        if let componentPrefix = FormulaProcessor.getComponentPrefix(fromFormula: formula) {
            if metaData.hasConstant(withKey: componentPrefix) {
                return FormulaUnit.constant(componentPrefix)
            }
            if metaData.hasDataSource(withKey: componentPrefix) {
                return FormulaUnit.connectorValue(ConnectorParameter(rawValue: componentPrefix)!)
            }
            if metaData.hasComplexComponent(withKey: componentPrefix) {
                return FormulaUnit.complexComponent(componentPrefix, metaData.complexComponents[componentPrefix
                    ]!)
            }
        }

        if formula.hasPrefix(FormulaProcessor.currentValuePrefix) {
            return FormulaUnit.currentValue
        }

        if let binaryOperation = FormulaProcessor.getBinaryOperationPrefix(fromFormula: formula) {
            return FormulaUnit.binaryOperation(binaryOperation)
        }

        if let subexpression = FormulaProcessor.getStartingSubexpression(fromFormula: formula) {
            return FormulaUnit.subexpression(subexpression)
        }

        if let (numberParameterValue, parameterPrefix) = FormulaProcessor.getNumberParameterPrefix(fromFormula: formula) {
            return FormulaUnit.rawNumber(numberParameterValue, parameterPrefix)
        }

        return nil
    }

    static private func getFirstStructureUnit(forFormula formula: String) -> StructureUnit? {

        if formula.hasPrefix(FormulaProcessor.currentValuePrefix) {
            return StructureUnit.currentValue
        }

        if let binaryOperation = FormulaProcessor.getBinaryOperationPrefix(fromFormula: formula) {
            return StructureUnit.binaryOperation(binaryOperation)
        }

        if let component = FormulaProcessor.getComponentPrefix(fromFormula: formula) {
            return StructureUnit.component(component)
        }

        return nil
    }

    static private func getParameterValue(forUnit formulaUnit: FormulaUnit,
                                          additionalInfo: FormulaEvaluationAdditionalInfo,
                                          andConnectorValues connectorValues: [ConnectorParameter: ParameterValue],
                                          basedOnCurrentValue currentValue: ParameterValue?) -> ParameterValue? {

        var value: ParameterValue?
        var formulaComponentId: String?

        switch formulaUnit {
        case .constant(let constName):
            if let constantValue = additionalInfo.metaData.constants[constName] {
                value = resolveConstantValueIfNeeded(parameterValue: constantValue,
                                                     constants: additionalInfo.metaData.constants)
                formulaComponentId = constName
            }
        case .connectorValue(let connectorParameter):
            if let connectorValue = connectorValues[connectorParameter] {
                value = connectorValue
                formulaComponentId = connectorParameter.rawValue
            }
        case .currentValue:
            if let currentValue = currentValue {
                value = currentValue
            }
        case .rawNumber(let parameterValue, _):
            value = parameterValue
        case .subexpression,
             .binaryOperation,
             .complexComponent:
            break
        }

        if let strongValue = value,
            let formulaComponentId = formulaComponentId {
            let conditionalDeclarations = additionalInfo.statementDeclarations(for: formulaComponentId)
            for conditionalDeclaration in conditionalDeclarations {
                value = FormulaProcessor.prepareValue(byConditionalStatement: conditionalDeclaration,
                                                      forComponent: formulaComponentId,
                                                      value: strongValue,
                                                      constants: additionalInfo.metaData.constants)
            }
        }

        return value
    }

    static private func resolveConstantValueIfNeeded(parameterValue: ParameterValue,
                                                     constants: [String: ParameterValue]) -> ParameterValue {
        guard case let .string(stringValue) = parameterValue,
            let constantValue = constants[stringValue] else { return parameterValue }
        return resolveConstantValueIfNeeded(parameterValue: constantValue, constants: constants)
    }

    static private func prepareValue(byConditionalStatement statement: String,
                                     forComponent componentId: String,
                                     value: ParameterValue,
                                     constants: [String: ParameterValue]) -> ParameterValue {
        let preparedValue = FormulaProcessor.prepareValueIfThenIfNeeded(byConditionalStatement: statement,
                                                                        forComponent: componentId,
                                                                        value: value,
                                                                        constants: constants)
        return preparedValue
    }

    static private func prepareValueIfThenIfNeeded(byConditionalStatement statement: String,
                                                   forComponent componentId: String,
                                                   value: ParameterValue,
                                                   constants: [String: ParameterValue]) -> ParameterValue {
        let statementParts = statement.split(separator: " ")
        guard statementParts.count == 4 else { return value }
        guard statementParts[0] == "IF", statementParts[2] == "THEN" else { return value }

        var condition = String(statementParts[1])
        guard condition.starts(with: componentId) else { return value }
        condition = condition.replaceFirst(of: componentId, with: "")
        guard let conditionSign = condition.first else { return value }
        condition = condition.replaceFirst(of: String(conditionSign), with: "")
        let conditionValue = condition
        if !value.isComparisonCorrect(comparisonSign: conditionSign, secondValue: conditionValue) {
            return value
        }

        var instructions = String(statementParts[3])
        guard instructions.starts(with: componentId) else { return value }
        instructions = instructions.replaceFirst(of: componentId, with: "")
        guard let equalitySign = instructions.first, equalitySign == "=" else { return value }
        instructions = instructions.replaceFirst(of: String(equalitySign), with: "")
        let newValue = instructions
        let parameterValue = value.getClone(withValue: newValue)

        return resolveConstantValueIfNeeded(parameterValue: parameterValue, constants: constants)
    }

    static private func removeFirst(unit: FormulaUnit, from formula: String) -> String {
        return formula.replaceFirst(of: unit.syntacticStructure, with: "")
    }

    static private func removeFirst(unit: StructureUnit, from formula: String) -> String {
        return formula.replaceFirst(of: unit.syntacticStructure, with: "")
    }

    static private func getBinaryOperationPrefix(fromFormula formula: String) -> FormulaProcessor.BinaryOperation? {

        let (prefixFound, prefix) = formula.hasPrefix(among: [
            FormulaProcessor.BinaryOperation.plus.rawValue,
            FormulaProcessor.BinaryOperation.multiply.rawValue,
            FormulaProcessor.BinaryOperation.minus.rawValue,
            FormulaProcessor.BinaryOperation.divide.rawValue
            ])
        if prefixFound, let prefix = prefix {
            return FormulaProcessor.BinaryOperation(rawValue: prefix)
        }

        return nil
    }

    static private func getFormulaComponentPrefix(fromFormula formula: String) -> String? {
        guard let componentPrefixValue = FormulaProcessor.startingMatch(for: FormulaProcessor.formulaComponentRegExp, in: formula) else {
                return nil
        }
        return componentPrefixValue
    }

    static private func getNumberParameterPrefix(fromFormula formula: String) -> (ParameterValue, String)? {

        guard let numberPrefixValue = FormulaProcessor.startingMatch(for: FormulaProcessor.numberRegExp, in: formula),
            let doubleValue = Double(numberPrefixValue) else {
            return nil
        }

        return (ParameterValue.double(doubleValue), numberPrefixValue)
    }

    static private func getComponentPrefix(fromFormula formula: String) -> String? {

        guard let componentPrefixValue = FormulaProcessor.startingMatch(for: FormulaProcessor.structureComponentRegExp, in: formula) else {
                return nil
        }
        return componentPrefixValue
    }

    static private func getStartingSubexpression(fromFormula formula: String) -> String? {

        if !FormulaProcessor.isStartingWithBrace(formula: formula) {
            return nil
        }

        var currentBracesBalance = 0
        let startIndex = formula.index(after: formula.startIndex)
        for characterIndex in formula.indices {
            let character = formula[characterIndex]
            if character == "(" {
                currentBracesBalance += 1
                continue
            }
            if character == ")" {
                currentBracesBalance -= 1
                if currentBracesBalance != 0 {
                    continue
                }
                let endIndex = formula.index(before: characterIndex)
                return String(formula[startIndex...endIndex])
            }
        }

        print("End of braced subexpression not found. Probably formula \(formula) is not valid")
        return nil
    }

    static private func isStartingWithBrace(formula: String) -> Bool {
        return formula.hasPrefix("(")
    }

}

// MARK: Helpers
extension FormulaProcessor {

    static private func matches(for regex: String, in text: String) -> [String] {

        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }

    static private func startingMatch(for regex: String, in text: String) -> String? {

        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            for result in results where result.range.contains(0) {
                return String(text[Range(result.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return nil
        }

        return nil
    }

}
