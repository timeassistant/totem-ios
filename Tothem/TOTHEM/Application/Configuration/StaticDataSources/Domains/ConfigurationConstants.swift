//
//  ConfigurationConstants.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

class ConfigurationConstants {
    private let storage = ParametersDataSource.constants
    static let shared = ConfigurationConstants()

    var constants = Variable([String: ParameterValue]())
    var constantsReplay: Observable<[String: ParameterValue]>
    private let disposeBag = DisposeBag()
    
    private init() {
        constantsReplay = constants.asObservable().replay(1)
        storage.rx_array
            .map({ [weak self] in self?.map($0) })
            .skipNil()
            .bind(to: constants)
            .disposed(by: disposeBag)
    }
    
    private func map(_ array: JSONArray) -> [String: ParameterValue] {
        var constants: [String: ParameterValue] = [:]
        for item in array {
            for (_, value) in item {
                guard let json = value as? JSONObject,
                    let key = json["name"] as? String else { continue }
                constants[key] = ParameterValue(json: json)
            }
        }
        return constants
    }

}
