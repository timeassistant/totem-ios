//
//  FormulaEvaluationMetaData.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct FormulaEvaluationMetaData {
    var constants: [String: ParameterValue]
    var complexComponents: [String: String]
    var dataSources: [String]

    init(constants: [String: ParameterValue], complexComponents: [String: String], dataSources: [String]) {
        self.constants = constants
        self.complexComponents = complexComponents
        self.dataSources = dataSources
    }

    init(constants: [String: ParameterValue]) {
        self.constants = constants
        self.complexComponents = [String: String]()
        self.dataSources = [String]()
    }

    func hasConstant(withKey key: String) -> Bool {
        return (self.constants[key] != nil)
    }

    func hasDataSource(withKey key: String) -> Bool {
        return self.dataSources.contains(key)
    }

    func hasComplexComponent(withKey key: String) -> Bool {
        return (self.complexComponents[key] != nil)
    }
}

class FormulaEvaluationMetaDataManager {

    static let shared = FormulaEvaluationMetaDataManager()

    private var constants: Variable<[String: ParameterValue]>
    private var complexComponents: Variable<[String: String]>
    private var dataSources: Variable<[String]>

    var data: Observable<FormulaEvaluationMetaData>

    private init() {
        self.constants = ConfigurationConstants.shared.constants
        self.complexComponents = ComplexFormulaComponents.shared.components
        self.dataSources = Variable(ConnectorParameter.dataSourcesList)

        self.data = Observable.combineLatest(self.constants.asObservable(),
            self.complexComponents.asObservable(), self.dataSources.asObservable())
            .map {
                FormulaEvaluationMetaData(constants: $0, complexComponents: $1, dataSources: $2)
        }
    }

}
