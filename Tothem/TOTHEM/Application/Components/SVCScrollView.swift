//
//  SVCScrollView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class SVCScrollView: UIScrollView, UIScrollViewDelegate {

    private weak var parentViewController: UIViewController!

    private var controllersToShowIntoScrollView: [UIViewController] = []
    var visibleVC = 0

    var controllersCount: Int {
        return self.controllersToShowIntoScrollView.count
    }

    var didScrollPercent: ((CGFloat) -> Void)?

    enum FrameSetter {
        case equalToView(view: UIView)
        case equalToWindow
    }

    init(frame: FrameSetter, parentViewController: UIViewController, controllers toShow: [UIViewController]) {
        switch frame {
        case .equalToView(view: let view):
            super.init(frame: view.bounds)
        case .equalToWindow:
            super.init(frame: UIScreen.main.bounds)
        }

        self.parentViewController = parentViewController
        self.controllersToShowIntoScrollView = toShow
        self.addScrollView(viewControllers: toShow)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var screenSize: CGSize {
        let screenBounds = UIScreen.main.bounds

        let width = screenBounds.width
        let height = screenBounds.height

        return CGSize(width: width, height: height)
    }

    private func addScrollView(viewControllers: [UIViewController]) {

        self.showsHorizontalScrollIndicator = false
        self.bounces = false

        self.contentSize = CGSize(width: self.screenSize.width * CGFloat(viewControllers.count),
                                  height: self.screenSize.height)
        self.isPagingEnabled = true
        self.delegate = self

        viewControllers.enumerated().forEach { index, viewController in
            self.parentViewController.addChild(viewController)
            let originX = CGFloat(index) * self.screenSize.width
            viewController.view.frame = CGRect(x: originX,
                                               y: 0, width: self.screenSize.width,
                                               height: self.screenSize.height)
            self.addSubview(viewController.view)

            viewController.didMove(toParent: self.parentViewController)
        }
        self.controllersToShowIntoScrollView.forEach { $0.view.backgroundColor = $0.view.backgroundColor?.withAlphaComponent(0) }
        self.parentViewController.view.backgroundColor = self.controllersToShowIntoScrollView.first?.view.backgroundColor?.withAlphaComponent(1)
    }

    enum StepType {
        case next
        case previous
    }

    @discardableResult
    func makeStep(_ direction: StepType) -> UIViewController? {
        switch direction {
        case .next:
            guard self.visibleVC < self.controllersToShowIntoScrollView.count else { return nil }
            self.visibleVC += 1
        case .previous:
            guard self.visibleVC > 0 else { return nil }
            self.visibleVC -= 1
        }
        self.scrollToVC(number: self.visibleVC)
        UIView.animate(CATransaction.animationDuration(), animations: {
            self.parentViewController.view.backgroundColor = self.controllersToShowIntoScrollView[self.visibleVC]
				.view.backgroundColor?.withAlphaComponent(1)
        })
        return self.controllersToShowIntoScrollView[self.visibleVC]
    }

    func controller(direction: StepType) -> UIViewController? {
        switch direction {
        case .next:
            guard (self.visibleVC + 1) <= self.controllersToShowIntoScrollView.count else { return nil }
            return self.controllersToShowIntoScrollView[self.visibleVC + 1]
        case .previous:
            guard (self.visibleVC - 1) >= 0 else { return nil }
            return self.controllersToShowIntoScrollView[self.visibleVC - 1]
        }
    }

    func scrollToVC(number viewController: Int, animate: Bool = true) {
        guard viewController <= (controllersToShowIntoScrollView.count - 1) else {
            print("Sorry, but you cant slide view controller which isnt exist")
            return
        }
        self.controllersToShowIntoScrollView[visibleVC].view.endEditing(true)
        self.visibleVC = viewController
        self.setContentOffset(CGPoint(x: self.screenSize.width * CGFloat(viewController), y: 0), animated: animate)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let percentScrolled = scrollView.contentOffset.x / screenSize.width
        self.controllersToShowIntoScrollView[visibleVC].view.endEditing(true)
        self.visibleVC = Int(percentScrolled)
        self.didScrollPercent?(percentScrolled)
    }
}
