//
//  ConfiguredButton.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class ConfiguredButton: UIButton {
	var circle: UIView!

	override init(frame: CGRect) {
		super.init(frame: frame)
        self.clipsToBounds = false
        self.translatesAutoresizingMaskIntoConstraints = false
		self.setupCircle()
		self.layer.masksToBounds = true
	}

	override func layoutSubviews() {
		super.layoutSubviews()
        self.layer.cornerRadius = bounds.width / 2
		self.circle.frame = CGRect(x: self.bounds.width/4,
								   y: self.bounds.height/4,
								   width: self.bounds.width/2,
								   height: self.bounds.height/2)
		self.circle.layer.cornerRadius = self.circle.frame.height/2
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupCircle() {
		self.circle = UIView()
		self.circle.layer.borderColor = UIColor.white.cgColor
		self.circle.layer.borderWidth = 1
		self.circle.isUserInteractionEnabled = false

		self.addSubview(self.circle)
        self.bringSubviewToFront(self.circle)
	}
}
