//
//  CreateButton.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CreateButton: ConfiguredButton {
    private var width: NSLayoutConstraint?
    private var height: NSLayoutConstraint?
    private var presenter = CreateButtonPresenter()
    private var disposeBag = DisposeBag()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        width = constraintWidth(constant: CGFloat(kCreateButtonSize))
        height = constraintHeight(constant: CGFloat(kCreateButtonSize))
        circle.isHidden = true
        presenter.state?
            .throttle(0.3, scheduler: MainScheduler.asyncInstance)
            .do(onNext: setup)
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func setup(with model: CreateButtonParametersModel) {
        let scale = CGFloat(model.size) / CGFloat(kCreateButtonSize)
        DispatchQueue.main.async {
            UIView.animate(model.animationDuration) {
                self.alpha = model.hidden ? 0 : 1
                self.backgroundColor = model.color
                self.layer.borderWidth = CGFloat(model.borderWidth)
                self.layer.borderColor = model.borderColor.cgColor
                self.transform = CGAffineTransform(scaleX: scale, y: scale)
            }
        }
    }
}
