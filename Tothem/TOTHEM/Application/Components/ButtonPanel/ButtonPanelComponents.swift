//
//  ButtonPanelComponents.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum ComponentEventType: String {

        case touchInside
        case longPress
        case didChangePosition
        case buttonPanGesture

        case unknown

    static var key: String {
        return "event"
    }
}

struct ButtonPanelComponent<T> where T: NSObject {
    typealias ComponentType = T
    private let notifCenter = NotificationCenter.default

    var component: ComponentType

    init() {
        self.component = ComponentType.init()
    }

    func setup(_ onSetup: (ComponentType) -> Void) {
        onSetup(self.component)
    }

    // swiftlint:disable discarded_notification_center_observer
    func observe(onModule: NSObject.Type, _ onCallback: @escaping ((ComponentEventType) -> Void)) {
		self.notifCenter
            .addObserver(forName:
                Notification.Name(String(self.component.hashValue)),
                        object: nil,
                         queue: OperationQueue.current,
                         using: { notification in
                            notification.object
                                .flatMap {$0 as? NSObject}
                                .map { ($0, onModule as AnyClass) }
                                .map { $0.0.isKind(of: $0.1) ? (onCallback(ComponentEventType(rawValue: notification.userInfo?[ComponentEventType.key] as? String ?? "") ?? .unknown)) : () }
        })
    }

    func postNotification(object: ButtonPanelModuleBehaviour, event: ComponentEventType) {
        let notif = Notification(name: Notification.Name(String(self.component.hashValue)),
                                 object: object,
                                 userInfo: [ComponentEventType.key: event.rawValue])
        self.notifCenter.post(notif)
    }
}

struct Components {

    indirect enum ComponentsElements {
        case createButton(value: RelatedComponent?)
        case scaleButton(value: RelatedComponent?)
        case timeLabel(value: RelatedComponent?)
        case buttonPanel
    }

    struct RelatedComponent {
        var component: ComponentsElements
        var value: Any
    }

    weak var parentView: UIView!

    var panGesture = ButtonPanelComponent<UIPanGestureRecognizer>()
    var createButton = ButtonPanelComponent<CreateButton>()

    var profileContainerView = ButtonPanelComponent<UIView>()
    var profileImageView = ButtonPanelComponent<UIImageView>()

    var horizontalLine = ButtonPanelComponent<UIView>()

	var dispose = DisposeBag()

    init(parentView: UIView) {
        self.parentView = parentView
        self.prepare()
    }

    mutating func prepare() {
        self.initialSetup()
    }

    mutating func setupPan(with selector: Selector, target: UIView) {
        self.panGesture.setup { pan in
            pan.addTarget(target, action: selector)
        }
    }

    private mutating func initialSetup() {
        self.setupHorizontalLine()
        self.setupCreateButton()
        let currentBundle = BundleIdentifier(bundle: Bundle.main)
        #if DEBUG
        setupProfileView()
        #else
        if currentBundle == .branch || currentBundle == .master {
            setupProfileView()
        }
        #endif
    }

    private func setupCreateButton() {
        self.createButton.setup { createButton in
            self.parentView.addSubview(createButton)
            createButton.constraintCenter(x: parentView)
            createButton.constraintCenter(y: parentView)
        }
    }

    private func setupProfileView() {
        self.profileContainerView.setup { profileCV in
            let profileViewSideSize = CGFloat(kCreateButtonSize)
            profileCV.translatesAutoresizingMaskIntoConstraints = false
            profileCV.layer.cornerRadius = profileViewSideSize/2
            profileCV.clipsToBounds = true

//            ConfigurationFacade.shared
//                .buttonPanelCenterModuleConstants
//                .getButtonCenterUpColorValue(.profileButton)
//                .subscribe(onNext: {
//                    profileCV.backgroundColor = $0
//                })
//                .disposed(by: dispose)
//
//            ConfigurationFacade.shared
//                .buttonPanelCenterModuleConstants
//                .getButtonCenterUpBorderWidthValue(.profileButton)
//                .subscribe(onNext: {
//                    profileCV.layer.borderWidth = $0
//                }).disposed(by: dispose)
//
//            ConfigurationFacade.shared
//                .buttonPanelCenterModuleConstants
//                .getButtonCenterUpBorderColorValue(.profileButton)
//                .subscribe(onNext: {
//                    profileCV.layer.borderColor = $0
//                })
//                .disposed(by: dispose)

            setupImageView(parentView: profileCV)

            parentView.addSubview(profileCV)
            parentView.bringSubviewToFront(profileCV)

            profileCV.constraintCenter(x: parentView, constant: 90)
            profileCV.constraintCenter(y: parentView)

            profileCV.constraintWidth(constant: profileViewSideSize)
            profileCV.constraintHeight(constant: profileViewSideSize)

            VisibilityRulesService.shared.visibility(for: .createButton)
                .startWith(true)
                .map { !$0 }
                .bind(to: profileCV.rx.isHidden)
                .disposed(by: dispose)
        }
    }

    private func setupImageView(parentView: UIView) {
        profileImageView.setup {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.contentMode = .scaleAspectFill
            parentView.addSubview($0)
            $0.image = UIImage(named: "backgroundView")

            $0.constraintCenter(x: parentView)
            $0.constraintCenter(y: parentView)
            $0.constraintWidth(to: parentView)
            $0.constraintHeight(to: parentView)
        }
    }

    private func setupHorizontalLine() {
        horizontalLine.setup {
            $0.translatesAutoresizingMaskIntoConstraints = false
            parentView.addSubview($0)

            $0.constraintCenter(x: parentView)
            $0.constraintCenter(y: parentView)
            $0.constraintWidth(to: parentView)
            $0.constraintHeight(constant: CGFloat(kCreateButtonLineHeight))
            $0.backgroundColor = .green

            VisibilityRulesService.shared.visibility(for: .createButton)
                .startWith(true)
                .map { !$0 }
                .bind(to: $0.rx.isHidden)
                .disposed(by: dispose)
        }
    }
}

func == (lhs: Components.RelatedComponent?, rhs: Components.RelatedComponent?) -> Bool {
    guard let lhsV = lhs, let rhsV = rhs else {
        switch(lhs, rhs) {
        case (nil, nil): return true
        default: return false
        }

    }
    return lhsV.component == rhsV.component
}

func == (lhs: Components.ComponentsElements?, rhs: Components.ComponentsElements?) -> Bool {
    guard let lhsV = lhs, let rhsV = rhs else {
		switch(lhs, rhs) {
		case (nil, nil):
			return true
		default:
			return false
		}
    }
    switch (lhsV, rhsV) {
    case (.createButton(let lhsValue), .createButton(let rhsValue)) where lhsValue == rhsValue:
        return true
    case (.scaleButton, .scaleButton):
        return true
    case (.timeLabel(let lhsValue), .timeLabel(let rhsValue)) where lhsValue == rhsValue:
        return true
    case (.buttonPanel, .buttonPanel):
        return true
    default:
        return false
    }
}
