//
//  ButtonPanel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

protocol ButtonPanelDelegate: class {
    func buttonPanelBeganPanning(gesture: UIPanGestureRecognizer)
    func buttonPanelChangedPanning(gesture: UIPanGestureRecognizer)
    func buttonPanelEndedPanning(gesture: UIPanGestureRecognizer)
    func didInteract(with event: ComponentEventType, componentType: Components.ComponentsElements)
}

final class ButtonPanel: UIView, ButtonPanelAppearance {
    var disposeBag = DisposeBag()
   weak var delegate: ButtonPanelDelegate?

    // Components (UIView) of button panel
    var components: Components!

    //Stored Property
    var isPanning: Bool = false

    /// INITS
    init() {
        super.init(frame: UIScreen.main.bounds)
        self.prepare()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.prepare()
    }

    /// Prepare button panel when init
    private func prepare() {
        translatesAutoresizingMaskIntoConstraints = false
        self.setupComponents()
        self.addPanGesture()
    }

    // MARK: - Setup methods

    fileprivate func setupComponents() {
        self.components = Components(parentView: self)
        self.observeCompronents()
    }

    fileprivate func addPanGesture() {
        self.components.setupPan(with: #selector(ButtonPanel.pan(gesture:)), target: self)
        self.gestureRecognizers?.forEach { self.removeGestureRecognizer($0) }
        self.addGestureRecognizer(self.components.panGesture.component)
    }

    fileprivate func observeCompronents() {
        let createButton = self.components.createButton
        createButton.component.rx.tap.do(onNext: {
            self.delegate?.didInteract(with: .touchInside, componentType: .createButton(value: nil))
        })
        .subscribe()
        .disposed(by: disposeBag)
    }

    // Button Punel under touching
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden &&
                subview.alpha > 0 &&
                subview.isUserInteractionEnabled &&
                subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }

    // MARK: - ButtonPanel gesture handling
    @objc func pan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.isPanning = true
            self.delegate?.buttonPanelBeganPanning(gesture: gesture)
        case .changed:
            self.delegate?.buttonPanelChangedPanning(gesture: gesture)
        case .ended:
            self.isPanning = false
            self.delegate?.buttonPanelEndedPanning(gesture: gesture)
        default: break
        }
    }
}

// MARK: - Mudules of current button panel, can be expanded by adding a new.
// To extend, update 'moduleAssociatedType', 'returnType' methods
// ButtonPanelModule has convinienced init of appearance.
extension ButtonPanel {
     enum Modules {

        // All Types(Classes) of existing modules
		// swiftlint:disable nesting
        struct ModulesTypes {
            typealias TimelineClassType = ButtonPanelModule
            typealias ChatClassType = ButtonPanelModule
        }

        case timeline
        case chat

        func object(_ appearance: ButtonPanelAppearance, side: TimesetType) -> ButtonPanelModuleBehaviour {
            switch self {
            case .chat:
                return Modules.ModulesTypes.ChatClassType(appearance, side: side)
            case .timeline:
                return Modules.ModulesTypes.TimelineClassType(appearance, side: side)
            }
        }

        var type: NSObject.Type {
            switch self {
            case .chat:
                return Modules.chatType
            case .timeline:
                return Modules.timelineType
            }
        }

		// Modules Types
        static var timelineType: ModulesTypes.TimelineClassType.Type {
            return Modules.ModulesTypes.TimelineClassType.self
        }

        static var chatType: ModulesTypes.ChatClassType.Type {
            return Modules.ModulesTypes.ChatClassType.self
        }
    }
}
