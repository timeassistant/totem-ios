//
//  ButtonPanelModuleProtocol.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol ButtonPanelModuleBehaviour {
    var frame: CGRect { get set }
    var side: TimesetType { get set }
    var appearance: ButtonPanelAppearance! { get set }
    var isHidden: Bool { get set }
    func updateAppearence()
    init()
}

extension ButtonPanelModuleBehaviour {

    func casted<S: NSObject>(_ type: S.Type, value: ((S) -> Void)) {
        guard let castedValue = self as? S else {
            return
        }
        value(castedValue)
    }

    func casted<S: NSObject>(_ type: S.Type) -> S? {
        return self as? S
    }

    init(_ appearance: ButtonPanelAppearance, side: TimesetType) {
        self.init()
        self.side = side
        self.appearance = appearance
    }
}
