//
//  ButtonPanelModule.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class ButtonPanelModule: NSObject, ButtonPanelModuleBehaviour {

	var dispose = DisposeBag()
    var profileButtonAlpha: CGFloat = 1
    var createButtonAlpha: CGFloat = 1
    var createButtonBorderColor: UIColor = .clear
    required override init() { }

    var frame: CGRect = UIScreen.main.bounds

    var appearance: ButtonPanelAppearance!
    var side: TimesetType = UserClassService.objectDs

    func updateAppearence() {
        var constants: ButtonPanelModuleConstants!

        ConfigurationFacade.shared
            .taskModuleConstants
            .getTaskBackgroundColorValue(side)
            .subscribe(onNext: { [weak self] in
                self?.appearance
                    .components
                    .horizontalLine
                    .component.backgroundColor = $0
            })
            .disposed(by: self.dispose)

        switch side {
        case .center: constants = ConfigurationFacade.shared
            .buttonPanelCenterModuleConstants
        case .left: constants = ConfigurationFacade.shared
            .buttonPanelLeftModuleConstants
        case .right: constants = ConfigurationFacade.shared
            .buttonPanelRightModuleConstants
        }

        let duration = constants.transitionAnimationDuration(.createButton)

        constants.scale(.profileButton)
            .subscribe(onNext: {
                let num = CGFloat($0 / kCreateButtonMaxSize)
                let transform = CGAffineTransform(scaleX: num, y: num)
                UIView.animate(withDuration: duration, animations: {
                    self.appearance
                        .components
                        .profileContainerView
                        .component.transform = transform
                })
            })
            .disposed(by: dispose)

        constants.borderColor(.profileButton)
            .subscribe(onNext: {
                self.appearance
                    .components
                    .profileContainerView
                    .component.layer.borderColor = $0.cgColor
            })
            .disposed(by: dispose)
    }

    var isHidden: Bool = false {
        didSet {
            [self.appearance.components.createButton.component]
                .forEach { $0.isUserInteractionEnabled = !self.isHidden }

            UIView.animate(CATransaction.animationDuration()) {
                self.appearance.components.horizontalLine.component.alpha = self.isHidden ? 0 : 1

                self.appearance.components
                    .profileContainerView.component.alpha = self.isHidden ? 0 : self.profileButtonAlpha
            }
        }
    }
}
