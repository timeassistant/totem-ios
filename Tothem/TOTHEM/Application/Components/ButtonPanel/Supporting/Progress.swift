//
//  Progress.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

/// **Progress** is a struct which is related with value from 0 -> 1. Can be changed. Using for transfare value of element's progress.
struct Progress {
    typealias ProgressValue = CGFloat

    var value: ProgressValue

    // Mirror reversing value
    var reversedValue: ProgressValue {
        return Progress.max - self.value
    }

    init(value: ProgressValue, constraints: Bool = true) {
        self.value = constraints ? (value > Progress.max) ? Progress.max : ((value < Progress.min) ? Progress.min : value) : value
    }

    static var max: ProgressValue {
        return 1.0
    }

    static var min: ProgressValue {
        return 0.0
    }

    static var mid: ProgressValue {
        return self.max / 2
    }

    static var random: ProgressValue {
        return ProgressValue(arc4random_uniform(UInt32(Progress.max - Progress.min))) + Progress.min
    }
}
