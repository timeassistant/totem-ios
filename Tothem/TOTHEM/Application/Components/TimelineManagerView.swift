//
//  TimelineManagerView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class TimelineManagerView: UIView, TimelineProssessorProtocol {
    var engineService: EngineTimelineService!

    let centerStack: TimelineStack

    let leftStack: TimelineStack
    let rightStack: TimelineStack

    var dispose = DisposeBag()

    var activeListViewController: ListViewController {
        switch engineService.stateService.currentState {
        case .center: return centerStack.controller
        case .right: return rightStack.controller
        case .left: return leftStack.controller
        }
    }
    
    init(frame: CGRect, state: TimesetType) {
        var controller = TasksViewController()
        controller.presenter = ListPresenter(side: .right)
        var collection = CollectionWireframe.createRight()
        controller.collectionViewController = collection
        self.rightStack = (controller: controller, collection: collection, cover: UIView())

        controller = TasksViewController()
        controller.presenter = ListPresenter(side: .center)
        collection = CollectionWireframe.createCenter()
        controller.collectionViewController = collection
        self.centerStack = (controller: controller, collection: collection, cover: UIView())

        controller = TasksViewController()
        controller.presenter = ListPresenter(side: .left)
        collection = CollectionWireframe.createLeft()
        controller.collectionViewController = collection
        self.leftStack = (controller: controller, collection: collection, cover: UIView())

        super.init(frame: frame)
        self.prepare(state: state)
        self.prepareView()
        self.observeCollectionViewChanging()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func prepare(state: TimesetType) {
        self.prepareServices(state: state)
    }

    private func prepareServices(state: TimesetType) {
        self.engineService = EngineTimelineService(prossessor: self, stateService: StateTimelineService.shared)

        self.prepareView()

        self.engineService.stateService.state
            .asObservable()
            .subscribeOn(MainScheduler.asyncInstance)
            .do(onNext: setupOn)
            .subscribe()
            .disposed(by: dispose)
    }

    private func setupOn(state: TimesetType) {
        switch state {
        case .left:
            self.setupOnLeftState()
        case .right:
            self.setupOnRightState()
        default: return
        }
    }

    private func setupOnLeftState() {
        ConfigurationFacade.shared
            .buttonPanelConstants
            .getShadowLayerContrastColorValue(.left)
            .subscribe(onNext: {
                self.centerStack.cover.backgroundColor = $0
            })
            .disposed(by: self.dispose)
    }

    private func setupOnRightState() {
        ConfigurationFacade.shared
            .buttonPanelConstants
            .getShadowLayerContrastColorValue(.right)
            .subscribe(onNext: {
                self.centerStack.cover.backgroundColor = $0
            })
            .disposed(by: self.dispose)
    }

    private func observeCollectionViewChanging() {
        TasksPreviewDataManager.shared
            .selected
            .map({ $0?.fromFrame != nil && $0?.fromView != nil })
            .bind(to: self.engineService.stateService.preview)
            .disposed(by: self.dispose)
    }

    fileprivate func prepareView() {
        self.backgroundColor = .clear
    }
}
