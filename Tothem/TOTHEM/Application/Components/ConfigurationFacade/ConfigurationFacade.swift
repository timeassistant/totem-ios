//
//  ConfigurationFacade.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift

class ConfigurationFacade {

	static let shared = ConfigurationFacade()

    lazy var buttonPanelCenterModuleConstants: ButtonPanelModuleConstants = ButtonPanelCenterModuleConstants()
	lazy var buttonPanelLeftModuleConstants: ButtonPanelModuleConstants = ButtonPanelLeftModuleConstants()
	lazy var buttonPanelRightModuleConstants: ButtonPanelModuleConstants = ButtonPanelRightModuleConstants()

	lazy var taskModuleConstants = TaskModuleConstants()
    lazy var backgroundConstants = BackgroundConstants()
	lazy var buttonPanelConstants = ButtonPanelConstants()
	lazy var collectionViewLayout = CollectionViewLayoutParameters()
	lazy var hapticParameters = HapticParameters()
	var parameters = BehaviorSubject<[Parameter]>(value: [])
    var parametersRestrictions = Variable<[ParameterRestriction]>([])
}
