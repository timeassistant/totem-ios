//
//  KeysPairs.swift
//  TOTHEM
//
//  Created by Yaroslav Khodorovskyi on 10.05.2018.
//  Copyright © 2018 r00t. All rights reserved.
//

import Foundation

class KeysPairs {

	//swiftlint:disable identifier_name
	enum ValueForKeyPair {
		case up_createButton_size
		case down_createButton_size
		case left_createButton_size
		case right_createButton_size

		case up_scaleButton_size
		case down_scaleButton_size
		case left_scaleButton_size
		case right_scaleButton_size
	}

	func getValue(for key: ValueForKeyPair) -> String {
		switch key {
		case .up_createButton_size: return "up_createButton_size"
		case .down_createButton_size: return "down_createButton_size"
		case .left_createButton_size: return "left_createButton_size"
		case .right_createButton_size: return "right_createButton_size"

		case .up_scaleButton_size: return "up_scaleButton_size"
		case .down_scaleButton_size: return "down_scaleButton_size"
		case .left_scaleButton_size: return "left_scaleButton_size"
		case .right_scaleButton_size: return "right_scaleButton_size"
		}
	}
}
