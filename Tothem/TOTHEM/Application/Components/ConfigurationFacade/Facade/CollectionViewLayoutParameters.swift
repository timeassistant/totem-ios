//
//  CollectionViewLayoutParameters.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift

class CollectionViewLayoutParameters {
	enum ModuleType {
        case none
		case down
		case up
		case left
		case right
	}

    func insets(side: TimesetType) -> Observable<UIEdgeInsets> {
        switch side {
        case .center:
            return Observable.just(UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
        case .right:
            return Observable.just(UIEdgeInsets(top: 0, left: shift(), bottom: 0, right: 0))
        case .left:
            return Observable.just(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: shift()))
        }
    }

    func maxCellHeight(in module: ModuleType, reordering: Bool) -> CGFloat {
        switch module {
        case .down: return reordering ? 40 : 70
        case .up: return reordering ? 40 : 70
        case .left: return reordering ? 40 : 70
        case .right: return reordering ? 40 : 70
        default: return 0
        }
    }

    func minCellHeight(in module: ModuleType, reordering: Bool) -> CGFloat {
        switch module {
        case .down: return reordering ? 30 : 50
        case .up: return reordering ? 30 : 50
        case .left: return reordering ? 30 : 50
        case .right: return reordering ? 30 : 50
        default: return 0
        }
    }

	func cellLeftInset(in module: ModuleType, selected: Bool) -> CGFloat {
        switch module {
        case .down: return selected ? 0 : 30
        case .up: return selected ? 0 : 30
        case .left: return 0
        case .right: return selected ? 0 : shift()
        default: return 0
        }
	}

    func cellRightInset(in module: ModuleType, selected: Bool) -> CGFloat {
        switch module {
        case .down: return selected ? 0 : 30
        case .up: return selected ? 0 : 30
        case .left: return selected ? 0 : shift()
        case .right: return 0
        default: return 0
        }
    }

    func topSpace(in module: ModuleType, selected: Bool, reordering: Bool) -> CGFloat {
        switch module {
        case .down: return reordering ? 40 : selected ? 20 : 10
        case .up: return reordering ? 40 : selected ? 20 : 10
        case .left: return reordering ? 40 : selected ? 20 : 0
        case .right: return reordering ? 40 : selected ? 20 : 0
        default: return 0
        }
    }

    func shift() -> CGFloat {
        return 40 //*0.3/2
    }
}
