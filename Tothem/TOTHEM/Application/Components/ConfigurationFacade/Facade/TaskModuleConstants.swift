//
//  TaskModuleConstants.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift

// swiftlint:disable type_body_length
class TaskModuleConstants {
    func color(with colorArray: [CGFloat]) -> UIColor {
        return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
    }

    func unactiveColor(with colorArray: [CGFloat]) -> UIColor {
        return UIColor(red: max(0, colorArray[0]/255.0 - 0.2), green: max(0, colorArray[1]/255.0 - 0.2), blue: max(0, colorArray[2]/255.0 - 0.2), alpha: colorArray[3])
    }

	func getTaskBackgroundColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskBackgroundColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskBackgroundColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskBackgroundColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		}
	}

    func getTaskUnactiveBackgroundColorValue(_ component: TimesetType) -> Observable<UIColor> {
        switch component {
        case .center:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeTaskBackgroundColor } }.skipNil().map {
                let array = $0.value.components(separatedBy: "/")
                var colorArray = [CGFloat]()
                for color in array {
                    let value = CGFloat(Double(color)!)
                    colorArray.append(value)
                }
                return self.unactiveColor(with: colorArray)
            }
        case .left:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskBackgroundColor } }.skipNil().map {
                let array = $0.value.components(separatedBy: "/")
                var colorArray = [CGFloat]()
                for color in array {
                    let value = CGFloat(Double(color)!)
                    colorArray.append(value)
                }
                return self.unactiveColor(with: colorArray)
            }
        case .right:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskBackgroundColor } }.skipNil().map {
                let array = $0.value.components(separatedBy: "/")
                var colorArray = [CGFloat]()
                for color in array {
                    let value = CGFloat(Double(color)!)
                    colorArray.append(value)
                }
                return self.unactiveColor(with: colorArray)
            }
        }
    }

	func getTaskTitleLabelColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared
                .parameters
                .asObservable()
                .map {
                    $0.first { $0.name == .passiveTaskTitleLabelColor }
                }
                .skipNil()
                .map {
                    let array = $0.value.components(separatedBy: "/")
                    var colorArray = [CGFloat]()
                    for color in array {
                        let value = CGFloat(Double(color)!)
                        colorArray.append(value)
                    }
                    return self.color(with: colorArray)
                }
		case .left:
			return ConfigurationFacade.shared
                .parameters
                .asObservable()
                .map({
                    $0.first { $0.name == .leftTaskTitleLabelColor }
                })
                .skipNil()
                .map({
                    let array = $0.value.components(separatedBy: "/")
                    var colorArray = [CGFloat]()
                    for color in array {
                        let value = CGFloat(Double(color)!)
                        colorArray.append(value)
                    }
                    return self.color(with: colorArray)
                })
		case .right:
			return ConfigurationFacade.shared
                .parameters
                .asObservable()
                .map({
                    $0.first { $0.name == .rightTaskTitleLabelColor }
                })
                .skipNil()
                .map({
                    let array = $0.value.components(separatedBy: "/")
                    var colorArray = [CGFloat]()
                    for color in array {
                        let value = CGFloat(Double(color)!)
                        colorArray.append(value)
                    }
                    return self.color(with: colorArray)
                })
		}
	}

	func getTaskChecklistLabelColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskChecklistLabelColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskChecklistLabelColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskChecklistLabelColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		}
	}

	func getTaskDescriptionLabelColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskDescriptionLabelColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskDescriptionLabelColor} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskDescriptionLabelColor} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		}
	}

	func getTaskSeparatorLineColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskSeapatorLineColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskSeapatorLineColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskSeapatorLineColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return self.color(with: colorArray)
			}
		}
	}

    func getEditingTaskSeparatorLineColorValue(_ component: TimesetType) -> Observable<UIColor> {
        switch component {
        case .center:
            return Observable.just(UIColor.clear)
        case .left:
            return Observable.just(UIColor.white)
        case .right:
            return Observable.just(UIColor.black)
        }
    }

	func getTaskSeparatorLineHeightValue(_ component: TimesetType) -> Observable<CGFloat> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskSeapatorLineHeight } }.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskSeapatorLineHeight } }.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskSeapatorLineHeight } }.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		}
	}

    func getTaskSeparatorLineOffsetValue(_ component: TimesetType) -> Observable<CGFloat> {
        switch component {
        case .left: return Observable.just(20)
        default: return Observable.just(0)
        }
    }

    //task create button
	func getTaskSendButtonColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskSendButtonColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskSendButtonColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskSendButtonColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}

	func getTaskSendButtonBorderColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskSendButtonBorderColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskSendButtonBorderColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskSendButtonBorderColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}

    func getTaskSendButtonBorderWidthValue(_ component: TimesetType) -> Observable<CGFloat> {
        switch component {
        case .center:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskSendButtonBorderWidth } }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0)}
        case .left:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskSendButtonBorderWidth } }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
        case .right:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskSendButtonBorderWidth } }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0)}
        }
    }

    func getTaskTitleFontValue(_ component: TimesetType, active: Bool = true) -> Observable<UIFont> {
        switch component {
        case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskTitleFont } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: 23)
			}
        case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskTitleFont} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: 23)
			}
        case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskTitleFont } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: 23)
			}
        }
    }

    func getTaskInactiveFontValue(_ component: TimesetType, active: Bool = true) -> Observable<UIFont> {
        return Observable.just(UIFont.systemFont(ofSize: 16))
    }

    func getTaskCompactFontValue(_ component: TimesetType, active: Bool = true) -> Observable<UIFont> {
        return Observable.just(UIFont.systemFont(ofSize: 12))
    }

    func getTaskChecklistFontValue(_ component: TimesetType, active: Bool = true) -> UIFont {
        return UIFont(name: "Helvetica-Bold", size: active ? 23 : 17)!
    }

	func getTaskChecklistFontValue(_ component: TimesetType, active: Bool = true) -> Observable<UIFont> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskChecklistFont } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: active ? size : size - 6) ?? UIFont.boldSystemFont(ofSize: active ? 23 : 16)
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskChecklistFont} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: active ? size : size - 6) ?? UIFont.boldSystemFont(ofSize: active ? 23 : 16)
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskChecklistFont} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: active ? size : size - 6) ?? UIFont.boldSystemFont(ofSize: active ? 23 : 16)
			}
		}
	}

    func getTaskContentFontValue(_ component: TimesetType, active: Bool = true) -> Observable<UIFont> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskContentFont} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: active ? size : size - 6) ?? UIFont.boldSystemFont(ofSize: active ? 23 : 16)
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskContentFont} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: active ? size : size - 6) ?? UIFont.boldSystemFont(ofSize: active ? 23 : 16)
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskContentFont} }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				let size = CGFloat(Double(array[1])!)
				return UIFont(name: "Helvetica-Bold", size: active ? size : size - 6) ?? UIFont.boldSystemFont(ofSize: active ? 23 : 16)
			}
		}
	}

	func getTaskContentScaleValue(_ component: TimesetType) -> Observable<CGFloat> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskContentScale} }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskContentScale} }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskContentScale} }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		}
	}

	func getTaskAdditionalHeightValue(_ component: TimesetType) -> Observable<CGFloat> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskAdditionalHeight} }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskAdditionalHeight} }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskAdditionalHeight} }
				.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		}
	}

	func getTaskCornerValue(_ component: TimesetType) -> Observable<CGFloat> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable()
                .map { $0.first { $0.name == .passiveTaskCorner} }
				.skipNil().map { Double($0.value) }
                .skipNil().map { CGFloat($0) }
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable()
                .map { $0.first { $0.name == .leftTaskCorner} }
				.skipNil().map { Double($0.value) }
                .skipNil().map { CGFloat($0) }
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable()
                .map { $0.first { $0.name == .rightTaskCorner} }
				.skipNil().map { Double($0.value) }
                .skipNil().map { CGFloat($0) }
		}
	}

    func getTextAlignmentValue(_ component: TimesetType) -> Observable<NSTextAlignment> {
        switch component {
        case .center:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveTaskTextAlignment} }
				.skipNil().map { Double($0.value) }.skipNil().map { NSTextAlignment(rawValue: Int($0))!}
        case .left:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftTaskTextAlignment} }
				.skipNil().map { Double($0.value) }.skipNil().map { NSTextAlignment(rawValue: Int($0))!}
        case .right:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightTaskTextAlignment} }
				.skipNil().map { Double($0.value) }.skipNil().map { NSTextAlignment(rawValue: Int($0))!}
        }
    }
}
