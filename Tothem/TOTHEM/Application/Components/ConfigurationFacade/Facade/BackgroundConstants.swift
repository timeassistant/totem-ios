//
//  BackgroundConstants.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift

class BackgroundConstants {

    func getAnimationDuration() -> Double {
        return 0.1
    }

    func getBlurType() -> Observable<UIBlurEffect.Style> {
		return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .primaryBackgroundBlurType } }
            .skipNil().map {Int($0.value)}.skipNil().map {UIBlurEffect.Style(rawValue: $0)!}
    }

    func getBlurAlpha() -> Observable<CGFloat> {
        return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .primaryBackgroundBlurAlpha } }
			.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
    }

	func getContrastLayerColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveContrastLayerColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftContrastLayerColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightContrastLayerColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}

	func getStatusbarColorsValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveStatusbarColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftStatusbarColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightStatusbarColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}

	func statusBarStyle() -> Observable<UIStatusBarStyle> {
		return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .statusBarStyle } }
			.skipNil().map {Int($0.value)}.skipNil().map {UIStatusBarStyle(rawValue: $0)!}
	}
}
