//
//  ButtonPanelConstants.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift

class ButtonPanelConstants {

    func getShadowLayerBlurTypeValue(_ component: TimesetType) -> Observable<UIBlurEffect.Style> {
		switch component {
		case .center:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passivePannelBlurType } }.skipNil().map { Int($0.value)}.skipNil().map {UIBlurEffect.Style(rawValue: $0)!}
		case .left:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftPannelBlurType } }.skipNil().map {Int($0.value)}.skipNil().map {UIBlurEffect.Style(rawValue: $0)!}
		case .right:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightPannelBlurType } }.skipNil().map {Int($0.value)}.skipNil().map {UIBlurEffect.Style(rawValue: $0)!}
		}
	}

	func getShadowLayerBlurAlphaValue(_ component: TimesetType) -> Observable<CGFloat> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passivePannelBlurAlpha } }.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftPannelBlurAlpha } }.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightPannelBlurAlpha } }.skipNil().map { Double($0.value) }.skipNil().map { CGFloat($0) }
		}
	}

	func getShadowLayerContrastColorValue(_ component: TimesetType) -> Observable<UIColor> {
		switch component {
		case .center:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passivePannelContrastLayerColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .left:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftPannelContrastLayerColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .right:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightPannelContrastLayerColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}
}
