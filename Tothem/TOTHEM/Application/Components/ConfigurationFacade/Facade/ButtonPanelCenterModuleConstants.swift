//
//  ButtonPanelCenterModuleConstants.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum ComponentConstant {
    case createButton
    case scaleButton
    case profileButton
}

protocol ButtonPanelModuleConstants {
    func transitionAnimationDuration(_ component: ComponentConstant) -> Number
    func scale(_ component: ComponentConstant) -> Observable<Number>
    func color(_ component: ComponentConstant) -> Observable<UIColor>
    func border(_ component: ComponentConstant) -> Observable<Number>
    func borderColor(_ component: ComponentConstant) -> Observable<UIColor>
    func testSize(forIndex index: Int, component: ComponentConstant) -> Observable<ComplexValue<Number>>
}

class ButtonPanelCenterModuleConstants: ButtonPanelModuleConstants {
	//General
	func transitionAnimationDuration(_ component: ComponentConstant) -> Number {
		switch component {
		case .createButton:
			return 0.3
		case .scaleButton:
			return 0.3
		case .profileButton:
			return 0.3
		}
	}

    func scale(_ component: ComponentConstant) -> Observable<Number> {
		switch component {
		case .createButton:
            return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeCreateButtonSize } }
                .skipNil().map { Number($0.value) }.skipNil()
		case .scaleButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeScaleButtonSize } }
				.skipNil().map { Number($0.value) }.skipNil()
		case .profileButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeProfileButtonSize } }
				.skipNil().map { Number($0.value) }.skipNil()
		}
	}

	func color(_ component: ComponentConstant) -> Observable<UIColor> {
		switch component {
		case .createButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeCreateButtonColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}

		case .scaleButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeScaleButtonColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .profileButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeProfileButtonColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}

	func borderColor(_ component: ComponentConstant) -> Observable<UIColor> {
		switch component {
		case .createButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeCreateButtonBorderColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .scaleButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeScaleButtonBorderColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		case .profileButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeProfileButtonBorderColor } }.skipNil().map {
				let array = $0.value.components(separatedBy: "/")
				var colorArray = [CGFloat]()
				for color in array {
					let value = CGFloat(Double(color)!)
					colorArray.append(value)
				}
				return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
			}
		}
	}

	func border(_ component: ComponentConstant) -> Observable<Number> {
		switch component {
		case .createButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeCreateButtonBorderWidth } }
				.skipNil().map { Number($0.value) }.skipNil()
		case .scaleButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeScaleButtonBorderWidth } }
				.skipNil().map { Number($0.value) }.skipNil()
		case .profileButton:
			return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeProfileButtonBorderWidth } }
				.skipNil().map { Number($0.value) }.skipNil()
		}
	}

    func testSize(forIndex index: Int, component: ComponentConstant) -> Observable<ComplexValue<Number>> {
        let parameterName = ParameterName(rawValue: "test_createButton_size\(index)") ?? .testCreateButtonSize1
        switch component {
        case .createButton, .profileButton, .scaleButton:
            return getObservableForProcessedComplexCGFloatValue(parameterName: parameterName)
        }
    }

    private func getObservableForProcessedCGFloatValue(parameterName: ParameterName) -> Observable<Number> {

        let parameterShared = ConfigurationFacade.shared.parameters.asObservable()
            .map { $0.first { $0.name == parameterName } }
            .skipNil()
            .distinctUntilChanged { ($0.value == $1.value) && ($0.evaluationType == $1.evaluationType) }
            .share()
        let parameterAndConstants = Observable.combineLatest(parameterShared,
                                                            ConfigurationConstants.shared.constants.asObservable(),
                                                             ConfigurationFacade.shared.parametersRestrictions.asObservable())
        let isDynamicValue = parameterShared
            .map { $0.evaluationType == .configurationUnit }
        let pureValueSequence = parameterAndConstants
            .filter { $0.0.evaluationType != .configurationUnit }
            .map { parameter, constants, _ -> ParameterValue? in
                if parameter.evaluationType == .constant {
                    var collector = FormulaParametersCollector()
                    let evaluationInfo = FormulaEvaluationAdditionalInfo.shortInstance(withConstants: constants,
                                                                                       resultType: .double)
                    return FormulaProcessor.evaluate(formula: parameter.value,
                                                     info: evaluationInfo,
                                                     parameterValuesCollector: &collector)
                } else {
                    return ParameterValue.string(parameter.value)
                }
            }
            .skipNil()
            .map { $0.asDouble() }
        let configurationUnitValueSequence = parameterAndConstants
            .filter { $0.0.evaluationType == .configurationUnit }
            .map { $0.0.value }
            .map { ConfigurationUnit(forDescription: $0) }
            .skipNil()
            .flatMapLatest { $0.targetValue }
            .withLatestFrom(isDynamicValue) { (parameterValue, isDynamic) in
                return (parameterValue, isDynamic)
            }
            .filter { $0.1 }
            .map { $0.0.asDouble() }
        return Observable.from([pureValueSequence, configurationUnitValueSequence])
            .merge()
            .map { value in
                let restriction = ConfigurationFacade.shared.parametersRestrictions.value
                    .first { $0.parameterName == parameterName }
                if let restriction = restriction,
                    let minValue = NumberFormatter().number(from: restriction.min),
                    let maxValue = NumberFormatter().number(from: restriction.max) {
                    return getInscribed(value: value, minValue: Number(truncating: minValue), maxValue: Number(truncating: maxValue))
                }
                return value
            }
    }

    private func getObservableForProcessedComplexCGFloatValue(parameterName: ParameterName) -> Observable<ComplexValue<Number>> {

        let parameterShared = ConfigurationFacade.shared.parameters.asObservable()
            .map { $0.first { $0.name == parameterName } }
            .skipNil()
            .distinctUntilChanged { ($0.value == $1.value) && ($0.evaluationType == $1.evaluationType) }
            .share(replay: 1, scope: SubjectLifetimeScope.forever)
        let parameterAndConstants = Observable.combineLatest(parameterShared,
                                                             ConfigurationConstants.shared.constants.asObservable())
        let isDynamicValue = parameterShared
            .map { $0.evaluationType == .configurationUnit }
        let pureValueSequence = parameterAndConstants
            .filter { $0.0.evaluationType != .configurationUnit }
            .map { parameter, constants -> ParameterValue? in
                if parameter.evaluationType == .constant {
                    var collector = FormulaParametersCollector()
                    let evaluationInfo = FormulaEvaluationAdditionalInfo.shortInstance(withConstants: constants,
                                                                                       resultType: .double)
                    return FormulaProcessor.evaluate(formula: parameter.value,
                                                     info: evaluationInfo,
                                                     parameterValuesCollector: &collector)
                } else {
                    return ParameterValue.string(parameter.value)
                }
            }
            .skipNil()
            .map { $0.asDouble() }
            .map { ComplexValue<Number>(minValue: 0.0, value: $0, maxValue: $0) }
            .share(replay: 1, scope: SubjectLifetimeScope.forever)
        let configurationUnitValueSequence = parameterAndConstants
            .filter { $0.0.evaluationType == .configurationUnit }
            .map { $0.0.value }
            .map { ConfigurationUnit(forDescription: $0, debugMode: true) }
            .skipNil()
            .flatMapLatest {
                Observable.combineLatest(
                    $0.targetValue,
                    $0.minTargetValue!,
                    $0.maxTargetValue!
                )
            }
            .withLatestFrom(isDynamicValue) { (parameterValue, isDynamic) in
                return (parameterValue, isDynamic)
            }
            .filter { $0.1 }
            .throttle(0.25, scheduler: MainScheduler.instance)
            .map { (arg) -> ComplexValue<Number> in
                let (targetValue, minValue, maxValue) = arg.0
                return ComplexValue<Number>(minValue: minValue.asDouble(),
                                             value: targetValue.asDouble(),
                                             maxValue: maxValue.asDouble())
            }
            .share(replay: 1, scope: SubjectLifetimeScope.forever)
        return Observable.of(pureValueSequence, configurationUnitValueSequence)
            .merge()
    }
}
