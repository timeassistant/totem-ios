//
//  HapticParameters.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

class HapticParameters {

	let dispose = DisposeBag()

	enum HapticType: Int {
		case impactFeedbackLight
		case impactFeedbackMedium
		case impactFeedbackHeavy
		case selectionFeedback
		case notificationFeedbackSuccess
		case notificationFeedbackWarning
		case notificationFeedbackError
	}

	private func activateFeedback(_ hapticType: HapticType) {
		switch hapticType {
		case .impactFeedbackLight:
			HapticEngine.shared.impactFeedback.activateFeedback(with: .light)
		case .impactFeedbackMedium:
			HapticEngine.shared.impactFeedback.activateFeedback(with: .medium)
		case .impactFeedbackHeavy:
			HapticEngine.shared.impactFeedback.activateFeedback(with: .heavy)
		case .selectionFeedback:
			HapticEngine.shared.selectionFeedback.activateFeedback()
		case .notificationFeedbackSuccess:
			HapticEngine.shared.notificationFeedback.activateFeedback(with: .success)
		case .notificationFeedbackWarning:
			HapticEngine.shared.notificationFeedback.activateFeedback(with: .warning)
		case .notificationFeedbackError:
			HapticEngine.shared.notificationFeedback.activateFeedback(with: .error)
		}
	}

	func getHapticCreateButton(_ position: TimesetType) {
		switch position {
		case .center:
			ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .passiveCreateButtonHaptic} }
				.skipNil().map { Int($0.value) }.skipNil()
				.subscribe(onNext: { self.activateFeedback(HapticParameters.HapticType(rawValue: $0)!)}).disposed(by: self.dispose)
		case .left:
			ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .leftCreateButtonHaptic} }
				.skipNil().map { Int($0.value) }.skipNil()
				.subscribe(onNext: { self.activateFeedback(HapticParameters.HapticType(rawValue: $0)!)}).disposed(by: self.dispose)
		case .right:
			ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightCreateButtonHaptic} }
				.skipNil().map { Int($0.value) }.skipNil()
				.subscribe(onNext: { self.activateFeedback(HapticParameters.HapticType(rawValue: $0)!)}).disposed(by: self.dispose)

		}
	}

	func getHapticScaleButton(_ position: TimesetType) {
		switch position {
		case .center:
			ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeScaleButtonHaptic} }
				.skipNil().map { Int($0.value) }.skipNil()
				.subscribe(onNext: { self.activateFeedback(HapticParameters.HapticType(rawValue: $0)!)}).disposed(by: self.dispose)
		case .left:
			return
		case .right:
			return
		}
	}

	func getHapticProfileButton(_ position: TimesetType) {
		switch position {
		case .center:
			ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .activeProfileButtonHaptic} }
				.skipNil().map { Int($0.value) }.skipNil()
				.subscribe(onNext: { self.activateFeedback(HapticParameters.HapticType(rawValue: $0)!)}).disposed(by: self.dispose)
		case .left:
			return
		case .right:
			return
		}
	}

	func getHapticButton() {
			ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .hapticButton} }
				.skipNil().map { Int($0.value) }.skipNil()
				.subscribe(onNext: { self.activateFeedback(HapticParameters.HapticType(rawValue: $0)!)}).disposed(by: self.dispose)
		}
}
