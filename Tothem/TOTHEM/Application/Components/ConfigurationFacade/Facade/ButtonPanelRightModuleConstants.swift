//
//  ButtonPanelRightModuleConstants.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class ButtonPanelRightModuleConstants: ButtonPanelModuleConstants {
    func testSize(forIndex index: Int, component: ComponentConstant) -> Observable<ComplexValue<Number>> {
        return Observable.empty()
    }

    //General
    func transitionAnimationDuration(_ component: ComponentConstant) -> Number {
        return 0.3
    }

    //Right section
    func scale(_ component: ComponentConstant) -> Observable<Number> {
        return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightCreateButtonSize } }.skipNil().map { Number($0.value) }.skipNil()
    }

	func color(_ component: ComponentConstant) -> Observable<UIColor> {
        return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightCreateButtonColor } }.skipNil().map {
            let array = $0.value.components(separatedBy: "/")
            var colorArray: [CGFloat] = []
            for color in array {
                let value = CGFloat(Double(color)!)
                colorArray.append(value)
            }
            return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
        }
	}

    func borderColor(_ component: ComponentConstant) -> Observable<UIColor> {
        return ConfigurationFacade.shared.parameters.asObservable().map { $0.first { $0.name == .rightCreateButtonBorderColor } }.skipNil().map {
            let array = $0.value.components(separatedBy: "/")
            var colorArray: [CGFloat] = []
            for color in array {
                let value = CGFloat(Double(color)!)
                colorArray.append(value)
            }
            return UIColor(red: colorArray[0]/255.0, green: colorArray[1]/255.0, blue: colorArray[2]/255.0, alpha: colorArray[3])
        }
    }

    func border(_ component: ComponentConstant) -> Observable<Number> {
		return ConfigurationFacade.shared.parameters
            .asObservable()
            .map { $0.first { $0.name == .rightCreateButtonBorderWidth } }
            .skipNil()
            .map { Number($0.value) }
            .skipNil()
	}
}
