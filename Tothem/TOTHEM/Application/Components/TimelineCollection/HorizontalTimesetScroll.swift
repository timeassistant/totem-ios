//
//  HorizontalTimesetScroll.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum ScrollShift {
    case left(progress: CGFloat)
    case right(progress: CGFloat)
    case center(progress: CGFloat)

    var index: CGFloat {
        switch self {
        case .left: return 0
        case .right: return 2
        case .center: return 1
        }
    }
}

extension ScrollShift: Equatable {
    static func == (lhs: ScrollShift, rhs: ScrollShift) -> Bool {
        switch (lhs, rhs) {
        case (.center, .center),
             (.right, .right),
             (.left, .left):
            return true
        default:
            return false
        }
    }
}

class HorizontalTimesetScroll: UIScrollView {
    var horizontalBias: CGFloat = 0
    private var horizontalSidePadding: CGFloat {
        return ConfigurationFacade.shared.collectionViewLayout.shift()
    }

    var isChangingPosition: BehaviorSubject<ScrollShift>

    var leftSideView: UIView!
    var rightSideView: UIView!
    var centerView: UIView!

    var currentPosition: ScrollShift
    private var isExpanded = false
    private var fastMoveDistance: CGFloat = 0.01

    // Loop
    var isLooped: Variable<Bool>
    private var availableToLoop = true
    private var loopTime: TimeInterval = 0
    private var timer = Timer()

    init(frame: CGRect, leftSide: UIView, rightSide: UIView, center: UIView) {
        switch UserClassService.objectDs {
        case .center:
            currentPosition = .center(progress: 1)
            isLooped = Variable(false)
        case .right:
            currentPosition = .right(progress: 1)
            isLooped = Variable(true)
        case .left:
            currentPosition = .left(progress: 1)
            isLooped = Variable(true)
        }
        isChangingPosition = BehaviorSubject<ScrollShift>(value: currentPosition)
        
        super.init(frame: frame)
        self.leftSideView = leftSide
        self.rightSideView = rightSide
        self.centerView = center
        self.prepare()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func prepare() {
        self.commonPreparations()
        self.setupViews()
        self.panGestureRecognizer.addTarget(self, action: #selector(pan(pan:)))
    }

    @objc func pan(pan: UIPanGestureRecognizer) {
        guard pan.state == .began else { return }
        
        func disable() {
            self.isScrollEnabled = false
            self.isScrollEnabled = true
            self.contentOffset.x = self.currentPosition.index * self.frame.width
        }
        
        let x = pan.location(in: self).x + horizontalBias
        switch self.currentPosition {
        case .center:
            if x > (self.frame.width * self.currentPosition.index) + horizontalSidePadding && x < (self.frame.width * self.currentPosition.index) + (self.frame.width - horizontalSidePadding) {
                disable()
            }
        case .left:
            if x < (self.frame.width + (self.frame.width * self.currentPosition.index - horizontalSidePadding)) {
                disable()
            }
        case .right:
            if x > (self.frame.width * self.currentPosition.index) + horizontalSidePadding {
                disable()
            }
        }
    }

    private func commonPreparations() {
        self.backgroundColor = .clear
        self.contentSize.width = self.frame.width * 3
        self.bounces = false
        self.isPagingEnabled = true
        self.showsHorizontalScrollIndicator = false
        self.delegate = self
    }

    private func setupViews() {
        self.delegate = nil
        [leftSideView, centerView, rightSideView]
            .enumerated().forEach {
            $0.element?.frame = self.frame
            $0.element?.frame.origin.x = self.frame.width * CGFloat($0.offset)
            $0.element?.frame.size.width = self.frame.width
            self.addSubview($0.element ?? UIView())
        }
        self.sendSubviewToBack(self.centerView)
        contentSize = CGSize(width: frame.width * 3, height: frame.height)
        
        var offset: CGFloat = 0
        switch currentPosition {
        case .left: offset = 0
        case .right: offset = 2*frame.width
        case .center: offset = frame.width
        }
        
        setContentOffset(CGPoint(x: offset, y: 0), animated: false)
        delegate = self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        [leftSideView, centerView, rightSideView]
            .enumerated().forEach {
                $0.element?.frame = self.frame
                $0.element?.frame.origin.x = self.frame.width * CGFloat($0.offset)
                $0.element?.frame.size.width = self.frame.width
        }
        contentSize = CGSize(width: frame.width * 3, height: frame.height)
    }

    func expandList(_ expand: Bool) {
        self.isExpanded = expand
        switch self.currentPosition {
        case .left:
            self.shiftCenterView(horizontaly: self.isExpanded ? horizontalSidePadding : -1 * horizontalSidePadding,
                                 verticaly: 0)
        case .right:
            self.shiftCenterView(horizontaly: self.isExpanded ? -1 * horizontalSidePadding : horizontalSidePadding,
                                 verticaly: 0)
        default: return
        }
    }

    func set(side: TimesetType, scroll: Bool = true) {
        if scroll {
            switch side {
            case .left: scrollTo(0)
            case .center: scrollTo(frame.width)
            case .right: scrollTo(frame.width * 2)
            }
        }
    }
    
    func scrollTo(_ offset: CGFloat = 0, animated: Bool = true) {
        setContentOffset(CGPoint(x: offset, y: 0), animated: animated)
    }

    func shiftCenterView(horizontaly: CGFloat, verticaly: CGFloat) {
        centerView.transform = CGAffineTransform(translationX: horizontaly, y: verticaly)
    }
}

extension HorizontalTimesetScroll: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollX = scrollView.contentOffset.x
        let rightShift: CGFloat = max(0, min(1, (scrollX - self.frame.width) / self.frame.width))
        let leftShift = max(0, min(1, scrollX / self.leftSideView.frame.maxX))
        
        if contentOffset.x.equal(to: leftSideView.frame.minX) {
            self.currentPosition = .center(progress: 0)
        } else if contentOffset.x.equal(to: rightSideView.frame.minX) {
            self.currentPosition = .center(progress: 1)
        }
        
        switch self.currentPosition {
        case .center:
            let isShiftingToRight = scrollX > self.centerView.frame.origin.x

            self.isChangingPosition.on(.next(isShiftingToRight ?
                .right(progress: rightShift) :
                .left(progress: 1 - leftShift)))
            self.shiftCenterView(horizontaly: isShiftingToRight ?
                (rightShift) * horizontalSidePadding :
                (1-leftShift)*(-self.horizontalSidePadding), verticaly: 0)
            if isShiftingToRight && rightShift == 1 {
                self.currentPosition = .right(progress: 0)
            } else if (1 - leftShift) == 1 {
                self.currentPosition = .left(progress: 0)
            }
            
            switch currentPosition {
            case .left, .right:
                guard self.availableToLoop else { return }
                self.invalidateLoop()
                self.isLooped.value = false
                self.timer = Timer.scheduledTimer(withTimeInterval: self.loopTime, repeats: false) { _ in
                    self.isLooped.value = true
                    self.availableToLoop = false
                }
            default: return
            }
        case .left:
            self.isChangingPosition.on(.next(.center(progress: leftShift)))
            self.shiftCenterView(horizontaly: (1-leftShift) * (-self.horizontalSidePadding), verticaly: 0)
            if leftShift == 1 {
                self.currentPosition = .center(progress: 0)
                self.invalidateLoop(predicate: self.timer.isValid)
            }
        case .right:
            self.isChangingPosition.on(.next(.center(progress: 1 - rightShift)))
            self.shiftCenterView(horizontaly: (rightShift) * horizontalSidePadding, verticaly: 0)
            if (1 - rightShift) == 1 {
                self.currentPosition = .center(progress: 0)
                self.invalidateLoop(predicate: self.timer.isValid)
            }
        }
    }
    
    private func invalidateLoop(predicate: Bool = true) {
        if predicate {
            self.timer.invalidate()
            self.availableToLoop = true
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let scrollX = scrollView.contentOffset.x
        let rightShift: CGFloat = max(0, min(1, (scrollX - self.frame.width) / self.frame.width))
        let leftShift = max(0, min(1, scrollX / self.leftSideView.frame.maxX))
        guard self.isLooped.value else { return }
        
        switch self.currentPosition {
        case .left:
            let distance = leftShift
            if distance > fastMoveDistance {
                self.set(side: .right)
            }
        case .right:
            let distance = 1 - rightShift
            if distance > fastMoveDistance {
                self.set(side: .left)
            }
        default: return   
        }
    }
}
