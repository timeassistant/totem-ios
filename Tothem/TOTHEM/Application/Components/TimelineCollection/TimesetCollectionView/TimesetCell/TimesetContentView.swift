//
//  TimesetContentView.swift
//  TOTHEM
//
//  Created by Sergei Perevoznikov on 15/07/2018.
//  Copyright © 2018 r00t. All rights reserved.
//

import UIKit
import RxSwift

class TimesetContentView: XibSubview, TimesetContentViewProtocol {
    var disposeBag = DisposeBag()
    var styleDisposeBag = DisposeBag()
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint?
    var font: UIFont = UIFont.boldSystemFont(ofSize: 17)
    var type: TimesetType = .center

    private var width: CGFloat = 0 {
        didSet {
            if oldValue != width {
                updateSubentityFrames(animate: true)
            }
        }
    }

    func height(width: CGFloat) -> CGFloat {
        return Text.height(texts, width: width, font: font)
    }

    var state: TimesetContentState = .compact {
        didSet {
            scrollView?.isScrollEnabled = state == .reading
            scrollView?.setContentOffset(.zero, animated: true)
        }
    }

    var texts: [String] = []
    private var layers: [TaskTextLayer] = []

    class func contentHeight(with task: TaskModel, type: TimesetType, font: UIFont, width: CGFloat) -> CGFloat {
        let texts = EditingTextHelper.viewModelsWithTitle(from: task, type: type).compactMap({ $0.asString })
        return Text.height(texts, width: width, font: font)
    }

    override func configureView() {
        super.configureView()
        view.translatesAutoresizingMaskIntoConstraints = false
    }

    func setup(task: TaskModel, type: TimesetType) {
        self.type = type
        styleDisposeBag = DisposeBag()
        self.contentView?
            .subviews
            .forEach({ $0.removeFromSuperview() })

        texts = EditingTextHelper.viewModelsWithTitle(from: task, type: type)
            .compactMap({ $0.asString })

        layers = texts.map({
            let layer = TaskTextLayer()
            layer.set(text: $0, textFont: font)
            layer.anchorPoint = CGPoint.zero
            contentView?.layer.addSublayer(layer)
            return layer
        })

        ConfigurationFacade.shared
            .taskModuleConstants
            .getTaskChecklistLabelColorValue(type)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { color in
                self.layers.forEach({ $0.foregroundColor = color.cgColor })
            })
            .disposed(by: styleDisposeBag)
    }

    private func textHeight(at index: Int) -> CGFloat {
        return Text.height(texts[index], width: width, font: font)
    }

    private func updateSubentityFrames(animate: Bool) {
        self.contentViewHeight?.constant = Text.height(self.texts, width: self.width, font: self.font)
        startUpdating()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        width = bounds.width
    }

    func startUpdating() {
        CATransaction.begin()
        CATransaction.setAnimationDuration(Text.animationDuration)
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut))

        var position: CGFloat = 0
        layers.forEach { layer in
            guard let text = layer.string as? String else { return }
            let height = Text.height(text, width: width, font: font)
            let from = CGPoint(x: 0, y: position/* + height - layer.bounds.height*/)
            let to = CGPoint(x: 0, y: position)
            let group = CAAnimationGroup()
            group.animations = [
                animateFont(layer, to: font),
                animatePosition(layer, from: from, to: to),
                animateHeight(layer, to: height)
            ]
            position += height
            layer.add(group, forKey: nil)
        }

        CATransaction.commit()
    }

    func animateFont(_ layer: CATextLayer, to font: UIFont) -> CAAnimation {
        let animation = CABasicAnimation(keyPath: "fontSize")
        animation.fromValue = layer.fontSize
        animation.toValue = font.pointSize
        layer.fontSize = font.pointSize
        return animation
    }

    func animateHeight(_ layer: CATextLayer, to height: CGFloat) -> CAAnimation {
        let animation = CABasicAnimation(keyPath: "frame.size")
        animation.fromValue = layer.frame.size
        animation.toValue = CGSize(width: width, height: height)
        layer.frame.size = CGSize(width: width, height: height)
        return animation
    }

    func animatePosition(_ layer: CATextLayer, from: CGPoint, to: CGPoint) -> CAAnimation {
        let animation = CABasicAnimation(keyPath: "position")
        animation.fromValue = from
        animation.toValue = to
        layer.position = to
        return animation
    }
}
