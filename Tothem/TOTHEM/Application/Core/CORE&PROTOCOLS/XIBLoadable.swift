//
//  Core&Protocols.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol XIBLoadable: class {

}

extension XIBLoadable where Self: UIViewController {
    init() {
        self.init(nibName: String(describing: Self.self), bundle: nil)
    }
}

extension UIView {
    // swiftlint:disable force_cast
    class func instantiateFromNib<T: UIView>(_ viewType: T.Type) -> T {
        let name = String(describing: viewType)
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as! T
    }
    class func instantiateFromNib() -> Self {
        return instantiateFromNib(self)
    }
}
