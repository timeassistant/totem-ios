//
//  ErrorController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class ErrorController {
    static let shared = ErrorController()

    func errors(errors: [String]) {
        // FIXME: display errors
//        let message = errors.joined(separator: "\n")
//        let alertView = CDAlertView(title: "Error", message: message, type: .error)
//        let doneAction = CDAlertViewAction(title: "OK")
//        alertView.add(action: doneAction)
//        alertView.show()
    }

    func error(_ error: Error) {
        // FIXME: display error
//        let alertView = CDAlertView(title: "Error", message: error.localizedDescription, type: .error)
//        let doneAction = CDAlertViewAction(title: "OK")
//        alertView.add(action: doneAction)
//        alertView.show()
    }
}

extension UIViewController {
    func display(errors: [String]) {
        ErrorController.shared.errors(errors: errors)
    }

    func display(error: Error) {
        ErrorController.shared.error(error)
    }
}
