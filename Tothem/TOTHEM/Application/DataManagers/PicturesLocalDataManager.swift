//
//  PicturesLocalDataManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class PicturesLocalDataManager {
    var maxSize: Int = 1000
    private var filesDataSource = FileLocalDataSource(path: "Images")
    
    func get(path: String, name: String) -> Observable<UIImage> {
        return self.filesDataSource
            .get(path: path, name: name)
            .flatMap({ data -> Observable<UIImage> in
                return Observable.create({ observer in
                    DispatchQueue.global().async {
                        if let image = UIImage(data: data) {
                            let newSize = self.maxSize
                            let size = image.size.convert(maxSize: newSize)
                            observer.onNext(image.resized(to: size))
                        } else {
                            observer.onError(GenericError.notFound)
                        }
                        observer.onCompleted()
                    }
                    return Disposables.create()
                })
            })
    }
    
    func put(_ image: UIImage, path: String, name: String) -> Observable<Void> {
        let observable: Observable<Data> = Observable.create({ observer in
            DispatchQueue.global().async {
                let maxSize = self.maxSize
                let imageSize = image.size.convert(maxSize: maxSize)
                let newImage = image.resized(to: imageSize)
                
                var quality: CGFloat = 1
                if var data = newImage.jpegData(compressionQuality: quality) {
                    while data.count > Constants.imageSize {
                        quality -= 0.05
                        data = image.jpegData(compressionQuality: quality) ?? Data()
                    }
                    observer.onNext(data)
                } else {
                    let error = PicturesError.Local
                        .imageConvertionToDataFailed
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            return Disposables.create()
        })
        
        return observable.flatMap({ data in
            self.filesDataSource.put(data: data, path: path, name: name)
        })
    }
    
    func remove(path: String, name: String) -> Observable<Void> {
        return self.filesDataSource
            .remove(path: path, name: name)
    }
}
