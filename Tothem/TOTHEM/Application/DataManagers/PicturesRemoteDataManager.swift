//
//  PicturesRemoteDataManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import FirebaseStorage

enum UploadPhotoState {
    case started(UIImage)
    case progress(Float)
    case succeeded(String, Int)
}

final class PicturesRemoteDataManager {
    private let fileDataSource = FileRemoteDataSource(path: "images/source")
    
    func put(from file: URL, path: String, name: String) ->
        Observable<FileStorageUploadState> {
            return self.fileDataSource
                .upload(at: file, path: path, name: name)
    }
    
    func put(image: UIImage, path: String, name: String) -> Observable<FileStorageUploadState> {
        let conversion: Observable<Data> = Observable.create({ observer in
            DispatchQueue.global(qos: .background).async {
                if let data = image.jpegData(compressionQuality: 1) {
                    observer.onNext(data)
                } else {
                    observer.onError(GenericError.convertion)
                }
                observer.onCompleted()
            }
            return Disposables.create()
        })
        
        return conversion.flatMap({
            self.fileDataSource.upload(data: $0, path: path, name: name)
        })
    }
    
    func get(path: String, name: String) -> Observable<UIImage> {
        return self.fileDataSource
            .get(path: path, name: name, maxSize: Constants.imageSize)
            .flatMap({ data -> Observable<UIImage> in
                if let image = UIImage(data: data) {
                    return .just(image)
                } else {
                    return .error(GenericError.convertion)
                }
            })
    }

    func remove(path: String, name: String) -> Observable<Void> {
        return self.fileDataSource.remove(path: path, name: name)
    }
}
