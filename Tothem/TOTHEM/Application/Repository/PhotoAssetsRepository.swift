//
//  PhotoAssetsRepository.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import Photos
import RxSwift

enum PhotoAssetsRepositoryError: Error {
    case noAssetImage
    case accessDenied
    case assetNotFound
}

final class PhotoAssetsRepository {
    private let dataManager = PicturesLocalDataManager()
    
    private func url(for assetId: String) -> URL? {
        let url = try? FileManager.default
            .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return url?.appendingPathComponent(assetId)
            .appendingPathExtension("jpeg")
    }

    func image(from asset: PHAsset, maxSize: Int) -> Observable<UIImage> {
        return Observable.create({ observer in
            let imageSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight).convert(maxSize: maxSize)

            let manager = PHImageManager.default()
            let options = PHImageRequestOptions()
            options.version = .current
            options.isSynchronous = false
            options.isNetworkAccessAllowed = true
            options.deliveryMode = .highQualityFormat
            manager.requestImage(for: asset, targetSize: imageSize, contentMode: .default, options: options) { image, _ in

                if let image = image {
                    DispatchQueue.global(qos: .default).async {
                        let renderer = UIGraphicsImageRenderer(size: imageSize)
                        let newImage = renderer.image { _ in
                            image.draw(in: CGRect(origin: .zero, size: imageSize))
                        }
                        observer.on(.next(newImage))
                        observer.on(.completed)
                    }
                } else {
                    observer.on(.error(PhotoAssetsRepositoryError.noAssetImage))
                    observer.on(.completed)
                }
            }
            return Disposables.create()
        })
    }

    func put(_ asset: PHAsset, path: String, name: String) -> Observable<Void> {
        return self.image(from: asset, maxSize: dataManager.maxSize)
            .flatMap({
                self.dataManager.put($0, path: path, name: name)
            })
    }
    
    func save(image: UIImage, to collection: PHAssetCollection, completion: @escaping (PHAsset?, Error?) -> Void) {
        var localId: String?
        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetChangeRequest.creationRequestForAsset(from: image)
            if let placeholder = request.placeholderForCreatedAsset {
                localId = request.placeholderForCreatedAsset?.localIdentifier
                let changeRequest = PHAssetCollectionChangeRequest(for: collection)
                changeRequest?.addAssets([placeholder] as NSArray)
            } else {
                completion(nil, nil)
            }
        }, completionHandler: { result, error in
            if result, let localId = localId {
                let results = PHAsset.fetchAssets(withLocalIdentifiers: [localId], options: nil)
                completion(results.firstObject, error)
            } else {
                completion(nil, error)
            }
        })
    }

    func authorize(cameraAccess callback: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized: callback(true)
        case .denied, .restricted: callback(false)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { result in
                callback(result)
            }
        @unknown default:
            break
        }
    }

    func authorize(photoAccess callback: @escaping (Bool) -> Void) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized: callback(true)
        case .denied, .restricted: callback(false)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { status in
                callback(status == .authorized)
            }
        @unknown default:
            break
        }
    }
}
