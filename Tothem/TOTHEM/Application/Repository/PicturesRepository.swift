//
//  PicturesRepository.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class PicturesRepository {
    private var localDataManager = PicturesLocalDataManager()
    private var remoteDataManager = PicturesRemoteDataManager()
    
    private var observables: [String: Observable<UIImage>] = [:]
    private var cache = AutoPurgingImageCache()
    
    func get(path: String, name: String) -> Observable<UIImage> {
        let id = path + "_" + name
        if let image = cache.image(withIdentifier: id) {
            return .just(image)
        }
        
        if let observable = observables[id] {
            return observable
        }
        
        let observable: Observable<UIImage> = self.localDataManager
            .get(path: path, name: name)
            .catchError({ _ in
                self.remoteDataManager
                    .get(path: path, name: name)
                    .flatMap({
                        self.localDataManager.put($0, path: path, name: name)
                    })
                    .flatMap({ _ in
                        self.localDataManager.get(path: path, name: name)
                    })
            })
            .do(onNext: {
                self.cache.add($0, withIdentifier: id)
                self.observables[id] = nil
            })
        
        observables[id] = observable
        
        return observable
    }
    
    func put(image: UIImage, path: String, name: String) -> Observable<FileStorageUploadState> {
        print("put path:\(path) name:\(name)")
        return self.localDataManager.put(image, path: path, name: name)
            .flatMap({ _ in
                self.localDataManager.get(path: path, name: name)
            })
            .flatMap({
                self.remoteDataManager.put(image: $0, path: path, name: name)
            })
    }
    
    func putLocal(image: UIImage, path: String, name: String) -> Observable<Void> {
        return self.localDataManager.put(image, path: path, name: name)
    }
    
    func getLocal(path: String, name: String) -> Observable<UIImage> {
        return self.localDataManager.get(path: path, name: name)
    }
    
    func remove(path: String, name: String) -> Observable<Void> {
        let observables = [
            self.localDataManager.remove(path: path, name: name),
            self.remoteDataManager.remove(path: path, name: name)
        ]
        return Observable.merge(observables)
    }
}
