//
//  AssetNamesRepository.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import Photos

final class AssetNamesRepository {
    enum PictureState: Int {
        case new
        case uploading
    }

    enum UploadState {
        case none
        case new(String, Bool)
        case uploading(String)
    }

    static let shared = AssetNamesRepository()
    private let userDefaultsKey = "PicturesToUpload"
    private let filenamesDefaultsKey = "PictureFilenames"
    
    var state = BehaviorSubject<UploadState>(value: .none)

    private var assets: [PHAsset] = []
    private var assetIds: [String] = []
    
    private init() {
        if let (asset, `public`) = checkAssetsToUpload() {
            state.on(.next(.new(asset, `public`)))
        }
    }

    func add(_ assets: [(String, Bool)]) -> Observable<Void> {
        var ids = UserDefaults.standard.dictionary(forKey: userDefaultsKey) ?? [:]
        assets.forEach {
            guard ids[$0.0] == nil else { return }
            
            ids[$0.0] = [
                "state": PictureState.new.rawValue,
                "public": $0.1
            ]
        }
        UserDefaults.standard.set(ids, forKey: userDefaultsKey)
        UserDefaults.standard.synchronize()
        
        self.next()
        
        return .just(())
    }

    func uploading(_ assetId: String) -> Observable<Void> {
        var ids = UserDefaults.standard.dictionary(forKey: userDefaultsKey) ?? [:]
        
        if var state = ids[assetId] as? JSONObject {
            state["state"] = PictureState.uploading.rawValue
            ids[assetId] = state
        }
        
        UserDefaults.standard.set(ids, forKey: userDefaultsKey)
        state.on(.next(.uploading(assetId)))
        return Observable.just(())
    }

    func uploaded(_ assetId: String) -> Observable<Void> {
        var ids = UserDefaults.standard.dictionary(forKey: userDefaultsKey) ?? [:]
        ids.removeValue(forKey: assetId)
        UserDefaults.standard.set(ids, forKey: userDefaultsKey)
        return Observable.just(())
    }

    func next() {
        if let asset = checkAssetsToUpload() {
            state.on(.next(.new(asset.0, asset.1)))
        } else {
            state.on(.next(.none))
        }
    }

    private func checkAssetsToUpload() -> (String, Bool)? {
        if let value = UserDefaults.standard
            .dictionary(forKey: userDefaultsKey)?
            .first(where: { (_, value) in
                guard let raw = value as? JSONObject else { return false }
                return raw["state"] as? Int == PictureState.new.rawValue
            }) {
            if let json = value.value as? JSONObject,
                let `public` = json["public"] as? Bool {
                return (value.key, `public`)
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func assetId(_ asset: PHAsset) -> Observable<String> {
        guard let index = assets.firstIndex(of: asset) else {
            let assetId = UUID().uuidString
            assets.append(asset)
            assetIds.append(assetId)
            return Observable.just(assetId)
        }
        return Observable.just(assetIds[index])
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    private var generateFilename: String {
        return "\(dateFormatter.string(from: Date()))_\(UUID().uuidString).jpeg"
    }
    
    func assetIdFilename(_ assetId: String) -> Observable<String> {
        let filenames = UserDefaults.standard.dictionary(forKey: filenamesDefaultsKey) ?? [:]
        let filename = filenames[assetId] as? String ?? ""
        if filename.isEmpty {
            return .error(GenericError.notFound)
        } else {
            return .just(filename)
        }
    }
    
    func assetFilename(_ asset: PHAsset) -> Observable<String> {
        var filenames = UserDefaults.standard.dictionary(forKey: filenamesDefaultsKey) ?? [:]
        
        return self.assetId(asset)
            .map({ assetId -> String in
                var filename = filenames[assetId] as? String ?? ""
                if filename.isEmpty {
                    filename = self.generateFilename
                    filenames[assetId] = filename
                    UserDefaults.standard.set(filenames, forKey: self.filenamesDefaultsKey)
                }
                return filename
            })
    }
    
    func asset(_ assetId: String) -> Observable<PHAsset> {
        guard let index = assetIds.firstIndex(of: assetId) else {
            return Observable.error(RxError.noElements)
        }
        return Observable.just(assets[index])
    }
}
