//
//  FileRemoteDataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
import FirebaseStorage

enum FileStorageUploadState {
    case started
    case progress(Float)
    case finished(Int64)
}

enum FileStorageDownloadState {
    case started
    case progress(Float)
    case finished(Data?)
}

final class FileRemoteDataSource {
    var reference: StorageReference
    init(path: String) {
        reference = FirebaseDataManager.shared.storage.child(path)
    }
    
    func upload(data: Data, path: String, name: String) -> Observable<FileStorageUploadState> {
        return Observable.create { observer in
            DispatchQueue.main.async {
                let uploadTask = self.reference
                    .child(path)
                    .child(name)
                    .putData(data, metadata: nil) { metadata, error in
                        if let error = error {
                            observer.onError(error)
                            observer.onCompleted()
                        } else {
                            observer.onNext(.finished(metadata?.size ?? 0))
                            observer.onCompleted()
                        }
                    }
                
                uploadTask.observe(.progress) { taskSnapshot in
                    if let progress = taskSnapshot.progress {
                        let percentComplete = Float(progress.completedUnitCount) / Float(progress.totalUnitCount)
                        observer.onNext(.progress(percentComplete))
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func upload(at url: URL, path: String, name: String) -> Observable<FileStorageUploadState> {
        return Observable.create { observer in
            DispatchQueue.main.async {
                let uploadTask = self.reference
                    .child(path)
                    .child(name)
                    .putFile(from: url, metadata: nil) { metadata, error in
                        if let error = error {
                            observer.onError(error)
                            observer.onCompleted()
                        } else {
                            observer.onNext(.finished(metadata?.size ?? 0))
                            observer.onCompleted()
                        }
                }
                
                uploadTask.observe(.progress) { taskSnapshot in
                    if let progress = taskSnapshot.progress {
                        let percentComplete = Float(progress.completedUnitCount) / Float(progress.totalUnitCount)
                        observer.onNext(.progress(percentComplete))
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func get(path: String, name: String, maxSize: Int64) -> Observable<Data> {
        return Observable.create { observer in
            self.reference
                .child(path)
                .child(name)
                .getData(maxSize: maxSize) {
                    if let error = $1 {
                        observer.onError(error)
                    } else if let data = $0 {
                        observer.onNext(data)
                    } else {
                        observer.onError(GenericError.notFound)
                    }
                    observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    func remove(path: String, name: String) -> Observable<Void> {
        return Observable.create({ observer in
            self.reference
                .child(path)
                .child(name)
                .delete(completion: {
                    if let error = $0 {
                        observer.onError(error)
                    } else {
                        observer.onNext(())
                    }
                    observer.onCompleted()
                })
            return Disposables.create()
        })
    }
}
