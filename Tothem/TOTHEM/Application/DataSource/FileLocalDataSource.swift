//
//  FileLocalDataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift
final class FileLocalDataSource {
    var path: String
    init(path: String) {
       self.path = path
    }
    
    private func url(path: String, name: String) -> URL {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let documentDirectorPath = paths[0]
        
        let filePath = documentDirectorPath + "/" + self.path + "/" + path
        
        let url = URL(fileURLWithPath: filePath + "/" + name)
        
        if !FileManager.default.fileExists(atPath: url.absoluteString, isDirectory: nil) {
            try? FileManager.default.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
        }
        
        return url
    }
    
    func put(data: Data, path: String, name: String) -> Observable<Void> {
        return Observable.create { observer in
            DispatchQueue.global().async {
                let url = self.url(path: path, name: name)
                do {
                    try data.write(to: url)
                    observer.onNext(())
                } catch let error {
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func get(path: String, name: String) -> Observable<Data> {
        return Observable.create({ observer in
            DispatchQueue.global().async {
                let url = self.url(path: path, name: name)
                do {
                    let data = try Data(contentsOf: url)
                    observer.onNext(data)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            
            return Disposables.create()
        })
    }
    
    func getURL(path: String, name: String) -> Observable<URL> {
        return .just(url(path: path, name: name))
    }
    
    func remove(path: String, name: String) -> Observable<Void> {
        return Observable.create({ observer in
            DispatchQueue.global().async {
                let url = self.url(path: path, name: name)
                do {
                    try FileManager.default.removeItem(at: url)
                    observer.onNext(())
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            
            return Disposables.create()
        })
    }
}
