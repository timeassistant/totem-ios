//
//  ViseraAnimations.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum AnimationType {
    case pullToRefresh
}

class ViseraAnimator {
    static let shared = ViseraAnimator()

    func animate(_ view: UIView, with animation: AnimationType, name: String? = nil) -> Observable<String> {
        return animate(view.layer, with: animation, name: name)
    }

    func animate(_ layer: CALayer, with animation: AnimationType, name: String? = nil) -> Observable<String> {
        switch animation {
        case .pullToRefresh: return animatePullToRefresh(layer, name: name)
        }
    }

    private func animatePullToRefresh(_ layer: CALayer, name: String?) -> Observable<String> {
        return Observable.create { observer in
            CATransaction.begin()
            let animationDuration: TimeInterval = 2.0
            let animationName = name ?? "pullToRefreshAnimation"
            let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation")
            rotateAnimation.values = [ 0.0, Float.pi, (2.0 * Float.pi) ]

            let headAnimation = CABasicAnimation(keyPath: "strokeStart")
            headAnimation.duration = (animationDuration / 2.0)
            headAnimation.fromValue = 0
            headAnimation.toValue = 0.25

            let tailAnimation = CABasicAnimation(keyPath: "strokeEnd")
            tailAnimation.duration = (animationDuration / 2.0)
            tailAnimation.fromValue = 0
            tailAnimation.toValue = 1

            let endHeadAnimation = CABasicAnimation(keyPath: "strokeStart")
            endHeadAnimation.beginTime = (animationDuration / 2.0)
            endHeadAnimation.duration = (animationDuration / 2.0)
            endHeadAnimation.fromValue = 0.25
            endHeadAnimation.toValue = 1

            let endTailAnimation = CABasicAnimation(keyPath: "strokeEnd")
            endTailAnimation.beginTime = (animationDuration / 2.0)
            endTailAnimation.duration = (animationDuration / 2.0)
            endTailAnimation.fromValue = 1
            endTailAnimation.toValue = 1

            let animations = CAAnimationGroup()
            animations.duration = animationDuration
            animations.animations = [ rotateAnimation, headAnimation, tailAnimation, endHeadAnimation, endTailAnimation ]
            animations.repeatCount = Float.infinity
            animations.isRemovedOnCompletion = false
            CATransaction.setCompletionBlock {
                observer.on(.completed)
            }
            CATransaction.completionBlock()
            layer.add(animations, forKey: animationName)
            observer.on(.next(animationName))
            CATransaction.commit()
            return Disposables.create()
        }
    }
}
