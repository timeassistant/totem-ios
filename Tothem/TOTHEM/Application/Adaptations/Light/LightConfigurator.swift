//
//  LightConfigurator.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class LightConfigurator {
    static let shared = LightConfigurator()
    let light = AdaptationParameter<Number>(dataSource: .ambientLightBrightness)

    let isManual = DataSourcesSettingsService.shared
        .observableIsDynamic(for: .ambientLightBrightness)
        .map { !$0 }

    var normalizedValue: Observable<Number>
    
    private init() {
        let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "LightConfigurator")
        
        let combined = Observable.combineLatest(
            light.minValue,
            light.maxValue,
            light.value
        ).observeOn(queue)

        let manual = self.isManual
            .observeOn(queue)
            .take(1)
            .flatMapLatest { _ -> Observable<(Number, Number, Number)> in
                return combined.take(1)
            }

        let lightning = combined
            .observeOn(queue)
            .throttle(0.3, scheduler: queue)
            .skip(1)
            .share(replay: 1, scope: .forever)

        normalizedValue = Observable.merge(lightning, manual)
            .observeOn(queue)
            .map(LightConfigurator.normalized)
    }

    private static func normalized(data: (Number, Number, Number)) -> Number {
        return SizeCalculation.calculate(from: RangeParameter(min: data.0, max: data.1), to: RangeParameter(min: 0, max: 1), originalValue: data.2)
    }

    func normalize(_ size: RangeParameter) -> Observable<Number> {
        return normalizedValue.map({ size.min + $0 * (size.max - size.min) })
    }
}
