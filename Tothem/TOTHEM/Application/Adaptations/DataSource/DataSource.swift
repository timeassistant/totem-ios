//
//  DataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

typealias JSONObject = [String: Any]
typealias JSONArray = [JSONObject]

protocol DataSource {
    func child(path: String) -> DataSource
    var rx_value: Observable<Any?> { get }
    var rx_object: Observable<JSONObject> { get }
    var rx_array: Observable<JSONArray> { get }
    func rx_set(value: Any) -> Observable<DataSource>
    func rx_remove() -> Observable<DataSource>
}
