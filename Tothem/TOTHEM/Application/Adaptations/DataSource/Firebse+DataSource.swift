//
//  Firebse+DataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import FirebaseDatabase
import RxSwift

extension DatabaseReference: DataSource {
    func child(path: String) -> DataSource {
        return self.child(path)
    }

    var rx_value: Observable<Any?> {
        return self.rx_observe(eventType: .value)
            .map({ $0.value })
    }
    
    var rx_object: Observable<JSONObject> {
        return self.rx_value
            .map({ $0 as? JSONObject ?? [:] })
    }

    var rx_array: Observable<JSONArray> {
        return self.rx_observe(eventType: .value)
            .map({ $0.children.compactMap({ $0 as? DataSnapshot }) })
            .map({ $0.compactMap({ $0.value as? JSONObject }) })
    }

    func observe(handler: @escaping (JSONObject) -> Void) {
        self.observe(.value) { handler($0.value as? JSONObject ?? [:]) }
    }

    func observe(handler: @escaping (JSONArray) -> Void) {
        self.observe(.value) { _ in
            self.observe(.value) {
                let snapshots = $0.children.compactMap({ $0 as? DataSnapshot })
                let array = snapshots.compactMap({ $0.value as? JSONObject })
                handler(array)
            }
        }
    }
    
    func rx_set(value: Any) -> Observable<DataSource> {
        return rx_setValue(value: value as AnyObject)
            .map({ $0 as DataSource })
    }
    
    func rx_remove() -> Observable<DataSource> {
        return rx_removeValue()
            .map({ $0 as DataSource })
    }
}
