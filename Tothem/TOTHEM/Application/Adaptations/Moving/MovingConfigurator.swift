//
//  MovingConfigurator.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class MovingConfigurator {
    static let shared = MovingConfigurator()

    let walking = AdaptationParameter<Number>(dataSource: .walkingSpeed)
    let driving = AdaptationParameter<Number>(dataSource: .drivingSpeed)

    let isManual = DataSourcesSettingsService.shared
        .observableIsDynamic(for: .walkingSpeed)
        .map { !$0 }

    let activeDataSource = DataSourcesSettingsService.shared
        .observableGeolocationLastlyChanged()
        .map({ ConnectorParameter(rawValue: $0) ?? .walkingSpeed })

    var normalizedValue: Observable<Number>

    var walkingSpeedValue: Observable<Number> {
        return walking.value
    }

    var walkingSpeedMinValue: Observable<Number> {
        return walking.minValue
    }

    var walkingSpeedMaxValue: Observable<Number> {
        return walking.maxValue
    }

    var drivingSpeedValue: Observable<Number> {
        return driving.value
    }

    var drivingSpeedMinValue: Observable<Number> {
        return driving.minValue
    }

    var drivingSpeedMaxValue: Observable<Number> {
        return driving.maxValue
    }
    
    private init() {
        let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "MovingConfigurator")
        
        let combinedWalking = Observable.combineLatest(
            walking.minValue,
            walking.maxValue,
            walking.value
        ).observeOn(queue)
        
        let combinedDriving = Observable.combineLatest(
            driving.minValue,
            driving.maxValue,
            driving.value
        ).observeOn(queue)

        let manual = self.isManual
            .observeOn(queue)
            .take(1)
            .withLatestFrom(activeDataSource)
            .flatMapLatest { source -> Observable<(Number, Number, Number)> in
                if source == .walkingSpeed {
                    return combinedWalking.take(1)
                } else {
                    return combinedDriving.take(1)
                }
        }

        let walkingObservable = combinedWalking
            .observeOn(queue)
            .throttle(0.3, scheduler: queue)
            .skip(1)
            .share(replay: 1, scope: .forever)

        let drivingObservable = combinedDriving
            .observeOn(queue)
            .throttle(0.3, scheduler: queue)
            .skip(1)
            .share(replay: 1, scope: .forever)

        normalizedValue = Observable.merge(walkingObservable, drivingObservable, manual)
            .observeOn(queue)
            .map(MovingConfigurator.normalized)
    }

    private static func normalized(data: (Number, Number, Number)) -> Number {
        return SizeCalculation.calculate(from: RangeParameter(min: data.0, max: data.1), to: RangeParameter(min: 0, max: 1), originalValue: data.2)
    }

    func normalize(_ size: RangeParameter) -> Observable<Number> {
        return normalizedValue.map({ size.min + $0 * (size.max - size.min) })
    }
}
