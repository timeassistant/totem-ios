//
//  CreateButtonPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CreateButtonPresenter {
    var state: Observable<CreateButtonParametersModel>?
    private var current = CreateButtonParametersModel.default
    private var adaptation = CreateButtonAdaptation()
    private let queue = DispatchQueue(label: "CreateButtonQueue", qos: .default, attributes: .concurrent)

    init() {
        state = Observable.merge(isHidden, size, color, borderWidth, borderColor, animationDuration)
    }

    private var size: Observable<CreateButtonParametersModel> {
        return adaptation.size.map({ [weak self] value in
                guard let self = self else { return .default }
                self.queue.sync { self.current.size = value }
                return self.current
            })
    }

    private var borderWidth: Observable<CreateButtonParametersModel> {
        return adaptation.borderWidth.map({ [weak self] value in
                guard let self = self else { return .default }
                self.queue.sync { self.current.borderWidth = value }
                return self.current
            })
    }

    private var color: Observable<CreateButtonParametersModel> {
        return adaptation.color.map({ [weak self] value in
                guard let self = self else { return .default }
                self.queue.sync { self.current.color = value }
                return self.current
            })
    }

    private var borderColor: Observable<CreateButtonParametersModel> {
        return adaptation.borderColor.map({ [weak self] value in
                guard let self = self else { return .default }
                self.queue.sync { self.current.borderColor = value }
                return self.current
            })
    }

    private var isHidden: Observable<CreateButtonParametersModel> {
        return adaptation.isHidden.map({ [weak self] visible in
                guard let self = self else { return .default }
                self.queue.sync { self.current.hidden = !visible }
                return self.current
            })
    }

    private var animationDuration: Observable<CreateButtonParametersModel> {
        return adaptation.animationDuration.map({ [weak self] value in
            guard let self = self else { return .default }
            self.queue.sync { self.current.animationDuration = value }
            return self.current
        })
    }
}
