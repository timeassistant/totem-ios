//
//  CreateButtonAdaptation.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CreateButtonAdaptation {
    var repository = CreateButtonParametersRepository()
    var stateService = StateTimelineService.shared
    var moving = MovingConfigurator.shared
    var lighting = LightConfigurator.shared

    var size: Observable<Number> {
        return stateService.state
            .asObserver()
            .map({ $0.path })
            .flatMap(repository.parameters)
            .map({ $0.size })
            .flatMap(moving.normalize)
    }

    var borderWidth: Observable<Number> {
        return stateService.state
            .asObserver()
            .map(constants)
            .flatMap({ $0.border(.createButton) })
    }

    var color: Observable<UIColor>

    var borderColor: Observable<UIColor> {
        return stateService.state
            .asObserver()
            .map(constants)
            .flatMap({ $0.borderColor(.createButton) })
    }

    var isHidden: Observable<Bool> {
        return Observable.merge(VisibilityRulesService.shared
            .visibility(for: .createButton)
            .startWith(true)
            .asObservable().map { !$0 }, stateService.preview.map({ !$0 }))
    }

    var animationDuration: Observable<Number> {
        return stateService.state
            .asObserver()
            .map(constants)
            .map({ $0.transitionAnimationDuration(.createButton) })
    }
    
    private func constants(from side: TimesetType) -> ButtonPanelModuleConstants {
            switch side {
            case .center: return ConfigurationFacade.shared
                .buttonPanelCenterModuleConstants
            case .left: return ConfigurationFacade.shared
                .buttonPanelLeftModuleConstants
            case .right: return ConfigurationFacade.shared
                .buttonPanelRightModuleConstants
            }
    }

    init() {
        let parameters = stateService.state
            .asObserver()
            .map({ $0.path })
            .flatMap(repository.parameters)

        let hue = parameters
            .map({ $0.hue })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        let saturation = parameters
            .map({ $0.saturation })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        let brightness = parameters
            .map({ $0.brightness })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        let alpha = parameters
            .map({ $0.opacity })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        color = Observable.combineLatest(hue, saturation, brightness, alpha)
            .map({
                UIColor(hue: $0.0 / 360, saturation: $0.1 / 100, brightness: $0.2 / 100, alpha: $0.3 / 100) })
    }
}
