//
//  CreateButtonParametersEntity.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct CreateButtonParametersEntity {
    var size: RangeParameter
    var hue: RangeParameter
    var saturation: RangeParameter
    var brightness: RangeParameter
    var opacity: RangeParameter

    init(_ object: JSONObject) {
        size = RangeParameter(object["size"] as? JSONObject ?? [:])
        hue = RangeParameter(object["hue"] as? JSONObject ?? [:])
        saturation = RangeParameter(object["saturation"] as? JSONObject ?? [:])
        brightness = RangeParameter(object["brightness"] as? JSONObject ?? [:])
        opacity = RangeParameter(object["opacity"] as? JSONObject ?? [:])
    }
}
