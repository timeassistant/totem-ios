//
//  TimesetAdaptation.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct StabilizationParameters: Codable {
    var damping: Number
    var amplifier: Number
    var reaction: Number
    static var `default` = StabilizationParameters(damping: 1, amplifier: 1, reaction: 1)
}

final class TimesetAdaptation {
    static let shared = TimesetAdaptation()

    var repository = TimesetParametersRepository()
    let stabilization = StabilizationConfigurator.shared

    var maxBias: Observable<CGRect>
    var bias: Observable<(Vector3, Vector3)>
    
    private init() {
        let path = TimesetType.center.path

        let parameters = repository.parameters(for: path)

        let maxBiasX = parameters
            .map({
                UIScreen.main.bounds.width * CGFloat($0.bias_x.max / 100)
            })

        let minBiasX = parameters
            .map({
                UIScreen.main.bounds.width * CGFloat($0.bias_x.min / 100)
            })

        let minMaxBiasX = Observable.combineLatest(minBiasX, maxBiasX)
            .map({
                RangeParameter(min: Double($0.0), max: Double($0.1))
            })

        let maxBiasY = parameters
            .map({
                UIScreen.main.bounds.height * CGFloat($0.bias_y.max / 100)
            })

        let minBiasY = parameters
            .map({
                UIScreen.main.bounds.height * CGFloat($0.bias_y.min / 100)
            }).share()

        let minMaxBiasY = Observable.combineLatest(minBiasY, maxBiasY)
            .map({
                RangeParameter(min: Double($0.0), max: Double($0.1))
            }).share()

        maxBias = Observable.combineLatest(minMaxBiasX, minMaxBiasY)
            .map({
                let origin = CGPoint(x: $0.0.min, y: $0.1.min)
                let size = CGSize(width: $0.0.max - $0.0.min, height: $0.1.max - $0.1.min)
                return CGRect(origin: origin, size: size)
            }).share()

        bias = Observable.combineLatest(stabilization.bias, maxBias.share())
            .map({ (vector, frame) -> (Vector3, Vector3) in
                let x = vector.x * Double(frame.maxX)
                let y = vector.y * Double(frame.maxY)
                let z = vector.x * 0
                return (vector, Vector3(x: x, y: y, z: z))
            }).share()
    }
}
