//
//  CellParametersEntity.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct CellParametersEntity {
    var background_hue: RangeParameter
    var background_saturation: RangeParameter
    var background_brightness: RangeParameter
    var background_opacity: RangeParameter
    var bias_x: RangeParameter
    var bias_y: RangeParameter

    init(_ object: JSONObject) {
        background_hue = RangeParameter(object["color_background_hue"] as? JSONObject ?? [:])
        background_saturation = RangeParameter(object["color_background_saturation"] as? JSONObject ?? [:])
        background_brightness = RangeParameter(object["color_background_brightness"] as? JSONObject ?? [:])
        background_opacity = RangeParameter(object["color_background_opacity"] as? JSONObject ?? [:])
        bias_x = RangeParameter(object["bias_x"] as? JSONObject ?? [:])
        bias_y = RangeParameter(object["bias_y"] as? JSONObject ?? [:])
    }
}
