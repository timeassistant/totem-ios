//
//  CellPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CellPresenter {
    var state: Observable<CellParametersModel>?
    private var current = CellParametersModel.default
    private var adaptation: CellAdaptation

    var userClass = UserClassService.shared.userClass.asObservable()
    
    init(side: TimesetType, checked: Bool?) {
        let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "CellPresenter_\(side.path)")
        
        adaptation = CellAdaptation(side: side, checked: checked)
        state = Observable.combineLatest(normal, background_color, separator, insets, radius, userClass)
            .observeOn(queue)
            .map({
                var current = CellParametersModel.default
                current.normal = $0.0
                current.background_color = $0.1
                current.separator = $0.2
                current.insets = $0.3
                current.corner_radius = $0.4
                current.agile = CGFloat(abs($0.5.subject - 1) / 2)
                return current
            })
    }

    private var background_color: Observable<UIColor> {
        return adaptation.background_color
    }

    private var normal: Observable<CGFloat> {
        return adaptation.normal.map({ CGFloat($0) })
    }

    private var separator: Observable<CGFloat> {
        return adaptation.separator.map({ CGFloat($0) })
    }

    private var insets: Observable<UIEdgeInsets> {
        return adaptation.insets
    }
    
    private var radius: Observable<CGFloat> {
        return adaptation.cornerRadius
    }
}
