//
//  CellAdaptation.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CellAdaptation {
    var repository = CellParametersRepository()

    let moving = MovingConfigurator.shared
    let lighting = LightConfigurator.shared

    var background_color: Observable<UIColor>
    var normal: Observable<Number>
    var insets: Observable<UIEdgeInsets>
    var separator: Observable<CGFloat>
    var cornerRadius: Observable<CGFloat>
    
    init(side: TimesetType, checked: Bool?) {
        let queue = SerialDispatchQueueScheduler(internalSerialQueueName: "CellAdaptation_\(side.path)")
        
        normal = moving.normalizedValue
            .observeOn(queue)

        separator = ConfigurationFacade.shared
            .taskModuleConstants
            .getTaskSeparatorLineHeightValue(side)
            .observeOn(queue)

        insets = ConfigurationFacade.shared
            .collectionViewLayout
            .insets(side: side)
            .observeOn(queue)
        
        cornerRadius = ConfigurationFacade.shared
            .taskModuleConstants
            .getTaskCornerValue(side)
            .observeOn(queue)

        var path = side.path
        if checked == true {
            path += "_checked"
        }

        let parameters = repository.parameters(for: path)
            .observeOn(queue)

        let hue = parameters
            .map({ $0.background_hue })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        let saturation = parameters
            .map({ $0.background_saturation })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        let brightness = parameters
            .map({ $0.background_brightness })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        let alpha = parameters
            .map({ $0.background_opacity })
            .flatMap(lighting.normalize)
            .map({ CGFloat($0) })

        background_color = Observable.combineLatest(hue, saturation, brightness, alpha).observeOn(queue).map({
                UIColor(hue: $0.0 / 360, saturation: $0.1 / 100, brightness: $0.2 / 100, alpha: $0.3 / 100) })
    }
}
