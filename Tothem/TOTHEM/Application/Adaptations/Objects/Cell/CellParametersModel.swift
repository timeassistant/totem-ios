//
//  CellParametersModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct CellParametersModel {
    static var `default` = CellParametersModel()

    var background_color: UIColor
    var separator_color: UIColor
    var normal: CGFloat
    var separator: CGFloat
    var insets: UIEdgeInsets
    var corner_radius: CGFloat
    var agile: CGFloat

    private init() {
        background_color = .white
        separator_color = .clear
        normal = 1
        separator = 1
        insets = .zero
        corner_radius = 0
        agile = 1
    }
}
