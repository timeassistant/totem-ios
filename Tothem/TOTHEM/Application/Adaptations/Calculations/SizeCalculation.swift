//
//  SizeCalculation.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

struct SizeCalculation {
    static func calculate(from originalScale: RangeParameter, to targetScale: RangeParameter, originalValue: Number) -> Number {
        let originalScaleLength = originalScale.max - originalScale.min // 1
        let targetScaleLength = targetScale.max - targetScale.min // -100 - берем модуль
        let scaleTranslationFactor = targetScaleLength / originalScaleLength // 100
        let originalMinTranslated = originalScale.min * scaleTranslationFactor // 0
        let originalScaleOffset = targetScale.min - originalMinTranslated // 150
        return originalValue * scaleTranslationFactor + originalScaleOffset // 125
    }
    
    static func scaleFactor(from originalScale: RangeParameter, to targetScale: RangeParameter, originalValue: Number) -> Number {
        let translatedValue = SizeCalculation.calculate(from: originalScale, to: targetScale, originalValue: originalValue)
        return translatedValue / targetScale.min
    }
}
