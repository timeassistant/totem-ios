//
//  StabilizationConfigurator.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum StabilizationFunction: Int {
    case parabolic
    case linear

    func y(_ x: Number, _ reaction: Number, _ damping: Number, _ amplifier: Number) -> Number {
        switch self {
        case .parabolic:
            return (1 - pow((2 * (x + (1 - reaction)) - 1) * damping, 2)) * amplifier
        case .linear:
            return (1 - fabs(2 * (x + (1 - reaction)) - 1) * damping) * amplifier
        }
    }
}

final class StabilizationConfigurator {
    static let shared = StabilizationConfigurator()

    var bias: Observable<Vector3>!
    var velocity: Observable<Vector3>!

    private init() {
        velocity = AttitudeAngularVector.shared.vector

        let activated = Observable.combineLatest(
            SettingsRepository.shared.isRoll,
            SettingsRepository.shared.isPitch,
            SettingsRepository.shared.isPitch)

        let roll = activated.flatMap { (roll, pitch, yaw) -> Observable<StabilizationParameters> in
            if !(roll || pitch || yaw) {
                return SettingsRepository.shared.attitude
            } else {
                return SettingsRepository.shared.attitude_roll
            }
        }

        let pitch = activated.flatMap { (roll, pitch, yaw) -> Observable<StabilizationParameters> in
            if !(roll || pitch || yaw) {
                return SettingsRepository.shared.attitude
            } else {
                return SettingsRepository.shared.attitude_pitch
            }
        }

        let yaw = activated.flatMap { (roll, pitch, yaw) -> Observable<StabilizationParameters> in
            if !(roll || pitch || yaw) {
                return SettingsRepository.shared.attitude
            } else {
                return SettingsRepository.shared.attitude_yaw
            }
        }

        bias = Observable.combineLatest(
            velocity, roll, pitch, yaw,
            SettingsRepository.shared.function
            ).map({ (vector, roll, pitch, yaw, fun) -> Vector3 in
                let function = StabilizationFunction(rawValue: fun) ?? .parabolic
                let xr = self.normalize(vector.x, roll, function)
                let yp = self.normalize(vector.y, pitch, function)
                let zy = self.normalize(vector.z, yaw, function)
                return Vector3(x: xr, y: yp, z: zy)
            })
    }

    func normalize(_ x: Number, _ params: StabilizationParameters, _ function: StabilizationFunction) -> Number {
        let x = max(-1, min(1, x))
        var y: Double = 0

        if x == 0 || fabs(x) == 1 {
            y = 0
        } else {
            let mX = fabs(x)

            y = self.y(mX, params: params, function: function)
            y *= x / mX

            if x > 0, y < 0 {
                y = 0
            }

            if x < 0, y > 0 {
                y = 0
            }
        }
        return y * -1
    }

    private func y(_ x: Number, params: StabilizationParameters, function: StabilizationFunction) -> Number {
        return function.y(x, params.reaction, params.damping, params.amplifier)
    }
}
