//
//  AttitudeAngularVector.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct Vector3 {
    var x: Number
    var y: Number
    var z: Number
    static let zero = Vector3(x: 0, y: 0, z: 0)
}

final class AttitudeAngularVector {
    static let shared = AttitudeAngularVector()

    var vector: Observable<Vector3>!

    private init() {
        vector = Observable.combineLatest(
            AttitudeAngularVelocity.roll.velocity,
            AttitudeAngularVelocity.pitch.velocity,
            AttitudeAngularVelocity.yaw.velocity
            ).map({ Vector3(x: $0.0, y: $0.1, z: $0.2) })
    }
}
