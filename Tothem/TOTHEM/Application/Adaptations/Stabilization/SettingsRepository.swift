//
//  SettingsRepository.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class SettingsRepository {

    static let shared = SettingsRepository()
    private var disposeBag = DisposeBag()

    private let decoder = PropertyListDecoder()
    private let encoder = PropertyListEncoder()

    var attitude_pitch = BehaviorSubject<StabilizationParameters>(value: .default)
    var attitude_roll = BehaviorSubject<StabilizationParameters>(value: .default)
    var attitude_yaw = BehaviorSubject<StabilizationParameters>(value: .default)

    var attitude = BehaviorSubject<StabilizationParameters>(value: .default)

    var isRoll = BehaviorSubject<Bool>(value: true)
    var isPitch = BehaviorSubject<Bool>(value: false)
    var isYaw = BehaviorSubject<Bool>(value: false)

    var function = BehaviorSubject<Int>(value: 0)
    var isAnimated = BehaviorSubject<Bool>(value: true)

    init() {
        setup("attitude_roll", attitude_roll)
        setup("attitude_pitch", attitude_pitch)
        setup("attitude_yaw", attitude_yaw)
        setup("attitude", attitude)
        setup("is_roll", isRoll)
        setup("is_pitch", isPitch)
        setup("is_yaw", isYaw)
        setup("function", function)
        setup("is_animated", isAnimated)
    }

    private func setup<T: Codable>(_ key: String, _ subj: BehaviorSubject<T>) {
        if let value: T = get(key: key) {
            subj.on(.next(value))
        }

        subj.asObservable()
            .skip(1)
            .subscribe(onNext: { [weak self] in
                self?.set(key: key, value: $0)
            })
            .disposed(by: disposeBag)
    }

    private func get<T: Codable>(key: String) -> T? {
        guard let params = UserDefaults.standard.dictionary(forKey: "stabilization") else { return nil }

        if let data = params[key] as? Data,
            let value = try? decoder.decode(T.self, from: data) {
            return value
        }
        return nil
    }

    private func set<T: Codable>(key: String, value: T) {
        var params = UserDefaults.standard.dictionary(forKey: "stabilization") ?? [:]
        params[key] = try? encoder.encode(value)
        UserDefaults.standard.set(params, forKey: "stabilization")
    }
}
