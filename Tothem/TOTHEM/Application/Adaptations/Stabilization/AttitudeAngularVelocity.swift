//
//  AttitudeAngularVelocity.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class AttitudeAngularVelocity {
    static let pitch = AttitudeAngularVelocity(.attitudePitch)
    static let roll = AttitudeAngularVelocity(.attitudeRoll)
    static let yaw = AttitudeAngularVelocity(.attitudeYaw)

    var velocity: Observable<Number>!
    static var maxAngularVelocity: Number = 3
    private var currentAngular: Number = 0

    private init(_ source: ConnectorParameter) {
        velocity = source.normalizedValueObservable
            .map({ $0.asDouble() })
            .filter({ $0.rounded(toPlaces: 3) != self.currentAngular })
            .map({
                let oldAngular = self.currentAngular
                self.currentAngular = $0.rounded(toPlaces: 3)
                return (180 * ($0 - oldAngular) / Double.pi) / AttitudeAngularVelocity.maxAngularVelocity
            })
    }
}
