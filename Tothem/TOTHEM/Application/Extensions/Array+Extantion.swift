//
//  Array+Extantion.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit

extension Array {

	public func getStringToDoubleArray(_ string: String) -> [CGFloat] {

		let array = string.components(separatedBy: "/")
		var colorArray: [CGFloat] = []
		for color in array {
			let value = CGFloat(Double(color)!)
			colorArray.append(value)
		}

		return colorArray
	}

    func toDictionary<T: Equatable>(withKeys keys: [T]) -> [T: Element]? {
        guard self.count == keys.count else { return nil }
        var result = [T: Element]()
        for index in 0...(self.count-1) {
            result[keys[index]] = self[index]
        }
        return result
    }
}
