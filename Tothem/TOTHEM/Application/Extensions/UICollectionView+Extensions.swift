//
//  UICollectionView + Extensions.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

extension UICollectionView {
    struct LayoutModel {
        var frame: CGRect
        var indexPath: IndexPath
    }

    func closestToScreenCenterCell(skip: [String]) -> LayoutModel? {
        let window = UIApplication.shared.keyWindow
        let collectionViewFrame = convert(bounds, to: window)
        let center = collectionViewFrame.midY
        guard let cell = visibleCells.sorted (by: { cell1, cell2 in
            if skip.contains(cell1.reuseIdentifier ?? "") {
                return false
            }

            if skip.contains(cell2.reuseIdentifier ?? "") {
                return true
            }

            let diff1 = abs(cell1.convert(cell1.bounds, to: window).midY - center)
            let diff2 = abs(cell2.convert(cell2.bounds, to: window).midY - center)
            return diff1 < diff2
        }).first else { return nil }

        guard let indexPath = indexPath(for: cell) else { return nil }

        var frame = cell.convert(cell.bounds, to: window)
        frame.origin.y -= collectionViewFrame.origin.y
        frame.origin.x -= collectionViewFrame.origin.x
        return LayoutModel(frame: frame, indexPath: indexPath)
    }
}

extension UIView {
    class func animate(_ duration: TimeInterval, animations: @escaping () -> Void) {
        animate(withDuration: duration, delay: 0, options: .allowUserInteraction, animations: animations, completion: nil)
    }
}
