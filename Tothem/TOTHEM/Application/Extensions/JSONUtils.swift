//
//  JSONUtils.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

struct JSONUtils {
    var bundle: Bundle
    init(bundle: Bundle) {
        self.bundle = bundle
    }

    func readJSON(filename: String) throws -> Data {
        guard let path = bundle.path(forResource: filename, ofType: "json") else {
            throw NSError(domain: "", code: 0, userInfo: nil)
        }
        return try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
    }
    
    func read(from file: String) throws -> JSONObject {
        let data = try readJSON(filename: file)
        return (try JSONSerialization.jsonObject(with: data, options: []) as? JSONObject) ?? [:]
    }
    
    func read(from file: String) throws -> JSONArray {
        let data = try readJSON(filename: file)
        return (try JSONSerialization.jsonObject(with: data, options: []) as? JSONArray) ?? []
    }
}
