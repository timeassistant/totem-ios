//
//  Observable+DynamicDelay.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

public enum DeviationChange {
    case increase
    case withoutChange
    case decrease
}

private struct DelayedValue<T> {
    var uuid: UUID
    var delay: TimeInterval
    var newValue: T
    var shouldUpdateimmediately: Bool
    var emittedAt: Date
    var emitted: Bool
}

extension ObservableType {

    static private var maxDelay: TimeInterval {
        return 0.5
    }
    static func normalizeDelay(delay: TimeInterval) -> TimeInterval {
        return (delay > maxDelay) ? maxDelay : delay
    }

    private func withPrevious(startWith first: E) -> Observable<(E, E)> {
        return scan((first, first)) { ($0.1, $1) }.skip(1)
    }

    // delayMultiplier should be higher than or equal to 1.0.
    // It isn't possible to perform emitting faster than source does
    func delay(startValue: E, delayMultiplier: Double,
               directionRelativeToAnchor: @escaping (E, E) -> DeviationChange,
               delayModifier: @escaping (TimeInterval) -> TimeInterval) -> Observable<E> {

        let startValueWithDate = (value: startValue, date: Date())

        var bufferStorage = [DelayedValue<E>]()
        var delayedObservable: Observable<DelayedValue<E>>?

        func appendTo(buffer: [DelayedValue<E>], delayedValue: DelayedValue<E>, observable: Observable<DelayedValue<E>>?) {
            let dateNow = Date()
            var bufferState = buffer
            var observableState = observable
            bufferState.append(delayedValue)
            if observableState == nil {
                observableState = Observable.just(delayedValue)
                    .delay(RxTimeInterval(delayedValue.delay), scheduler: MainScheduler.instance)
                    .do(onNext: { delayedValue in
                        markAsEmitted(delayedValue: delayedValue)
                    })

                bufferStorage = bufferState
                delayedObservable = observableState

                return
            }

            var singleEvents = [Observable<DelayedValue<E>>]()
            for itemIndex in 0..<bufferState.count {
                let bufferItem = bufferState[itemIndex]
                if bufferItem.emitted { continue }
                let totalDelay = bufferState[0...itemIndex].reduce(0.0) { (result, delayedValue) in
                    return result + delayedValue.delay
                }
                let unnormalizedDelay = bufferItem.emittedAt.timeIntervalSince(dateNow) + totalDelay
                let normalizedDelay = (unnormalizedDelay >= 0) ? unnormalizedDelay : 0.0
                let delayedEvent = Observable.just(bufferItem)
                    .delay(normalizedDelay, scheduler: MainScheduler.instance)
                    .do(onNext: { delayedValue in
                        markAsEmitted(delayedValue: bufferItem)
                    })
                singleEvents.append(delayedEvent)
            }
            observableState = Observable.merge(singleEvents)

            bufferStorage = bufferState
            delayedObservable = observableState
        }
        func clearBuffer() {
            delayedObservable = nil
            bufferStorage = []
        }
        func markAsEmitted(delayedValue: DelayedValue<E>) {
            let bufferValueIndex = bufferStorage.firstIndex(where: { $0.uuid == delayedValue.uuid })
            guard let valueIndex = bufferValueIndex else { return }
            bufferStorage[valueIndex].emitted = true
        }

        let preparedObservable = self
            .map { (value: $0, date: Date()) }
            .startWith(startValueWithDate)
            .withPrevious(startWith: startValueWithDate)
            .map { data -> DelayedValue<E> in
                let deviationChange = directionRelativeToAnchor(data.0.value, data.1.value)
                switch deviationChange {
                case .increase:
                    return DelayedValue(uuid: UUID(), delay: 0.0, newValue: data.1.value, shouldUpdateimmediately: true,
                                        emittedAt: data.1.date, emitted: false)
                case .withoutChange:
                    return DelayedValue(uuid: UUID(), delay: 0.0, newValue: data.1.value, shouldUpdateimmediately: false,
                                        emittedAt: data.1.date, emitted: false)
                case .decrease:
                    let delayRaw = data.1.date.timeIntervalSince(data.0.date) * (delayMultiplier - 1.0)
                    let delay = bufferStorage.isEmpty ? 0.0 : delayModifier(delayRaw)
                    return DelayedValue(uuid: UUID(), delay: delay, newValue: data.1.value, shouldUpdateimmediately: false,
                                        emittedAt: data.1.date, emitted: false)
                }
        }

        return preparedObservable
            .flatMapLatest { delayedValue -> Observable<DelayedValue<E>> in
                if delayedValue.shouldUpdateimmediately ||
                    (bufferStorage.isEmpty && delayedValue.delay == 0.0) {
                    clearBuffer()
                    return Observable.just(delayedValue)
                } else {
                    appendTo(buffer: bufferStorage, delayedValue: delayedValue,
                             observable: delayedObservable)
                    print("Items in buffer: \(bufferStorage.count)")
                    print("Buffer: \(bufferStorage)")
                    return delayedObservable!
                }
            }
            .map { $0.newValue }
    }
}
