//
//  Double+Extensions.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

extension CGFloat {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return (self * divisor).rounded() / divisor
    }
}

func convertHalfRange(_ value: CGFloat, from1: Double, from2: Double, to1: Double, to2: Double ) -> CGFloat {
	let originValue = Double(value)
	let newValue = (originValue - from1)/(from2-from1)*(to2 - to1) + to1
	print("--------------")
	print(originValue)
	print("++++++++++")
	print(newValue)
	return CGFloat(newValue)
}

func getInscribed<T: Comparable>(value: T, minValue: T, maxValue: T) -> T {
    if value < minValue { return minValue }
    if value > maxValue { return maxValue }
    return value
}

extension Double {

    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }

    func equal(to: Double) -> Bool {
        return abs(self - to) < 0.000000001
    }
}

extension CGFloat {
    func equal(to: CGFloat) -> Bool {
        return abs(self - to) < 0.000000001
    }
}
