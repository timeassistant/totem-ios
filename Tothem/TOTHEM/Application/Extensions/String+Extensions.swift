//
//  String+Extensions.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

extension String {

    func cut(fromStart startOffset: Int, fromEnd endOffset: Int) -> String {

        let start = self.index(self.startIndex, offsetBy: startOffset)
        let end = self.index(self.endIndex, offsetBy: -endOffset)
        let range = start..<end
        let substring = self[range]
        return String(substring)
    }

    func replaceFirst(of pattern: String, with replacement: String) -> String {
        if let range = self.range(of: pattern) {
            return self.replacingCharacters(in: range, with: replacement)
        } else {
            return self
        }
    }

    func hasPrefix(among prefixes: [String]) -> (Bool, String?) {
        for prefix in prefixes where self.hasPrefix(prefix) {
            return (true, prefix)
        }
        return (false, nil)
    }

    var hasVisibleCharacters: Bool {
        return !self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
