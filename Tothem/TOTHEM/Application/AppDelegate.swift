//
//  AppDelegate.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import Instabug

final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        _ = FirebaseDataManager.shared
        
        ParametersDataManager.shared.getParameters()
        
        handleFirstLaunch()
        appCoordinator = AppCoordinator(window: window!)

        CreateCollectionCellsUseCase.imageLoader = GetEntityPhotoUseCase.shared.execute(uid:photoEntityValue:)
        SimilarTasksTransformer.imageLoader = GetEntityPhotoUseCase.shared.execute(uid:photoEntityValue:)
        SimilarEntitiesTransformer.imageLoader = GetEntityPhotoUseCase.shared.execute(uid:photoEntityValue:)
        
        return true
    }

    func handleFirstLaunch() {
        if let token = Bundle.main.infoDictionary?["InstabugToken"] as? String {
            Instabug.start(withToken: token, invocationEvents: [.shake, .screenshot])
        }
    }

    @objc func authorizationSuccedeed(notification: Notification) {
        print("Authorization passed!")
        print(String(describing: notification.object))
    }

    @objc func authorizationFailed(notification: Notification) {
        print("Authorization failed!")
        print(String(describing: notification.object))
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        LocalSettings.update()
        SessionService.shared.start()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        SessionService.shared.end()
    }
}
