//
//  AppCoordinator.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

protocol CoordinatorProtocol {
    associatedtype Controller: UIViewController
    var initialController: Controller { get set }
    var navigationController: UINavigationController! { get set }
    func start()
    init()
}

extension CoordinatorProtocol {
    init(navigationController: UINavigationController) {
        self.init()
        self.navigationController = navigationController
    }

    func start() {
        navigationController.viewControllers = [initialController]
    }
}

class AppCoordinator {
    var window: UIWindow!
    private var disposeBag = DisposeBag()

    var navigationController: UINavigationController {
        let navigationBar = UINavigationController()
        navigationBar.setNavigationBarHidden(true, animated: false)
        return navigationBar
    }

    init(window: UIWindow) {
        self.window = window
        self.start()
    }

    private func start() {
        window.rootViewController = UIViewController()
        
        if FirebaseDataManager.shared.currentUser != nil {
            changeToMain()
        }
        
        AuthManager.shared
            .user
            .skipNil()
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { _ in
                // if nil changeToAuthCoordinator()
                self.changeToMain()
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func changeToAuthCoordinator() {
        let authCoordinator = StoryboardScene.Authentication.authStartViewController.instantiate()
        window.set(rootViewController: authCoordinator, withTransition: transition())
    }

    private func changeToMain() {
        let mainCoordinator = MainTaskPageCoordinator(navigationController: navigationController)
        window.rootViewController = mainCoordinator.navigationController
        mainCoordinator.start()
        
        UserClassService.shared
            .userOnboarding
            .subscribe(onNext: { [weak self] in
                if $0 == nil {
                    self?.showOnboarding()
                }
            })
            .disposed(by: disposeBag)
    }

    private func transition() -> CATransition {
        let transition = CATransition()
        transition.type = CATransitionType.fade
        transition.duration = 0.3

        return transition
    }
    
    private func showOnboarding() {
        OperationQueue.main.addOperation {
            let view = OnboardingWireframe.create()
            view.modalPresentationStyle = .fullScreen
            self.window.rootViewController?.present(view, animated: false, completion: nil)
        }
    }
}
