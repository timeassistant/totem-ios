// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let arrowsRight = ImageAsset(name: "arrows_right")
  internal static let arrowsleft = ImageAsset(name: "arrowsleft")
  internal static let slider = ImageAsset(name: "slider")
  internal static let sliderButton = ImageAsset(name: "slider_button")
  internal static let sliderPhone = ImageAsset(name: "slider_phone")
  internal static let sliderSms1 = ImageAsset(name: "slider_sms_1")
  internal static let launchTest1Deonix = ImageAsset(name: "launch_test1_deonix")
  internal static let launchTest1Haggo = ImageAsset(name: "launch_test1_haggo")
  internal static let launchTest1Titan = ImageAsset(name: "launch_test1_titan")
  internal static let launchTest1Viosity = ImageAsset(name: "launch_test1_viosity")
  internal static let launchTest2Alpha = ImageAsset(name: "launch_test2_alpha")
  internal static let launchTest2Beta = ImageAsset(name: "launch_test2_beta")
  internal static let launchTest2Delta = ImageAsset(name: "launch_test2_delta")
  internal static let launchTest2Omega = ImageAsset(name: "launch_test2_omega")
  internal static let addPhotoChat = ImageAsset(name: "addPhotoChat")
  internal static let back = ImageAsset(name: "back")
  internal static let backButton = ImageAsset(name: "backButton")
  internal static let backGrayIcon = ImageAsset(name: "backGrayIcon")
  internal static let backgroundView = ImageAsset(name: "backgroundView")
  internal static let buttonFinish = ImageAsset(name: "button_finish")
  internal static let buttonStart = ImageAsset(name: "button_start")
  internal static let checkOff = ImageAsset(name: "checkOff")
  internal static let checkOn = ImageAsset(name: "checkOn")
  internal static let clock = ImageAsset(name: "clock")
  internal static let drivenCar = ImageAsset(name: "drivenCar")
  internal static let drivenCarGrayBackground = ImageAsset(name: "drivenCarGrayBackground")
  internal static let email = ImageAsset(name: "email")
  internal static let fLeft = ImageAsset(name: "f_left")
  internal static let fRight = ImageAsset(name: "f_right")
  internal static let gentleman = ImageAsset(name: "gentleman")
  internal static let gentleman512 = ImageAsset(name: "gentleman_512")
  internal static let lady = ImageAsset(name: "lady")
  internal static let lady512 = ImageAsset(name: "lady_512")
  internal static let password = ImageAsset(name: "password")
  internal static let plashkaCode = ImageAsset(name: "plashka_code")
  internal static let plashkaPhone = ImageAsset(name: "plashka_phone")
  internal static let subbackground = ImageAsset(name: "subbackground")
  internal static let subtitanbackground = ImageAsset(name: "subtitanbackground")
  internal static let walkingPerson = ImageAsset(name: "walkingPerson")
  internal static let walkingPersonGrayBackground = ImageAsset(name: "walkingPersonGrayBackground")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct DataAsset {
  internal fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  internal var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
