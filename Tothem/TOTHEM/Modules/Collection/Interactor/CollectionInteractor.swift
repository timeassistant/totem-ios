//
//  CollectionInteractor.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit

enum CollectionInteractorJob {
    case subscribeForData
    case subscribeForPreviewState
    case model(String, (Any?) -> Void)
    case action(CollectionCellModel, ListCellAction, ListCellActionPosition)
}

enum CollectionInteractorResult {
    case entities([CollectionCellModel])
    case parameters(CellParametersModel)
    case preview(CollectionCellPreviewModel)
}

protocol CollectionInteractorInputProtocol: class {
    func `do`(_ job: CollectionInteractorJob)
}

protocol CollectionInteractorOutputProtocol: class {
    func present(_ result: CollectionInteractorResult)
}

protocol CollectionRepositoryProtocol {
    func parameters(callback: @escaping (CellParametersModel) -> Void)
    func entities(callback: @escaping ([Any]) -> Void)
    func previews(callback: @escaping(CollectionCellPreviewModel) -> Void)
    func delete(_ model: CollectionCellModel)
    func check(_ model: CollectionCellModel)
    func transfer(_ model: CollectionCellModel, position: ListCellActionPosition)
    func model(_ modelId: String, callback: (Any?) -> Void)
}

final class CollectionInteractor: CollectionInteractorInputProtocol {
    private var subscribedForEntities: Bool = false
    var repository: CollectionRepositoryProtocol?
    var output: CollectionInteractorOutputProtocol?
    private var models: [CollectionCellModel] = []
    var transformer: (([Any]) -> [CollectionCellModel])?
    
    func `do`(_ job: CollectionInteractorJob) {
        switch job {
        case .subscribeForData:
            repository?.parameters(callback: { [weak self] parameters in
                self?.output?.present(.parameters(parameters))
                if self?.subscribedForEntities == false {
                    self?.subscribedForEntities = true
                    self?.subscribeForEntities()
                }
            })
        case .subscribeForPreviewState:
            repository?.previews(callback: { [weak self] in
                self?.output?.present(.preview($0))
            })
        case .model(let modelId, let callback):
            repository?.model(modelId, callback: callback)
        case .action(let model, let action, let position):
            process(model, action: action, position: position)
        }
    }
    
    private func process(_ model: CollectionCellModel, action: ListCellAction, position: ListCellActionPosition) {
        switch action {
        case .delete:
            repository?.delete(model)
        case .check:
            repository?.check(model)
        case .transfer:
            repository?.transfer(model, position: position)
        default: break//return Observable.empty()
        }
    }
    
    private func subscribeForEntities() {
        repository?.entities(callback: { [weak self] in
            let models = self?.transformer?($0) ?? []
            self?.models = models
            self?.output?.present(.entities(models))
        })
    }
}
