//
//  CollectionViewController.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CollectionViewController: UIViewController, UICollectionViewDelegate, CollectionViewProtocol {
    @IBOutlet weak var collectionView: CollectionView!
    
    var presenter: CollectionPresenter?
    var manager: CollectionViewManager?

    private let disposeBag = DisposeBag()
    private var helper: PreviewDragDropHelper!
    private var handler: CollectionHandler!
    var cellRegistrationBlock: ((UICollectionView) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handler = CollectionHandler(collectionView: collectionView, frameView: nil, disposeBag: disposeBag)
        cellRegistrationBlock?(collectionView)
        
        presenter?.perform(.loaded)
        collectionView.delegate = self
        
        self.collectionView
            .collectionDataSource
            .actions
            .map({ CollectionAction.action($0.0, $0.1, $0.2) })
            .subscribe(onNext: { [weak self] in
                self?.presenter?.perform($0)
            })
            .disposed(by: disposeBag)
    }
    
    func populate(_ state: CollectionState) {
        switch state {
        case .manager(let model):
            manager = model
            manager?.collectionView = collectionView
            manager?.viewController = self
            manager?.handler = handler
        case .viewModels(let models): set(models)
        case .parameters(let model): set(model)
        case .preview(let model): manager?.set(preview: model)
        case .processor(let model):
            collectionView.processor = model
        case .dragAvailable:
            helper = PreviewDragDropHelper(view: UIApplication.shared.keyWindow, collectionView: collectionView, delegate: handler)
        }
    }
    
    private func set(_ models: [CollectionCellModel]) {
        manager?.set(.setViewModels(models))
        manager?.set(.apply)
    }
    
    private func set(_ parameters: CellParametersModel) {
        collectionView.collectionDataSource.parameters = parameters
        UIView.animate(CATransaction.animationDuration()) {
            self.collectionView.parameters = parameters
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let model = self.collectionView.collectionDataSource.models[indexPath.row]
        
        presenter?.perform(.selected(model))
    }
    
    func cell(for model: CollectionCellModel) -> CollectionViewCell? {
        if let index = collectionView.collectionDataSource.indexPath(model) {
            return collectionView.cellForItem(at: index) as? CollectionViewCell
        }
        return nil
    }
    
    func frame(_ model: CollectionCellModel) -> CGRect {
        return manager?.frame(model, in: view) ?? .zero
    }
}
