//
//  CollectionWireframe.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum CollectionWireframeRoute {
    case selected(Any?, CollectionCellModel, Bool)
}

protocol CollectionWireframeProtocol {
    func navigate(_ route: CollectionWireframeRoute)
}

final class CollectionWireframe {
    static func createCenter() -> CollectionViewController {
        let side = TimesetType.center
        let viewController = CollectionViewController()
        viewController.cellRegistrationBlock = { collectionView in
            collectionView.register(UINib(nibName: "SeparatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.separatorReuseId)
            collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.emptyReuseId)
            collectionView.register(UINib(nibName: "CenterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.centerCellReuseId)
            collectionView.register(UINib(nibName: "CenterActionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.centerActionCellReuseId)
            collectionView.register(UINib(nibName: "CenterDescriptionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.centerDescriptionCellReuseId)
        }
        
        let presenter = CollectionPresenter(processor: ReversedCollectionViewLayoutProcessor(), dragDropAvailable: true)
        
        let interactor = CollectionInteractor()
        
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.wireframe = TasksWireframe(side: side)
        interactor.transformer = {
            guard let tasks = $0 as? [TaskModel] else { return [] }
            let (m1, m2) = CreateTaskCollectionsUseCase.create(side, tasks)
            let model = CollectionViewModel(side: side, items: m2 + m1)
            return CreateCollectionCellsUseCase.create(model, false)
        }
        
        interactor.output = presenter
        
        let repository = CollectionRepository(side: side)
        repository.dataManager = TasksPreviewDataManager.shared
        interactor.repository = repository
        
        return viewController
    }
    
    static func createLeft() -> CollectionViewController {
        let side = TimesetType.left
        let viewController = CollectionViewController()
        viewController.cellRegistrationBlock = { collectionView in
            collectionView.register(UINib(nibName: "SeparatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.separatorReuseId)
            collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.emptyReuseId)
            collectionView.register(UINib(nibName: "LeftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.leftCellReuseId)
        }
        
        let presenter = CollectionPresenter(processor: ReversedCollectionViewLayoutProcessor(), dragDropAvailable: true)
        
        let interactor = CollectionInteractor()
        
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.wireframe = TasksWireframe(side: side)
        interactor.transformer = {
            guard let tasks = $0 as? [TaskModel] else { return [] }
            let (m1, m2) = CreateTaskCollectionsUseCase.create(side, tasks)
            let model = CollectionViewModel(side: side, items: m1 + m2)
            return CreateCollectionCellsUseCase.create(model, false)
        }
        
        interactor.output = presenter
        
        let repository = CollectionRepository(side: side)
        repository.dataManager = TasksPreviewDataManager.shared
        interactor.repository = repository
        
        return viewController
    }
    
    static func createRight() -> CollectionViewController {
        let side = TimesetType.right
        let viewController = CollectionViewController()
        viewController.cellRegistrationBlock = { collectionView in
            collectionView.register(UINib(nibName: "SeparatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.separatorReuseId)
            collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.emptyReuseId)
            collectionView.register(UINib(nibName: "RightCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.rightCellReuseId)
        }
        
        let presenter = CollectionPresenter(processor: CollectionViewLayoutProcessor(), dragDropAvailable: true)
        
        let interactor = CollectionInteractor()
        
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.wireframe = TasksWireframe(side: side)
        interactor.transformer = {
            guard let tasks = $0 as? [TaskModel] else { return [] }
            let (m1, m2) = CreateTaskCollectionsUseCase.create(side, tasks)
            let model = CollectionViewModel(side: side, items: m1 + m2)
            return CreateCollectionCellsUseCase.create(model, false)
        }
        
        interactor.output = presenter
        
        let repository = CollectionRepository(side: side)
        repository.dataManager = TasksPreviewDataManager.shared
        interactor.repository = repository
        
        return viewController
    }
    
    static func createSimilarTasks(side: TimesetType, userId: String, taskId: String) -> CollectionViewController {
        let viewController = CollectionViewController()
        viewController.cellRegistrationBlock = { collectionView in
            collectionView.register(UINib(nibName: "SeparatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.separatorReuseId)
            collectionView.register(UINib(nibName: "SimilarTaskCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.similarTaskCellReuseId)
        }
        
        let presenter = CollectionPresenter(processor: CollectionViewLayoutProcessor(), dragDropAvailable: false)
        
        let interactor = CollectionInteractor()
        
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        
        let wireframe = SimilarsWireframe()
        wireframe.collectionViewController = viewController
        presenter.wireframe = wireframe
        interactor.transformer = {
            guard let tasks = $0 as? [TaskModel] else { return [] }
            let model = CollectionViewModel(side: side, items: tasks)
            return SimilarTasksTransformer.create(model)
        }
        
        interactor.output = presenter
        
        let repository = SimilarTasksRepository(side: side, userId: userId, taskId: taskId)
        repository.dataManager = SimilarsPreviewDataManager.shared
        interactor.repository = repository
        
        return viewController
    }
    
    static func createSimilarEntities(side: TimesetType, userId: String, taskId: String) -> CollectionViewController {
        let viewController = CollectionViewController()
        viewController.cellRegistrationBlock = { collectionView in
            collectionView.register(UINib(nibName: "SeparatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.separatorReuseId)
            collectionView.register(UINib(nibName: "SimilarTextEntityCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.similarTextEntityCellReuseId)
            collectionView.register(UINib(nibName: "SimilarPhotoEntityCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.similarPhotoEntityCellReuseId)
        }
        
        let presenter = CollectionPresenter(processor: CollectionViewLayoutProcessor(), dragDropAvailable: false)
        
        let interactor = CollectionInteractor()
        
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        
        let wireframe = SimilarsWireframe()
        wireframe.collectionViewController = viewController
        presenter.wireframe = wireframe
        interactor.transformer = {
            guard let models = $0 as? [EntityModel] else { return [] }
            return SimilarEntitiesTransformer.create(models)
        }
        
        interactor.output = presenter
        
        let repository = SimilarEntitiesRepository(side: side, userId: userId, taskId: taskId)
        repository.dataManager = SimilarsPreviewDataManager.shared
        interactor.repository = repository
        
        return viewController
    }
}
