//
//  SimilarsWireframe.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class SimilarsWireframe: CollectionWireframeProtocol {
    var collectionViewController: CollectionViewController?
    
    func navigate(_ route: CollectionWireframeRoute) {
        switch route {
        case .selected(let model, let viewModel, _):
            guard let model = model else { return }
            preview(model, viewModel: viewModel)
        }
    }
    
    private func preview(_ model: Any, viewModel: CollectionCellModel) {
        guard let viewController = collectionViewController else { return }
        var transition = CollectionViewCellTransition(side: .center, model: viewModel, controller: viewController)
        transition.dataManager = SimilarsPreviewDataManager.shared
        guard let controller = PreviewWireframe.createModule(side: .center, model: model, transitions: [transition], showSimilar: false) else { return }
        controller.modalPresentationStyle = .overCurrentContext
        viewController.present(controller, animated: false, completion: nil)
        controller.events
            .subscribe(onNext: { event in
                switch event {
                case .edit: break
                case .exit:
                    controller.exitToList()
                case .dismiss:
                    controller.dismiss()
                case .previewPhoto: break
                }
            })
            .disposed(by: controller.disposeBag)
    }
}
