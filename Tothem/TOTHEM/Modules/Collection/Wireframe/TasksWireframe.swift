//
//  TasksWireframe.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

final class TasksWireframe: CollectionWireframeProtocol {
    var side: TimesetType
    init(side: TimesetType) {
        self.side = side
    }
    
    func navigate(_ route: CollectionWireframeRoute) {
        switch route {
        case .selected(_, let viewModel, let selected):
            _ = viewModel.viewModel.isEmpty ? ListCreateUseCase().execute(side: side, selected: selected) : ListPreviewUseCase().execute(viewModel, side: side)
        }
    }
}
