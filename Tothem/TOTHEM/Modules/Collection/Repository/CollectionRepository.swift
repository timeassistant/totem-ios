//
//  CollectionRepository.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct CollectionCellPreviewModel {
    var model: CollectionCellModel
    var fromView: UIView?
    var fromFrame: CGRect?
}

final class CollectionRepository: CollectionRepositoryProtocol {
    private var presenter: CellPresenter!
    private var parameters: Observable<CellParametersModel>!
    private var models: Observable<[TaskModel]>!
    private var tasks: [TaskModel] = []
    private var disposeBag = DisposeBag()
    var dataManager: CollectionPreviewDataManagerProtocol?
    
    init(side: TimesetType) {
        presenter = CellPresenter(side: side, checked: false)
        parameters = presenter.state?.asObservable() ?? Observable.just(CellParametersModel.default)
        models = GetTasksUseCase.shared.execute(side: side)
            .map({ $0 as? [TaskModel] })
            .skipNil()
    }
    
    func parameters(callback: @escaping (CellParametersModel) -> Void) {
        self.parameters
            .subscribe(onNext: callback)
            .disposed(by: disposeBag)
    }
    
    func entities(callback: @escaping ([Any]) -> Void) {
        self.models
            .do(onNext: { [weak self] in
                self?.tasks = $0
            })
            .subscribe(onNext: callback)
            .disposed(by: disposeBag)
    }
    
    func previews(callback: @escaping(CollectionCellPreviewModel) -> Void) {
        dataManager?.subscribe(callback: callback)
    }
    
    func delete(_ model: CollectionCellModel) {
        ListDeleteUseCase.shared
            .execute(model)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func check(_ model: CollectionCellModel) {
        ListCheckUseCase.shared
            .execute(model)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func transfer(_ model: CollectionCellModel, position: ListCellActionPosition) {
        var side: TimesetType
        let task = GetTasksUseCase.shared.task(id: model.viewModel.modelId)
        if task?.mode == .combined {
            side = position == .right ? .left : .right
        } else {
            side = .center
        }
        
        return ChangeSidesUseCase.shared
            .execute(model, side: side)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func model(_ modelId: String, callback: (Any?) -> Void) {
        callback(tasks.first { $0.id == modelId })
    }
}
