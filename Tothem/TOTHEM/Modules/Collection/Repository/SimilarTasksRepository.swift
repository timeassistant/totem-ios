//
//  SimilarTasksRepository.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class SimilarTasksRepository: CollectionRepositoryProtocol {
    private var presenter: CellPresenter!
    private var parameters: Observable<CellParametersModel>!
    private var disposeBag = DisposeBag()
    private let dataSource = NetworkDataSource()
    private var userId: String
    private var taskId: String
    private var models: [TaskModel] = []
    
    var dataManager: CollectionPreviewDataManagerProtocol?
    
    private var observer = BehaviorSubject<[TaskModel]>(value: [])
    
    init(side: TimesetType, userId: String, taskId: String) {
        self.userId = userId
        self.taskId = taskId
        
        presenter = CellPresenter(side: .center, checked: false)
        parameters = presenter.state?.asObservable() ?? Observable.just(CellParametersModel.default)
    }
    
    func parameters(callback: @escaping (CellParametersModel) -> Void) {
        self.parameters
            .subscribe(onNext: callback)
            .disposed(by: disposeBag)
    }
    
    func entities(callback: @escaping ([Any]) -> Void) {
        observer.asObserver()
            .do(onNext: { models in
                callback(models)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        similarTasks(userId: userId, taskId: taskId)
    }
    
    func previews(callback: @escaping(CollectionCellPreviewModel) -> Void) {
        dataManager?.subscribe(callback: callback)
    }
    
    private func similarTasks(userId: String, taskId: String) {
        let endpoint = Endpoint.UsersEndpoint.similar(userId, taskId)
        dataSource.object(forEndpoint: endpoint)
            .do(onNext: { object in
                let taskObjects = (object["data"] as? JSONArray) ?? []
                let tasks = taskObjects.map({ TaskModel($0) })
                self.models = tasks
                self.sendNewModels()
            }, onError: { error in
                
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func delete(_ model: CollectionCellModel) {
        
    }
    
    func check(_ model: CollectionCellModel) {
        
    }
    
    func transfer(_ model: CollectionCellModel, position: ListCellActionPosition) {
        guard let task = models.first(where: {
            $0.id == model.viewModel.modelId
        }) else { return }
        FollowedDataManager.shared.add(task)
        sendNewModels()
    }
    
    private func sendNewModels() {
        let tasks = FollowedDataManager.shared.tasks
        let filtered = self.models.filter { model in
            return !tasks.contains { task in
                model.id == task.id && model.uid == task.uid
            }
        }
        observer.onNext(filtered)
    }
    
    func model(_ modelId: String, callback: (Any?) -> Void) {
        callback(models.first { $0.id == modelId })
    }
}
