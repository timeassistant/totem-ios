//
//  CollectionPresenter.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum CollectionAction {
    case loaded
    case selected(CollectionCellModel)
    case action(CollectionCellModel, ListCellAction, ListCellActionPosition)
}

enum CollectionState {
    case manager(CollectionViewManager)
    case viewModels([CollectionCellModel])
    case parameters(CellParametersModel)
    case preview(CollectionCellPreviewModel)
    case processor(CollectionViewLayoutProcessorProtocol)
    case dragAvailable
}

protocol CollectionViewProtocol: class {
    func populate(_ state: CollectionState)
}

final class CollectionPresenter: CollectionInteractorOutputProtocol {
    var interactor: CollectionInteractorInputProtocol?
    weak var view: CollectionViewProtocol?
    var processor: CollectionViewLayoutProcessorProtocol
    var wireframe: CollectionWireframeProtocol?
    var manager = CollectionViewManager()
    var dragDropAvailable: Bool
    init(processor: CollectionViewLayoutProcessorProtocol, dragDropAvailable: Bool) {
        self.processor = processor
        self.dragDropAvailable = dragDropAvailable
    }
    
    func perform(_ action: CollectionAction) {
        switch action {
        case .loaded:
            view?.populate(.processor(processor))
            view?.populate(.manager(manager))
            if dragDropAvailable {
                view?.populate(.dragAvailable)
            }
            interactor?.do(.subscribeForData)
            interactor?.do(.subscribeForPreviewState)
        case .selected(let model):
            guard model.viewModel.isEditable else { return }
            interactor?.do(.model(model.identifier, { task in
                self.wireframe?.navigate(.selected(task, model, false))
            }))
        case .action(let model, let action, let position):
            interactor?.do(.action(model, action, position))
        }
    }
    
    func present(_ result: CollectionInteractorResult) {
        switch result {
        case .entities(let models):
            guard !manager.preview else { return }
            DispatchQueue.main.async {
                self.view?.populate(.viewModels(models))
            }
        case .parameters(let parameters):
            guard !manager.preview else { return }
            DispatchQueue.main.async {
                self.view?.populate(.parameters(parameters))
            }
        case .preview(let model):
            DispatchQueue.main.async {
                self.view?.populate(.preview(model))
            }
        }
    }
}
