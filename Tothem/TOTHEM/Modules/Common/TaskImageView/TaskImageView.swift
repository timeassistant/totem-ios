//
//  TaskImageView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class TaskImageView: XibSubview {
    private let disposeBag = DisposeBag()
    private var pictureDisposeBag: DisposeBag!

    @IBOutlet weak var imageView: UIImageView?

    var image: UIImage? {
        return self.imageView?.image
    }

    deinit {
        print("deinit TaskImageView")
    }

    override func configureView() {
        super.configureView()
        MovingConfigurator.shared
            .normalizedValue
            .map({ CGFloat($0) })
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.updateScale($0)
            })
            .disposed(by: disposeBag)
    }

    func reset() {
        pictureDisposeBag = nil
        imageView?.image = nil
    }

    func set(_ observable: Observable<UIImage>) {
        pictureDisposeBag = DisposeBag()

        if let imageView = imageView {
            imageView.image = nil
            observable
                .catchErrorJustReturn(UIImage())
                .bind(to: imageView.rx.image)
                .disposed(by: pictureDisposeBag)
        }
    }

    func updateScale(_ value: CGFloat) {
        let scale = CGFloat(1 + value/2)
        UIView.animate(CATransaction.animationDuration()) {
            self.imageView?.transform = CGAffineTransform(scaleX: scale, y: scale)
            self.transform = CGAffineTransform(scaleX: CGFloat(1 - value/10), y: CGFloat(1 - value/10))
        }
    }
}
