//
//  PreviewWireframe.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class PreviewWireframe {
    static func createModule(side: TimesetType, model: Any, transitions: [CollectionViewCellTransition], showSimilar: Bool = true) -> PreviewContainerController? {
        guard let model = model as? TaskModel else { return nil }
        let controller = PreviewContainerController()
        controller.showSimilar = showSimilar
        controller.setup(with: side, model: model, transitions: transitions)
        
        let presenter = PreviewPresenter()
        let interactor = PreviewInteractor()
        interactor.model = model
        
        let repository = PreviewRepository()
        
        presenter.interactor = interactor
        controller.preseter = presenter
        interactor.output = presenter
        interactor.repository = repository
        
        presenter.view = controller
        
        return controller
    }
}
