//
//  SimilarEntityCellViewModel.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct SimilarEntityCellViewModel: CollectionCellViewModelProtocol {
    var modelId: String = UUID().uuidString
    var isEmpty: Bool = true
    var isEditable: Bool = true
    var contentIdentifier: String = ""
    var position: Double = .greatestFiniteMagnitude
    var value: String = UUID().uuidString
    var photo: Observable<UIImage>?
    var entity: EntityModel
    
    init(_ entity: EntityModel, imageLoader: (String?, String) -> Observable<UIImage>) {
        self.entity = entity
        modelId = entity.id
        isEmpty = false
        contentIdentifier = entity.value
        switch entity.type {
        case .bullet:
            value = "\(Symbol.bullet.rawValue) \(entity.value)"
        case .photo:
            photo = imageLoader(entity.uid, entity.value)
        default:
            value = entity.value
        }
    }
}
