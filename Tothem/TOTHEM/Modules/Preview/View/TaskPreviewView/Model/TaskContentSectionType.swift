//
//  TaskContentSectionType.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

enum TaskContentSectionType {
    case title
    case text
    case photos
    case checklist
    case none

    static func type(mode: TaskContentMode, side: TimesetType, section: Int) -> TaskContentSectionType {
        switch mode {
        case .list:
            return listType(side: side, section: section)
        case .preview:
            return previewType(side: side, section: section)
        }
    }

    private static func listType(side: TimesetType, section: Int) -> TaskContentSectionType {
        switch side {
        case .center:
            switch section {
            case 0: return .title
            case 1: return .text
            default: return .none
            }
        case .left:
            switch section {
            case 0: return .title
            case 1: return .photos
            default: return .none
            }
        case .right:
            switch section {
            case 0: return .title
            case 1: return .checklist
            default: return .none
            }
        }
    }

    private static func previewType(side: TimesetType, section: Int) -> TaskContentSectionType {
        switch side {
        case .center:
            switch section {
            case 0: return .title
            case 1: return .text
            case 2: return .photos
            case 3: return .checklist
            default: return .none
            }
        case .left:
            switch section {
            case 0: return .title
            case 1: return .photos
            default: return .none
            }
        case .right: return .none
        }
    }
}
