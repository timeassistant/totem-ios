//
//  PreviewContainerController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum PreviewControllerEvent {
    case exit
    case dismiss
    case edit(TaskViewEditAction?, TimesetType)
    case previewPhoto([EntityModel], Int)
}

enum PreviewSide {
    case center
    case left
    case right
    case similarLeft
    case similarRight
    
    var modelSide: TimesetType {
        switch self {
        case .center: return .center
        case .left, .similarLeft: return .left
        case .right, .similarRight: return .right
        }
    }
}

class PreviewContainerController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, PreviewViewProtocol {
    var preseter: PreviewPresenter?
    var disposeBag = DisposeBag()

    @IBOutlet weak var buttonsContainer: UIView!
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var rightView: UIView!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBackgroundView: UIView!
    @IBOutlet weak var titleVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!

    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var events = PublishSubject<PreviewControllerEvent>()
    var leftController = TaskCollectionPreviewController()
    var centerController = TaskCollectionPreviewController()
    var rightController = TaskCollectionPreviewController()
    
    private var leftSimilarViewController: CollectionViewController!
    
    private var rightSimilarViewController: CollectionViewController!
    
    private var side: PreviewSide = .center
    
    private var model: TaskModel! {
        didSet {
            leftController.side = .left
            centerController.side = .center
            rightController.side = .right
            leftController.model = model
            centerController.model = model
            rightController.model = model
            if isViewLoaded {
                updateMatchingButtonsVisibility()
            }
        }
    }

    var beginPanningPoint = CGPoint.zero
    var panningPoint = CGPoint.zero
    
    deinit {
        print("deinit preview container controller")
    }
    
    var showSimilar = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        leftController.loadViewIfNeeded()
        rightController.loadViewIfNeeded()
        centerController.loadViewIfNeeded()
        
        [blueButton, redButton].forEach {
            $0?.layer.borderWidth = 1
            $0?.layer.borderColor = UIColor.black.cgColor
        }
        
        buttonsContainer.alpha = 0

        var gesture: UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapEdit(gesture:)))
        titleBackgroundView.addGestureRecognizer(gesture)
       
        gesture = UIPanGestureRecognizer(target: self, action: #selector(panTitle(gesture:)))
        titleBackgroundView.addGestureRecognizer(gesture)

        titleLabel?.text = model.title
        setupTitleHeight()

        centerController.events
            .bind(to: events)
            .disposed(by: disposeBag)
        centerController.view.fill(in: centerView)
        
        if side == .center {
            if showSimilar {
                leftSimilarViewController = CollectionWireframe.createSimilarTasks(side: .left, userId: model.uid ?? "", taskId: model.id)
                leftSimilarViewController.view.fill(in: leftView)
                
                rightSimilarViewController = CollectionWireframe.createSimilarEntities(side: .right, userId: model.uid ?? "", taskId: model.id)
                rightSimilarViewController.view.fill(in: rightView)
                
                FollowedDataManager.shared
                    .tasksObserver
                    .observeOn(MainScheduler.asyncInstance)
                    .subscribe(onNext: { [weak self] in
                        self?.centerController.setAttached($0)
                    })
                    .disposed(by: disposeBag)
                
                FollowedDataManager.shared
                    .entitiesObserver
                    .observeOn(MainScheduler.asyncInstance)
                    .subscribe(onNext: { [weak self] in
                        self?.centerController.setAttached($0)
                    })
                    .disposed(by: disposeBag)
            }
        } else {
            leftController.events
                .bind(to: events)
                .disposed(by: disposeBag)
            leftController.view.fill(in: leftView)
            
            rightController.events
                .bind(to: events)
                .disposed(by: disposeBag)
            rightController.view.fill(in: rightView)
        }

        attachTitleView()

        centerController.setInitialTransparency()
        leftController.setInitialTransparency()
        rightController.setInitialTransparency()

        if side == .center {
            centerController.view.alpha = 1
            rightController.view.alpha = 0
            leftController.view.alpha = 0
        }
        
        preseter?.perform(.loaded)
    }

    private var initial = true
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if initial {
            initial = false
            setInitialPreviewFrames()
        }
    }

    func setup(with side: TimesetType, model: TaskModel, transitions: [CollectionViewCellTransition]) {
        self.model = model
        switch side {
        case .center: self.side = .center
        case .left: self.side = .left
        case .right: self.side = .right
        }
        
        if let transition = transitions.first(where: { $0.side == .right }) {
            rightController.transition = transition
        }

        if let transition = transitions.first(where: { $0.side == .left }) {
            leftController.transition = transition
        }

        if let transition = transitions.first(where: { $0.side == .center }) {
            centerController.transition = transition
        }
    }
    
    private func updateMatchingButtonsVisibility() {
        [blueButton, redButton].forEach {
            $0?.isHidden = model.mode == .combined
        }
    }
    
    private func setInitialPreviewFrames() {
        if side == .center {
            centerController.setInitialPreviewFrames(view: nil)
        } else {
            rightController.setInitialPreviewFrames(view: centerController.view)
            leftController.setInitialPreviewFrames(view: centerController.view)
        }
        
        setOffset(to: side)

        UIView.animate(CATransaction.animationDuration()) {
            self.buttonsContainer.alpha = 1
        }
    }

    private var titleHeightValue: CGFloat = 50
    private func setupTitleHeight() {
        let titleSize = model.title
            .size(width: UIScreen.main.bounds.width - 20, font: UIFont.systemFont(ofSize: 23))
        titleHeightValue = max(50, titleSize.height + 24)
        titleHeight.constant = titleHeightValue
        leftController.titleInset = 10 + titleHeightValue
        centerController.titleInset = 10 + titleHeightValue
        rightController.titleInset = 10 + titleHeightValue
    }

    private func attachTitleView() {
        titleBackgroundView.constraintRemove(titleVerticalConstraint)
        if side == .center {
            titleVerticalConstraint = centerController.attachTitle(view: titleBackgroundView)
        } else if side == .right {
            titleVerticalConstraint = rightController.attachTitle(view: titleBackgroundView)
        } else if side == .left {
            titleVerticalConstraint = leftController.attachTitle(view: titleBackgroundView)
        } else {
            titleVerticalConstraint = view.constraintTop(titleBackgroundView, constant: UIApplication.shared.statusBarFrame.height)
        }
    }

    private func setOffset(to side: PreviewSide, changeSide: Bool = true) {
        var offset: CGPoint = .zero
        let center = GetTaskSidesUseCase.shared.execute(model).contains(.center)
        
        switch side {
        case .left, .similarLeft: offset.x = leftView.frame.minX
        case .center: offset.x = centerView.frame.minX
        case .right, .similarRight: offset.x = rightView.frame.minX
        }

        scrollView.contentOffset = offset
        
        if showSimilar && changeSide && !center {
            ChangeSideUseCase.shared.change.on(.next((offset.x)))
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if GetTaskSidesUseCase.shared.execute(model).contains(.center) {
            scrollWithFixedBackground()
        } else {
            scrollWithChangingBackground()
        }
    }

    private func scrollWithChangingBackground() {
        var newSide = side
        if side == .left &&
        scrollView.contentOffset.x > leftView.frame.midX {
            newSide = .right
        }

        if side == .right &&
            scrollView.contentOffset.x < centerView.frame.midX {
            newSide = .left
        }

        if newSide != side {
            side = newSide
            //scrollView.delegate = nil
            attachTitleView()
            UIView.animate(CATransaction.animationDuration()) {
                self.view.layoutIfNeeded()
                self.setOffset(to: newSide)
            }
        } else {
            ChangeSideUseCase.shared
                .change.on(.next((scrollView.contentOffset.x)))
        }
    }

    private func scrollWithFixedBackground() {
        if side == .similarLeft && // similarLeft -> center
            scrollView.contentOffset.x > leftView.frame.midX {
            side = .center
            print("similarLeft -> center")
            centerController.setTransition(frame: centerController.previewFrame)
            attachTitleView()
            
            UIView.animate(CATransaction.animationDuration()) {
                self.leftView.alpha = 0
                self.centerController.view.alpha = 1
                self.view.layoutIfNeeded()
            }
        } else if side == .center && // center -> similarRight
            scrollView.contentOffset.x > centerView.frame.midX {
            side = .similarRight
            print("center -> similarRight")
            centerController.setTransition(frame: view.bounds)
            attachTitleView()

            UIView.animate(CATransaction.animationDuration()) {
                self.centerController.view.alpha = 0
                self.rightView.alpha = 1
                self.view.layoutIfNeeded()
            }
        } else if side == .similarRight && // center <- similarRight
            scrollView.contentOffset.x < centerView.frame.midX {
            side = .center
            print("similarRight -> center")
            centerController.setTransition(frame: centerController.previewFrame)
            attachTitleView()

            UIView.animate(CATransaction.animationDuration()) {
                self.rightView.alpha = 0
                self.centerController.view.alpha = 1
                self.view.layoutIfNeeded()
            }
        } else if side == .center && // similarLeft <- center
            scrollView.contentOffset.x < leftView.frame.midX {
            side = .similarLeft
            print("similarLeft <- center")
            centerController.setTransition(frame: view.bounds)
            attachTitleView()

            UIView.animate(CATransaction.animationDuration()) {
                self.leftView.alpha = 1
                self.centerController.view.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }

    func exitToEdit() {
        guard let window = UIApplication.shared.keyWindow else { return }
        scrollView.fill(in: window)
        
        UIView.animate(withDuration: 1, animations: {
            self.rightView.alpha = 0
            self.leftView.alpha = 0
            self.centerView.alpha = 0
            self.buttonsContainer.alpha = 0
        }, completion: { _ in
            self.scrollView.removeFromSuperview()
        })

        events.on(.next(.dismiss))
    }

    func exitToList() {
        rightController.releaseContentToInitialFrame()
        leftController.releaseContentToInitialFrame()
        centerController.releaseContentToInitialFrame()

        UIView.animate(CATransaction.animationDuration(), animations: {
            self.buttonsContainer.alpha = 0
        })
    }

    func dismiss() {
        // FIXME: don't know what is it for
//        rightController.transition?.controller.set(state: .previewDismissed)
//        leftController.transition?.controller.set(state: .previewDismissed)
//        centerController.transition?.controller.set(state: .previewDismissed)
        dismiss(animated: false, completion: nil)
    }

    @objc private func tapEdit(gesture: UITapGestureRecognizer) {
        events.on(.next(.edit(nil, side.modelSide)))
    }

    @objc private func panTitle(gesture: UIPanGestureRecognizer) {
        switch side.modelSide {
        case .left: leftController.panContentView(gesture: gesture)
        case .center: centerController.panContentView(gesture: gesture)
        case .right: rightController.panContentView(gesture: gesture)
        }
    }
    
    @objc private func pan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began: beginPanningPoint = scrollView.contentOffset
        case .changed:
            guard scrollView.delegate != nil else { return }
            var contentOffset = beginPanningPoint
            contentOffset.x -= gesture.translation(in: view).x
            if contentOffset.x < 0 {
                contentOffset.x = 0
            }

            if contentOffset.x > 2 * scrollView.bounds.width {
                contentOffset.x = 2 * scrollView.bounds.width
            }

            if model.side == .center, model.photos.count == 0,
                contentOffset.x < scrollView.bounds.width {
                contentOffset.x = scrollView.bounds.width
            }

            if model.side == .center, model.bullets.count == 0,
                contentOffset.x > scrollView.bounds.width {
                contentOffset.x = scrollView.bounds.width
            }

            if model.side == .right, model.photos.count == 0,
                contentOffset.x < 2 * scrollView.bounds.width {
                contentOffset.x = 2 * scrollView.bounds.width
            }

            if model.side == .left, model.bullets.count == 0,
                contentOffset.x > 0 {
                contentOffset.x = 0
            }

            scrollView.contentOffset = contentOffset
        case .ended:
            self.scrollView.delegate = self
            UIView.animate(CATransaction.animationDuration()) {
                self.setOffset(to: self.side, changeSide: false)
            }
        default: break
        }
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let pan = gestureRecognizer as? UIPanGestureRecognizer else { return true }
        let velocity = pan.velocity(in: view)
        return abs(velocity.x) > abs(velocity.y)
    }
    
    // MARK: button actions
    @IBAction func shareButton(_ sender: UIButton) {
        guard !self.model.isContentEmpty else { return }
        let text = self.model.title
            + "\n\n"
            + self.model.descriptionText
            + "\n\n"
            + self.model.bullets
                .map { " \(Symbol.bullet.character) " + $0.value }
                .joined(separator: "\n")
        let shareAlert = UIActivityViewController(activityItems: [text], applicationActivities: [])
        self.present(shareAlert, animated: true, completion: nil)
    }
    
    @IBAction func editButton(_ sender: UIButton) {
        events.onNext(.edit(nil, side.modelSide))
    }
    
    func populate(state: PreviewViewState) {
        
    }
}
