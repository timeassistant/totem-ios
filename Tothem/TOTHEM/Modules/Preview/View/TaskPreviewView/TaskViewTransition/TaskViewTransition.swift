//
//  TaskViewTransition.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct CollectionViewCellTransition {
    var side: TimesetType
    var model: CollectionCellModel
    var controller: CollectionViewController
    var initialFrame: CGRect = .zero
    var dataManager: CollectionPreviewDataManagerProtocol?
    
    init(side: TimesetType, model: CollectionCellModel, controller: CollectionViewController) {
        self.side = side
        self.model = model
        self.controller = controller
        initialFrame = controller.frame(model)
    }

    func set(frame: CGRect, from view: UIView) {
        let model = CollectionCellPreviewModel(model: self.model, fromView: view, fromFrame: frame)
        dataManager?.set(selected: model)
    }

    func exit() {
        let model = CollectionCellPreviewModel(model: self.model, fromView: nil, fromFrame: nil)
        dataManager?.set(selected: model)
    }
    
    func translateCellVerticaly(by value: CGFloat) {
        guard let cell = controller.cell(for: model) else { return }
        cell.superview?.bringSubviewToFront(cell)
        cell.apply(panningTransform: CGAffineTransform(translationX: 0, y: value))
    }
}
