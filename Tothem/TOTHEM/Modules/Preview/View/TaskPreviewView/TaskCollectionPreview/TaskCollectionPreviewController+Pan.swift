//
//  TaskCollectionPreviewController+Pan.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

extension TaskCollectionPreviewController {
    @objc
    func panScrollView(gesture: UIPanGestureRecognizer) {
        guard let scrollView = collectionView else { return }
        switch gesture.state {
        case .began:
            let panOnBottom = scrollView.contentSize.height - scrollView.bounds.height - scrollView.contentOffset.y < 5 && gesture.velocity(in: nil).y < -100
            let panOnTop = scrollView.contentOffset.y - -1 * scrollView.contentInset.top < 5 && gesture.velocity(in: nil).y > 100
            
            guard panOnBottom || panOnTop else {
                return
            }
            
            if beginPanningPoint == nil {
                beginPanningPoint = gesture.location(in: view)
            }
        case .changed:
            guard beginPanningPoint != nil else { return }
            var diff: CGFloat = 0
            if scrollView.contentOffset.y <= -1 * scrollView.contentInset.top {
                diff = -1 * (scrollView.contentOffset.y + scrollView.contentInset.top)
            } else {
                diff = scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.bounds.height
            }
            transition?.translateCellVerticaly(by: diff)
            titleView?.transform = CGAffineTransform(translationX: 0, y: diff)
        case .cancelled:
            restoreFromPaning()
            beginPanningPoint = nil
        case .ended:
            guard let beginPanningPoint = beginPanningPoint else { return }
            let diff = beginPanningPoint.y - gesture.location(in: view).y
            if abs(diff) > panToExitOffset {
                events.on(.next(.exit))
            } else {
                restoreFromPaning()
            }
            self.beginPanningPoint = nil
        default: break
        }
    }

    @objc
    func panContentView(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            contentView.clipsToBounds = false
            collectionView?.clipsToBounds = false
            beginPanningPoint = gesture.location(in: view)
        case .changed:
            let diff = gesture.location(in: view).y - beginPanningPoint!.y
            transition?.translateCellVerticaly(by: diff)
            contentView.transform = CGAffineTransform(translationX: 0, y: diff)
            titleView?.transform = CGAffineTransform(translationX: 0, y: diff)
        case .cancelled:
            restoreFromPaning()
        case .ended:
            let diff = beginPanningPoint!.y - gesture.location(in: view).y
            if abs(diff) > panToExitOffset {
                events.on(.next(.exit))
            } else {
                restoreFromPaning()
            }
        default: break
        }
    }

    private func restoreFromPaning() {
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.contentView.transform = .identity
            self.titleView?.transform = .identity
            self.transition?.translateCellVerticaly(by: 0)
        }, completion: { _ in
            self.contentView.clipsToBounds = true
            self.collectionView?.clipsToBounds = true
        })
    }
}
