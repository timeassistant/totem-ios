//
//  RightPreviewLayoutModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol PreviewLayoutModelProtocol {
    var collectionViewLayoutModel: CollectionViewLayoutModel { get }
}

struct RightPreviewLayoutModel: PreviewLayoutModelProtocol {
    var sections: [PreviewSection]
    
    var collectionViewLayoutModel = CollectionViewLayoutModel()
    var cellHeight: CGFloat = 60
    var checkboxWidth: CGFloat = 60
    var sectionSpace: CGFloat = 10
    var cellSpace: CGFloat = 0
    var spacing: CGFloat = 10
    var columns: CGFloat = 2
    
    init(sections: [PreviewSection], width: CGFloat, dragging: IndexPath? = nil) {
        self.sections = sections
        
        collectionViewLayoutModel.contentWidth = width
        if dragging != nil {
            sectionSpace = 15
            cellHeight = 50
            cellSpace = 15
        }
        
        for (section, sectionModel) in sections.enumerated() {
            for (item, itemModel) in sectionModel.items.enumerated() {
                let indexPath = IndexPath(item: item, section: section)
                switch itemModel.type {
                case .statistics:
                    add(indexPath, height: cellHeight/3)
                case .suggestLoader:
                    add(indexPath, height: cellHeight)
                case .suggestNote, .suggestActions:
                    add(indexPath, height: 3 * cellHeight)
                case .checkbox, .text, .photo:
                    add(indexPath, height: cellHeight)
                case .space:
                    let height = indexPath.row > 0 ? cellSpace : sectionSpace
                    add(indexPath, height: height)
                case .attachedTask, .attachedEntity, .separator:
                    break
                }
            }
        }
    }
    
    private mutating func add(_ indexPath: IndexPath, height: CGFloat) {
        let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        let width = collectionViewLayoutModel.contentWidth
        let y = collectionViewLayoutModel.contentHeight
        let x = indexPath.row == 0 ? 0 : checkboxWidth
        let origin = CGPoint(x: x, y: y)
        let size = CGSize(width: width - x, height: height)
        attribute.frame = CGRect(origin: origin, size: size)
        
        var attributes = collectionViewLayoutModel.attributes
        attributes.append(attribute)
        collectionViewLayoutModel.attributes = attributes
        
        var contentHeight = collectionViewLayoutModel.contentHeight
        contentHeight = max(attribute.frame.maxY, contentHeight)
        collectionViewLayoutModel.contentHeight = contentHeight
    }
}
