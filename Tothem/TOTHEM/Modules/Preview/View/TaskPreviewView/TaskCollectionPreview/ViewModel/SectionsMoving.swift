//
//  SectionsMoving.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class SectionsMoving {
    var path: String = "path"
    weak var task: TaskModel?
    var sections: [PreviewSection] = []

    func setChecked(_ item: PreviewItem) {
        task?.items[item.id]?.checked = !item.checked
    }
    
    func prevId(indexPath: IndexPath) -> String? {
        if indexPath.section == 0 && indexPath.row == 0 {
            return nil
        } else if indexPath.row == 0 {
            return sections[indexPath.section - 1].items[0].id
        } else {
            return sections[indexPath.section].items[indexPath.row - 1].id
        }
    }
    
    func move(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceId = sections[sourceIndexPath.section]
            .items[sourceIndexPath.row].id
        
        let prev = prevId(indexPath: destinationIndexPath)
        
        if let prev = prev, destinationIndexPath.row > 0 {
            task?.insertChild(sourceId, after: prev)
        } else {
            task?.insert(sourceId, after: prev)
        }
    }
}
