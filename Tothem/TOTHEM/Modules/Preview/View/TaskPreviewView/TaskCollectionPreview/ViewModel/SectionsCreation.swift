//
//  SectionsCreation.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class SectionsCreation {
    private var sections: [PreviewSection] = []
    private var items: [PreviewItem] = []

    var photoReuse = "LeftPreviewCell"
    var checkReuse = "RightPreviewCell"
    var textReuse = "RightPreviewCell"
    private let emptyReuse = "PreviewEmptyCell"
    
    private let suggestLoaderReuse = "PreviewSuggestLoaderCell"
    private let attachedTaskReuse = "SimilarTaskCell"
    private let attachedPhotoReuse = "PreviewAttachedPhotoCell"
    private let attachedTextReuse = "PreviewAttachedTextCell"
    private let separatorReuse = "PreviewSeparatorCell"
    
    func build(_ model: TaskModel, _ entities: [EntityProtocol], sortByOrder: Bool = false) -> [PreviewSection] {
        var items: [String: (PreviewType, EntityProtocol)] = [:]

        var checked: [String] = []
        var child: [String] = []

        model.bullets.forEach {
            if $0.checked {
                checked.append($0.id)
            }
            child.append(contentsOf: $0.child)
            items[$0.id] = (PreviewType.checkbox, $0)
        }

        model.photos.forEach {
            if $0.checked {
                checked.append($0.id)
            }
            child.append(contentsOf: $0.child)
            items[$0.id] = (PreviewType.photo, $0)
        }

        model.texts.forEach {
            if $0.checked {
                checked.append($0.id)
            }
            child.append(contentsOf: $0.child)
            items[$0.id] = (PreviewType.text, $0)
        }

        let roots = model.order.filter({ !child.contains($0) })

        var order: [String] = []
        
        if sortByOrder {
            order = model.order
                .filter({ items[$0]?.1.checked == false && roots.contains($0) })
            order.append(contentsOf: model.order.filter({
                items[$0]?.1.checked == true && roots.contains($0)
            }))
        } else {
            order = entities
                .filter({ !$0.checked && roots.contains($0.id) })
                .map({ $0.id })
            
            order.append(contentsOf: roots.filter({
                !order.contains($0) && !checked.contains($0)
            }))
            
            order.append(contentsOf: entities
                .filter({ $0.checked && roots.contains($0.id) }).map({ $0.id }))
            
            order.append(contentsOf: roots.filter({
                !order.contains($0) && checked.contains($0)
            }))
        }

        sections = []
        
        addEmptySection()

        for id in order {
            guard let (type, entity) = items[id] else { continue }
            switch type {
            case .text:
                addTextSection(entity)
            case .checkbox:
                addSection(items, entity, checkReuse, .checkbox)
            case .photo:
                addSection(items, entity, photoReuse, .photo, uid: model.uid)
            default: break
            }
            addEmptySection()
        }

        return sections
    }

    private func addTextSection(_ entity: EntityProtocol) {
        self.items = []
        let model = PreviewItem(reuseId: textReuse, id: entity.id, type: .text, value: entity.value, checked: entity.checked)
        sections.append(PreviewSection(id: model.id, items: [model]))
    }

    private func addSection(_ items: [String: (PreviewType, EntityProtocol)], _ entity: EntityProtocol, _ reuseId: String, _ type: PreviewType, uid: String? = nil) {
        self.items = []
        let checked = entity.checked
        let model = PreviewItem(reuseId: reuseId, id: entity.id, uid: uid, type: type, value: entity.value, checked: checked)

        self.items.append(model)

        for id in entity.child {
            guard let (type, model) = items[id] else { continue }
            var item = PreviewItem(reuseId: emptyReuse, type: .space, checked: checked, child: true)
            self.items.append(item)

            let value = model.value
            let id = model.id

            switch type {
            case .text:
                item = PreviewItem(reuseId: textReuse, id: id, type: .text, value: value, checked: checked, child: true)
                self.items.append(item)
            case .checkbox:
                item = PreviewItem(reuseId: checkReuse, id: id, type: .checkbox, value: value, checked: checked, child: true)
                self.items.append(item)
            case .photo:
                item = PreviewItem(reuseId: photoReuse, id: id, type: .photo, value: value, checked: checked, child: true)
                self.items.append(item)
            default: break
            }
        }

        let item = PreviewItem(reuseId: emptyReuse, type: .space, checked: checked)
        self.items.append(item)

        sections.append(PreviewSection(id: model.id, items: self.items))
    }

    private func addEmptySection() {
        items = []
        let model = PreviewItem(reuseId: emptyReuse, type: .space)
        sections.append(PreviewSection(id: model.id, items: [model]))
    }
    
    func build(_ attachedModels: [TaskModel]) -> [PreviewSection] {
        
        var sections: [PreviewSection] = []
        
        var model = PreviewItem(reuseId: separatorReuse, type: .separator)
        
        if attachedModels.count > 0 {
            sections.append(PreviewSection(id: model.id, items: [model]))
        }
        
        let viewModel = CollectionViewModel(side: .center, items: attachedModels)
        let items = SimilarTasksTransformer.create(viewModel)
        items.forEach { item in
            model = PreviewItem(reuseId: item.reuseId, type: .attachedTask)
            model.value = item
            sections.append(PreviewSection(id: model.id, items: [model]))
        }
        
        return sections
    }
    
    func build(_ attachedEntities: [EntityModel]) -> [PreviewSection] {
        let items = SimilarEntitiesTransformer.create(attachedEntities)
        
        var sections: [PreviewSection] = []
        if attachedEntities.count > 0 {
            let model = PreviewItem(reuseId: separatorReuse, type: .separator)
            sections.append(PreviewSection(id: model.id, items: [model]))
        }
        
        items.forEach {
            var model = PreviewItem(reuseId: $0.reuseId, type: .attachedEntity)
            model.value = $0
            sections.append(PreviewSection(id: model.id, items: [model]))
        }
        
        return sections
    }
}
