//
//  RightPreviewViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct RightPreviewViewModel: PreviewViewModelProtocol {
    var task: TaskModel?

    private var creator = SectionsCreation()
    private var moving = SectionsMoving()

    var sections: [PreviewSection] = []
    private var items: [PreviewItem] = []
    let path = TimesetType.right.path

    init(_ model: TaskModel) {
        self.task = model

        creator.textReuse = "RightPreviewCell"
        creator.photoReuse = "RightPreviewCell"
        creator.checkReuse = "RightPreviewCell"

        moving.path = path
        moving.task = task

        sections = creator.build(model, model.bullets)
        moving.sections = sections
    }

    func setChecked(_ item: PreviewItem) {
        moving.setChecked(item)
    }

    func move(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        moving.move(from: sourceIndexPath, to: destinationIndexPath)
    }
    
    func canMove(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) -> Bool {
        return true
    }
}
