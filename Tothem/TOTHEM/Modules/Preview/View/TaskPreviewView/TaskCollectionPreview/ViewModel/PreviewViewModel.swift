//
//  PreviewViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct PreviewSuggest {
    var value: TaskModel?
    init(value: TaskModel?) {
        self.value = value
    }
}

struct PreviewItem: Equatable {
    enum PreviewType: String {
        case checkbox
        case text
        case photo
        case space
        case suggestNote
        case suggestActions
        case suggestLoader
        case statistics
        case attachedTask
        case attachedEntity
        case separator
    }

    var id: String
    var reuseId: String
    var value: Any?
    var type: PreviewType
    var checked: Bool
    var child: Bool
    var uid: String?
    
    init(reuseId: String, id: String = UUID().uuidString, uid: String? = nil, type: PreviewType, value: Any? = nil, checked: Bool = false, child: Bool = false) {
        self.type = type
        self.id = id
        self.reuseId = reuseId
        self.value = value
        self.checked = checked
        self.child = child
        self.uid = uid
    }

    static func == (lhs: PreviewItem, rhs: PreviewItem) -> Bool {
        return lhs.id == rhs.id && lhs.checked == rhs.checked
    }
}

protocol PreviewCell: class {
    var deleteAction: ((PreviewItem) -> Void)? { get set }
    func populate(_ model: PreviewItem)
}

protocol PreviewViewModelProtocol {
    func sectionValues() -> SectionedValues<PreviewSection, PreviewItem>
    var sections: [PreviewSection] { get set }
    func canMove(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) -> Bool
    mutating func setChecked(_ item: PreviewItem)
    mutating func setRemoved(_ item: PreviewItem)
    mutating func move(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    var task: TaskModel? { get }
}

extension PreviewViewModelProtocol {
    func sectionValues() -> SectionedValues<PreviewSection, PreviewItem> {
        return SectionedValues(sections.map({ ($0, $0.items) }))
    }

    mutating func setRemoved(_ item: PreviewItem) {
        let sections = self.sections

        for (index, section) in sections.enumerated() {
            var newSection = section
            newSection.items = section.items
                .filter({ $0.id != item.id })

            self.sections[index] = newSection
        }

        //observable.on(.next(self.sections))
    }
    
    mutating func move(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

    }
}

typealias PreviewType = PreviewItem.PreviewType

struct PreviewSection: Equatable {
    var identity: String

    typealias Identity = String

    public typealias Item = PreviewItem

    var items: [Item]

    init(id: String, items: [Item]) {
        self.identity = id
        self.items = items
    }

    init(original: PreviewSection, items: [Item]) {
        self = original
        self.items = items
    }

    static func == (l: PreviewSection, r: PreviewSection) -> Bool {
        return l.identity == r.identity
    }
}
