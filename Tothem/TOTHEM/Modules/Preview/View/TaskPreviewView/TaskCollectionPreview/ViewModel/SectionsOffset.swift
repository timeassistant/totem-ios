//
//  SectionsOffset.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct SectionsOffset {
    static func calculate(_ collectionView: UICollectionView, _ cell: UICollectionViewCell, _ model: CollectionViewLayoutModel) -> CollectionViewLayoutModel {
        let cellFrame = cell.convert(cell.bounds, to: collectionView.superview)
        let viewFrame = collectionView.convert(collectionView.bounds, to: collectionView.superview)
        let diff = cellFrame.minY - viewFrame.minY

        if let indexPath = collectionView.indexPath(for: cell),
            let attribute = model.attributes
                .first(where: { $0.indexPath == indexPath}) {
            var newModel = model
            newModel.contentOffset = CGPoint(x: 0, y: attribute.frame.minY - diff)
            return newModel
        }

        return model
    }
}
