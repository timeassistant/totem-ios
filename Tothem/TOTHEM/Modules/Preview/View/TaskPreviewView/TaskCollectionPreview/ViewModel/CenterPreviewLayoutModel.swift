//
//  CenterPreviewLayoutModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct CenterPreviewLayoutModel: PreviewLayoutModelProtocol {
    var sections: [PreviewSection]
    
    var collectionViewLayoutModel = CollectionViewLayoutModel()
    var cellHeight: CGFloat = 60
    var cellWidth: CGFloat = 60
    var checkboxWidth: CGFloat = 60
    var sectionSpace: CGFloat = 10
    var cellSpace: CGFloat = 1
    var spacing: CGFloat = 10
    var columns: CGFloat = 2
    
    init(sections: [PreviewSection], width: CGFloat, dragging: IndexPath? = nil) {
        self.sections = sections
        
        collectionViewLayoutModel.contentWidth = width

        if dragging != nil {
            cellHeight = 50
            cellSpace = 10
        }

        var section = 0
        var minY: CGFloat = 0

        while section < sections.count {
            var item = 0
            let sectionModel = sections[section]

            let combined = sectionModel.items.count > 2
            let space = combined ? cellSpace : (dragging == nil ? sectionSpace : (sectionSpace + cellSpace))
            
            cellWidth = (width - 2 * space) / 3
            cellHeight = cellWidth * 3 / 4
            var nextSectionCombined = false
            var nextSectionText = false
            if section + 1 < sections.count {
                nextSectionCombined = sections[section + 1].items.count > 2
                nextSectionText = sections[section + 1].items.first?.type == .text || sections[section + 1].items.first?.type == .checkbox
            }

            while item < sectionModel.items.count {
                let indexPath = IndexPath(item: item, section: section)
                let itemModel = sectionModel.items[item]
                let maxY = collectionViewLayoutModel.contentHeight
                var startX = collectionViewLayoutModel.attributes
                    .last?.frame.maxX ?? 0

                var nextItemText = false
                if item + 1 < sectionModel.items.count {
                    nextItemText = sectionModel.items[item + 1].type == .text ||
                        sectionModel.items[item + 1].type == .checkbox
                }

                if startX == width || itemModel.type == .text ||
                itemModel.type == .checkbox || itemModel.type == .suggestLoader || itemModel.type == .attachedTask || itemModel.type == .attachedEntity || itemModel.type == .separator {
                    startX = 0
                    minY = maxY
                }

                if item == 0, combined {
                    startX = 0
                    minY = maxY
                }

                if item == sectionModel.items.count - 1, combined {
                    startX = 0
                    minY = maxY
                }

                if nextSectionCombined || nextSectionText,
                    item == sectionModel.items.count - 1 {
                    startX = 0
                    minY = maxY
                }

                if nextItemText {
                    startX = 0
                    minY = maxY
                }

                var frame: CGRect = .zero

                switch itemModel.type {
                case .photo:
                    let origin = CGPoint(x: startX, y: minY)
                    let size = CGSize(width: cellWidth, height: cellHeight)
                    frame = CGRect(origin: origin, size: size)
                case .text, .checkbox:
                    let x: CGFloat = startX + (itemModel.child ? checkboxWidth : 0)
                    let origin = CGPoint(x: x, y: minY)
                    let size = CGSize(width: width - x, height: cellHeight)
                    frame = CGRect(origin: origin, size: size)
                case .space:
                    let x = startX
                    let y = minY
                    let origin = CGPoint(x: x, y: y)
                    if combined {
                        let space = item + 1 == sectionModel.items.count ? (dragging == nil ? 0 : cellSpace) : cellSpace
                        let height = startX > 0 ? cellHeight : space
                        let width = startX > 0 ? space : width
                        let size = CGSize(width: width, height: height)
                        frame = CGRect(origin: origin, size: size)
                    } else {
                        if item == 0 {
                            let width = x > 0 ? sectionSpace : width
                            let height = x > 0 ? cellHeight : sectionSpace
                            let size = CGSize(width: width, height: height)
                            frame = CGRect(origin: origin, size: size)
                        } else {
                            let space = dragging == nil ? 0 : cellSpace
                            let width = x > 0 ? space : width
                            let height = x > 0 ? cellHeight : space
                            let size = CGSize(width: width, height: height)
                            frame = CGRect(origin: origin, size: size)
                        }
                    }
                case .suggestLoader:
                    let origin = CGPoint(x: startX, y: minY)
                    let size = CGSize(width: width, height: cellHeight/2)
                    frame = CGRect(origin: origin, size: size)
                    
                case .separator:
                    let origin = CGPoint(x: startX, y: minY)
                    let size = CGSize(width: width, height: cellHeight/3)
                    frame = CGRect(origin: origin, size: size)
                    
                case .attachedTask, .attachedEntity:
                    var size = CGSize.zero
                    let origin = CGPoint(x: startX, y: minY)
                    if let model = itemModel.value as? CollectionCellModel {
                        size = model.sizing(width, CellParametersModel.default)
                    }
                    frame = CGRect(origin: origin, size: size)
                default: frame = .zero
                }

                add(indexPath, frame: frame)

                item += 1
            }
            section += 1
        }
    }
    
    private mutating func add(_ indexPath: IndexPath, frame: CGRect) {
        let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attribute.frame = frame
        
        var attributes = collectionViewLayoutModel.attributes
        attributes.append(attribute)
        collectionViewLayoutModel.attributes = attributes
        
        var contentHeight = collectionViewLayoutModel.contentHeight
        contentHeight = max(attribute.frame.maxY, contentHeight)
        collectionViewLayoutModel.contentHeight = contentHeight
    }
}
