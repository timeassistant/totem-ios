//
//  PreviewCheckCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class PreviewCheckCell: UICollectionViewCell, PreviewCell, PhotoCellProtocol, Highlightable {
    @IBOutlet weak var backView: UIView?
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var imageView: TaskImageView?
    @IBOutlet weak var checkButton: UIButton?
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var overview: UIView?
    @IBOutlet weak var checkButtonWidth: NSLayoutConstraint?
    
    var checkAction: ((PreviewItem) -> Void)?
    var deleteAction: ((PreviewItem) -> Void)?
    
    private var model: PreviewItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        overview?.isHidden = true
    }
    
    func populate(_ model: PreviewItem) {
        self.model = model
        let value = model.value ?? ""

        if model.type == .checkbox ||
            model.type == .text {
            textLabel?.text = value as? String
            textLabel?.isHidden = false
            imageView?.isHidden = true
        }
        
        if model.type == .photo,
            let photoValue = value as? String {
            textLabel?.text = nil
            textLabel?.isHidden = true

            let observable = GetEntityPhotoUseCase.shared
                .execute(uid: model.uid, photoEntityValue: photoValue)

            imageView?.set(observable)
            imageView?.isHidden = false
        }
        
        checkButtonWidth?.constant = model.child || model.type == .text ? 0 : 60

        checkButton?.isSelected = model.checked
        checkButton?.isHidden = model.child || model.type == .text
    }

    @IBAction func deleteButton(_ sender: UIButton) {
        guard let model = model else { return }
        deleteAction?(model)
    }

    @IBAction func checkButtonPressed(_ sender: UIButton) {
        guard let model = model else { return }
        if let button = checkButton {
            button.isSelected = !button.isSelected
        }
        checkAction?(model)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        UIView.animate(CATransaction.animationDuration()) {
            self.contentView.layoutIfNeeded()
        }
    }
    
    func highlight(_ highlight: Bool) {
        overview?.isHidden = !highlight
    }
}
