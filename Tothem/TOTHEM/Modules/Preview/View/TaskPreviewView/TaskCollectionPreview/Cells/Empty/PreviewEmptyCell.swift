//
//  PreviewEmptyCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol Highlightable {
    func highlight(_ highlight: Bool)
}

final class PreviewEmptyCell: UICollectionViewCell, Highlightable {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }

    private var color: UIColor?
    func highlight(_ highlight: Bool) {
        if highlight {
            if color == nil {
                color = backgroundColor
                backgroundColor = .green
            }
        } else {
            if color != nil {
                backgroundColor = color
                color = nil
            }
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        UIView.animate(CATransaction.animationDuration()) {
            self.contentView.layoutIfNeeded()
        }
    }
}
