//
//  PreviewFlowDelegate.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol PreviewFlowDelegateProtocol: UIScrollViewDelegate {
    func didSelectItem(at indexPath: IndexPath)
}

final class PreviewFlowDelegate: NSObject, UICollectionViewDelegate {
    var contentWidth: CGFloat
    var model: PreviewViewModelProtocol
    weak var delegate: PreviewFlowDelegateProtocol?
    
    init(model: PreviewViewModelProtocol, contentWidth: CGFloat, delegate: PreviewFlowDelegateProtocol?) {
        self.contentWidth = contentWidth
        self.model = model
        self.delegate = delegate
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectItem(at: indexPath)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll?(scrollView)
    }
}
