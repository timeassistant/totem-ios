//
//  TaskPreviewCenterFlowDelegate.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class TaskPreviewCenterFlowDelegate: NSObject, UICollectionViewDelegateFlowLayout {
    private static var spacing: CGFloat = 10
    private static var titleHeight: CGFloat = 50

    private var spacing: CGFloat {
        return TaskPreviewCenterFlowDelegate.spacing
    }

    private var titleHeight: CGFloat {
        return TaskPreviewCenterFlowDelegate.titleHeight
    }
    
    private var photoCellWidth: CGFloat {
        return TaskPreviewCenterFlowDelegate.photoCellWidth(viewWidth: contentWidth, spacing: spacing, columns: CGFloat(model.photos.columns))
    }

    var contentWidth: CGFloat
    var model: TaskViewModel
    weak var delegate: UIScrollViewDelegate?

    init(model: TaskViewModel, contentWidth: CGFloat, delegate: UIScrollViewDelegate?) {
        self.contentWidth = contentWidth
        self.model = model
        self.delegate = delegate
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch TaskContentSectionType.type(mode: model.mode, side: model.side, section: indexPath.section) {
        case .title: return CGSize(width: contentWidth, height: titleHeight)
        case .photos:
            return CGSize(width: photoCellWidth, height: photoCellWidth)
        case .text:
            let width = contentWidth - 2 * spacing
            let height = model.text[indexPath.row].height(width: width)
            return CGSize(width: width, height: height)
        case .checklist:
            let width = contentWidth - 2 * spacing
            let height = model.checklist[indexPath.row].height(width: width)
            return CGSize(width: width, height: height)
        default: return .zero
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch TaskContentSectionType.type(mode: model.mode, side: model.side, section: section) {
        case .text:
            return UIEdgeInsets(top: model.text.isEmpty ? 0 : spacing, left: spacing, bottom: 0, right: spacing)
        case .photos: return UIEdgeInsets(top: model.photos.items.count > 0 ? spacing : 0, left: spacing, bottom: 0, right: spacing)
        case .checklist:
            return UIEdgeInsets(top: model.checklist.isEmpty ? 0 : spacing, left: spacing, bottom: spacing, right: spacing)
        default: return .zero
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch TaskContentSectionType.type(mode: model.mode, side: model.side, section: section) {
        case .photos: return spacing
        default: return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    private class func photoCellWidth(viewWidth: CGFloat, spacing: CGFloat, columns: CGFloat) -> CGFloat {
        return (viewWidth - spacing * (columns + 1)) / columns
    }

    class func height(_ model: TaskViewModel, width: CGFloat) -> CGFloat {
        let contentWidth = width - 2 * spacing
        let textHeight = model.text.reduce(0) { $0 + $1.height(width: contentWidth) }
        let columns = CGFloat(model.photos.columns)
        let photosHeight = (photoCellWidth(viewWidth: width, spacing: spacing, columns: columns) + spacing) * (CGFloat(model.photos.items.count) / columns).rounded(.up)
        let checklistHeight = model.checklist.reduce(0) { $0 + $1.height(width: contentWidth) }

        var height = titleHeight

        if textHeight > 0 {
            height += spacing + textHeight
        }

        if photosHeight > 0 {
            height += photosHeight
        }

        if checklistHeight > 0 {
            height += spacing + checklistHeight
        }

        if photosHeight > 0 || textHeight > 0 || checklistHeight > 0 {
            height += spacing
        }

        return height
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll?(scrollView)
    }
}
