//
//  PreviewViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class PreviewViewController: UIViewController {

    var disposeBag = DisposeBag()
    var events = PublishSubject<PreviewControllerEvent>()

    var model: TaskModel?

    var transition: CollectionViewCellTransition?
    var side: TimesetType = .center
    var previewFrame = CGRect.zero
    var contentFrame = CGRect.zero
    var minOffset: CGFloat = 40
    var viewHeight = UIScreen.main.bounds.height
    var viewWidth = UIScreen.main.bounds.width
    var animationDuration: TimeInterval = 0.3
    var font = UIFont.boldSystemFont(ofSize: 23)
    let panToExitOffset: CGFloat = 100
    var beginPanningPoint: CGPoint?
    weak var panContentViewGesture: UIPanGestureRecognizer?

    @IBOutlet weak var contentViewHeight: NSLayoutConstraint?
    @IBOutlet weak var contentViewTop: NSLayoutConstraint?
    @IBOutlet weak var contentViewBottom: NSLayoutConstraint?
    @IBOutlet weak var contentViewCenter: NSLayoutConstraint?

    @IBOutlet weak var editButton: UIButton?

    func contentHeight(model: TaskModel, width: CGFloat) -> CGFloat {
        return 0
    }

    func updateDataSource(dragging: IndexPath?) {
        fatalError("abstract class")
    }

    func setInitialScrollViewFrame() {
        guard let transition = transition else { return }
        let initial = transition.initialFrame
        contentViewCenter?.constant = initial.midX - previewFrame.midX
        contentViewHeight?.constant = initial.height
        contentViewTop?.constant = initial.minY
        contentViewBottom?.constant = viewHeight - initial.maxY
    }

    func setContent(frame: CGRect) {
        contentViewCenter?.constant = 0
        contentViewHeight?.constant = frame.height
        contentViewTop?.constant = frame.minY

        let bottom = max(0, viewHeight - frame.maxY)
        contentViewBottom?.constant = bottom

        contentFrame = frame
        contentFrame.size.height = viewWidth - bottom
    }

    func setScrollViewTargetFrame() {
        setContent(frame: previewFrame)
    }

    func updatePreviewFrame() {
        guard let model = model else { return }
        let height = max(100, contentHeight(model: model, width: viewWidth))
        let top = max(minOffset, (viewHeight - height) / 2)
        previewFrame = CGRect(x: 0, y: top, width: viewWidth, height: height)
    }

    func updateScrollViewFrame(scrollView: UIScrollView) {
        var frame = previewFrame
        let contentDiff = abs(frame.height - scrollView.bounds.height)

        let scrolled = (scrollView.contentInset.top + scrollView.contentOffset.y) / contentDiff

        if scrolled > 0 {
            frame.origin.y = min(minOffset, minOffset - minOffset * scrolled)
        } else {
            frame.origin.y = max(frame.origin.y, min(minOffset, minOffset - minOffset * scrolled))
        }

        frame.origin.y = max(0, frame.origin.y)
        contentViewTop?.constant = frame.origin.y

        let bottom = max(minOffset - frame.origin.y, viewHeight - frame.maxY)
        contentViewBottom?.constant = bottom

        frame.size.height = viewHeight - bottom - frame.minY
        contentFrame = frame
    }

    func releaseContentToInitialFrame() {
        
    }
}
