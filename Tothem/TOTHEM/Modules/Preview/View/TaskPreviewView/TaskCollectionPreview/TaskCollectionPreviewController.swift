//
//  TaskCollectionPreviewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

protocol PhotoCellProtocol {
    var imageView: TaskImageView? { get }
}

final class TaskCollectionPreviewController: PreviewViewController, PreviewFlowDelegateProtocol, UIGestureRecognizerDelegate, UICollectionViewDataSource, PreviewDragDropHelperDelegate {

    @IBOutlet weak var contentView: UIView!
    weak var titleView: UIView?
    var titleInset: CGFloat = 60 {
        didSet {
            collectionView?.contentInset = .init(top: titleInset, left: 0, bottom: 0, right: 0)
        }
    }

    @IBOutlet weak var collectionViewTop: NSLayoutConstraint? //for panning
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var collectionViewLayout: PreviewCollectionLayout!
    
    private var flow: UICollectionViewDelegate?
    private var dragHelper: PreviewDragDropHelper?
    private var diff: CollectionViewDiffCalculator<PreviewSection, PreviewItem>!

    override var model: TaskModel? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let collectionView = collectionView {
            self.updateDataSource(dragging: nil)

            self.updatePreviewFrame()
            self.updateScrollViewFrame(scrollView: collectionView)

            UIView.animate(withDuration: CATransaction.animationDuration(),
                           animations: {
                self.setTransition(frame: self.contentFrame)
                self.transition?.controller.view.layoutIfNeeded()
                self.view.layoutIfNeeded()
                self.titleView?.superview?.layoutIfNeeded()
            }, completion: { _ in
                self.addContentPanGestures()
            })
        } else {
            updatePreviewFrame()
        }
    }
    
    deinit {
        print("deinit task collection preview controller")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dragHelper = PreviewDragDropHelper(view: UIApplication.shared.keyWindow, collectionView: collectionView, delegate: self)

        let gesture = UITapGestureRecognizer(target: self, action: #selector(tap(gesture:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)

        collectionView?.dataSource = self
        collectionView?.delegate = nil
        collectionView?.contentInset = .init(top: titleInset, left: 0, bottom: 0, right: 0)

        collectionView?.register(UINib(nibName: "RightPreviewCell", bundle: nil), forCellWithReuseIdentifier: "RightPreviewCell")
        
        collectionView?.register(UINib(nibName: "RightPreviewCell", bundle: nil), forCellWithReuseIdentifier: "RightPreviewCell")

        collectionView?.register(UINib(nibName: "LeftPreviewCell", bundle: nil), forCellWithReuseIdentifier: "LeftPreviewCell")

        collectionView?.register(UINib(nibName: "PreviewEmptyCell", bundle: nil), forCellWithReuseIdentifier: "PreviewEmptyCell")
        
        collectionView?.register(UINib(nibName: "PreviewAttachedPhotoCell", bundle: nil), forCellWithReuseIdentifier: "PreviewAttachedPhotoCell")
        
        collectionView?.register(UINib(nibName: "PreviewAttachedTextCell", bundle: nil), forCellWithReuseIdentifier: "PreviewAttachedTextCell")
        
        collectionView?.register(UINib(nibName: "SeparatorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.separatorReuseId)
        collectionView?.register(UINib(nibName: "SimilarTaskCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.similarTaskCellReuseId)
        collectionView?.register(UINib(nibName: "SimilarTextEntityCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.similarTextEntityCellReuseId)
        collectionView?.register(UINib(nibName: "SimilarPhotoEntityCell", bundle: nil), forCellWithReuseIdentifier: CollectionViewDataSource.similarPhotoEntityCellReuseId)
        
        collectionView?.register(UINib(nibName: "PreviewSuggestLoaderCell", bundle: nil), forCellWithReuseIdentifier: "PreviewSuggestLoaderCell")
        
        collectionView?.register(UINib(nibName: "PreviewSeparatorCell", bundle: nil), forCellWithReuseIdentifier: "PreviewSeparatorCell")

        updateDataSource(dragging: nil)
        updatePreviewFrame()

        contentView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        setInitialScrollViewFrame()

        editButton?.backgroundColor = .red
        editButton?.layer.cornerRadius = 20
        editButton?.layer.masksToBounds = true

        collectionView?.backgroundColor = .clear
    }
    
    func layoutIfNeeded() {
        view.layoutIfNeeded()
        collectionView?.layoutIfNeeded()
    }

    private var appeared: Bool = false

    func setInitialPreviewFrames(view: UIView?) {
        collectionView?.isScrollEnabled = false
        setScrollViewTargetFrame()

        transition?.set(frame: previewFrame, from: view ?? self.view)

        UIView.animate(withDuration: animationDuration, animations: {
            self.transition?.controller.view.layoutIfNeeded()
            self.contentView.transform = CGAffineTransform.identity
            self.titleView?.superview?.layoutIfNeeded()
            self.setTargetTransparency()
            self.layoutIfNeeded()
        }, completion: { _ in
            self.addContentPanGestures()
            self.appeared = true
        })
    }

    func setTransition(frame: CGRect) {
        transition?.set(frame: frame, from: view)
    }

    func set(transparent: Bool) {
        contentView.alpha = transparent ? 0 : 1
        titleView?.alpha = transparent ? 0 : 1
        collectionView?.alpha = transparent ? 0 : 1
        editButton?.alpha = transparent ? 0 : 1
        view.alpha = transparent ? 0 : 1
    }

    func setInitialTransparency() {
        set(transparent: true)
    }

    private func setTargetTransparency() {
        set(transparent: false)
    }

    override func releaseContentToInitialFrame() {
        collectionView?.delegate = nil
        
        guard let transition = transition else { return }

        transition.exit()

        setInitialScrollViewFrame()

        UIView.animate(withDuration: self.animationDuration, animations: {
            transition.translateCellVerticaly(by: 0)
            transition.controller.view.layoutIfNeeded()
            self.titleView?.superview?.layoutIfNeeded()
            self.contentView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.setInitialTransparency()
            self.layoutIfNeeded()
        }, completion: { _ in
            self.events.on(.next(.dismiss))
        })
    }

    private func addContentPanGestures() {
        guard let scrollView = collectionView else { return }
        scrollView.panGestureRecognizer.removeTarget(self, action: nil)

        if abs(previewFrame.height - contentView.bounds.height) > 1 {
            if let pan = panContentViewGesture {
                view.removeGestureRecognizer(pan)
            }
            scrollView.isScrollEnabled = true
            scrollView.panGestureRecognizer.addTarget(self, action: #selector(panScrollView(gesture:)))
        } else {
            guard panContentViewGesture == nil else { return }
            scrollView.isScrollEnabled = false
            let pan = UIPanGestureRecognizer(target: self, action: #selector(panContentView(gesture:)))
            panContentViewGesture = pan
            panContentViewGesture?.delegate = self
            view.addGestureRecognizer(pan)
        }
    }

    override func contentHeight(model: TaskModel, width: CGFloat) -> CGFloat {
        guard let model = layoutModel else { return titleInset }
        return titleInset + max(model.collectionViewLayoutModel
            .contentHeight + 10, 50)
    }

    var viewModel: PreviewViewModelProtocol!
    var layoutModel: PreviewLayoutModelProtocol!
    var collectionDisposeBag: DisposeBag!
    var suggests: [PreviewSuggest] = []
    var statistics: [String] = []
    
    private var attachedTasks: [TaskModel] = []
    func setAttached(_ tasks: [TaskModel]) {
        attachedTasks = tasks
        updateView()
    }
    
    private var attachedEntities: [EntityModel] = []
    func setAttached(_ entities: [EntityModel]) {
        attachedEntities = entities
        updateView()
    }
    
    override func updateDataSource(dragging: IndexPath?) {
        guard let collectionView = collectionView,
            let model = model else { return }

        switch side {
        case .left:
            viewModel = LeftPreviewViewModel(model)
            layoutModel = LeftPreviewLayoutModel(sections: viewModel.sections, width: viewWidth - 10, dragging: dragging)
            
        case .center:
            viewModel = CenterPreviewViewModel(model, attachedTasks: attachedTasks, attachedEntities: attachedEntities)
            layoutModel = CenterPreviewLayoutModel(sections: viewModel.sections, width: viewWidth - 10, dragging: dragging)
            
        case .right:
            viewModel = RightPreviewViewModel(model)
            layoutModel = RightPreviewLayoutModel(sections: viewModel.sections, width: viewWidth - 10, dragging: dragging)
        }

        flow = NewPreviewFlowDelegate(model: viewModel, contentWidth: viewWidth, delegate: self)
        collectionView.delegate = flow

        if let dragging = dragging,
            let cell = collectionView.cellForItem(at: dragging) {
            let collectionViewModel = SectionsOffset
                .calculate(collectionView, cell, layoutModel.collectionViewLayoutModel)
            collectionViewLayout.viewModel = collectionViewModel
        } else {
            collectionViewLayout.viewModel = layoutModel.collectionViewLayoutModel
        }

        if diff == nil {
            diff = CollectionViewDiffCalculator<PreviewSection, PreviewItem>(collectionView: collectionView, initialSectionedValues: viewModel.sectionValues())
        } else {
            diff.sectionedValues = viewModel.sectionValues()
        }
    }

    private func cell(_ collectionView: UICollectionView, item: PreviewItem, index: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseId, for: index)
        if let cell = cell as? PreviewCell {
            cell.populate(item)

            cell.deleteAction = { [weak self] in
                self?.delete($0)
            }
            
            if let cell = cell as? PreviewCheckCell {
                cell.checkAction = { [weak self] in
                    self?.check($0)
                }
                
                cell.backView?.backgroundColor = UIColor.white.withAlphaComponent(item.checked ? 0.2 : 0.5)
            }
        }

        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if appeared {
            updateScrollViewFrame(scrollView: scrollView)
            if beginPanningPoint == nil {
                setTransition(frame: contentFrame)
            }
        }
    }

    private func check(_ item: PreviewItem) {
        viewModel.setChecked(item)
        if let task = viewModel.task {
            model = task
            save(task: task)
        }
    }
    
    private func delete(_ item: PreviewItem) {
        guard let model = model else { return }
        viewModel.setRemoved(item)
        save(task: model)
    }

    func reordered() {
        guard let model = model else { return }
        save(task: model)
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let tap = gestureRecognizer as? UITapGestureRecognizer {
            return !contentView.bounds.contains(tap.location(in: contentView))
        }

        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let velocity = pan.velocity(in: contentView)
            return abs(velocity.x) < abs(velocity.y)
        }
        
        return false
    }

    func attachTitle(view: UIView) -> NSLayoutConstraint {
        titleView = view
        return contentView.constraintTop(view)
    }

    @objc private func tap(gesture: UITapGestureRecognizer) {
        events.on(.next(.exit))
    }

    func didSelectItem(at indexPath: IndexPath) {
        let item = diff.value(atIndexPath: indexPath)
        
        guard item.type == .photo,
            let photos = model?.photos,
            photos.count > 0 else { return }

        let index = photos.firstIndex(where: { $0.id == item.id }) ?? 0
        events.on(.next(.previewPhoto(photos, index)))
    }

    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return diff.numberOfObjects(inSection: section)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return diff.numberOfSections()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = diff.value(atIndexPath: indexPath)
        return cell(collectionView, item: item, index: indexPath)
    }

    // MARK: - PreviewDragHelperDelegate
    func canMoveItem(at indexPath: IndexPath) -> Bool {
        let model = diff.value(atIndexPath: indexPath)
        let types: [PreviewItem.PreviewType] = [.checkbox, .text, .photo]
        return types.contains(model.type)
    }

    func endMovement(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        highlight(destinationIndexPath, false)
        viewModel.move(from: sourceIndexPath, to: destinationIndexPath)
        
        if let task = viewModel.task {
            model = task
            save(task: task)
        }
    }
    
    func beginMovement(at indexPath: IndexPath) {
        collectionView?.performBatchUpdates({
            self.updateDataSource(dragging: indexPath)
        }, completion: nil)
    }

    func changeMovement(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        highlight(sourceIndexPath, false)
        highlight(destinationIndexPath, true)

        if sourceIndexPath.section < destinationIndexPath.section {
            scrollToNext(from: destinationIndexPath)
        } else if sourceIndexPath.section > destinationIndexPath.section {
            scrollToPrev(from: destinationIndexPath)
        } else if sourceIndexPath.row < destinationIndexPath.row {
            scrollToNext(from: destinationIndexPath)
        } else if sourceIndexPath.row > destinationIndexPath.row {
            scrollToPrev(from: destinationIndexPath)
        }
    }
    
    private func highlight(_ indexPath: IndexPath, _ highlight: Bool) {
        if let cell = collectionView?.cellForItem(at: indexPath) as? Highlightable {
            cell.highlight(highlight)
        }
    }

    private func scrollToNextRow(_ indexPath: IndexPath, _ rows: Int) -> Bool {
        guard let collectionView = collectionView else { return false }

        if indexPath.row + rows < collectionView
            .numberOfItems(inSection: indexPath.section) {
            var newIndexPath = indexPath
            newIndexPath.row += rows
            collectionView.scrollToItem(at: newIndexPath, at: .centeredVertically, animated: true)
            return true
        }
        return false
    }

    private func scrollToPrevRow(_ indexPath: IndexPath, _ rows: Int) -> Bool {
        guard let collectionView = collectionView else { return false }

        if indexPath.row - rows >= 0 {
            var newIndexPath = indexPath
            newIndexPath.row -= rows
            collectionView.scrollToItem(at: newIndexPath, at: .centeredVertically, animated: true)
            return true
        }
        return false
    }

    private func scrollToNextSection(_ indexPath: IndexPath, _ row: Int) -> Bool {
        guard let collectionView = collectionView else { return false }

        let section = indexPath.section + 1
        if section < collectionView.numberOfSections,
            row < collectionView
                .numberOfItems(inSection: section) {
            var newIndexPath = indexPath
            newIndexPath.section += 1
            newIndexPath.row = 0
            collectionView.scrollToItem(at: newIndexPath, at: .centeredVertically, animated: true)
            return true
        }

        return false
    }

    private func scrollToPrevSection(_ indexPath: IndexPath, _ row: Int) -> Bool {
        guard let collectionView = collectionView else { return false }

        let section = indexPath.section - 1
        if section >= 0,
            collectionView.numberOfItems(inSection: section) - row >= 0 {
            var newIndexPath = indexPath
            newIndexPath.section -= 1
            newIndexPath.row = collectionView.numberOfItems(inSection: section) - row
            collectionView.scrollToItem(at: newIndexPath, at: .centeredVertically, animated: true)
            return true
        }

        return false
    }

    private func scrollToNext(from indexPath: IndexPath) {
        guard let collectionView = collectionView else { return }
        if let cell = collectionView.cellForItem(at: indexPath) {
            let frame = cell.convert(cell.bounds, to: view)
            if frame.maxY + 40 > contentView.frame.maxY {
                if scrollToNextRow(indexPath, 3) { } else
                if scrollToNextRow(indexPath, 2) { } else
                if scrollToNextRow(indexPath, 1) { } else
                if scrollToNextSection(indexPath, 2) { } else
                if scrollToNextSection(indexPath, 1) { } else
                if scrollToNextSection(indexPath, 0) { }
            }
        }
    }

    private func scrollToPrev(from indexPath: IndexPath) {
        guard let collectionView = collectionView else { return }
        if let cell = collectionView.cellForItem(at: indexPath) {
            let frame = cell.convert(cell.bounds, to: view)
            if frame.minY - 40 < contentView.frame.minY {
                if scrollToPrevRow(indexPath, 3) { } else
                if scrollToPrevRow(indexPath, 2) { } else
                if scrollToPrevRow(indexPath, 1) { } else
                if scrollToPrevSection(indexPath, 2) { } else
                if scrollToPrevSection(indexPath, 1) { } else
                if scrollToPrevSection(indexPath, 0) { }
            }
        }
    }
    
    func save(task: TaskModel) {
        SaveTaskUseCase.shared.save(task)
            .subscribe()
            .disposed(by: disposeBag)
        updateDataSource(dragging: nil)
    }
}
