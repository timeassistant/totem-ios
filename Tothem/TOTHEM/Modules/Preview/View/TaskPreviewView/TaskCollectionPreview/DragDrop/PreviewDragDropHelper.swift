//
//  PreviewDragDropHelper.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol PreviewDragDropHelperDelegate: class {
    func canMoveItem(at indexPath: IndexPath) -> Bool
    func beginMovement(at indexPath: IndexPath)
    func endMovement(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    func changeMovement(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
}

final class PreviewDragDropHelper {
    private weak var collectionView: UICollectionView?
    private var sourseIndexPath: IndexPath?
    private var destinationIndexPath: IndexPath?
    private weak var sourceImageView: UIView?
    private weak var delegate: PreviewDragDropHelperDelegate?
    private weak var view: UIView?

    init(view: UIView?, collectionView: UICollectionView?, delegate: PreviewDragDropHelperDelegate?) {
        self.delegate = delegate
        self.collectionView = collectionView
        self.view = view

        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture(gesture:)))
        collectionView?.addGestureRecognizer(longPressGesture)
    }

    @objc func handleGesture(gesture: UILongPressGestureRecognizer) {
        guard let collectionView = collectionView else { return }
        switch gesture.state {
        case .began:
            sourseIndexPath = nil
            destinationIndexPath = nil
            guard let indexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)),
                delegate?.canMoveItem(at: indexPath) == true else {
                return
            }

            sourseIndexPath = indexPath
            destinationIndexPath = indexPath

            if let view = representationImage(at: indexPath) {
                self.view?.addSubview(view)
                view.center = gesture.location(in: self.view)
                sourceImageView = view
            }

            delegate?.beginMovement(at: indexPath)
        case .changed:
            changed(gesture: gesture)
        default:
            changed(gesture: gesture)
            
            sourceImageView?.removeFromSuperview()
            if let indexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) {
                destinationIndexPath = indexPath
            }

            if let from = sourseIndexPath,
                let to = destinationIndexPath {
                delegate?.endMovement(from: from, to: to)
            }
        }
    }

    private func changed(gesture: UILongPressGestureRecognizer) {
        guard let collectionView = collectionView else { return }
        sourceImageView?.center = gesture.location(in: view)
        guard let indexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
            return
        }

        if let fromIndexPath = destinationIndexPath {
            delegate?.changeMovement(from: fromIndexPath, to: indexPath)
        }

        destinationIndexPath = indexPath
    }

    private func representationImage(at indexPath: IndexPath) -> UIView? {
        guard let collectionView = collectionView,
            let cell = collectionView.cellForItem(at: indexPath) else {
            return nil
        }

        let frame = cell.convert(cell.bounds, to: collectionView)

        let renderer = UIGraphicsImageRenderer(bounds: frame)
        let image = renderer.image { collectionView.layer.render(in: $0.cgContext) }

        let imageView = UIImageView(image: image)
        imageView.frame = cell.bounds
        imageView.alpha = 0.5
        
        return imageView
    }
}
