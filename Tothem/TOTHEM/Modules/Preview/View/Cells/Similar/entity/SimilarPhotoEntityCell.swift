//
//  SimilarPhotoEntityCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class SimilarPhotoEntityCell: ContentCollectionViewCell, PreviewCell {
    @IBOutlet weak var containerView: UIView?
    @IBOutlet weak var imageView: TaskImageView!
    static var height: CGFloat = 200
    var deleteAction: ((PreviewItem) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        resetToEmpty()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        resetToEmpty()
    }

    override func cellLayoutSubviews() {
        super.cellLayoutSubviews()

        let spacing: CGFloat = 5
        let contentWidth = contentView.bounds.width

        var frame = contentView.bounds
        frame.size.width = contentWidth
        contentContainerView?.frame = frame

        frame.origin.x = spacing
        frame.origin.y = 2 * spacing
        frame.size.width = contentWidth - 2 * spacing
        frame.size.height = SimilarPhotoEntityCell.height - 4 * spacing
        imageView?.frame = frame
        imageView.imageView?.frame = imageView.bounds
    }

    private func resetToEmpty() {
        imageView?.reset()
    }

    func populate(_ model: PreviewItem) {
        guard let model = model.value as? CollectionCellModel else { return }
        set(model)
    }
    
    override func set(_ model: CollectionCellModel) {
        super.set(model)

        guard let viewModel = model.viewModel as? SimilarEntityCellViewModel else { return }

        if let observable = viewModel.photo {
            imageView?.set(observable)
        }
    }

    override func apply(parameters: CellParametersModel) {
        super.apply(parameters: parameters)
        var scale = CGFloat(parameters.normal / 10) + 1
        scale = CGFloat(1 + parameters.normal / 2)
        let viewScale = CGFloat(1 - parameters.normal / 10)
        [imageView].forEach {
            $0?.transform = CGAffineTransform(scaleX: viewScale, y: viewScale)
            $0?.imageView?.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
    }

    override func apply(cornerRadius: CGFloat) {
        super.apply(cornerRadius: cornerRadius)
        containerView?.layer.cornerRadius = cornerRadius
    }

    override func set(contentHidden: Bool) {
        super.set(contentHidden: contentHidden)
        containerView?.alpha = contentHidden ? 0 : 1
    }

    @IBAction func checkButtonPressed(_ sender: Any) {
        actions?.on(.next((model, .check, .none)))
    }

    static func height(_ model: EntityModel, width: CGFloat) -> CGFloat {
        return SimilarPhotoEntityCell.height
    }
}
