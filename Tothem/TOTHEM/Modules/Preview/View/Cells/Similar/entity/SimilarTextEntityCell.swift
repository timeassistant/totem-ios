//
//  SimilarTextEntityCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class SimilarTextEntityCell: ContentCollectionViewCell, PreviewCell {
    @IBOutlet weak var containerView: UIView?
    @IBOutlet weak var textLabel: UILabel!
    static var textFont = UIFont.systemFont(ofSize: 17)
    var deleteAction: ((PreviewItem) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        resetToEmpty()
        textLabel?.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        resetToEmpty()
    }

    override func cellLayoutSubviews() {
        super.cellLayoutSubviews()

        let spacing: CGFloat = 5
        let contentWidth = contentView.bounds.width

        var frame = contentView.bounds
        frame.size.width = contentWidth
        contentContainerView?.frame = frame

        var height: CGFloat = 0
        frame = CGRect(x: 0, y: 2*spacing, width: contentWidth, height: height)

        if let text = textLabel.text, !text.isEmpty {
            height = Symbol.referenceSymbol.size(width: contentWidth - 2 * spacing, font: SimilarTextEntityCell.textFont).height

            let textHeight = text.size(width: contentWidth - 2 * spacing, font: CenterCollectionViewCell.textFont).height
            
            frame.origin.x = spacing
            frame.size.width = contentWidth - 2 * spacing
            frame.size.height = min(3 * height, textHeight)
            textLabel.frame = frame
            frame.origin.y += frame.height
        }
    }

    private func resetToEmpty() {
        textLabel?.isHidden = true
        textLabel?.font = CenterCollectionViewCell.textFont
    }

    override func set(_ model: CollectionCellModel) {
        super.set(model)

        guard let viewModel = model.viewModel as? SimilarEntityCellViewModel else { return }

        textLabel?.text = viewModel.value
        textLabel?.isHidden = viewModel.value.isEmpty
        textLabel?.numberOfLines = 3
    }

    override func apply(parameters: CellParametersModel) {
        super.apply(parameters: parameters)
        let scale = CGFloat(parameters.normal / 10) + 1
        textLabel?.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

    override func apply(cornerRadius: CGFloat) {
        super.apply(cornerRadius: cornerRadius)
        containerView?.layer.cornerRadius = cornerRadius
    }

    func populate(_ model: PreviewItem) {
        guard let model = model.value as? CollectionCellModel else { return }
        set(model)
    }
    
    override func set(contentHidden: Bool) {
        super.set(contentHidden: contentHidden)
        containerView?.alpha = contentHidden ? 0 : 1
    }

    static func height(_ model: EntityModel, width: CGFloat) -> CGFloat {
        guard !model.value.isEmpty else { return CGFloat(kCreateButtonSize) }

        let spacing: CGFloat = 5
        let contentWidth = width - 2 * spacing
        var height: CGFloat = 4 * spacing

        let textHeight = model.value.size(width: contentWidth, font: SimilarTextEntityCell.textFont).height
                   
        let visibleLines: CGFloat = 3
        let maxTextHeight = Symbol.referenceSymbol.size(width: contentWidth, font: SimilarTextEntityCell.textFont).height * visibleLines
                   
        height += min(maxTextHeight, textHeight)

        return height
    }
}
