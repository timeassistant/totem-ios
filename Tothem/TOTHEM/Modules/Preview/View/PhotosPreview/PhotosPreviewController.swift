//
//  PhotosPreviewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class PhotosPreviewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UIGestureRecognizerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    var photos: [EntityModel] = []
    var openAtIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.register(UINib(nibName: "PreviewPhotoCell", bundle: nil), forCellWithReuseIdentifier: "PreviewPhotoCell")
        pageControl.numberOfPages = photos.count

        let gesture = UIPanGestureRecognizer(target: self, action: #selector(pan(gesture:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let offset = CGPoint(x: view.bounds.width * CGFloat(openAtIndex), y: 0)
        collectionView.contentOffset = offset
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviewPhotoCell", for: indexPath)
        if let cell = cell as? PreviewPhotoCell {
            cell.populate(photos[indexPath.row])
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int((scrollView.bounds.width / 2 + scrollView.contentOffset.x) / scrollView.bounds.width)
    }

    @objc func pan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .ended:
            let velocity = gesture.velocity(in: nil)
            if abs(velocity.y) > abs(velocity.x) {
                dismiss(animated: true, completion: nil)
            }
        default: break
        }
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let pan = gestureRecognizer as? UIPanGestureRecognizer else { return false }
        let velocity = pan.velocity(in: nil)
        return abs(velocity.y) > abs(velocity.x)
    }
}
