//
//  SimilarEntitiesTransformer.swift
//  Copyright © 2020 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct SimilarEntitiesTransformer {
    static var imageLoader: ((String?, String) -> Observable<UIImage>)!

    static func create(_ items: [EntityModel]) -> [CollectionCellModel] {
        var models: [CollectionCellModel] = []
        var separatorIndex = 0
        if items.count > 0 {
            models.append(separator(separatorIndex))
            items.forEach {
                separatorIndex += 1
                models.append(content($0))
                models.append(separator(separatorIndex))
            }
        }
        return models
    }

    private static func action() -> [(ListCellAction, ListCellActionPosition)] {
        return [(.transfer, .right), (.transfer, .left)]
    }

    private static func content(_ entity: EntityModel) -> CollectionCellModel {
        var model: CollectionCellModel
        if entity.type == .photo {
            let sizing: CollectionCellSizing = { width, _ in
                let height = SimilarPhotoEntityCell.height(entity, width: width)
                return CGSize(width: width, height: height)
            }
            
            model = CollectionCellModel(reuseId: CollectionViewDataSource.similarPhotoEntityCellReuseId, sizing: sizing, actions: action(), viewModel: SimilarEntityCellViewModel(entity, imageLoader: imageLoader))
        } else {
            let sizing: CollectionCellSizing = { width, _ in
                let height = SimilarTextEntityCell.height(entity, width: width)
                return CGSize(width: width, height: height)
            }
            
            model = CollectionCellModel(reuseId: CollectionViewDataSource.similarTextEntityCellReuseId, sizing: sizing, actions: action(), viewModel: SimilarEntityCellViewModel(entity, imageLoader: imageLoader))
        }
        
        model.identifier = entity.id
        
        return model
    }

    private static func separator(_ index: Int) -> CollectionCellModel {
        let sizing: CollectionCellSizing = { width, params in
            return CGSize(width: width, height: 20)
        }

        var model = CollectionCellModel(reuseId: CollectionViewDataSource.separatorReuseId, sizing: sizing, viewModel: CollectionCellViewModel())
        
        model.identifier = "separator_\(index)"
        return model
    }
}
