//
//  SignInViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
//import FBSDKCoreKit
//import FBSDKLoginKit

class SignInViewController: UIViewController, XIBLoadable {

    lazy var presenter: SignInViewControllerPresenter = SignInViewControllerPresenter(view: self)

    //OUTLETS
    @IBOutlet weak var logInContainer: UIView!
    @IBOutlet weak var logInContainerCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var signInLoader: JTMaterialSpinner!

    let viewModel = SignInModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.prepare()
        self.bind()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func bind() {
        loginTextField.rx.text
            .orEmpty
            .bind(to: viewModel.email)
            .disposed(by: disposeBag)

        passwordTextField.rx.text
            .orEmpty
            .bind(to: viewModel.password)
            .disposed(by: disposeBag)

        viewModel.isValid.map { $0 }
            .bind(to: confirmButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

//ACTIONS 

extension SignInViewController {
    @IBAction func close(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func signIn(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        presenter.signIn(model: viewModel)
    }
}
