//
//  SignInViewControllerPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class SignInViewControllerPresenter: NSObject {
    typealias PresenterView = SignInViewController

    weak var view: PresenterView!

    required init(view: PresenterView/* ,service:RestServiceManager = RestServiceManager()*/) {
        self.view = view
      //  self.service = service
    }

    func prepare() {
        self.observeKeyboard()
        self.view.signInLoader.circleLayer.strokeColor = self.view.confirmButton.backgroundColor?.cgColor
    }

    func signIn(model: SignInModel) {
        self.view.view.endEditing(true)
        self.loadAnimation(animating: true)
        AuthManager.shared.signIn(with: model.email.value, password: model.password.value)
    }

    func loadAnimation(animating: Bool) {
        animating ? self.view.signInLoader.beginRefreshing() : self.view.signInLoader.endRefreshing()
        self.view.confirmButton.alpha = animating ? 0 : 1
    }

    func observeKeyboard() {
        KeyboardManager.keyboardHeight()
            .observeOn(KeyboardManager.scheduler)
            .subscribe(onNext: { [weak self] keyboardHeight in
                let keyboardHeight = keyboardHeight ?? 0
                UIView.animate(CATransaction.animationDuration(), animations: {
                    let first = ((self?.view.view.bounds.height ?? 0) - keyboardHeight)
                    let shift = first - ((self?.view.view.center.y ?? 0) + ((self?.view.logInContainer.bounds.height ?? 0) / 2))
                    self?.view.logInContainerCenterYConstraint.constant = keyboardHeight != 0 ? shift : 0
                    self?.view.view.layoutIfNeeded()
                })
            })
            .disposed(by: self.view.disposeBag)
    }
}
