//
//  BaseSignUpViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class BaseSignUpViewController: UIViewController {

    var sessionModel: SignUpModel!

    //Callbacks
    var didChousePrevious: (() -> Void)?
    var didTapNext: (() -> Void)?
    var didChouseClose: (() -> Void)?
    var didChouseFinish: ((SignUpModel) -> Void)?

    let disposeBag = DisposeBag()

    var step: SignUpStepsGenerator.StepType {
       return .unknown
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        // Do any additional setup after loading the view.
    }

    func apply(model: SignUpModel) {
        self.sessionModel = model
        sessionModel.currentStep.value = self.step
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    func bind() {

	}
}
