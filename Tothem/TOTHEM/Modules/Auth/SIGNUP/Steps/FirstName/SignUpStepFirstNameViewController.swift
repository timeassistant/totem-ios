//
//  SignUpStepFirstNameViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
class SignUpStepFirstNameViewController: BaseSignUpViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var errorDescriptionLabel: UILabel!

    override var step: SignUpStepsGenerator.StepType {
        return .firstName
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepare()
    }

    override func bind() {
        firstNameTextField.rx.text
            .orEmpty
            .bind(to: sessionModel.firstName)
            .disposed(by: disposeBag)

        sessionModel.isValid.map { $0 }.bind(onNext: {[weak self] isValid in
            self?.nextStep.isHidden = !isValid.0
            self?.errorDescriptionLabel.text = isValid.1
        }).disposed(by: disposeBag)
    }

    func prepare() {
        self.firstNameTextField.delegate = self
    }

    @IBOutlet weak var nextStep: UIButton!

    @IBAction func nextStep(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didTapNext?()
    }

    @IBAction func back(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didChousePrevious?()
    }
}

extension SignUpStepFirstNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.nextStep.alpha = !(textField.text?.isEmpty ?? true) ? 1 : 0
        return true
    }
}
