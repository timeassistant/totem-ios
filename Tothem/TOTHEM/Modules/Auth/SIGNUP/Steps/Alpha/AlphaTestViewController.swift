//
//  AlphaTestViewController.swift
//  MedFIt
//
//  Created by r00t on 14.09.17.
//  Copyright © 2017 DI. All rights reserved.
//

import UIKit
import RxSwift

class AlphaTestViewController: BaseSignUpViewController {

    @IBOutlet weak var errorDescriptionLabel: UILabel!

    var testResult: SignUpModel.Alpha = .unknown {
        didSet {
            sessionModel.alpha.value = testResult
        }
    }

    override var step: SignUpStepsGenerator.StepType {
        return .alpha
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func bind() {
        sessionModel.isValid.map { $0 }.bind(onNext: {  [weak self] isValid in
            self?.nextStep.isHidden = !isValid.0
            self?.errorDescriptionLabel.text = isValid.1
            UIView.animate(withDuration: 0.3) {
                self?.nextStep.alpha = isValid.0 ? 1 : 0
            }
        }).disposed(by: disposeBag)
    }

    @IBOutlet weak var nextStep: UIButton!

    @IBAction func nextStep(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didTapNext?()
    }

    @IBOutlet weak var deonixButton: UIButton!
    @IBAction func deonixButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.testResult = .deonix
        self.animateChouse(self.testResult)
    }

    @IBOutlet weak var haggoButton: UIButton!
    @IBAction func haggoButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.testResult = .haggo
        self.animateChouse(self.testResult)
    }

    @IBAction func closeButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didChouseClose?()
    }

    func animateChouse(_ type: SignUpModel.Alpha) {
        UIView.animate(withDuration: 0.3) {
            switch type {
            case .deonix:
                self.haggoButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.deonixButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            case .haggo:
                self.deonixButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.haggoButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            default:
                break
            }
        }
    }
}
