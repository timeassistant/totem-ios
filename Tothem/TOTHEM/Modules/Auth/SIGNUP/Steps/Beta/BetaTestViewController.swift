//
//  BetaTestViewController.swift
//  MedFIt
//
//  Created by r00t on 14.09.17.
//  Copyright © 2017 DI. All rights reserved.
//

import UIKit
import RxSwift

class BetaTestViewController: BaseSignUpViewController {

    @IBOutlet weak var errorDescriptionLabel: UILabel!

    var testResult: SignUpModel.Beta = .unknown {
        didSet {
            sessionModel.beta.value = testResult
        }
    }

    override var step: SignUpStepsGenerator.StepType {
        return .beta
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func bind() {
        sessionModel.isValid.map { $0 }.bind(onNext: {  [weak self] isValid in
            self?.nextStep.isHidden = !isValid.0
            self?.errorDescriptionLabel.text = isValid.1
            UIView.animate(withDuration: 0.3) {
                self?.nextStep.alpha = isValid.0 ? 1 : 0
            }
        }).disposed(by: disposeBag)
    }

    @IBOutlet weak var nextStep: UIButton!

    @IBAction func nextStep(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didTapNext?()
    }

    @IBOutlet weak var alphaButton: UIButton!
    @IBAction func alphaButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.testResult = .alpha
        self.animateChouse(self.testResult)
    }

    @IBOutlet weak var betaButton: UIButton!
    @IBAction func betaButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.testResult = .beta
        self.animateChouse(self.testResult)
    }

    @IBAction func closeButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didChouseClose?()
    }

    func animateChouse(_ type: SignUpModel.Beta) {
        UIView.animate(withDuration: 0.3) {
            switch type {
            case .alpha:
                self.betaButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.alphaButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            case .beta:
                self.alphaButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.betaButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            default:
                break
            }
        }
    }
}
