//
//  SignUpStepGenerator.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class SignUpStepsGenerator {
    typealias Step = UIViewController

    enum StepType {
        case sex
        case email
        case password
        case alpha
        case beta
        case finish
        case firstName
        case lastName
        case unknown

        var viewControllerDescription: UIViewController.Type {
			switch self {
			case .email:
				return SignUpStepEmailViewController.self
			case .sex:
				return SignUpStepSexChouseViewController.self
			case .password:
				return SignUpStepPasswordViewController.self
			case .firstName:
				return SignUpStepFirstNameViewController.self
			case .lastName:
				return SignUpStepLastNameViewController.self
			case .finish:
				return SignUpStepFinishViewController.self
			default:
				return UIViewController.self
			}
        }
    }

    private var steps: [StepType] = []

    var stepControllers: [UIViewController] = []

    init(steps: [StepType]) {
        self.steps = steps
        self.generateControllers()
    }

    private func generateControllers() {
        self.stepControllers = self.steps.map { $0.viewControllerDescription.init() }
    }

    func step<T>() -> T? {
        let stepController = self.stepControllers.compactMap { $0 as? T }.first
        return stepController
    }
}
