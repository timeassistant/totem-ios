//
//  SignUpStepFinishViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class SignUpStepFinishViewController: BaseSignUpViewController {

    @IBOutlet weak var activityLoader: JTMaterialSpinner!

    override var step: SignUpStepsGenerator.StepType {
        return .finish
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didChousePrevious?()
    }

    @IBOutlet weak var finishRegistration: UIButton!
    @IBAction func finishRegistration(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didChouseFinish?(self.sessionModel)
    }

    var isLoading: Bool = false {
        didSet {
            self.isLoading ? self.activityLoader.beginRefreshing() : self.activityLoader.endRefreshing()
            UIView.animate(CATransaction.animationDuration()) { self.finishRegistration.alpha = self.isLoading ? 0 : 1 }
        }
    }
}
