//
//  SignUpStepSexChouse.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class SignUpStepSexChouseViewController: BaseSignUpViewController {

    @IBOutlet weak var errorDescriptionLabel: UILabel!

    var sexType: SignUpModel.SexType = .unknown {
        didSet {
            sessionModel.sex.value = sexType
        }
    }
    override var step: SignUpStepsGenerator.StepType {
        return .sex
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func bind() {
        sessionModel.isValid.map { $0 }.bind(onNext: {  [weak self] isValid in
            self?.nextStep.isHidden = !isValid.0
            self?.errorDescriptionLabel.text = isValid.1
            UIView.animate(CATransaction.animationDuration()) {
                self?.nextStep.alpha = isValid.0 ? 1 : 0
            }
        }).disposed(by: disposeBag)
    }

    @IBOutlet weak var nextStep: UIButton!

    @IBAction func nextStep(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didTapNext?()
    }

    @IBOutlet weak var maleButton: UIButton!
    @IBAction func maleButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.sexType = .male
        self.animateChouse(self.sexType)
    }

    @IBOutlet weak var femaleButton: UIButton!
    @IBAction func femaleButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.sexType = .female
        self.animateChouse(self.sexType)
    }

    @IBAction func closeButton(_ sender: Any) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.didChouseClose?()
    }

    func animateChouse(_ type: SignUpModel.SexType) {
        UIView.animate(CATransaction.animationDuration()) {
            switch type {
            case .female:
                self.maleButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.femaleButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            case .male:
                self.femaleButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.maleButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            default:
                break
            }
        }
    }
}
