//
//  SignUpViewControllerPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class SignUpViewControllerPresenter: NSObject {
    typealias PresenterView = SignUpViewController

    weak var view: PresenterView!
    var bag = DisposeBag()

    // CONTROLS
    private var stepScroll: SVCScrollView!
    private var stepGenerator: SignUpStepsGenerator!

    required init(view: PresenterView/* , service:RestServiceManager = RestServiceManager()*/) {
        self.view = view
    }

    func prepare() {
        self.generateSteps()
        self.setupPageControl()
    }

    func setupPageControl() {
        self.view.pageIndicatorControl.numberOfPages = self.stepScroll.controllersCount
        self.view.pageIndicatorControl.radius = 4
        self.view.pageIndicatorControl.tintColor = .white
        self.view.pageIndicatorControl.currentPageTintColor = .white
        self.view.pageIndicatorControl.padding = 6
    }

    func generateSteps() {

        self.stepGenerator = SignUpStepsGenerator(steps: [.sex, .alpha, .beta, .email, .password, .firstName, .lastName, .finish])

        let sessionModel = SignUpModel()

        stepGenerator.stepControllers.filter({$0 is BaseSignUpViewController }).compactMap({$0 as? BaseSignUpViewController }).forEach {
            $0.sessionModel = sessionModel
            self.observe($0)
        }

        self.stepScroll = SVCScrollView(frame: .equalToWindow,
                                       parentViewController: self.view,
                                       controllers: stepGenerator.stepControllers)
        stepScroll.isScrollEnabled = false
        self.view.view.addSubview(stepScroll)
    }

    private func observe(_ step: BaseSignUpViewController?) {

        step?.didChousePrevious = { [weak self] in
            let controller = self?.stepScroll.makeStep(.previous)
            self?.changeControlState()
            if let model = step?.sessionModel {
                self?.send(model: model, direction: .previous, with: controller)
            }
        }

        step?.didChouseClose = { [weak self] in
            self?.view.dismiss(animated: true, completion: nil)
        }

        step?.didChouseFinish = { [weak self] model in
            self?.signUp(with: model)
        }

        step?.didTapNext = { [weak self] in
            let controller = self?.stepScroll.makeStep(.next)
            self?.changeControlState()
            if let model = step?.sessionModel {
                self?.send(model: model, with: controller)
            }
        }
    }

    func send(model: SignUpModel, direction: SVCScrollView.StepType = .next, with controller: UIViewController?) {
		// swiftlint:disable identifier_name
        guard let vc =  controller as? BaseSignUpViewController else { return }
        vc.apply(model: model)
    }

    private func signUp(with model: SignUpModel) {
        let finishStep: SignUpStepFinishViewController? = self.stepGenerator.step()
        finishStep?.isLoading = true
        AuthManager.shared.signUp(with: model.email.value, password: model.password.value)
    }

    private func changeControlState() {
        self.view.pageIndicatorControl.set(progress: self.stepScroll.visibleVC, animated: true)
    }
}
