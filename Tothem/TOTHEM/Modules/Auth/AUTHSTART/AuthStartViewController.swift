//
//  ViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class AuthStartViewController: UIViewController {

    @IBOutlet weak var backgroundVideoView: BackgroundVideo!

    lazy var presenter: AuthStartViewControllerPresenter = AuthStartViewControllerPresenter(view: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepare()
        self.addLogo()
    }

    func prepare() {
        self.presenter.prepare()
    }

    func addLogo() {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "logo")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)

        imageView.constraintWidth(constant: 200)
        imageView.constraintHeight(constant: 200)
        imageView.constraintCenter(x: self.view)
        imageView.constraintTop(self.view, constant: 100)
    }
}

//ACTIONS 

extension AuthStartViewController {
    @IBAction func signUpPressed(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.presenter.callSignUp()
    }

    @IBAction func signInPressed(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        self.presenter.callSignIn()
    }
}
