//
//  AuthStartViewControllerPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class AuthStartViewControllerPresenter: NSObject {
    typealias PresenterView = AuthStartViewController

    weak var view: PresenterView!

    required init(view: PresenterView) {
        self.view = view
    }

    func prepare() {
        self.view.navigationController?.navigationBar.isHidden = true
        self.view.backgroundVideoView.createBackgroundVideo(name: "Background", type: "mp4", alpha: 0.5)
    }

    func callSignUp() {
        let signUpController = SignUpViewController()
        signUpController.modalTransitionStyle = .crossDissolve
        self.view.present(signUpController, animated: true, completion: nil)
    }

    func callSignIn() {
        let signInController = SignInViewController()
        signInController.modalTransitionStyle = .crossDissolve
        self.view.present(signInController, animated: true, completion: nil)
    }
}
