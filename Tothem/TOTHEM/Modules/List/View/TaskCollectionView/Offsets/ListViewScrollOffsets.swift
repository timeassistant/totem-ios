//
//  ListViewScrollOffsets.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct ListViewScrollOffsets: CollectionViewControllerOffsets {
    private(set) var height: CGFloat = 0
    private(set) var topContentHeight: CGFloat = 0
    private(set) var bottomContentHeight: CGFloat = 0
    private(set) var middle: CGFloat = 0
    private(set) var topInsets: UIEdgeInsets = .zero
    private(set) var bottomInsets: UIEdgeInsets = .zero
    var topOffset: CGPoint = .zero
    var bottomOffset: CGPoint = .zero
    private(set) var minOffset: CGFloat = 0
    
    var topStreching: Bool = false
    var bottomStreching: Bool = false
    
    init(height: CGFloat, min: CGFloat, top: CGFloat, bottom: CGFloat) {
        self.height = height
        self.topContentHeight = top
        self.bottomContentHeight = bottom
        self.minOffset = min
        
        calcInsets()
        self.middle = initalMiddle
        calcOffsets()
    }
    
    private mutating func calcInsets() {
        if topContentHeight == 0 {
            bottomInsets = .init(top: minOffset, left: 0, bottom: 0, right: 0)
        } else {
            topInsets = .init(top: 0, left: 0, bottom: height - maxMiddle, right: 0)
            bottomInsets = .init(top: maxMiddle, left: 0, bottom: 0, right: 0)
        }

        topStreching = bottomContentHeight > topInsets.bottom
        bottomStreching = !topStreching
    }

    private var maxMiddle: CGFloat {
        return max(height * 0.5, min(topContentHeight, height * 0.85))
    }

    private var minMiddle: CGFloat {
        return topContentHeight > 0 ? max(height * 0.25, height - bottomContentHeight) : minOffset
    }
    
    private var initalMiddle: CGFloat {
        return bottomInsets.top
    }
    
    private mutating func calcOffsets() {
        topOffset = CGPoint(x: 0, y: max(topContentHeight - middle, 0))
        bottomOffset = CGPoint(x: 0, y: -1 * middle)
    }

    func setting(top: CGFloat, bottom: CGFloat) -> CollectionViewControllerOffsets {
        var offsets = ListViewScrollOffsets(height: height, min: minOffset, top: top, bottom: bottom)
        
        offsets.topOffset = topOffset
        offsets.bottomOffset = bottomOffset
        offsets.middle = middle
        offsets.bottomStreching = bottomStreching
        offsets.topStreching = topStreching

        if top == 0 {
            offsets.bottomStreching = true
            offsets.topStreching = false
            offsets = offsets.bottomOpened
        } else if bottom == 0 {
            offsets.bottomStreching = false
            offsets.topStreching = true
            offsets = offsets.topOpened
        } else if top + bottom < height {
            offsets.bottomStreching = false
            offsets.topStreching = false
            offsets = offsets.topOpened
        } else {
            offsets = topStreching ? offsets.topOpened : offsets.bottomOpened
        }

        return offsets
    }

    private var copy: ListViewScrollOffsets {
        var offsets = ListViewScrollOffsets(height: height, min: minOffset, top: topContentHeight, bottom: bottomContentHeight)
        offsets.topOffset = topOffset
        offsets.middle = middle
        offsets.bottomOffset = bottomOffset

        offsets.topStreching = topStreching
        offsets.bottomStreching = bottomStreching

        offsets.topInsets = topInsets
        offsets.bottomInsets = bottomInsets
        return offsets
    }

    private var topOpened: ListViewScrollOffsets {
        return ListViewScrollOffsets(height: height, min: minOffset, top: topContentHeight, bottom: bottomContentHeight)
    }

    private var bottomOpened: ListViewScrollOffsets {
        var offsets = copy
        offsets.middle = minMiddle
        offsets.bottomOffset.y = bottomOffset.y + middle - minMiddle
        offsets.topOffset.y = max(topContentHeight - minMiddle, 0)
        offsets.topInsets = .init(top: 0, left: 0, bottom: height - minMiddle, right: 0)
        offsets.bottomInsets = .init(top: minMiddle, left: 0, bottom: 0, right: 0)
        offsets.bottomStreching = true
        offsets.topStreching = false
        return offsets
    }

    private let pullOffset: CGFloat = -70
    func scrolling(top: CGFloat) -> CollectionViewControllerOffsets {
        let scrollingUp = top > topOffset.y
        let scrollingDown = top < topOffset.y
        
        if scrollingDown, !topStreching, topContentHeight - (top + middle) > 0 {
            return topOpened
        } else if scrollingUp, topStreching, topContentHeight - (top + middle) < pullOffset {
            return bottomOpened
        } else {
            var offsets = copy
            offsets.topOffset.y = top
            return offsets
        }
    }

    func scrolling(bottom: CGFloat) -> CollectionViewControllerOffsets {
        let scrollingUp = bottom > bottomOffset.y
        let scrollingDown = bottom < bottomOffset.y

        if scrollingUp, !bottomStreching, middle + bottom > 0 {
            return bottomOpened
        } else if scrollingDown, bottomStreching, middle + bottom < pullOffset {
            return topOpened
        } else {
            var offsets = copy
            offsets.bottomOffset.y = bottom
            return offsets
        }
    }

    var initialTopOffsets: CGPoint {
        return topStreching ? topOpened.topOffset : bottomOpened.topOffset
    }

    var initialBottomOffsets: CGPoint {
        return CGPoint(x: 0, y: -1 * middle)
    }
}
