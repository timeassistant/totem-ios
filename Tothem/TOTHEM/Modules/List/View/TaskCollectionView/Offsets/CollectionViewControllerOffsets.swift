//
//  CollectionViewControllerOffsets.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol CollectionViewControllerOffsets {
    var topContentHeight: CGFloat { get }
    var bottomContentHeight: CGFloat { get }
    
    var middle: CGFloat { get }
    var topInsets: UIEdgeInsets { get }
    var bottomInsets: UIEdgeInsets { get }
    var topOffset: CGPoint { get set }
    var bottomOffset: CGPoint { get set  }
    var topStreching: Bool { get set }
    var bottomStreching: Bool { get set }
    
    func setting(top: CGFloat, bottom: CGFloat) -> CollectionViewControllerOffsets
    func scrolling(top: CGFloat) -> CollectionViewControllerOffsets
    func scrolling(bottom: CGFloat) -> CollectionViewControllerOffsets

    var initialTopOffsets: CGPoint { get }
    var initialBottomOffsets: CGPoint { get }
}
