//
//  ListPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct TaskCollectionViewModel {
    var items: [TaskModel]
}

enum UseCaseResult {
    case loading
    case viewModel(TaskCollectionViewModel)
    case error(Error?)
    case insert(Int)
    case delete(TaskModel)
    case transfer(TaskModel)
    case parameters(CellParametersModel, CellParametersModel)
}

enum ListEvent: Event {
    case tapOnZero(TimesetType)
    case tapCreateButton(TimesetType)
    case dragPanel(TimesetType)
    case preview(TimesetType)
    var key: String {
        switch self {
        case .tapOnZero(let side):
            return "z" + side.eventKey
        case .tapCreateButton(let side):
            return "c" + side.eventKey
        case .dragPanel(let side):
            return "d" + side.eventKey
        case .preview(let side):
            return "v" + side.eventKey
        }
    }
    
    var classify: Bool {
        return false
    }
}

final class ListPresenter {
    var state: Observable<ListViewState>!
    var events = PublishSubject<ListViewEvent>()
    var side: TimesetType
    init(side: TimesetType) {
        self.side = side
        
        let result = events
            .flatMap { [weak self] event -> Observable<UseCaseResult> in
            guard let self = self else { return Observable.empty() }
            return self.perform(event: event)
        }

        state = result
            .scan(ListViewState.loading) { [weak self] _, result in
            guard let self = self else { return .loading }
            return self.catchUseCase(result: result)
        }
    }

    private func perform(event: ListViewEvent) -> Observable<UseCaseResult> {
        switch event {
        case .loaded:
            return ListPresenter.loaded(side: side)
        case .parameters:
            return ListPresenter.parameters(side: side)
        case .tap(let model, let selected):
            return ListPresenter.tap(side, model, selected)
        case .action(let model, let action, let position):
            switch action {
            case .delete:
                return ListDeleteUseCase.shared
                    .execute(model)
                    .map({ _ in .loading })
            case .check:
                return ListCheckUseCase.shared
                    .execute(model)
                    .map({ _ in .loading })
            case .transfer:
                var side: TimesetType
                let task = GetTasksUseCase.shared.task(id: model.viewModel.modelId)
                if task?.mode == .combined {
                    side = position == .right ? .left : .right
                } else {
                    side = .center
                }
                
                return ChangeSidesUseCase.shared
                    .execute(model, side: side)
                    .map({ _ in .loading })
            default: return Observable.empty()
            }
        case .createBetween(let empty, let m1, let m2):
            return ListPresenter.create(side, empty, m1, m2)
            
        }
    }
    
    private func catchUseCase(result: UseCaseResult) -> ListViewState {
        switch result {
        case .error(let err): return .error(err)
        case .viewModel(let viewModel):
            return ListPresenter.dataSourceState(side, viewModel)
        case .parameters(let top, let bottom):
            return .parameters(top, bottom)
        default: return .loading
        }
    }
    
    private class func dataSourceState(_ side: TimesetType, _ viewModel: TaskCollectionViewModel) -> ListViewState {
        let items = CreateTaskCollectionsUseCase.create(side, viewModel.items)
        let top = CollectionViewModel(side: side, items: items.0)
        let bottom = CollectionViewModel(side: side, items: items.1)
        return .update(CollectionViewControllerViewModel(side: side, top: top, bottom: bottom))
    }

    private static func loaded(side: TimesetType) -> Observable<UseCaseResult> {
        return GetTasksUseCase.shared.execute(side: side)
            .map({ $0 as? [TaskModel] })
            .skipNil()
            .map({ .viewModel(TaskCollectionViewModel(items: $0)) })
            .catchError({ Single.just(.error($0)).asObservable() })
            .startWith(.loading)
    }

    private static func tap(_ side: TimesetType, _ model: CollectionCellModel, _ selected: Bool) -> Observable<UseCaseResult> {
        return Observable.just(.loading)
//        guard model.viewModel.isEditable else { return Observable.empty() }
//        return model.viewModel.isEmpty ? ListCreateUseCase().execute(side: side, selected: selected) : ListPreviewUseCase().execute(model, side: side)
    }

    private static func create(_ side: TimesetType, _ empty: CollectionCellModel, _ m1: CollectionCellModel, _ m2: CollectionCellModel) -> Observable<UseCaseResult> {
        guard m1.viewModel.isEditable && m2.viewModel.isEditable else { return Observable.empty() }
        let position = (m1.viewModel.position + m2.viewModel.position) / 2
        return ListCreateUseCase().execute(side: side, position: position, drag: true)
    }

    private static var topPresenters: [TimesetType: CellPresenter] = [:]
    private static var bottomPresenters: [TimesetType: CellPresenter] = [:]

    private static func parameters(side: TimesetType) -> Observable<UseCaseResult> {

        var top: Observable<CellParametersModel>
        var bottom: Observable<CellParametersModel>
        switch side {
        case .center:
            topPresenters[side] = CellPresenter(side: side, checked: false)
            bottomPresenters[side] = CellPresenter(side: side, checked: true)
        case .left:
            topPresenters[side] = CellPresenter(side: side, checked: nil)
        case .right:
            bottomPresenters[side] = CellPresenter(side: side, checked: nil)
        }

        if let state = topPresenters[side]?.state {
            top = state.asObservable()
        } else {
            top = Observable.just(CellParametersModel.default)
        }
        if let state = bottomPresenters[side]?.state {
            bottom = state.asObservable()
        } else {
            bottom = Observable.just(CellParametersModel.default)
        }

        return Observable.combineLatest(top, bottom)
            .map({ UseCaseResult.parameters($0.0, $0.1) })
    }
}
