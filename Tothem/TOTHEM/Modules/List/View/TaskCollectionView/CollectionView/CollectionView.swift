//
//  CollectionView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CollectionView: UICollectionView {
    var visibleFrame: CGRect = .zero

    var focusedCell: LayoutModel?
    var previewCell: LayoutModel?
    
    var processor: CollectionViewLayoutProcessorProtocol = CollectionViewLayoutProcessor()
    
    let collectionDataSource = CollectionViewDataSource()
    var parameters = CellParametersModel.default {
        didSet {
            insets.left = parameters.insets.left
            insets.right = parameters.insets.right
            
            visibleCells.forEach {
                guard let cell = $0 as? CollectionViewCell else { return }
                cell.apply(parameters: parameters)
            }
        }
    }

    var insets: UIEdgeInsets = .zero
    
    var contentWidth: CGFloat {
        guard let layout = collectionViewLayout as? CollectionViewLayout else { return contentSize.height }

        return layout.viewModel.contentHeight
    }

    var contentHeight: CGFloat {
        guard let layout = collectionViewLayout as? CollectionViewLayout else { return contentSize.height }

        return layout.viewModel.contentHeight
    }

    var updateCompletion: ((Bool) -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        dataSource = collectionDataSource
        addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
    }
    
    // MARK: - Updating CollectionViewLayout

    func setLayout(viewModel: CollectionViewLayoutModel) {
        guard let layout = collectionViewLayout as? CollectionViewLayout else { return }
        layout.viewModel = viewModel
    }

    func set(_ models: [CollectionCellModel]) {
        let diff = collectionDataSource.set(models)

        var inserts: [IndexPath] = []
        var deletes: [IndexPath] = []
        var replaces: [IndexPath] = []
        var moves: [(IndexPath, IndexPath)] = []

        diff.forEach({
            switch $0 {
            case .delete(_, let row):
                deletes.append(IndexPath(row: row, section: 0))
            case .insert(_, let row):
                inserts.append(IndexPath(row: row, section: 0))
            case .replace(_, _, let row):
                replaces.append(IndexPath(row: row, section: 0))
            case .move(_, let from, let to):
                moves.append((IndexPath(row: from, section: 0), IndexPath(row: to, section: 0)))
            }
        })

        performBatchUpdates({
            self.insertItems(at: inserts)
            self.deleteItems(at: deletes)
            moves.forEach({
                self.moveItem(at: $0.0, to: $0.1)
            })
        }, completion: {
            self.reloadItems(at: replaces)
            self.updateCompletion?($0)
        })
    }

    func updateLayout() {
        performBatchUpdates(nil, completion: updateCompletion)
    }

    func reloadLayout() {
        setLayout(viewModel: newLayoutViewModel)
        collectionViewLayout.invalidateLayout()
    }

    var newLayoutViewModel: CollectionViewLayoutModel {
        var focused = focusedCell
        let width = bounds.size.width
        let models = collectionDataSource.models
        let parameters = self.parameters
        let preview = previewCell != nil
        if models.count == 0 {
            focused = nil
        }
        processor.viewHeight = bounds.height
        processor.insets = insets
        return processor.currentViewModel(models, focused: focused, width: width, parameters: parameters, preview: preview)
    }

    func height(_ models: [CollectionCellModel]) -> CGFloat {
        let width = bounds.size.width
        let parameters = self.parameters
        let contentWidth = width - parameters.insets.left - parameters.insets.right
        let sizes: [CGSize] = models.map { return $0.sizing(contentWidth, parameters) }
        return sizes.reduce(0, { $0 + $1.height })
    }
    
    override func performBatchUpdates(_ updates: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
        let viewModel = self.newLayoutViewModel
        super.performBatchUpdates({
            self.setLayout(viewModel: viewModel)
            updates?()
        }, completion: completion)
    }

    func bottomCellIndexPath(inset: CGFloat) -> IndexPath? {
        guard let superview = superview else { return nil }
        let border = superview.bounds.maxY - inset
        var closest: UICollectionViewCell?
        var closestBottom: CGFloat = CGFloat.greatestFiniteMagnitude
        if let cell = visibleCells.first(where: {
                let cellFrame = $0.convert($0.bounds, to: superview)
                if border - cellFrame.maxY <= closestBottom {
                    closest = $0
                    closestBottom = border - cellFrame.maxY
                }
                return cellFrame.minY < border && cellFrame.maxY >= border
            }) {
            return indexPath(for: cell)
        }

        if let cell = closest {
            return indexPath(for: cell)
        }

        return nil
    }

    func topCellIndexPath(inset: CGFloat) -> IndexPath? {
        guard let superview = superview else { return nil }
        let border = inset
        var closest: UICollectionViewCell?
        var closestTop: CGFloat = CGFloat.greatestFiniteMagnitude
        if let cell = visibleCells.first(where: {
            let cellFrame = $0.convert($0.bounds, to: superview)
            if cellFrame.minY <= closestTop {
                closest = $0
                closestTop = cellFrame.minY
            }
            return cellFrame.minY <= border && cellFrame.maxY > border
        }) {
            return indexPath(for: cell)
        }

        if let cell = closest {
            return indexPath(for: cell)
        }

        return nil
    }

    override func reloadData() {
        // use reload layout or update layout
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        //print(contentOffset)
    }
}
