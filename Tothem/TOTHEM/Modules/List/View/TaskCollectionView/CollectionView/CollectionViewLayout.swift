//
//  CollectionViewLayout.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct CollectionViewLayoutModel {
    var attributes: [UICollectionViewLayoutAttributes] = []
    var contentHeight: CGFloat = 0
    var contentWidth: CGFloat = 0
    var contentOffset: CGPoint?
    init() { }
}

final class CollectionViewLayout: UICollectionViewLayout {

    var viewModel: CollectionViewLayoutModel
    
    override public var collectionViewContentSize: CGSize {
        return CGSize(width: viewModel.contentWidth, height: viewModel.contentHeight)
    }

    init(viewModel: CollectionViewLayoutModel) {
        self.viewModel = viewModel
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        viewModel = CollectionViewLayoutModel()
        super.init(coder: aDecoder)
    }

    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return layoutAttributesForItem(at: itemIndexPath)
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard indexPath.row < viewModel.attributes.count else { return nil }
        return viewModel.attributes[indexPath.row]
    }
    
    override public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return false
    }

    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        for attributes in viewModel.attributes where attributes.frame.intersects(rect) {
            visibleLayoutAttributes.append(attributes)
        }
        return visibleLayoutAttributes
    }

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return viewModel.contentOffset ?? proposedContentOffset
    }
}
