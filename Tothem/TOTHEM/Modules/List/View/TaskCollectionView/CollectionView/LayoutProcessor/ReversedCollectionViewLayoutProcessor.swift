//
//  ReversedCollectionViewLayoutProcessor.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class ReversedCollectionViewLayoutProcessor: CollectionViewLayoutProcessorProtocol {
    var insets: UIEdgeInsets = .zero
    var offsets: CGPoint?
    
    var screenHeight: CGFloat = UIScreen.main.bounds.height
    var viewHeight: CGFloat = UIScreen.main.bounds.height
    
    func currentViewModel(_ models: [CollectionCellModel], focused: UICollectionView.LayoutModel?, width: CGFloat, parameters: CellParametersModel, preview: Bool) -> CollectionViewLayoutModel {
        let contentWidth = width - insets.left - insets.right
        let sizes: [CGSize] = models.map { return $0.sizing(contentWidth, parameters) }

        guard sizes.count > 0 else {
            return CollectionViewLayoutModel()
        }

        var viewModel: CollectionViewLayoutModel
        if let model = focused {
            viewModel = self.focused(model, sizes: sizes, preview: preview)
        } else {
            viewModel = self.setup(sizes: sizes)
            viewModel.contentOffset = offsets
        }
        
        viewModel.contentWidth = contentWidth
        return viewModel
    }

    private func setup(sizes: [CGSize]) -> CollectionViewLayoutModel {
        var viewModel = CollectionViewLayoutModel()
        var attributes: [UICollectionViewLayoutAttributes] = []
        var contentHeight: CGFloat = insets.bottom
        let cellInsets = insets
        for (index, item) in sizes.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            let origin = CGPoint(x: cellInsets.left, y: contentHeight)
            attribute.frame = CGRect(origin: origin, size: item)
            attribute.zIndex = index
            attributes.append(attribute)
            contentHeight = attribute.frame.maxY
        }

        var origin: CGFloat = contentHeight - insets.bottom
        for item in attributes {
            item.frame.origin.y = origin - item.frame.size.height
            origin -= item.frame.size.height
        }
        
        if contentHeight < viewHeight {
            let diff = viewHeight - contentHeight
            contentHeight = viewHeight
            attributes.forEach {
                $0.frame.origin.y += diff
            }
        }
        
        viewModel.attributes = attributes
        viewModel.contentHeight = contentHeight
        return viewModel
    }
    
    private func focused(_ model: UICollectionView.LayoutModel, sizes: [CGSize], preview: Bool) -> CollectionViewLayoutModel {
        let focusedFrame = model.frame
        var viewModel = setup(sizes: sizes)
        var contentOffset: CGPoint = .zero
        let attributes = viewModel.attributes
        var contentHeight: CGFloat = viewModel.contentHeight
        let focused = model.indexPath.row

        if preview {
            attributes[focused].frame = focusedFrame
            var frame = focusedFrame
            for index in stride(from: focused + 1, to: attributes.count, by: 1) {
                let item = attributes[index]
                item.frame.origin.y = frame.minY - item.frame.height
                frame = item.frame
            }

            frame = focusedFrame
            for index in stride(from: focused - 1, to: -1, by: -1) {
                let item = attributes[index]
                item.frame.origin.y = frame.maxY
                frame = item.frame
            }

            contentHeight = attributes.last?.frame.maxY ?? 0
            contentHeight = max(contentHeight, screenHeight)
        } else if focused < attributes.count {
            contentOffset.y = attributes[focused].frame.maxY - focusedFrame.maxY
        }

        viewModel.attributes = attributes
        viewModel.contentHeight = contentHeight
        viewModel.contentOffset = contentOffset
        return viewModel
    }
}
