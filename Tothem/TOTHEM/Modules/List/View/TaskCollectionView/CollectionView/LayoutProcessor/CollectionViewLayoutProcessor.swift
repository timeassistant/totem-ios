//
//  CollectionViewLayoutProcessor.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class CollectionViewLayoutProcessor: CollectionViewLayoutProcessorProtocol {
    var insets: UIEdgeInsets = .zero
    var offsets: CGPoint?
    var contentWidth: CGFloat = UIScreen.main.bounds.width
    var screenHeight: CGFloat = UIScreen.main.bounds.height
    var viewHeight: CGFloat = UIScreen.main.bounds.height
    func currentViewModel(_ models: [CollectionCellModel], focused: UICollectionView.LayoutModel?, width: CGFloat, parameters: CellParametersModel, preview: Bool) -> CollectionViewLayoutModel {
        contentWidth = width - parameters.insets.left - parameters.insets.right
        let sizes: [CGSize] = models.map { return $0.sizing(contentWidth, parameters) }

        guard sizes.count > 0 else {
            return CollectionViewLayoutModel()
        }

        if let model = focused {
            return self.focused(model, sizes: sizes, preview: preview)
        } else {
            return self.setup(sizes: sizes)
        }
    }
    
    private func setup(sizes: [CGSize]) -> CollectionViewLayoutModel {
        var viewModel = CollectionViewLayoutModel()
        var attributes: [UICollectionViewLayoutAttributes] = []
        var contentHeight: CGFloat = insets.top
        let cellInsets = insets
        for (index, item) in sizes.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            let origin = CGPoint(x: cellInsets.left, y: contentHeight)
            attribute.frame = CGRect(origin: origin, size: item)
            attribute.zIndex = index
            attributes.append(attribute)
            contentHeight = attribute.frame.maxY
        }
        
        viewModel.contentWidth = contentWidth
        viewModel.attributes = attributes
        viewModel.contentHeight = max(contentHeight + insets.bottom, viewHeight)
        return viewModel
    }
    
    private func focused(_ model: UICollectionView.LayoutModel, sizes: [CGSize], preview: Bool) -> CollectionViewLayoutModel {
        let focusedFrame = model.frame
        var viewModel = setup(sizes: sizes)
        let contentOffset: CGPoint = .zero
        let attributes = viewModel.attributes
        var contentHeight: CGFloat = viewModel.contentHeight
        let focused = model.indexPath.row

        if preview {
            attributes[focused].frame = focusedFrame
            var frame = focusedFrame
            for index in stride(from: focused - 1, to: -1, by: -1) {
                let item = attributes[index]
                item.frame.origin.y = frame.minY - item.frame.height
                frame = item.frame
            }

            frame = focusedFrame
            for index in stride(from: focused + 1, to: attributes.count, by: 1) {
                let item = attributes[index]
                item.frame.origin.y = frame.maxY
                frame = item.frame
            }

            contentHeight = attributes.last?.frame.maxY ?? 0
            contentHeight = max(contentHeight, screenHeight)
        }

        viewModel.attributes = attributes
        viewModel.contentHeight = contentHeight
        viewModel.contentOffset = contentOffset
        return viewModel
    }
}
