//
//  CollectionViewDataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum CollectionDiff {
    case delete(CollectionCellModel, Int)
    case insert(CollectionCellModel, Int)
    case move(CollectionCellModel, Int, Int)
    case replace(CollectionCellModel, CollectionCellModel, Int)
}

final class CollectionViewDataSource: NSObject, UICollectionViewDataSource {
    static let separatorReuseId = "SeparatorCell"
    static let emptyReuseId = "EmptyCollectionViewCell"
    static let centerCellReuseId = "CenterCollectionViewCell"
    static let centerDescriptionCellReuseId = "CenterDescriptionViewCell"
    static let centerActionCellReuseId = "CenterActionViewCell"
    static let rightCellReuseId = "RightCollectionViewCell"
    static let leftCellReuseId = "LeftCollectionViewCell"
    static let similarTaskCellReuseId = "SimilarTaskCell"
    static let similarTextEntityCellReuseId = "SimilarTextEntityCell"
    static let similarPhotoEntityCellReuseId = "SimilarPhotoEntityCell"

    private(set) var models: [CollectionCellModel] = []

    var inPreview: IndexPath?
    var parameters = CellParametersModel.default
    
    var actions = PublishSubject<(CollectionCellModel, ListCellAction, ListCellActionPosition)>()

    var emptyModel: CollectionCellModel? {
        return models.first(where: { $0.viewModel.isEmpty && $0.viewModel.isEditable })
    }

    func indexPath(_ model: CollectionCellModel) -> IndexPath? {
        if let index = models
            .firstIndex(where: { $0 == model }) {
            return IndexPath(item: index, section: 0)
        }
        return nil
    }
    
    func indexPath(_ modelIdentifier: String) -> IndexPath? {
        if let index = models
            .firstIndex(where: { $0.identifier == modelIdentifier }) {
            return IndexPath(item: index, section: 0)
        }
        return nil
    }

    func set(_ models: [CollectionCellModel]) -> [CollectionDiff] {
        let diff = CollectionCellsManager.calculate(self.models, newModels: models)
        self.models = models
        return diff
    }

    // MARK: - UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model = models[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: model.reuseId, for: indexPath)

        if let cell = cell as? CollectionViewCell {
            cell.set(model)
            cell.apply(parameters: parameters)
            if let preview = inPreview {
                cell.set(fade: preview != indexPath)
            }
        }

        if let cell = cell as? ContentCollectionViewCell {
            cell.actions = actions
            if let preview = inPreview {
                cell.set(contentHidden: preview == indexPath)
            }
        }

        return cell
    }
}
