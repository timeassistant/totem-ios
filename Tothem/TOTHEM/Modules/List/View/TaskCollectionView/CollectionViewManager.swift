//
//  CollectionViewManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum CollectionViewState {
    case set(CollectionViewModel)
    case setViewModels([CollectionCellModel])
    case apply
    case insets(UIEdgeInsets)
    case offsets(CGPoint?)
}

final class CollectionViewManager {
    weak var collectionView: CollectionView?
    weak var viewController: UIViewController?
    
    var handler: CollectionHandler!
    var side: TimesetType = .center
    weak var presenter: ListPresenter?
    
    private var offset: CGPoint?
    private var insets: UIEdgeInsets = .zero
    var preview: Bool = false
    
    func set(_ state: CollectionViewState) {
        switch state {
        case .set(let viewModel):
            self.side = viewModel.side
            self.handler.viewModel = viewModel
        case .setViewModels(let viewModels):
            self.handler.cellModels = viewModels
        case .apply:
            handler.applyChanges()
        case .insets(let insets):
            self.insets = insets
            collectionView?.insets.top = insets.top
            collectionView?.insets.bottom = insets.bottom
        case .offsets(let offset):
            self.offset = offset
            collectionView?.processor.offsets = offset
        }
    }
    
    private let sourceIndexPath = IndexPath(item: 1, section: 0)
    private var fromIndex: IndexPath?
    func insertionBegin(with view: UIView) {
        guard !preview else { return }
        if collectionView?.collectionDataSource.emptyModel != nil {
            fromIndex = sourceIndexPath
            handler.beginMovement(at: sourceIndexPath)
        }
    }
    
    func insertionChanged(with view: UIView) {
        guard let from = fromIndex,
            let collectionView = collectionView else { return }
        
        let location = view.convert(CGPoint(x: view.bounds.midX, y: view.bounds.midY), to: collectionView)
        if let to = collectionView.indexPathForItem(at: location) {
            handler.changeMovement(from: from, to: to)
            fromIndex = to
        }
    }
    
    func insertionEnded(with view: UIView) {
        guard fromIndex != nil,
            let collectionView = collectionView else { return }
        
        fromIndex = nil
        
        let location = view.convert(CGPoint(x: view.bounds.midX, y: view.bounds.midY), to: collectionView)
        
        if let to = collectionView.indexPathForItem(at: location) {
            handler.endInsertion(from: sourceIndexPath, to: to)
            
            let dataSource = collectionView.collectionDataSource
            if let empty = dataSource.emptyModel {
                var model1: CollectionCellModel?
                var model2: CollectionCellModel?
                
                if to.row < dataSource.models.count - 1 {
                    model1 = dataSource.models[to.row + 1]
                }
                if to.row > 0 {
                    model2 = dataSource.models[to.row - 1]
                }
                
                if let m1 = model1, let m2 = model2 {
                    let event = ListViewEvent.createBetween(empty: empty, m1: m1, m2: m2)
                    presenter?.events.on(.next(event))
                }
            }
        } else {
            handler.endInsertion(from: sourceIndexPath, to: sourceIndexPath)
        }
    }
    
    private var initialPreviewFrame: CGRect?
    
    func set(preview model: CollectionCellPreviewModel) {
        guard let index = collectionView?
            .collectionDataSource.indexPath(model.model.identifier), let viewController = viewController else { return }
        
        var preview = false
        var frame = self.frame(model.model, in: viewController.view) ?? .zero
        
        if let fromView = model.fromView, let fromFrame = model.fromFrame {
            preview = true
            
            if initialPreviewFrame == nil {
                initialPreviewFrame = frame
            }
            
            frame = fromView.convert(fromFrame, to: viewController.view)
            frame.origin.x = 0
            
        } else {
            frame = initialPreviewFrame ?? .zero
            initialPreviewFrame = nil
        }
        
        self.preview = preview
        
        collectionView?.collectionDataSource.inPreview = preview ? index : nil
        
        if let cell = collectionView?.cellForItem(at: index) {
            animate(cell, to: frame, preview: preview)
        } else {
            UIView.animate(withDuration: 0, animations: {
                self.collectionView?.scrollToItem(at: index, at: .centeredVertically, animated: false)
            }, completion: { _ in
                self.set(preview: model)
            })
        }
    }
    
    private func animate(_ cell: UICollectionViewCell, to frame: CGRect, preview: Bool) {
        guard let previewCell = cell as? ContentCollectionViewCell else { return }
        
        collectionView?.bringSubviewToFront(previewCell)
        
        let newFrame = viewController?.view.convert(frame, to: previewCell) ?? .zero
        let oldPreviewFrame = previewCell.bounds
        
        let dy = newFrame.midY - oldPreviewFrame.midY
        let dh = (newFrame.height - oldPreviewFrame.height) / 2
        
        UIView.animate(CATransaction.animationDuration()) {
            previewCell.set(contentHidden: preview)
            self.collectionView?.visibleCells.forEach({
                guard $0 != previewCell, let cell = $0 as? ContentCollectionViewCell else { return }
                cell.set(fade: preview)
                
                if !preview {
                    cell.transform = .identity
                } else if cell.center.y < previewCell.center.y {
                    let y: CGFloat = dy - dh
                    cell.transform = CGAffineTransform(translationX: 0, y: y)
                } else {
                    let y: CGFloat = dy + dh
                    cell.transform = CGAffineTransform(translationX: 0, y: y)
                }
            })
            previewCell.colorView.frame = newFrame
            previewCell.contentView.frame = previewCell.contentView.bounds
        }
    }
    
    func frame(_ model: CollectionCellModel, in view: UIView) -> CGRect? {
        if let index = collectionView?.collectionDataSource.indexPath(model) {
            if let layout = collectionView?.collectionViewLayout as? CollectionViewLayout,
                let attr = layout.layoutAttributesForItem(at: index) {
                return collectionView?.convert(attr.frame, to: view)
            }
        }
        return nil
    }
}
