//
//  LeftCollectionViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class LeftCollectionViewCell: ContentCollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBackView: UIView!

    @IBOutlet weak var imageView1: TaskImageView!
    @IBOutlet weak var imageView2: TaskImageView!
    @IBOutlet weak var imageView3: TaskImageView!
    
    static var titleFont = UIFont.systemFont(ofSize: 23, weight: .semibold)
    
    @IBOutlet weak var morePhotos: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        resetToEmpty()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        resetToEmpty()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let spacing: CGFloat = 5
        let contentWidth = contentView.bounds.width - spacing * 2

        var frame = contentView.bounds
        contentContainerView?.frame = frame

        let height = Symbol.referenceSymbol.size(width: contentWidth, font: LeftCollectionViewCell.titleFont).height
        frame = CGRect(x: spacing, y: 0, width: contentWidth, height: height)
        frame.origin = .zero
        frame.origin.x = 5
        titleLabel.frame = frame

        frame.origin = .zero
        frame.size.width = contentView.bounds.width
        titleBackView.frame = frame

        frame.origin.y += height + spacing
        let width = (contentView.bounds.width - 4 * spacing) / 3
        frame.size.height = width
        frame.size.width = width
        frame.origin.x = spacing
        imageView1.frame = frame
        imageView1.imageView?.frame = imageView1.bounds

        frame.origin.x += width + spacing
        imageView2.frame = frame
        imageView2.imageView?.frame = imageView2.bounds

        frame.origin.x += width + spacing
        imageView3.frame = frame
        imageView3.imageView?.frame = imageView3.bounds
        
        morePhotos.frame = frame
    }

    private func resetToEmpty() {
        imageView1.reset()
        imageView2.reset()
        imageView3.reset()

        titleLabel.text = nil
        titleLabel.font = CenterCollectionViewCell.titleFont
        morePhotos.isHidden = true
    }

    override func set(_ model: CollectionCellModel) {
        super.set(model)

        guard let viewModel = model.viewModel as? LeftCollectionViewCellViewModel else { return }

        contentContainerView?.isHidden = viewModel.isEmpty

        titleLabel.text = viewModel.title

        if let observable = viewModel.photo1 {
            imageView1.set(observable)
        }

        if let observable = viewModel.photo2 {
            imageView2.set(observable)
        }

        if let observable = viewModel.photo3 {
            imageView3.set(observable)
        }
        
        morePhotos.isHidden = viewModel.morePhotos == 0
        morePhotos.text = "+\(viewModel.morePhotos)"
    }

    override func apply(parameters: CellParametersModel) {
        super.apply(parameters: parameters)
        var scale = CGFloat(parameters.normal / 10) + 1
        titleLabel.transform = CGAffineTransform(scaleX: scale, y: scale)

        scale = CGFloat(1 + parameters.normal/2)
        let viewScale = CGFloat(1 - parameters.normal/10)
        [imageView1, imageView2, imageView3].forEach {
            $0?.transform = CGAffineTransform(scaleX: viewScale, y: viewScale)
            $0?.imageView?.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
    }

    static func height(_ model: TaskModel, width: CGFloat) -> CGFloat {
        guard !model.isEmpty else { return 60 }

        let contentWidth = width
        var height: CGFloat = 0

        //title
        height += Symbol.referenceSymbol.size(width: contentWidth, font: LeftCollectionViewCell.titleFont).height

        let imagesNumber: CGFloat = 3
        let spacing: CGFloat = 5
        height += (contentWidth - (imagesNumber * 1) * spacing) / imagesNumber + 2 * spacing

        return max(60, height)
    }
}
