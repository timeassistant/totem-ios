//
//  CollectionViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class CollectionViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {

    var colorView: UIView!
    var fadeView: UIView!

    var disposeBag = DisposeBag()
    weak var panGestureRecognizer: UIPanGestureRecognizer?
    var actions: PublishSubject<(CollectionCellModel, ListCellAction, ListCellActionPosition)>?

    private var availableActions: [(ListCellAction, ListCellActionPosition)] = []
    private let actionVelocity: CGFloat = 500
    private(set) var model: CollectionCellModel!
    
    private var leftActionViews: [ListCellActionButton] = []
    private var rightActionViews: [ListCellActionButton] = []

    var opened: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func configureView() {
        clipsToBounds = false
        
        colorView = UIView(frame: bounds)
        colorView.translatesAutoresizingMaskIntoConstraints = false
        colorView.backgroundColor = .clear
        colorView.isUserInteractionEnabled = false
        
        contentView.addSubview(colorView)
        contentView.sendSubviewToBack(colorView)
        
        fadeView = UIView(frame: bounds)
        fadeView.translatesAutoresizingMaskIntoConstraints = false
        fadeView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        fadeView.isUserInteractionEnabled = false
        
        contentView.addSubview(fadeView)
        fadeView.alpha = 0
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
        set(fade: false)
        
        if let pan = panGestureRecognizer {
            removeGestureRecognizer(pan)
        }

        contentView.frame = bounds
        
        availableActions = []
        
        leftActionViews.forEach({ $0.removeFromSuperview() })
        leftActionViews = []
        
        rightActionViews.forEach({ $0.removeFromSuperview() })
        rightActionViews = []
    }

    func set(_ model: CollectionCellModel) {
        self.model = model
        if model.actions.count > 0 {
            addPanGesture()
            availableActions = model.actions
            
            for (action, position) in model.actions where action.short {
                let button = ListCellActionButton(type: .custom)
                button.position = position
                button.action = action
                button.backgroundColor = action.color
                button.addTarget(self, action: #selector(actionPressed(_:)), for: .touchUpInside)
                button.alpha = 0
                button.layer.masksToBounds = false
                button.layer.cornerRadius = action.size / 2
                insertSubview(button, belowSubview: contentView)
                
                switch position {
                case .left: leftActionViews.append(button)
                case .right: rightActionViews.append(button)
                case .none: break
                }
            }
        }
    }

    func apply(color: UIColor) {
        colorView.backgroundColor = color
    }

    func apply(parameters: CellParametersModel) {
        let scale = (/*max(1, parameters.agile + 0.9)*/1 - parameters.normal / 10)
        parametersTransform = CGAffineTransform(scaleX: 1, y: scale)
        applyParametersAndPanningTransforms()
        apply(color: parameters.background_color)
        apply(cornerRadius: parameters.corner_radius)
    }
    
    func apply(cornerRadius: CGFloat) {
        colorView.layer.cornerRadius = cornerRadius
    }
    
    private var parametersTransform = CGAffineTransform.identity
    private var panningTransform = CGAffineTransform.identity
    private var colorTransform = CGAffineTransform.identity
    
    func apply(panningTransform: CGAffineTransform) {
        self.panningTransform = panningTransform
        applyParametersAndPanningTransforms()
    }
    
    func apply(colorTransform: CGAffineTransform) {
        self.colorTransform = colorTransform
        applyParametersAndPanningTransforms()
    }
    
    private func applyParametersAndPanningTransforms() {
        colorView.transform = parametersTransform.concatenating(panningTransform)
            .concatenating(colorTransform)
    }

    func set(fade: Bool) {
        fadeView.alpha = fade ? 1 : 0
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        UIView.animate(CATransaction.animationDuration()) {
            self.cellLayoutSubviews()
            self.colorView.layoutIfNeeded()
        }
    }

    func cellLayoutSubviews() {
        colorView.frame = contentView.bounds
        fadeView.frame = contentView.bounds

        var frame = CGRect.zero
        for (index, button) in leftActionViews.enumerated() {
            let size: CGFloat = button.action.size
            let x: CGFloat = frame.maxX + CGFloat(index) == 0 ? 0 : 10
            let y: CGFloat = (contentView.bounds.height - size) / 2
            frame = CGRect(x: x, y: y, width: size, height: size)
            button.frame = frame
        }

        frame = contentView.bounds
        frame.origin.x = frame.size.width
        frame.size.width = 0
        for (index, button) in rightActionViews.enumerated() {
            let size: CGFloat = button.action.size
            let x: CGFloat = frame.minX - ((CGFloat(index) == 0) ? 0 : 10) - size
            let y: CGFloat = (contentView.bounds.height - size) / 2
            frame = CGRect(x: x, y: y, width: size, height: size)
            button.frame = frame
        }
    }
    
    private func layoutPan() {
        guard let pan = panGestureRecognizer else { return }

        if pan.state == UIGestureRecognizer.State.changed {
            apply(translationX: initialPanContentViewFrame.minX + pan.translation(in: self).x)

            let frame = contentView.frame
            if frame.origin.x > 0 {
                leftActionViews.forEach {
                    $0.alpha = frame.origin.x / $0.frame.maxX
                }
            } else if frame.origin.x < 0 {
                rightActionViews.forEach {
                    $0.alpha = abs(frame.origin.x) / (contentView.bounds.width - $0.frame.minX)
                }
            } else {
                leftActionViews.forEach { $0.alpha = 0 }
                rightActionViews.forEach { $0.alpha = 0 }
            }

            if opened {
                opened = abs(frame.origin.x) > 15
            }
        }
    }

    private func apply(translationX: CGFloat) {
        var frame = contentView.frame
        frame.origin.x = translationX
        contentView.frame = frame
    }

    private func addPanGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
        panGestureRecognizer = pan
    }

    private var initialPanContentViewFrame: CGRect = .zero
    @objc func onPan(_ pan: UIPanGestureRecognizer) {
        guard !model.viewModel.isEmpty else { return }

        if pan.state == .began {
            initialPanContentViewFrame = contentView.frame
        } else if pan.state == .changed {
            layoutPan()
        } else {
            if opened {
                closeActions()
            } else {
                handleActions()
            }
        }
    }

    private func handleActions() {
        guard let pan = panGestureRecognizer else { return }
        if abs(pan.velocity(in: self).x) > actionVelocity ||
            abs(pan.translation(in: self).x) > contentView.frame.width / 2 {
            if contentView.frame.origin.x < 0,
                let action = availableActions.filter({
                    $0.0.fast && $0.1 == .right }).first {
                actions?.on(.next((model, action.0, action.1)))
            }

            if contentView.frame.origin.x > 0,
                let action = availableActions.filter({
                    $0.0.fast && $0.1 == .left }).first {
                actions?.on(.next((model, action.0, action.1)))
            }
        } else {
            let left = contentView.frame.origin.x > 0

            if (left && leftActionViews.count == 0) ||
                (!left && rightActionViews.count == 0) {
                closeActions()
                return
            }

            opened = true

            let translation = left ? (leftActionViews.last?.frame.maxX ?? 0) + 10 :  (rightActionViews.last?.frame.minX ?? 0) - contentView.frame.width - 10

            UIView.animate(CATransaction.animationDuration(), animations: {
                self.apply(translationX: translation)
                self.leftActionViews.forEach({ $0.alpha = left ? 1 : 0 })
                self.rightActionViews.forEach({ $0.alpha = left ? 0 : 1 })
            })
        }
    }

    func closeActions() {
        opened = false
        UIView.animate(CATransaction.animationDuration(), animations: {
            self.setNeedsLayout()
            self.layoutIfNeeded()
        })
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let pan = panGestureRecognizer else { return true }
        guard abs((pan.velocity(in: pan.view)).x) > abs((pan.velocity(in: pan.view)).y) else { return false }
        
        return (availableActions.contains(where: { $0.1 == .left }) && pan.velocity(in: pan.view).x > 0) ||
        (availableActions.contains(where: { $0.1 == .right }) && pan.velocity(in: pan.view).x < 0)
    }
    
    @objc func actionPressed(_ button: ListCellActionButton) {
        closeActions()
        actions?.on(.next((model, button.action, button.position)))
    }
}
