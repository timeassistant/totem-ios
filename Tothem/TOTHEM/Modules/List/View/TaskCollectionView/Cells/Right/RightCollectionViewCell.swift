//
//  RightCollectionViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class RightCollectionViewCell: ContentCollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var checklistLabel1: UILabel!
    @IBOutlet weak var checklistLabel2: UILabel!

    static var titleFont = UIFont.systemFont(ofSize: 23, weight: .semibold)
    static var textFont = UIFont.systemFont(ofSize: 17)

    override func awakeFromNib() {
        super.awakeFromNib()
        
        resetToEmpty()

        titleLabel.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        checklistLabel1.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        checklistLabel2.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        resetToEmpty()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let spacing: CGFloat = 5
        let contentWidth = contentView.bounds.width - spacing * 2

        var frame = contentView.bounds
        frame.size.width = contentWidth
        frame.origin.x = spacing
        contentContainerView?.frame = frame

        var height = Symbol.referenceSymbol.size(width: contentWidth, font: RightCollectionViewCell.titleFont).height
        frame = CGRect(x: 0, y: 0, width: contentWidth, height: height)
        if let text = titleLabel.text, !text.isEmpty {
            titleLabel.frame = frame
            frame.origin.y += height
        }

        frame.origin.y += spacing
        height = Symbol.referenceSymbol.size(width: contentWidth, font: RightCollectionViewCell.textFont).height
        frame.size.height = height
        checklistLabel1.frame = frame
        frame.origin.y += height
        checklistLabel2.frame = frame
    }

    private func resetToEmpty() {
        titleLabel.text = nil
        checklistLabel1.text = nil
        checklistLabel2.text = nil

        titleLabel.font = CenterCollectionViewCell.titleFont
        checklistLabel1.font = CenterCollectionViewCell.textFont
        checklistLabel2.font = CenterCollectionViewCell.textFont
    }

    override func set(_ model: CollectionCellModel) {
        super.set(model)

        guard let viewModel = model.viewModel as? RightCollectionViewCellViewModel else { return }

        titleLabel.text = viewModel.title

        checklistLabel1.text = viewModel.bullet1
        checklistLabel2.text = viewModel.bullet2
    }

    override func apply(parameters: CellParametersModel) {
        super.apply(parameters: parameters)
        let scale = CGFloat(parameters.normal / 10) + 1
        titleLabel.transform = CGAffineTransform(scaleX: scale, y: scale)
        checklistLabel1.transform = CGAffineTransform(scaleX: scale, y: scale)
        checklistLabel2.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

    static func height(_ model: TaskModel, width: CGFloat) -> CGFloat {
        guard !model.isEmpty else { return 60 }

        let contentWidth = width
        let spacing: CGFloat = 5
        var height: CGFloat = 0
        //title
        if !model.title.isEmpty {
            height += Symbol.referenceSymbol.size(width: contentWidth, font: RightCollectionViewCell.titleFont).height
        }

        height += spacing
        let maxLineHeight = Symbol.referenceSymbol.size(width: contentWidth, font: RightCollectionViewCell.textFont).height

        if model.bullets.count > 0 {
            height += maxLineHeight + spacing
        }

        if model.bullets.count > 1 {
            height += maxLineHeight
        }

        height += spacing
        return max(60, height)
    }
}
