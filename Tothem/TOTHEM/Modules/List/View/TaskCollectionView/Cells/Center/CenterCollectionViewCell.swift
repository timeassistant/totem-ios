//
//  CenterCollectionViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CenterCollectionViewCell: ContentCollectionViewCell {
    @IBOutlet weak var containerView: UIView?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBackView: UIView!

    @IBOutlet weak var textLabel: UILabel!

    @IBOutlet weak var checklistLabel1: UILabel!
    @IBOutlet weak var checklistLabel2: UILabel!
    @IBOutlet weak var imageView1: TaskImageView!
    @IBOutlet weak var imageView2: TaskImageView!
    @IBOutlet weak var mediaContentView: UIView!

    @IBOutlet weak var checkButton: UIButton?
    @IBOutlet weak var checkedView: UIView?
    @IBOutlet weak var checkedCircleView: UIView?
    @IBOutlet weak var checkContentView: UIView?

    @IBOutlet weak var verticalView: UIView?

    static var titleFont = UIFont.systemFont(ofSize: 23, weight: .semibold)
    static var textFont = UIFont.systemFont(ofSize: 17)

    private var noMedia = true

    @IBOutlet weak var morePhotos: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        resetToEmpty()

        checkedCircleView?.layer.borderWidth = 1
        checkedCircleView?.layer.borderColor = UIColor.black.cgColor
        checkedCircleView?.layer.masksToBounds = true
        checkedCircleView?.layer.cornerRadius = 20

        checkedView?.layer.masksToBounds = true
        checkedView?.layer.cornerRadius = 10

        titleLabel?.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        textLabel?.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        checklistLabel1?.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        checklistLabel2?.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        resetToEmpty()
    }

    override func cellLayoutSubviews() {
        super.cellLayoutSubviews()

        let spacing: CGFloat = 5
        let contentWidth = contentView.bounds.width - 60

        var frame = contentView.bounds
        frame.size.width = contentWidth
        frame.origin.x = 60
        contentContainerView?.frame = frame

        var height = Symbol.referenceSymbol.size(width: contentWidth - 2 * spacing, font: RightCollectionViewCell.titleFont).height
        frame = CGRect(x: 0, y: 0, width: contentWidth, height: height)
        titleBackView.frame = frame
        if let text = titleLabel.text, !text.isEmpty {
            frame.origin.x = spacing
            frame.size.width = contentWidth - 2 * spacing
            titleLabel.frame = frame
            frame.origin.y += height
        }

        if let text = textLabel.text, !text.isEmpty {
            height = Symbol.referenceSymbol.size(width: contentWidth - 2 * spacing, font: CenterCollectionViewCell.textFont).height

            let textHeight = text.size(width: contentWidth - 2 * spacing, font: CenterCollectionViewCell.textFont).height
            
            frame.origin.x = spacing
            frame.size.width = contentWidth - 2 * spacing
            frame.size.height = min(noMedia ? 3 * height : 2 * height, textHeight)
            textLabel.frame = frame
            frame.origin.y += frame.height
        }

        frame.origin.x = 0
        frame.size.width = contentWidth
        frame.size.height = 60
        mediaContentView.frame = frame

        height = Symbol.referenceSymbol.size(width: contentWidth / 2 - 2 * spacing, font: CenterCollectionViewCell.textFont).height
        frame.origin.x = contentWidth / 2 + spacing
        frame.origin.y = spacing
        frame.size.height = height
        frame.size.width = contentWidth / 2 - 2 * spacing
        checklistLabel1.frame = frame

        frame.origin.y += spacing + height
        checklistLabel2.frame = frame

        frame.size.height = 60 - 2 * spacing
        frame.size.width = 1
        frame.origin.y = spacing
        frame.origin.x = contentWidth / 2
        verticalView?.frame = frame

        frame.origin.x = spacing
        frame.origin.y = 2 * spacing
        frame.size.width = (contentWidth / 2 - 3 * spacing) / 2
        frame.size.height = 60 - 4 * spacing
        imageView1?.frame = frame
        imageView1.imageView?.frame = imageView1.bounds

        frame.origin.x = 2 * spacing + frame.size.width
        imageView2?.frame = frame
        imageView2.imageView?.frame = imageView2.bounds

        morePhotos.frame = frame
    }

    private func resetToEmpty() {
        noMedia = true
        titleBackView?.isHidden = true
        titleLabel?.isHidden = true
        textLabel?.isHidden = true
        checkContentView?.isHidden = true
        mediaContentView?.isHidden = true
        checklistLabel1?.text = nil
        checklistLabel2?.text = nil
        imageView1?.reset()
        imageView2?.reset()

        titleLabel?.font = CenterCollectionViewCell.titleFont
        textLabel?.font = CenterCollectionViewCell.textFont
        checklistLabel1?.font = CenterCollectionViewCell.textFont
        checklistLabel2?.font = CenterCollectionViewCell.textFont
    }

    override func set(_ model: CollectionCellModel) {
        super.set(model)

        guard let viewModel = model.viewModel as? CenterCollectionViewCellViewModel else { return }

        titleLabel?.isHidden = viewModel.isEmpty
        titleBackView?.isHidden = viewModel.isEmpty
        checkContentView?.isHidden = viewModel.isEmpty
        checkedView?.isHidden = !viewModel.checked

        titleLabel?.text = viewModel.title.isEmpty ? " " : viewModel.title

        noMedia = !viewModel.isContainsMedia
        mediaContentView?.isHidden = noMedia

        textLabel?.text = viewModel.text
        textLabel?.isHidden = viewModel.text.isEmpty
        textLabel?.numberOfLines = 2 + (noMedia ? 1 : 0)

        checklistLabel1?.text = viewModel.bullet1
        checklistLabel2?.text = viewModel.bullet2

        if let observable = viewModel.photo1 {
            imageView1?.set(observable)
        }

        if let observable = viewModel.photo2 {
            imageView2?.set(observable)
        }
        
        morePhotos.isHidden = viewModel.morePhotos == 0
        morePhotos.text = "+\(viewModel.morePhotos)"
        
        colorView.layer.masksToBounds = false
        colorView.layer.borderWidth = viewModel.borderWidth
        colorView.layer.borderColor = viewModel.borderColor.cgColor
    }

    override func apply(parameters: CellParametersModel) {
        super.apply(parameters: parameters)
        var scale = CGFloat(parameters.normal / 10) + 1
        titleLabel?.transform = CGAffineTransform(scaleX: scale, y: scale)
        textLabel?.transform = CGAffineTransform(scaleX: scale, y: scale)
        checklistLabel1?.transform = CGAffineTransform(scaleX: scale, y: scale)
        checklistLabel2?.transform = CGAffineTransform(scaleX: scale, y: scale)

        scale = CGFloat(1 + parameters.normal/2)
        let viewScale = CGFloat(1 - parameters.normal/10)
        [imageView1, imageView2].forEach {
            $0?.transform = CGAffineTransform(scaleX: viewScale, y: viewScale)
            $0?.imageView?.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
    }

    override func apply(cornerRadius: CGFloat) {
        super.apply(cornerRadius: cornerRadius)
        containerView?.layer.cornerRadius = cornerRadius
    }

    override func set(contentHidden: Bool) {
        super.set(contentHidden: contentHidden)
        containerView?.alpha = contentHidden ? 0 : 1
    }

    @IBAction func checkButtonPressed(_ sender: Any) {
        actions?.on(.next((model, .check, .none)))
    }

    static func height(_ model: TaskModel, width: CGFloat) -> CGFloat {
        guard !model.isEmpty else { return CGFloat(kCreateButtonSize) }

        let spacing: CGFloat = 5
        let contentWidth = width - 60 - 2 * spacing // check content view
        var height: CGFloat = 0

        //title
        height += Symbol.referenceSymbol.size(width: contentWidth, font: CenterCollectionViewCell.titleFont).height

        let noMedia = !model.isContainsMedia
        if !noMedia {
            height += 60
        }

        if !model.texts.isEmpty {
            height += noMedia ? spacing : 0
            let textHeight = model.descriptionText.size(width: contentWidth, font: CenterCollectionViewCell.textFont).height
            
            let visibleLines = CGFloat(2 + (noMedia ? 1 : 0))
            let maxTextHeight = Symbol.referenceSymbol.size(width: contentWidth, font: CenterCollectionViewCell.textFont).height * visibleLines
            
            height += min(maxTextHeight, textHeight)
        }

        return max(CGFloat(kCreateButtonSize), height)
    }
}
