//
//  CollectionHandler.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class CollectionHandler: PreviewDragDropHelperDelegate {
    private var disposeBag: DisposeBag
    private weak var collectionView: CollectionView?
    private weak var frameView: UIView?
    
    var cellModels: [CollectionCellModel] = []
    var viewModel: CollectionViewModel? {
        didSet {
            createCellViewModels()
        }
    }
    
    var itemsHeight: CGFloat {
        return collectionView?.height(cellModels) ?? 0
    }
    
    init(collectionView: CollectionView, frameView: UIView?, disposeBag: DisposeBag) {
        self.disposeBag = disposeBag
        self.collectionView = collectionView
        self.frameView = frameView
    }
    
    private func createCellViewModels(dragging: Bool = false) {
        guard let viewModel = viewModel else { return }
        cellModels = CreateCollectionCellsUseCase.create(viewModel, dragging)
    }
    
    func canMoveItem(at indexPath: IndexPath) -> Bool {
        return indexPath.row > 0 && collectionView?.collectionDataSource
            .models[indexPath.row].viewModel.isEmpty == false
    }
    
    func beginMovement(at indexPath: IndexPath) {
        guard let cell = collectionView?.cellForItem(at: indexPath) else { return }
        
        HapticEngine.shared.impactFeedback.activateFeedback(with: .light)
        
        let frame = cell.convert(cell.bounds, to: frameView?.superview)
        collectionView?.focusedCell = UICollectionView.LayoutModel(frame: frame, indexPath: indexPath)
        
        createCellViewModels(dragging: true)
        applyChanges()
    }
    
    func endMovement(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        endInsertion(from: sourceIndexPath, to: destinationIndexPath, layout: false)

        guard let models = collectionView?.collectionDataSource.models else {
            return
        }

        guard let viewModel = viewModel else { return }
        RearrangeTasksUseCase.execute(viewModel, models: models, from: sourceIndexPath, to: destinationIndexPath)
            .subscribe(onError: { [weak self] _ in
                guard let self = self else { return }
                self.createCellViewModels()
                self.applyChanges()
            })
            .disposed(by: disposeBag)
    }

    func endInsertion(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath, layout: Bool = true) {
        guard let cell = collectionView?.cellForItem(at: destinationIndexPath) else { return }
        HapticEngine.shared.impactFeedback.activateFeedback(with: .light)

        let frame = cell.convert(cell.bounds, to: frameView?.superview)
        collectionView?.focusedCell = UICollectionView.LayoutModel(frame: frame, indexPath: destinationIndexPath)

        highlight(destinationIndexPath, false)

        if layout {
            createCellViewModels()
            applyChanges()
        }
    }
    
    func changeMovement(from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

        highlight(sourceIndexPath, false)
        highlight(destinationIndexPath, true)

        if sourceIndexPath.row < destinationIndexPath.row {
            scrollToNext(from: destinationIndexPath)
        } else if sourceIndexPath.row > destinationIndexPath.row {
            scrollToPrev(from: destinationIndexPath)
        }
    }
    
    private func highlight(_ indexPath: IndexPath, _ highlight: Bool) {
        if let cell = collectionView?.cellForItem(at: indexPath) as? Highlightable {
            cell.highlight(highlight)
        }
    }
    
    private func scrollToNext(from indexPath: IndexPath) {
        guard let collectionView = collectionView,
            let view = frameView else { return }
        if let cell = collectionView.cellForItem(at: indexPath) {
            let frame = cell.convert(cell.bounds, to: view)
            print(frame)
            if frame.minY - 40 < view.frame.minY {
                if scrollToNextRow(indexPath, 3) { } else
                if scrollToNextRow(indexPath, 2) { } else
                if scrollToNextRow(indexPath, 1) { }
            } else if frame.maxY + 40 > view.frame.maxY {
                if scrollToNextRow(indexPath, 3) { } else
                if scrollToNextRow(indexPath, 2) { } else
                if scrollToNextRow(indexPath, 1) { }
            }
        }
    }
    
    private func scrollToNextRow(_ indexPath: IndexPath, _ rows: Int) -> Bool {
        guard let collectionView = collectionView else { return false }
        
        if indexPath.row + rows < collectionView
            .numberOfItems(inSection: indexPath.section) {
            var newIndexPath = indexPath
            newIndexPath.row += rows
            collectionView.scrollToItem(at: newIndexPath, at: .centeredVertically, animated: true)
            return true
        }
        return false
    }
    
    private func scrollToPrev(from indexPath: IndexPath) {
        guard let collectionView = collectionView,
            let view = frameView else { return }
        if let cell = collectionView.cellForItem(at: indexPath) {
            let frame = cell.convert(cell.bounds, to: view)
            if frame.minY - 40 < view.frame.minY {
                if scrollToPrevRow(indexPath, 3) { } else
                if scrollToPrevRow(indexPath, 2) { } else
                if scrollToPrevRow(indexPath, 1) { }
            } else if frame.maxY + 40 > view.frame.maxY {
                if scrollToPrevRow(indexPath, 3) { } else
                if scrollToPrevRow(indexPath, 2) { } else
                if scrollToPrevRow(indexPath, 1) { }
            }
        }
    }
    
    private func scrollToPrevRow(_ indexPath: IndexPath, _ rows: Int) -> Bool {
        guard let collectionView = collectionView else { return false }
        
        if indexPath.row - rows >= 0 {
            var newIndexPath = indexPath
            newIndexPath.row -= rows
            collectionView.scrollToItem(at: newIndexPath, at: .centeredVertically, animated: true)
            return true
        }
        return false
    }
    
    func applyChanges() {
        collectionView?.set(cellModels)
    }
}
