//
//  ListViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

var kCreateButtonSize: Number = 60
var kCreateButtonMaxSize: Number = 100
var kCreateButtonLineHeight: Number = 8

class ListScrollContentView: UIView { }
class ListMaskView: UIView { }

enum ListViewPreviewMode {
    case none
    case compact
    case overContext
    case full(_ width: CGFloat, _ center: CGFloat)
}

class ListViewController: UIViewController {
    var collectionViewController: CollectionViewController?
    
    var disposeBag = DisposeBag()

    var presenter: ListPresenter?

    var createButtonFrame = BehaviorSubject<CGRect>(value: UIScreen.main.bounds)
    
    var contentInsets: UIEdgeInsets = .zero
    var cellInsets: UIEdgeInsets = .zero
    var separatorHeight: CGFloat = 10
    var separatorColor: UIColor = .clear
    var cellDragModeHeight: CGFloat = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let viewContrller = collectionViewController {
            addChild(viewContrller)
            viewContrller.view.fill(in: view)
        }
    }
    
    var contentWidth: CGFloat {
        return cellWidth - contentInsets.left - contentInsets.right
    }

    var cellCenterX: CGFloat {
        return (cellInsets.left - cellInsets.right) / 2 + (contentInsets.left - contentInsets.right) / 2
    }

    var cellWidth: CGFloat {
        return UIScreen.main.bounds.width - cellInsets.left - cellInsets.right
    }
    
    func set(state: ListViewState) {
        
    }

    var emptyModel: CollectionCellModel? {
        return nil
    }

    func has(model: TaskModel) -> Bool {
        return true
    }

    func set(preview: Bool, attached: Bool) {

    }

    func insertionBegin(with view: UIView) {

    }

    func insertionChanged(with view: UIView) {

    }

    func insertionEnded(with view: UIView) {
        
    }

    @objc func previewEmtyCell() {

    }
}
