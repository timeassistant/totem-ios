//
//  TasksViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class TasksViewController: ListViewController, UICollectionViewDelegate {
    private var loaded = false
    private var side: TimesetType = .center
    private var isScrolling = false
    private var insets: UIEdgeInsets = .zero
    private var currentValue: CGFloat = 0
    private let delta: CGFloat = 0.05
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter?.state
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: set)
            .disposed(by: disposeBag)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.events.on(.next(.parameters))
    }
    
    override var emptyModel: CollectionCellModel? {
        return collectionViewController?.collectionView
                .collectionDataSource.emptyModel
    }

    override func previewEmtyCell() {
        guard let task = collectionViewController?.collectionView
            .collectionDataSource.emptyModel else { return }
        collectionViewController?.presenter?.perform(.selected(task))
    }
    
    @objc
    private func emtyCellTapped() {
        if let task = collectionViewController?
            .collectionView
            .collectionDataSource.emptyModel {
            presenter?.events.on(.next(.tap(task, true)))
        }
    }
    
    var previewMiddleConstant: CGFloat = 0
    var initialUpdate = true

    var previewState: ListViewState?
    //var inPreview: Bool = false
    
    override func set(state: ListViewState) {
        switch state {
        case .update(let viewModel):
            self.side = viewModel.side
            
//            guard !inPreview else {
//                previewState = state
//                return
//            }
            
            //top.set(.set(viewModel.top))
            //bottom.set(.set(viewModel.bottom))
            
            //contentSizeDidChange(initial: initialUpdate)
            //initialUpdate = false
            
            //top.set(.apply)
            //bottom.set(.apply)
        case .parameters(let top, let bottom):
            setParameters(top, bottom, forced: !loaded)
            if !loaded {
                loaded = true
                presenter?.events.on(.next(.loaded))
            }
        case .previewDismissed:
            guard let state = previewState else { return }
            set(state: state)
            
        case .preview: break
            //selectedToPreview.on(.next(preview))
            //inPreview = preview
            //collectionViewController?.populate(.preview(model.identifier, preview, frame))
            
//            if preview {
//                if top.set(preview: preview, frame: frame, model: model) {
//                    previewMiddleConstant = middleConstraint.constant
//                    middleConstraint.constant = view.bounds.height + (topStackView?.frame.height ?? 0)
//                } else if bottom.set(preview: preview, frame: frame, model: model) {
//                    previewMiddleConstant = middleConstraint.constant
//                    middleConstraint.constant = -1 * (bottomStackView?.frame.height ?? 0)
//                }
//            } else if top.set(preview: preview, frame: frame, model: model) || bottom.set(preview: preview, frame: frame, model: model) {
//                middleConstraint.constant = offsets.middle
//            }
        default:
            break
        }
    }

    private var canApplyNormal: Bool {
        return !isScrolling &&
            collectionViewController?.collectionView.previewCell == nil &&
            side == StateTimelineService.shared.currentState
    }

    private func setParameters(_ top: CellParametersModel, _ bottom: CellParametersModel, forced: Bool) {
//        guard canApplyNormal || forced else { return }
//        insets = UIEdgeInsets(top: max(top.insets.top, bottom.insets.top), left: max(top.insets.left, bottom.insets.left), bottom: max(top.insets.bottom, bottom.insets.bottom), right: max(top.insets.right, bottom.insets.right))
//
//        topStackViewLeading.constant = top.insets.left
//        topStackViewTrailing.constant = top.insets.right
//
//        bottomStackViewLeading.constant = bottom.insets.left
//        bottomStackViewTrailing.constant = bottom.insets.right
//
//        topCollectionView.collectionDataSource.parameters = top
//        bottomCollectionView.collectionDataSource.parameters = bottom
//
//        UIView.animate(CATransaction.animationDuration()) {
//            self.topStackView.arrangedSubviews.forEach({
//                $0.backgroundColor = top.background_color
//            })
//
//            self.bottomStackView?.arrangedSubviews.forEach({
//                $0.backgroundColor = bottom.background_color
//            })
//
//            self.topCollectionView.parameters = top
//            self.bottomCollectionView.parameters = bottom
//        }
    }

    private func contentSizeDidChange(initial: Bool) {
//        if initial {
//            offsets = createOffsets(on: side, viewHeight: view.bounds.height, topContentHeight: top.handler.itemsHeight, bottomContentHeight: bottom.handler.itemsHeight)
//        } else {
//            offsets = offsets.setting(top: top.handler.itemsHeight, bottom: bottom.handler.itemsHeight)
//        }
//
//        applyOffsets(initial: initial)
    }
    
    private func createOffsets(on side: TimesetType, viewHeight: CGFloat, topContentHeight: CGFloat, bottomContentHeight: CGFloat) -> CollectionViewControllerOffsets {
        return ListViewScrollOffsets(height: viewHeight, min: CGFloat(kCreateButtonSize), top: topContentHeight, bottom: bottomContentHeight)
    }

    private func applyOffsets(initial: Bool) {
        
    }

    func updateCreateButtonFrame() {
        var rect = CGRect.zero
        rect.size.height = CGFloat(kCreateButtonMaxSize)
        rect.size.width = view.bounds.width - insets.left - insets.right
        rect.origin.x = insets.left
//        rect.origin.y = middleConstraint.constant - CGFloat(kCreateButtonMaxSize) / 2

        createButtonFrame.on(.next(rect))
    }

    private var panningView: UIScrollView?

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard !animatingOffsets, let collectionView = collectionView as? CollectionView else {
//            return
//        }
//
//        guard let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell else { return }
//
//        let opened = !cell.opened
//
//        topCollectionView.visibleCells
//            .compactMap({ $0 as? CollectionViewCell })
//            .forEach({ $0.closeActions() })
//
//        bottomCollectionView.visibleCells
//            .compactMap({ $0 as? CollectionViewCell })
//            .forEach({ $0.closeActions() })
//
//        guard opened else { return }
//
//        let model = collectionView.collectionDataSource.models[indexPath.row]
//
//        if (try? selectedToPreview.value()) == false {
//            let event = ListViewEvent.tap(model, true)
//            presenter?.events.on(.next(event))
//        }
//    }

    // MARK: - Scrolling

//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        isScrolling = true
//        panningView = scrollView
//
//        if scrollView == topCollectionView {
//            let offset = bottomCollectionView.contentOffset
//            bottomCollectionView.setContentOffset(offset, animated: false)
//        }
//
//        if scrollView == bottomCollectionView {
//            let offset = topCollectionView.contentOffset
//            topCollectionView.setContentOffset(offset, animated: false)
//        }
//    }
//
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        isScrolling = decelerate
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        isScrolling = false
//        offsets.topOffset = topCollectionView.contentOffset
//        offsets.bottomOffset = bottomCollectionView.contentOffset
//        top.set(.offsets(topCollectionView.contentOffset))
//        bottom.set(.offsets(bottomCollectionView.contentOffset))
//    }
//
//    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        isScrolling = false
//        offsets.topOffset = topCollectionView.contentOffset
//        offsets.bottomOffset = bottomCollectionView.contentOffset
//        top.set(.offsets(topCollectionView.contentOffset))
//        bottom.set(.offsets(bottomCollectionView.contentOffset))
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        guard !animatingOffsets else { return }
//
//        if scrollView == topCollectionView {
//            addTopStackItems()
//        }
//
//        if scrollView == bottomCollectionView {
//            addBottomStackItems()
//        }
//    }
//
//    private weak var topFirstCellStackViewHeight: NSLayoutConstraint?
//    private weak var bottomFirstCellStackViewHeight: NSLayoutConstraint?
//
//    private func addTopStackItems() {
//        guard let stackView = topStackView else { return }
//
//        if let indexPath = topCollectionView.bottomCellIndexPath(inset: 5),
//            (indexPath.row == 1 && stackView.arrangedSubviews.count == 2) ||
//                (indexPath.row == 3 && stackView.arrangedSubviews.count == 3) ||
//                (indexPath.row == 5 && stackView.arrangedSubviews.count == 4) ||
//                (indexPath.row == 7 && stackView.arrangedSubviews.count == 5) {
//            if indexPath.row == 1 {
//                setupTopFirstCollapsedView()
//            } else if indexPath.row == 6 {
//                let view = UIView(frame: .zero)
//                view.constraintHeight(constant: 0)
//                stackView.insertArrangedSubview(view, at: 0)
//            } else {
//                let view = UIView(frame: .zero)
//                view.backgroundColor = topCollectionView.parameters.background_color
//                view.constraintHeight(constant: 10)
//                stackView.insertArrangedSubview(view, at: 0)
//            }
//        } else if let indexPath = topCollectionView.bottomCellIndexPath(inset: 0) {
//            if indexPath.row == 1 && stackView.arrangedSubviews.count > 3 {
//                while stackView.arrangedSubviews.count > 3 {
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[0])
//                }
//            } else if indexPath.row == 3 && stackView.arrangedSubviews.count > 4 {
//                while stackView.arrangedSubviews.count > 4 {
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[0])
//                }
//            } else if indexPath.row == 5 && stackView.arrangedSubviews.count > 5 {
//                while stackView.arrangedSubviews.count > 5 {
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[0])
//                }
//            } else if indexPath.row == 7 && stackView.arrangedSubviews.count > 6 {
//                while stackView.arrangedSubviews.count > 6 {
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[0])
//                }
//            }
//
//            if indexPath.row < 3 {
//                var height: CGFloat = 0
//                if let cell = topCollectionView
//                    .cellForItem(at: IndexPath(row: 1, section: 0)),
//                    let stack = bottomStackView {
//                    let cellFrame = cell.convert(cell.bounds, to: view)
//                    let stackFrame = stack.convert(stack.bounds, to: view)
//                    height = stackFrame.maxY - cellFrame.midY
//                }
//
//                if stackView.arrangedSubviews.count > 2, !offsets.topStreching, height > 0 {
//                    topFirstCellStackViewHeight?.constant = 10
//                } else {
//                    if topFirstCellStackViewHeight == nil {
//                        setupTopFirstCollapsedView()
//                    }
//                    topFirstCellStackViewHeight?.constant = max(10, height)
//                }
//            } else {
//                topFirstCellStackViewHeight?.constant = 10
//            }
//        }
//    }
//
//    private func setupTopFirstCollapsedView() {
//        let view = UIView(frame: .zero)
//        view.backgroundColor = topCollectionView.parameters.background_color
//        topFirstCellStackViewHeight = view.constraintHeight(constant: 10)
//        topStackView.insertArrangedSubview(view, at: 0)
//        view.isUserInteractionEnabled = true
//        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(emtyCellTapped)))
//    }
//
//    private func addBottomStackItems() {
//        guard let stackView = bottomStackView else { return }
//
//        if let indexPath = bottomCollectionView.topCellIndexPath(inset: 5),
//            (indexPath.row == 1 && stackView.arrangedSubviews.count == 2) ||
//                (indexPath.row == 3 && stackView.arrangedSubviews.count == 3) ||
//                (indexPath.row == 5 && stackView.arrangedSubviews.count == 4) ||
//                (indexPath.row == 6 && stackView.arrangedSubviews.count == 5) {
//            if indexPath.row == 6 {
//                let view = UIView(frame: .zero)
//                view.constraintHeight(constant: 0)
//                stackView.addArrangedSubview(view)
//            } else {
//                let view = UIView(frame: .zero)
//                view.backgroundColor = bottomCollectionView.parameters.background_color
//                view.constraintHeight(constant: 10)
//                stackView.addArrangedSubview(view)
//            }
//        } else if let indexPath = bottomCollectionView.topCellIndexPath(inset: 0) {
//            if indexPath.row < 1 {
//                while stackView.arrangedSubviews.count > 2 {
//                    let ind = stackView.arrangedSubviews.count - 1
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[ind])
//                }
//            } else if indexPath.row < 3 {
//                while stackView.arrangedSubviews.count > 3 {
//                    let ind = stackView.arrangedSubviews.count - 1
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[ind])
//                }
//            } else if indexPath.row < 5 {
//                while stackView.arrangedSubviews.count > 4 {
//                    let ind = stackView.arrangedSubviews.count - 1
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[ind])
//                }
//            } else if indexPath.row < 7 {
//                while stackView.arrangedSubviews.count > 5 {
//                    let ind = stackView.arrangedSubviews.count - 1
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[ind])
//                }
//            }
//        }
//    }
//
//    @objc func panTop(gesture: UIPanGestureRecognizer) {
//        let ended = gesture.state == .cancelled || gesture.state == .ended || !gesture.isEnabled
//        if !ended {
//            let insets = offsets.topInsets
//            offsets = offsets.scrolling(top: topCollectionView.contentOffset.y)
//            if insets.bottom != offsets.topInsets.bottom {
//                animateNewOffsets(gesture: gesture)
//            }
//        }
//    }
//
//    @objc func panBottom(gesture: UIPanGestureRecognizer) {
//        let ended = gesture.state == .cancelled || gesture.state == .ended
//
//        if !ended {
//            let insets = offsets.bottomInsets
//            offsets = offsets.scrolling(bottom: bottomCollectionView.contentOffset.y)
//            if insets.top != offsets.bottomInsets.top {
//                animateNewOffsets(gesture: gesture)
//            }
//        }
//    }

//    private var animatingOffsets: Bool = false
//
//    private func animateNewOffsets(gesture: UIPanGestureRecognizer?) {
//        animatingOffsets = true
//
//        middleConstraint.constant = offsets.middle
//        topFirstCellStackViewHeight?.constant = 10
//        bottomFirstCellStackViewHeight?.constant = 10
//
//        top.set(.insets(offsets.topInsets))
//        bottom.set(.insets(offsets.bottomInsets))
//
//        bottom.set(.offsets(offsets.bottomOffset))
//        top.set(.offsets(offsets.topOffset))
//
//        print("new offsets: \(offsets.topOffset)")
//
//        self.top.set(.apply)
//        self.bottom.set(.apply)
//
//        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
//            self.updateCreateButtonFrame()
//            self.view.layoutIfNeeded()
//        }, completion: { _ in
//            self.bottom.set(.offsets(nil))
//            self.top.set(.offsets(nil))
//            gesture?.isEnabled = true
//            if let stackView = self.topStackView {
//                while stackView.arrangedSubviews.count > 3 {
//                    stackView.removeArrangedSubview(stackView.arrangedSubviews[0])
//                }
//            }
//            self.animatingOffsets = false
//        })
//        gesture?.isEnabled = false
//    }

    override func insertionBegin(with view: UIView) {
//        guard (try? selectedToPreview.value()) == false else { return }
//        top.insertionBegin(with: view)
//        bottom.insertionBegin(with: view)
    }

    override func insertionChanged(with view: UIView) {
//        top.insertionChanged(with: view)
//        bottom.insertionChanged(with: view)
    }

    override func insertionEnded(with view: UIView) {
//        top.insertionEnded(with: view)
//        bottom.insertionEnded(with: view)
    }

    @objc func scrollTopCollectionToBegining(_ gesture: UITapGestureRecognizer) {
//        let y = topCollectionView.contentSize.height - topCollectionView.bounds.height
//        topCollectionView.setContentOffset(CGPoint(x: 0, y: y), animated: true)
    }

    @objc func scrollBottomCollectionToBegining(_ gesture: UITapGestureRecognizer) {
//        bottomCollectionView.setContentOffset(.zero, animated: true)
    }
}
