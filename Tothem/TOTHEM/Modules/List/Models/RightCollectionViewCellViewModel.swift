//
//  RightCollectionViewCellViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct RightCollectionViewCellViewModel: CollectionCellViewModelProtocol {
    var modelId: String = UUID().uuidString
    var isEmpty: Bool = true
    var isEditable: Bool = true

    var title: String = ""
    
    var bullet1: String?
    var bullet2: String?

    var contentIdentifier: String = ""
    var position: Double = .greatestFiniteMagnitude
    
    init(_ task: TaskModel) {
        self.modelId = task.id
        if let value = task.positions.first(where: { $0.side == .right })?.value {
            self.position = value
        }
        self.set(task: task)
    }
    
    init() { }
    
    mutating func set(task: TaskModel?) {
        self.isEmpty = task?.isEmpty ?? true
        
        self.title = task?.title ?? ""
        contentIdentifier = title

        let bullets = task?.bullets ?? []
        
        if bullets.count > 0 {
            contentIdentifier += bullets[0].id
            bullet1 = Symbol.bullet.rawValue + " " + bullets[0].value
        }
        
        if bullets.count > 1 {
            contentIdentifier += bullets[1].id
            bullet2 = Symbol.bullet.rawValue + " " + bullets[1].value
        }
    }
}
