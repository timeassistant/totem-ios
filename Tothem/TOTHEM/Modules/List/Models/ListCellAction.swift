//
//  ListCellAction.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum ListCellAction {
    case delete
    case check
    case transfer
    case none
    
    var size: CGFloat {
        return 40
    }
    
    var color: UIColor {
        switch self {
        case .delete: return .red
        case .check: return .blue
        case .transfer: return .clear
        case .none: return .clear
        }
    }
    
    var fast: Bool {
        return true
    }
    
    var short: Bool {
        return self == .delete
    }
    
    var instantly: Bool {
        return self == .none
    }
}
