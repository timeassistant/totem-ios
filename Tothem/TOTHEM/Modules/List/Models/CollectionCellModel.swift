//
//  CollectionCellModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

typealias CollectionCellSizing = (CGFloat, CellParametersModel) -> CGSize

struct CollectionCellModel {
    var reuseId: String
    var sizing: CollectionCellSizing
    var actions: [(ListCellAction, ListCellActionPosition)]
    var viewModel: CollectionCellViewModelProtocol
    var identifier: String = UUID().uuidString
    
    init(reuseId: String, sizing: @escaping CollectionCellSizing = { _, _ in .zero }, actions: [(ListCellAction, ListCellActionPosition)] = [], viewModel: CollectionCellViewModelProtocol) {
        self.reuseId = reuseId
        self.sizing = sizing
        self.actions = actions
        self.viewModel = viewModel
    }
    
    static func == (left: CollectionCellModel, right: CollectionCellModel) -> Bool {
        return left.identifier == right.identifier
    }
}
