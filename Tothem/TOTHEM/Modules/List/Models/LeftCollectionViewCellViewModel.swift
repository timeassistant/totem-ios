//
//  LeftCollectionViewCellViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct LeftCollectionViewCellViewModel: CollectionCellViewModelProtocol {
    var modelId: String = UUID().uuidString
    var isEmpty: Bool = true
    var isEditable: Bool = true
    
    var title: String = ""
    
    var morePhotos: Int = 0
    var photo1: Observable<UIImage>?
    var photo2: Observable<UIImage>?
    var photo3: Observable<UIImage>?

    var contentIdentifier: String = ""
    var position: Double = Double.greatestFiniteMagnitude
    
    init(_ task: TaskModel, imageLoader: (String?, String) -> Observable<UIImage>) {
        self.modelId = task.id
        if let value = task.positions.first(where: { $0.side == .left })?.value {
            self.position = value
        }
        set(task: task, imageLoader: imageLoader)
    }
    
    init() { }
    
    mutating func set(task: TaskModel?, imageLoader: (String?, String) -> Observable<UIImage>) {
        self.isEmpty = task?.isEmpty ?? true
        
        self.title = task?.title ?? ""
        contentIdentifier = title

        let photos = task?.photos ?? []
        self.morePhotos = max(0, photos.count - 3)
        
        if photos.count > 0 {
            contentIdentifier += photos[0].id
            photo1 = imageLoader(task?.uid, photos[0].value)
        }
        
        if photos.count > 1 {
            contentIdentifier += photos[1].id
            photo2 = imageLoader(task?.uid, photos[1].value)
        }
        
        if photos.count > 2 {
            contentIdentifier += photos[2].id
            photo3 = imageLoader(task?.uid, photos[2].value)
        }
    }
}
