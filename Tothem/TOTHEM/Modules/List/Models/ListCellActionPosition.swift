//
//  ListCellActionPosition.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum ListCellActionPosition {
    case none
    case right
    case left
    
    var offset: CGFloat {
        switch self {
        case .right: return 20
        case .left: return -20
        default: return 0
        }
    }
    
    var openOffset: CGFloat {
        switch self {
        case .right: return -70
        case .left: return 70
        default: return 0
        }
    }
}
