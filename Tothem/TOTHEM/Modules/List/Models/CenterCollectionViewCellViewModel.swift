//
//  CenterCollectionViewCellViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct CenterCollectionViewCellViewModel: CollectionCellViewModelProtocol {
    var modelId: String = UUID().uuidString
    var isEmpty: Bool = true
    var isEditable: Bool = true
    var contentIdentifier: String = ""
    var position: Double = .greatestFiniteMagnitude
    
    var title: String = ""
    var checked: Bool = false
    var text: String = ""
    
    var isContainsMedia: Bool = false
    
    var bullet1: String?
    var bullet2: String?
    var bullet3: String?
    
    var morePhotos: Int = 0
    var photo1: Observable<UIImage>?
    var photo2: Observable<UIImage>?
    var photo3: Observable<UIImage>?

    var borderWidth: CGFloat = 0
    var borderColor: UIColor = .clear
    
    init(_ task: TaskModel, imageLoader: (String?, String) -> Observable<UIImage>) {
        self.modelId = task.id
        if let value = task.positions.first(where: { $0.side == .center })?.value {
            self.position = value
        }
        
        self.set(task: task, imageLoader: imageLoader)
    }

    init() { }
    
    mutating func set(task: TaskModel?, imageLoader: (String?, String) -> Observable<UIImage>) {
        self.isEmpty = task?.isEmpty ?? true
        
        self.title = task?.title ?? ""
        self.isContainsMedia = task?.isContainsMedia ?? false
        self.checked = task?.checked ?? false
        self.text = task?.descriptionText ?? ""

        contentIdentifier = title

        let bullets = task?.bullets ?? []
        
        if bullets.count > 0 {
            contentIdentifier += bullets[0].id
            bullet1 = Symbol.bullet.rawValue + " " + bullets[0].value
        }
        
        if bullets.count > 1 {
            contentIdentifier += bullets[1].id
            bullet2 = Symbol.bullet.rawValue + " " + bullets[1].value
        }
        
        if bullets.count > 2 {
            contentIdentifier += bullets[2].id
            bullet3 = Symbol.bullet.rawValue + " " + bullets[2].value
        }
        
        let photos = task?.photos ?? []
        self.morePhotos = max(0, photos.count - 3)
        
        if photos.count > 0 {
            contentIdentifier += photos[0].id
            photo1 = imageLoader(task?.uid, photos[0].value)
        }
        
        if photos.count > 1 {
            contentIdentifier += photos[1].id
            photo2 = imageLoader(task?.uid, photos[1].value)
        }
        
        if photos.count > 2 {
            contentIdentifier += photos[2].id
            photo3 = imageLoader(task?.uid, photos[2].value)
        }
        
        if let priority = task?.priority {
            switch priority {
            case .description:
                borderWidth = 1
                borderColor = .blue
            case .action:
                borderWidth = 1
                borderColor = .red
            }
        }
    }
}
