//
//  CollectionCellsManager.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import DeepDiff

struct CollectionCellsManager {
    static func calculate(_ models: [CollectionCellModel], newModels: [CollectionCellModel]) -> [CollectionDiff] {
        return diff(old: models, new: newModels)
            .map({
                switch $0 {
                case .insert(let op):
                    return CollectionDiff.insert(op.item, op.index)
                case .delete(let op):
                    return CollectionDiff.delete(op.item, op.index)
                case .move(let op):
                    return CollectionDiff.move(op.item, op.fromIndex, op.toIndex)
                case .replace(let op):
                    return CollectionDiff.replace(op.oldItem, op.newItem, op.index)
                }
            })
    }
}

extension CollectionCellModel: DiffAware {
    typealias DiffId = String

    var diffId: String {
        return identifier
    }

    static func compareContent(_ a: CollectionCellModel, _ b: CollectionCellModel) -> Bool {
        return a.viewModel.compareContent(b.viewModel)
    }
}

extension CollectionCellViewModelProtocol {
    func compareContent(_ to: CollectionCellViewModelProtocol) -> Bool {
        return contentIdentifier == to.contentIdentifier
    }
}
