//
//  ListDeleteUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct ListDeleteUseCase {
    static let shared = ListDeleteUseCase()
    private let picturesRepository = PicturesRepository()
    private let userRepository = UserRepository()
    private let usersDataRepository = UsersDataService.shared
    
    func execute(_ model: CollectionCellModel) -> Observable<Void> {
        let modelId = model.viewModel.modelId
        guard let model = GetTasksUseCase.shared.task(id: modelId),
        let uid = userRepository.user?.uid else {
            return Observable.error(RxError.unknown)
        }

        var observables = model.photos.map({ entity in
            self.usersDataRepository
                .photoFilename(uid: uid, photoEntityValue: entity.value)
                .flatMap({
                    self.picturesRepository.remove(path: uid, name: $0)
                })
                .catchErrorJustReturn(())
        })
        observables.append(UsersDataService.shared.remove(task: model))

        return Observable.merge(observables)
    }
}
