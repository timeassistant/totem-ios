//
//  ListCreateUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class ListCreateUseCase {
    func execute(side: TimesetType, position: Double? = nil, selected: Bool? = nil, drag: Bool? = nil) -> Observable<UseCaseResult> {
        guard let window = UIApplication.shared.keyWindow else { return Observable.error(RxError.unknown) }
        guard let root = window.rootViewController as? UINavigationController else { return Observable.error(RxError.unknown) }
        guard let mainViewController = root.viewControllers.first as? MainTaskViewController else { return  Observable.error(RxError.unknown) }
        
        mainViewController.create(on: side, position: position)
        
        if let selected = selected {
            let event: ListEvent = selected ? .tapOnZero(side) : .tapCreateButton(side)
            EventsDataManager.shared.add(event)
        } else if drag != nil {
            EventsDataManager.shared.add(ListEvent.dragPanel(side))
        }
        
        return Observable.empty()
    }
}
