//
//  RearrangeTasksUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

enum RearrangeError: Error {
    case wrongPosition
    case badCalculation
}
struct RearrangeTasksUseCase {
    static func execute(_ viewModel: CollectionViewModel, models: [CollectionCellModel], from: IndexPath, to: IndexPath) -> Completable {

        let side = viewModel.side
        let items = viewModel.items
        
        guard to.row > 1, abs(from.row - to.row) > 1 else {
            return .error(RearrangeError.wrongPosition)
        }

        print("\(from.row) -> \(to.row)")
        
        var rearrangedModels = models
        let fromModel = rearrangedModels[from.row]
        let toModel = rearrangedModels[to.row]

        rearrangedModels.remove(at: from.row)

        if let index = rearrangedModels.firstIndex(where: {
            $0.viewModel.modelId == toModel.viewModel.modelId }) {
            rearrangedModels.insert(fromModel, at: index)
        }
        
        var data: [String: TaskModel] = [:]
        items.forEach {
            data[$0.id] = $0
        }
        
        let positions = models
            .filter({ !$0.viewModel.isEmpty })
            .compactMap({ data[$0.viewModel.modelId]?.positions })
        
        let newItems = rearrangedModels
            .compactMap({ data[$0.viewModel.modelId] })
            .filter({ !$0.isEmpty })
        
        guard newItems.count == positions.count else { return .error(RearrangeError.badCalculation) }
        
        var updated: [TaskModel] = []
        
        for (index, item) in newItems.enumerated() {
            let itemNewPositions = positions[index]
            if let newPosition = itemNewPositions.first(where: { $0.side == side }),
                let oldPosition = item.positions.first(where: { $0.side == side }),
                newPosition != oldPosition {
                item.positions = item.positions.filter({ $0.side != side })
                item.positions.append(newPosition)
                updated.append(item)
            }
        }
        
        if updated.count > 0 {
            return UsersDataService.shared.update(updated)
        }
        
        return .error(RearrangeError.wrongPosition)
    }
}
