//
//  CreateTaskCollectionsUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

struct CreateTaskCollectionsUseCase {
    static func create(_ side: TimesetType, _ data: [TaskModel], _ new: Bool = true) -> ([TaskModel], [TaskModel]) {
        let models: [TaskModel] = data.sorted(by: {
            let pos0 = $0.positions.first(where: { $0.side == side })
            let pos1 = $1.positions.first(where: { $0.side == side })
            switch (pos0, pos1) {
            case (nil, .some): return false
            case (.some, nil): return true
            case (.some(let pos0), .some(let pos1)):
                return pos0.value > pos1.value
            default: return false
            }
        })

        var topModels: [TaskModel] = []
        var bottomModels: [TaskModel] = []

        switch side {
        case .left:
            topModels = models
            if new {
                topModels.insert(GetTasksUseCase.shared.new(side), at: 0)
            }
        case .right:
            bottomModels = models
            if new {
                bottomModels.insert(GetTasksUseCase.shared.new(side), at: 0)
            }
        case .center:
            bottomModels = models.filter({ $0.checked })
            topModels = models.filter({ !$0.checked })
            if new {
                topModels.insert(GetTasksUseCase.shared.new(side), at: 0)
            }
        }

        return (topModels, bottomModels)
    }
}
