//
//  ChangeSidesUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

struct ChangeSidesUseCase {
    static let shared = ChangeSidesUseCase()
    func execute(_ model: CollectionCellModel, side: TimesetType) -> Observable<TaskModel> {
        guard let task = GetTasksUseCase.shared.task(id: model.viewModel.modelId) else { return Observable.error(RxError.unknown) }
        guard let window = UIApplication.shared.keyWindow else { return Observable.error(RxError.unknown) }
        guard let root = window.rootViewController as? UINavigationController else { return Observable.error(RxError.unknown) }
        guard let mainViewController = root.viewControllers.first as? MainTaskViewController else { return  Observable.error(RxError.unknown) }

        if side == .center, task.title.isEmpty && task.texts.isEmpty {
            mainViewController.edit(model, on: side)
            return Observable.empty()
        }

        if side == .left, task.photos.isEmpty {
            mainViewController.edit(model, on: side)
            return Observable.empty()
        }

        if side == .right, task.bullets.isEmpty {
            mainViewController.edit(model, on: side)
            return Observable.empty()
        }

        task.side = side
        switch side {
        case .center:
            task.mode = .combined
        default: task.mode = .splitted
        }
        
        return SaveTaskUseCase.shared.save(task)
    }
}
