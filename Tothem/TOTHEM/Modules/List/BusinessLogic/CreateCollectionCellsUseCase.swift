//
//  CreateCollectionCellsUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct CreateCollectionCellsUseCase {
    static var imageLoader: ((String?, String) -> Observable<UIImage>)!

    static func create(_ viewModel: CollectionViewModel, _ dragging: Bool = false) -> [CollectionCellModel] {
        let side = viewModel.side
        var models: [CollectionCellModel] = []
        var separatorIndex = 0
        if viewModel.items.count > 0 {
            models.append(separator(side, separatorIndex, dragging))
            viewModel.items.forEach {
                separatorIndex += 1
                models.append(content(side: side, task: $0))
                models.append(separator(side, separatorIndex, dragging))
            }
        }
        return models
    }

    private static func action(_ side: TimesetType, _ checked: Bool) -> [(ListCellAction, ListCellActionPosition)] {
        switch side {
        case .center:
            return checked ? [(.delete, .right), (.delete, .left)] : [(.transfer, .right), (.transfer, .left)]
        case .right: return [(.delete, .left), (.transfer, .right)]
        case .left: return [(.delete, .right), (.transfer, .left)]
        }
    }

    private static func content(side: TimesetType, task: TaskModel) -> CollectionCellModel {
        var model: CollectionCellModel
        switch side {
        case .center:
            let sizing: CollectionCellSizing = { width, _ in
                var height: CGFloat = 0
                switch task.priority {
                case .description:
                    height = CenterDescriptionViewCell.height(task, width: width)
                case .action:
                    height = CenterActionViewCell.height(task, width: width)
                default:
                    height = CenterCollectionViewCell.height(task, width: width)
                }
                return CGSize(width: width, height: height)
            }
            var reuseId = CollectionViewDataSource.centerCellReuseId
            switch task.priority {
            case .description: reuseId = CollectionViewDataSource.centerDescriptionCellReuseId
            case .action: reuseId = CollectionViewDataSource.centerActionCellReuseId
            default: break
            }
            model = CollectionCellModel(reuseId: reuseId, sizing: sizing, actions: action(side, task.checked), viewModel: CenterCollectionViewCellViewModel(task, imageLoader: imageLoader))
        case .right:
            let sizing: CollectionCellSizing = { width, _ in
                let height = RightCollectionViewCell.height(task, width: width)
                return CGSize(width: width, height: height)
            }
            model = CollectionCellModel(reuseId: CollectionViewDataSource.rightCellReuseId, sizing: sizing, actions: action(side, task.checked), viewModel: RightCollectionViewCellViewModel(task))
        case .left:
            let sizing: CollectionCellSizing = { width, _ in
                let height = LeftCollectionViewCell.height(task, width: width)
                return CGSize(width: width, height: height)
            }
            model = CollectionCellModel(reuseId: CollectionViewDataSource.leftCellReuseId, sizing: sizing, actions: action(side, task.checked), viewModel: LeftCollectionViewCellViewModel(task, imageLoader: imageLoader))
        }
        
        if task.isEmpty {
            model.identifier = "\(side.path)_empty"
        } else {
            model.identifier = task.id
        }
        
        return model
    }

    private static func separator(_ side: TimesetType, _ index: Int, _ dragging: Bool) -> CollectionCellModel {
        let sizing: CollectionCellSizing = { width, params in
            var separator = params.separator
            switch side {
            case .center, .right:
                separator = params.separator
            default: break
            }
            return CGSize(width: width, height: separator + (dragging ? 20 : 0))
        }

        var model = CollectionCellModel(reuseId: CollectionViewDataSource.separatorReuseId, sizing: sizing, viewModel: CollectionCellViewModel())
        model.identifier = "\(side.path)_separator_\(index)"
        return model
    }
}
