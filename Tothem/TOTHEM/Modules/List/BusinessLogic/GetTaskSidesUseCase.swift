//
//  GetTaskSidesUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation

struct GetTaskSidesUseCase {
    static let shared = GetTaskSidesUseCase()
    func execute(_ model: CollectionCellModel) -> [TimesetType] {
        let modelId = model.viewModel.modelId
        guard let model = GetTasksUseCase.shared.task(id: modelId) else {
            return []
        }
        return execute(model)
    }

    func execute(_ model: TaskModel) -> [TimesetType] {
        switch model.mode {
        case .combined: return [.center]
        case .splitted:
            var sides: [TimesetType] = []
            if model.side == .right || model.bullets.count > 0 {
                sides.append(.right)
            }
            if model.side == .left || model.photos.count > 0 {
                sides.append(.left)
            }
            return sides
        }
    }
}
