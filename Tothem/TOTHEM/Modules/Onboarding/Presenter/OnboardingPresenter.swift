//
//  OnboardingPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 
import Foundation
import RxSwift

enum OnboardingViewEvent {
    case alphaPressed
    case betaPressed
}

enum OnboardingViewState {
    case loaded
}

final class OnboardingPresenter {
    var events = PublishSubject<OnboardingViewEvent>()
    var state: Observable<OnboardingViewState>
    
    init(_ interactor: OnboardingInteractor, _ wireframe: OnboardingWireframe) {
        state = self.events
            .flatMap({ event -> Observable<OnboardingInteractorResult> in
                switch event {
                case .alphaPressed: return interactor.execute(.setUserUniqueAlpha)
                case .betaPressed: return interactor.execute(.setUserUniqueBeta)
                }
            })
            .observeOn(MainScheduler.asyncInstance)
            .scan(.loaded) { _, result -> OnboardingViewState in
                switch result {
                case .done:
                    wireframe.navigate(to: .close)
                    return .loaded
                }
            }
    }
}
