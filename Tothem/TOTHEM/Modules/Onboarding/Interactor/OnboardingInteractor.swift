//
//  OnboardingInteractor.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import Foundation
import RxSwift

enum OnboardingInteractorResult {
    case done
}

enum OnboardingInteractorJob {
    case setUserUniqueAlpha
    case setUserUniqueBeta
}

final class OnboardingInteractor {
    func execute(_ job: OnboardingInteractorJob) -> Observable<OnboardingInteractorResult> {
        switch job {
        case .setUserUniqueAlpha:
            return UserClassService.shared
                .set(userOnboarding: .alpha)
                .map({ _ in .done })
        case .setUserUniqueBeta:
            return UserClassService.shared
                .set(userOnboarding: .beta)
                .map({ _ in .done })
        }
    }
}
