//
//  FullScreenPhotosViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import Photos
import RxSwift

final class FullScreenPhotosViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    var initialFrame = CGRect.zero
    var fetchResult: PHFetchResult<PHAsset>!
    var image: UIImage?
    var dataSource: GalleryDataSource!
    var index: Int = NSNotFound

    private var disposeBag = DisposeBag()
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var backView: UIView?
    @IBOutlet weak var collectionView: GalleryPanableCollectionView!
    weak var sendTaskButtonBottom: NSLayoutConstraint?

    deinit {
        print("deinit FullScreenPhotosViewController")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.isHidden = true

        collectionView.register(GalleryAssetCell.self, forCellWithReuseIdentifier: "AssetCell")
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        self.collectionView.actions
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] action in
                switch action {
                case .changed: self?.handleChange()
                case .ended(let diff): self?.handleEnded(diff)
                }
            })
            .disposed(by: disposeBag)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = dataSource.targetFrame
        collectionView.targetFrame = dataSource.targetFrame
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let imageView = imageView {
            if let window = UIApplication.shared.windows.first {
                let frame = window.convert(initialFrame, to: view)
                imageView.frame = frame
                imageView.isHidden = false
            }

            UIView.animate(withDuration: CATransaction.animationDuration(), delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                imageView.frame = self.dataSource.targetFrame
            }, completion: { _ in
                self.setupCollectionView()
            })

            UIView.animate(CATransaction.animationDuration(), animations: {
                self.backView?.backgroundColor = .black
            })

            self.backView?.frame = self.dataSource.targetFrame
        }

        collectionView.dataSource = dataSource
        collectionView.delegate = self
        collectionView.reloadData()
    }

    private func setupCollectionView() {
        collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .left, animated: false)
        collectionView.isHidden = false
        imageView.isHidden = true
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchResult.count
    }

    // swiftlint:disable all
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return dataSource.targetFrame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    // swiftlint:enable all
    func dismiss() {
        handleEnded(51)
    }

    private func handleChange() {
        backView?.frame = collectionView.frame
        dataSource.actions.on(.next(.fullScreen(collectionView.frame, false)))
    }

    private func handleEnded(_ diff: CGFloat) {
        if abs(diff) > 50 {
            var frame = collectionView.frame
            frame.origin.y = UIScreen.main.bounds.height * 2
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
                self.backView?.alpha = 0
                self.collectionView.alpha = 0
                self.collectionView.frame = frame
                self.backView?.frame = frame
                self.sendTaskButtonBottom?.isActive = false
                self.view.superview?.layoutIfNeeded()
            }, completion: { _ in
                self.view.removeFromSuperview()
                self.removeFromParent()
            })
            dataSource.actions.on(.next(.fullScreen(nil, true)))
        } else {
            let frame = dataSource.targetFrame
            UIView.animate(CATransaction.animationDuration()) {
                self.collectionView.frame = frame
                self.backView?.frame = frame
            }
            dataSource.actions.on(.next(.fullScreen(frame, true)))
        }
    }
}
