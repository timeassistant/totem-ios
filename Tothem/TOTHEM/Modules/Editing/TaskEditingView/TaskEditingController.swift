//
//  TaskEditingController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//swiftlint:disable all

import UIKit
import RxSwift
import Photos

enum TaskViewEditAction {
    case title(NSRange)
    case text(NSRange)
    case checklist(NSRange)
    case image(Int)
}

enum TaskEditingControllerEvent {
    case created(TaskModel)
    case updated(TaskModel)
    case exit
    case dismiss
    case didAppear
    case willAppear
    case empty
}

enum TaskEditingState {
    case photos
    case checklist
    case description
    var type: TimesetType {
        switch self {
        case .photos: return .left
        case .checklist: return .right
        case .description: return .center
        }
    }
}

enum TaskContentViewEvent {
    case add(EntityModel)
    case remove(EntityModel)
}

protocol TaskContentViewProtocol {
    func handleTitle(event: EditingViewEvent)
    var contentHeight: BehaviorSubject<CGFloat> { get }
    var contentHeightValue: CGFloat { get }
    var events: PublishSubject<TaskContentViewEvent> { get }
}

final class TaskEditingController: UIViewController {
    //OUTLETS
    private var stateDisposeBag: DisposeBag!
    private var state: TaskEditingState = .description

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var sendTaskButton: UIButton!
    @IBOutlet var sendTaskButtonBottom: NSLayoutConstraint!
    private weak var photosViewController: FullScreenPhotosViewController?

    private lazy var presenter =  TaskEditingPresenter()

    var newTask: TaskModel!
    var side: TimesetType = .center
    var transitions: [CollectionViewCellTransition] = []

    var window: UIWindow!
    var events = PublishSubject<TaskEditingControllerEvent>()

    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var contentViewLeading: NSLayoutConstraint!
    @IBOutlet weak var contentViewBottom: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!

    var shouldChangeListSideOnChangeInput: Bool = true //creation
    var changeListSideLocked: Bool = false

    var canExitFromPreview: Bool = true

    private var titleViewModel: EditingTextViewModel!
    private var checklistViewModel: EditingTextViewModel!
    private var descriptionViewModel: EditingTextViewModel!

    @IBOutlet weak var photosView: TaskPhotosContentView!
    @IBOutlet weak var descriptionView: EditingTextView!
    @IBOutlet weak var checklistView: EditingTextView!
    @IBOutlet weak var galleryCollectionView: GalleryCollectionView!
    @IBOutlet weak var titleView: EditingTextView!
    @IBOutlet weak var backgroundView: UIView?

    private var cursorLocation: Int = 0

    //rx dispose
    var dispose = DisposeBag()
	var additionalHeight: CGFloat = 0

    private var panPhotosGesture: UIPanGestureRecognizer!
    private var panContentGesture: UIPanGestureRecognizer!
    private var editingAction: TaskViewEditAction

    private var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }

    var position: Double?
    
    required init(editingAction: TaskViewEditAction) {
        self.editingAction = editingAction
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("use: init(task: TaskModel, editingAction: TaskViewEditAction")
    }
    
    deinit {
        print("deinit editing controller")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0

        titleView.minHeight = 20
        descriptionView.minHeight = 100
        checklistView.minHeight = 100
        photosView.minHeight = 100
        
        var cellModel: CollectionCellModel?

        if let model = transitions.first(where: {
            !$0.model.viewModel.isEmpty })?.model {
                cellModel = model
        } else if let model = transitions.first(where: { $0.side == side })?.model {
            cellModel = model
        } else {
            cellModel = transitions[0].model
        }

        if let modelId = cellModel?.viewModel.modelId,
            let model = GetTasksUseCase.shared.task(id: modelId) {
            newTask = model.copy
        } else {
            newTask = TaskModel(side)
        }

        setupGestures()

        let created = newTask.reference == nil

        self.presenter.state
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] event in
                switch event {
                case .loading: break
                case .loaded: break
                case .addToContent(let model):
                    self?.photosView.add(model)
                case .removeFromContent(let model):
                    self?.photosView.remove(model)
                case .saved(let model):
                    self?.saved(model, created: created)
                case .updateContent(let model):
                    self?.photosView.update(model)
                }
            })
            .subscribe()
            .disposed(by: dispose)

        self.photosView
            .events
            .subscribe(onNext: { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .add(let model):
                    var photos = self.newTask.photos
                    photos.append(model)
                    self.newTask.update(photos: photos, side: self.side)
                case .remove(let model):
                    var photos = self.newTask.photos
                    photos.removeAll(where: { $0.id == model.id })
                    self.newTask.update(photos: photos, side: self.side)
                    self.presenter.events.on(.next(.removeFromGallery(model)))
                }
            })
            .disposed(by: dispose)

        presenter.events.onNext(.loaded)
    }

    private var initialContentViewBottom: CGFloat?
    private func update(_ rect: CGRect?) {
        if let rect = rect {
            if initialContentViewBottom == nil {
                initialContentViewBottom = contentViewBottom.constant
            }
            contentViewBottom.constant = view.bounds.height - rect.minY
        } else {
            contentViewBottom.constant = initialContentViewBottom ?? 0
            initialContentViewBottom = nil
        }
        photosView.updateContentHeight()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        galleryCollectionView.set(width: screenWidth)
        if contentViewBottom?.constant == 0 {
            contentViewBottom?.constant = galleryCollectionView.twoRowsHeight
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setupView()
        events.onNext(.willAppear)
    }

    private func setupView() {
        switch editingAction {
        case .checklist: state = .checklist
        case .text: state = .description
        case .image: state = .photos
        default:
            switch side {
            case .right: state = .checklist
            case .center: state = .description
            case .left: state = .photos
            }
        }

        sendTaskButtonBottom.isActive = false

        setupUI(with: side)

        let style = TaskContentStyle(type: side, state: .inactive)
        setupTitleView(style: style)
        setupChecklistView(style: style)
        setupDescriptionView(style: style)

        setupSaveButtonObservers()

        observeKeyboard()

        contentViewBottom.constant = 0
        galleryCollectionView.scrollingEnabled = false
        galleryCollectionView.dataSource.actions
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] action in
                switch action {
                case .fullScreen(let rect, let animated):
                    if animated {
                        UIView.animate(CATransaction.animationDuration(), animations: {
                            self?.update(rect)
                            self?.view.layoutIfNeeded()
                        })
                    } else {
                        self?.update(rect)
                    }

                    if rect == nil {
                        self?.galleryCollectionView.reloadData()
                    }
                default: break
                }
            })
            .disposed(by: dispose)

        self.contentView?.rx
            .tapGesture()
            .subscribe(onNext: { [weak self] _ in
                UIView.animate(CATransaction.animationDuration()) {
                    self?.setPhotoCollectionViewIntialState()
                    self?.view.layoutIfNeeded()
                }
            })
            .disposed(by: self.dispose)

        photosView.set(newTask.photos)
        updateContentViewState(animated: false)
        
        Observable.combineLatest(
            descriptionView.contentHeight.asObservable(),
            checklistView.contentHeight.asObservable(),
            photosView.contentHeight.asObservable())
            .subscribe(onNext: { [weak self] in
                print($0)
                self?.updateContentViewHeight()
            })
            .disposed(by: dispose)
    }

    private func setupGestures() {
        emptyView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panEmptyView(gesture:))))
        contentView?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panEmptyView(gesture:))))
        titleView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panEmptyView(gesture:))))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(TaskEditingController.close))
        emptyView.addGestureRecognizer(tap)

        panPhotosGesture = UIPanGestureRecognizer(target: self, action: #selector(panPhotosCollectionView))
        galleryCollectionView.addGestureRecognizer(panPhotosGesture)

        panContentGesture = UIPanGestureRecognizer(target: self, action: #selector(panContentView))
        photosView.addGestureRecognizer(panContentGesture)
        panContentGesture.isEnabled = false
    }

    private func setupSaveButtonObservers() {
        Observable.merge([
            descriptionViewModel.text.asObserver().map({ $0.isEmpty }),
            checklistViewModel.text.asObserver().map({ $0.isEmpty }),
            photosView.contentIsEmpty.asObservable(),
            titleViewModel.text.asObservable().map({ $0.isEmpty })
            ])
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] _ in
                self?.updateButtonState()
            })
            .subscribe()
            .disposed(by: dispose)
    }
    
    private func setupTitleView(style: TaskContentStyle) {
        titleViewModel = EditingTextViewModel(.title)
        titleView.set(model: titleViewModel)
        titleView.formatter = EditingViewType.title.formatter
        titleView.placeholderLabel?.text = "Title"
        titleViewModel?.text.on(.next(newTask.title))
        titleView?.textView.text = newTask.title
        self.titleView.set(style: style.titleStyle)
        self.titleView
            .events
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] event in
                guard let self = self else { return }

                switch event {
                case .nextResponder: break
                case .textWillChange(let text):
                    self.newTask.title = text
                    return
                default: return
                }

                switch self.side {
                case .right: self.checklistView.beginEditing(backspaced: false)
                case .center: self.descriptionView.beginEditing(backspaced: false)
                case .left: self.photosView.handleTitle(event: event)
                }
            })
            .subscribe()
            .disposed(by: dispose)

        photosView.titleView = titleView
        photosView.titleViewModel = titleViewModel
    }

    private func setupChecklistView(style: TaskContentStyle) {
        checklistViewModel = EditingTextHelper.checklistViewModel(newTask.bullets)
        checklistView.set(model: checklistViewModel)
        checklistView.formatter = EditingViewType.checklist.formatter
        checklistView.placeholderLabel?.text = "Checklist"
        checklistView?.textView.text = checklistViewModel.textValue
        self.checklistView.set(style: style.checklistStyle)
        self.checklistView
            .events
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .nextResponder:
                    self.titleView.beginEditing(backspaced: true)
                case .textWillChange(let text):
                    self.newTask.update(bullets: text, side: self.side)
                default: break
                }
            })
            .subscribe()
            .disposed(by: dispose)
    }

    private func setupDescriptionView(style: TaskContentStyle) {
        descriptionViewModel = EditingTextHelper.textViewModel(newTask.texts)
        descriptionView.set(model: descriptionViewModel)
        descriptionView.formatter = EditingViewType.text.formatter
        descriptionView.placeholderLabel?.text = "Text"
        descriptionView?.textView.text = descriptionViewModel.textValue
        self.descriptionView.set(style: style.textStyle)
        self.descriptionView
            .events
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .nextResponder:
                    self.titleView.beginEditing(backspaced: true)
                case .textWillChange(let text):
                    self.newTask.update(texts: text, side: self.side)
                default: break
                }
            })
            .subscribe()
            .disposed(by: dispose)
    }

    private func setupUI(with side: TimesetType) {
        let constants = ConfigurationFacade.shared.taskModuleConstants
		constants.getTaskSendButtonColorValue(side)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sendTaskButton.backgroundColor = $0
            })
            .disposed(by: dispose)

		constants.getTaskSendButtonBorderColorValue(side)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sendTaskButton.layer.borderColor = $0.cgColor
            })
            .disposed(by: dispose)

		constants.getTaskSendButtonBorderWidthValue(side)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sendTaskButton.layer.borderWidth = $0
            })
            .disposed(by: dispose)

        constants.getTaskContentScaleValue(side)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sendTaskButton.transform = CGAffineTransform(scaleX: $0, y: $0)
            })
            .disposed(by: dispose)

		constants.getTaskAdditionalHeightValue(side)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.additionalHeight = $0
            })
            .disposed(by: self.dispose)
    }

    private var saving = false
    private let animationDuration = 0.3
    private func save(task: TaskModel) {
        guard !saving else { return }

        sendTaskButton.isEnabled = false
        saving = true

        UIView.animate(CATransaction.animationDuration()) {
            self.contentView?.isUserInteractionEnabled = false
            self.galleryCollectionView.isUserInteractionEnabled = false
        }

        presenter.events.on(.next(.save(task, state)))
    }

    private func saved(_ model: TaskModel, created: Bool) {
        if created {
            events.onNext(.created(model))
        }
        events.onNext(.updated(model))

        saving = false

        close()
    }

    @IBAction func addTaskButton(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
		let pulse = CASpringAnimation(keyPath: "transform.scale")
		pulse.duration = animationDuration
		pulse.fromValue = 1.0
		pulse.toValue = 1.4
		pulse.autoreverses = true
		pulse.initialVelocity = 0.5
		pulse.damping = 1.0
		sendTaskButton.layer.add(pulse, forKey: nil)

        newTask.side = side
        if newTask.title.isEmpty &&
            newTask.texts.count == 0 {
            newTask.mode = .splitted
        } else {
            newTask.mode = side == .center ? .combined : .splitted
        }

        let sides = GetTaskSidesUseCase.shared.execute(newTask)
        newTask.createPositions(position: position)

        self.transitions
            .filter({ sides.contains($0.side) })
            .forEach({
                let modelId = $0.model.viewModel.modelId
                let task = GetTasksUseCase.shared.task(id: modelId)
                task?.id = newTask.id
                task?.mode = newTask.mode
                task?.side = newTask.side
                task?.title = newTask.title
                task?.items = newTask.items
                task?.order = newTask.order
                task?.reference = newTask.reference
                task?.positions = newTask.positions
        })
        
        photosViewController?.dismiss()
        save(task: newTask)
    }

    var closing = false
    @objc func close() {
        guard !saving else { return }
        closing = true
        events.on(.next(.exit))
    }

    func exitToList() {
        transitions.forEach( { $0.exit() })
        
        UIView.animate(withDuration: CATransaction.animationDuration(),
                       animations: {
            self.transitions.forEach( { $0.translateCellVerticaly(by: 0) })
            self.transitions.forEach( { $0.controller.view.layoutIfNeeded() })
            self.view.alpha = 0
        }, completion: { _ in
            self.events.on(.next(.dismiss))
        })
    }

    private func updateButtonState() {
        var isEmpty = descriptionViewModel.isTextEmpty
        isEmpty = isEmpty && photosView.contentIsEmptyValue
        isEmpty = isEmpty && checklistViewModel.isTextEmpty
        isEmpty = isEmpty && titleViewModel.isTextEmpty

        shouldChangeListSideOnChangeInput = isEmpty && !changeListSideLocked

        UIView.animate(CATransaction.animationDuration(), animations: {
            self.sendTaskButton.alpha = !isEmpty ? 1 : 0
            self.sendTaskButton.isUserInteractionEnabled = !isEmpty
        })
    }

    private func createEditingController(type: TimesetType) -> AbstractEditingHandler? {
        switch type {
        case .center:
            let controller = PlainTextController()
            controller.titleTextView = titleView
            return controller
        case .right:
            let controller = ChecklistController()
            controller.titleTextView = titleView
            return controller
        case .left:
            return nil
        }
    }

    var beginPoint = CGPoint.zero
    @objc func panPhotosCollectionView() {
        guard let gesture = panPhotosGesture else { return }
        switch panPhotosGesture.state {
        case .began:
            beginPoint = gesture.location(in: view)
        case .changed:
            guard self.contentViewBottom.constant < 420 else {
                self.panPhotosGesture.isEnabled = false
                self.panPhotosGesture.isEnabled = true
                self.endGestureTransition()
                return
            }
            let diff = beginPoint.y - gesture.location(in: view).y
            self.contentViewBottom.constant = galleryCollectionView.twoRowsHeight + diff
        case .ended:
            self.endGestureTransition()
        default: break
        }
    }
    
    private func endGestureTransition() {
        guard let gesture = panPhotosGesture else { return }
        let diff = beginPoint.y - gesture.location(in: view).y
        self.beginPoint = CGPoint.zero
        UIView.animate(CATransaction.animationDuration()) {
            if diff > 50 {
                self.setPhotoCollectionViewScrollableState()
            } else {
                self.setPhotoCollectionViewIntialState()
            }
            self.view.layoutIfNeeded()
        }
    }

    @objc func panContentView() {
        guard let gesture = panContentGesture else { return }
        
        switch gesture.state {
        case .began:
            beginPoint = gesture.location(in: view)
        case .changed:
            let diff = beginPoint.y - gesture.location(in: view).y
            contentViewBottom.constant = galleryCollectionView.fullHeight + diff
        case .ended:
            let diff = beginPoint.y - gesture.location(in: view).y
            beginPoint = CGPoint.zero
            UIView.animate(CATransaction.animationDuration()) {
                if diff < -50 {
                    self.setPhotoCollectionViewIntialState()
                } else {
                    self.setPhotoCollectionViewScrollableState()
                }
                self.view.layoutIfNeeded()
            }
        default: break
        }
    }

    func setPhotoCollectionViewIntialState(bottom: Bool = true) {
        panContentGesture.isEnabled = false
        panPhotosGesture.isEnabled = true
        galleryCollectionView.scrollingEnabled = false
        if bottom {
            contentViewBottom.constant = galleryCollectionView.twoRowsHeight
        }
        photosView.updateContentHeight()
    }

    private func setPhotoCollectionViewScrollableState() {
        panPhotosGesture.isEnabled = false
        panContentGesture.isEnabled = true
        galleryCollectionView.scrollingEnabled = true
        contentViewBottom.constant = galleryCollectionView.fullHeight
        photosView.updateContentHeight()
    }

    func display(photos: FullScreenPhotosViewController) {
        photosViewController = photos
        photos.view.fill(in: view)
        view.bringSubviewToFront(sendTaskButton)
        addChild(photos)
        photos.sendTaskButtonBottom = sendTaskButtonBottom
        UIView.animate(CATransaction.animationDuration()) {
            self.sendTaskButtonBottom.isActive = true
            self.view.layoutIfNeeded()
        }
    }

    private var currentContentViewLeading: CGFloat = 0
    private var panningHorizontaly: Bool = false
    
    @objc func panEmptyView(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            let velocity = gesture.velocity(in: nil)
            panningHorizontaly = abs(velocity.x) > abs(velocity.y)
        default: break
        }
        
        panningHorizontaly ? panEmptyViewHorizontaly(gesture) : panEmptyViewVerticaly(gesture)
    }
    
    private func panEmptyViewHorizontaly(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            currentContentViewLeading = contentViewLeading.constant
            beginPoint = gesture.location(in: emptyView)
        case .changed:
            let diff = beginPoint.x - gesture.location(in: emptyView).x
            let newLeading = currentContentViewLeading - diff
            guard abs(newLeading) <= screenWidth else { return }
            contentViewLeading.constant = newLeading
            if shouldChangeListSideOnChangeInput {
                ChangeSideUseCase.shared.change.on(.next((screenWidth - newLeading)))
            }
        case .ended:
            beginPoint = CGPoint.zero
            if contentViewLeading.constant == 0 {
                state = .description
            } else if contentViewLeading.constant < 0 {
                state = abs(contentViewLeading.constant) > screenWidth / 2 ? .checklist : .description
            } else {
                state = abs(contentViewLeading.constant) > screenWidth / 2 ? .photos : .description
            }
            if shouldChangeListSideOnChangeInput {
                side = state.type
                ChangeSideUseCase.shared.change.on(.next((0)))
            }
            updateContentViewState(setup: false, animated: true)
        default: break
        }
    }
    
    private var initialContentViewBottomPan: CGFloat?
    private func panEmptyViewVerticaly(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            beginPoint = gesture.location(in: view)
            initialContentViewBottomPan = contentViewBottom.constant
        case .changed:
            let diff = beginPoint.y - gesture.location(in: view).y
            contentViewBottom.constant = (initialContentViewBottomPan ?? 0) + diff
            self.transitions
                .filter({ $0.side == side })
                .forEach({  $0.translateCellVerticaly(by: -1*diff) })
        case .cancelled:
            restoreFromPaning()
        case .ended:
            let diff = beginPoint.y - gesture.location(in: view).y
            if abs(diff) > 50 {
                close()
            } else {
                restoreFromPaning()
            }
        default: break
        }
    }
    
    private func restoreFromPaning() {
        UIView.animate(withDuration: CATransaction.animationDuration(),
                       animations: {
            self.contentViewBottom.constant = self.initialContentViewBottomPan ?? 0
            self.view.layoutIfNeeded()
            self.transitions
                .filter({ $0.side == self.side })
                .forEach({  $0.translateCellVerticaly(by: 0) })
        }, completion: nil)
    }
    
    private func updateContentViewHeight() {
        var newContentHeight: CGFloat = descriptionView.contentHeightValue
        if contentViewLeading.constant < 0 {
            newContentHeight += (checklistView.contentHeightValue - descriptionView.contentHeightValue) * abs(contentViewLeading.constant) / screenWidth
        } else if contentViewLeading.constant > 0 {
            newContentHeight += (photosView.contentHeightValue - descriptionView.contentHeightValue) * abs(contentViewLeading.constant) / screenWidth
        }
        setContentViewHeight(with: newContentHeight)
    }
    
    private func updateContentViewState(setup: Bool = true, animated: Bool) {
        stateDisposeBag = DisposeBag()
        
        UIView.animate(withDuration: animated ? CATransaction.animationDuration() : 0, animations: {
            var offset = self.contentViewLeading.constant
            switch self.state {
            case .description:
                offset = 0
                self.galleryCollectionView.alpha = 0
            case .checklist:
                offset = self.screenWidth * -1
                self.galleryCollectionView.alpha = 0
            case .photos:
                self.galleryCollectionView.alpha = 1
                offset = self.screenWidth
            }
            
            self.contentViewLeading.constant = offset
            self.contentView?.layoutIfNeeded()
            
            self.view.endEditing(true)
            if setup {
                switch self.editingAction {
                case .title(let range):
                    self.titleView?.beginEditing(backspaced: false)
                    self.titleView?.textView.selectedRange = range
                case .text:
                    self.descriptionView.beginEditing(backspaced: false)
                case .checklist:
                    self.checklistView.beginEditing(backspaced: false)
                case .image:
                    self.photosView.handleTitle(event: .nextResponder)
                }
            } else {
                switch self.state {
                case .description:
                    self.descriptionView.beginEditing(backspaced: false)
                case .checklist:
                    self.checklistView.beginEditing(backspaced: false)
                case .photos:
                    self.photosView.handleTitle(event: .nextResponder)
                }
            }

            self.backgroundView?.layer.masksToBounds = false
            self.backgroundView?.layer.borderWidth = 2
            self.backgroundView?.layer.borderColor = (self.newTask.public ? UIColor.red : UIColor.green).cgColor
            
            ConfigurationFacade.shared
                .taskModuleConstants
                .getTaskBackgroundColorValue(self.side)
                .subscribe(onNext: { [weak self] in
                    self?.backgroundView?.backgroundColor = $0
                })
                .disposed(by: self.stateDisposeBag)

        }, completion: { _ in
            self.updateContentViewHeight()
        })
    }

    private var initialAnimation = true
    private func setContentViewHeight(with height: CGFloat) {
        guard !closing else { return }

        let maxHeight = view.bounds.height - 70 - contentViewBottom.constant
        let newHeight = min(maxHeight, height + 50)
        guard !newHeight.equal(to: contentViewHeight.constant) else { return }
        contentViewHeight.constant = newHeight
        contentView?.superview?.layoutIfNeeded()
        view.layoutIfNeeded()

        if let backgroundView = backgroundView {
            transitions.forEach({
                $0.set(frame: backgroundView.frame, from: view)
            })
        }

        if initialAnimation {
            initialAnimation = false
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
                self.transitions.forEach({
                    $0.controller.view.layoutIfNeeded()
                })
            }, completion: { _ in
                self.events.onNext(.didAppear)
            })
        }

        UIView.animate(CATransaction.animationDuration()) {
            self.view.alpha = 1
        }
    }
    
    @IBAction func publicButtonPressed(_ sender: UIButton) {
        backgroundView?.layer.borderColor = UIColor.red.cgColor
        newTask.public = true
    }
    
    @IBAction func privateButtonPressed(_ sender: UIButton) {
        backgroundView?.layer.borderColor = UIColor.green.cgColor
        newTask.public = false
    }
}
