//
//  TaskSelectedPhotosUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 
import RxSwift
import Photos

enum TaskPhotoSelectionAction {
    case add(EntityModel)
    case remove(EntityModel)
}

final class TaskSelectedPhotosUseCase {
    static let shared = TaskSelectedPhotosUseCase()
    var events = PublishSubject<TaskPhotoSelectionAction>()
    var selected: [EntityModel] = []
    private let picturesRepository = PicturesRepository()
    private let photoAssetsRepository = PhotoAssetsRepository()
    private let userRepository = UserRepository()
    private let gallerySelectionUseCase = GallerySelectedAssetsUseCase.shared
    private let assetNamesRepository = AssetNamesRepository.shared
    
    func execute() -> Observable<TaskEditingUseCaseResult> {
        selected = []
        return gallerySelectionUseCase.execute()
            .flatMap({ action -> Observable<TaskEditingUseCaseResult> in
                switch action {
                case .add(let asset):
                    return self.assetNamesRepository
                        .assetId(asset)
                        .filter({ assetId in
                            !self.selected.contains(where: {
                                $0.value == assetId
                            })
                        })
                        .map({ EntityModel(.photo, value: $0) })
                        .do(onNext: { self.selected.append($0) })
                        .flatMap({ self.makeEntityPhoto($0, asset) })

                case .remove(let asset):
                    return self.assetNamesRepository
                        .assetId(asset)
                        .map({ entityValue in
                            self.selected.first(where: {
                                $0.value == entityValue
                            })
                        })
                        .skipNil()
                        .do(onNext: { entity in
                            self.selected = self.selected
                                .filter({ $0.value != entity.value })
                        })
                        .map({ .photoRemoved($0) })
                }
            })
    }
    
    func makeEntityPhoto(_ entity: EntityModel, _ asset: PHAsset) -> Observable<TaskEditingUseCaseResult> {
        guard let path = userRepository.user?.uid else {
            return .error(GenericError.notAuthorized)
        }
        
        return self.assetNamesRepository
            .assetFilename(asset)
            .flatMap({
                self.picturesRepository.getLocal(path: path, name: $0)
            })
            .map({ _ in .photoAdded(entity) })
            .catchError({ _ -> Observable<TaskEditingUseCaseResult> in
                self.assetNamesRepository
                    .assetFilename(asset)
                    .flatMap({
                        self.photoAssetsRepository
                            .put(asset, path: path, name: $0)
                            .map({ _ in .photoUpdated(entity) })
                            .startWith(.photoAdded(entity))
                    })
            })
    }

    func remove(_ model: EntityModel) -> Observable<TaskEditingUseCaseResult> {
        return self.assetNamesRepository
            .asset(model.value)
            .flatMap({ asset -> Observable<TaskEditingUseCaseResult> in
                self.gallerySelectionUseCase
                    .remove(asset)
                    .map({ _ in .loaded })
            })
            .catchErrorJustReturn(.photoRemoved(model))
    }
}
