//
//  ChangeSideUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class ChangeSideUseCase {
    static let shared = ChangeSideUseCase()
    var change = PublishSubject<CGFloat>()
    private var disposeBag = DisposeBag()
    private init() {
        self.change
            .asObserver()
            .observeOn(MainScheduler.instance)
            .do(onNext: {
                self.scroll(offset: $0)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func scroll(offset: CGFloat = 0) {
        guard let window = UIApplication.shared.keyWindow else { return }
        guard let navigationController = window.rootViewController as? UINavigationController else { return }
        guard let mainViewController = navigationController.viewControllers.first as? MainTaskViewController else { return }
        mainViewController.timesetManger.engineService.scrollTo(offset: offset)
    }
}
