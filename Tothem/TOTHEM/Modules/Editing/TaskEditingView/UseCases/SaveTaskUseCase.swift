//
//  SaveTaskUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class SaveTaskUseCase {
    static let shared = SaveTaskUseCase()
    private let usersDataService = UsersDataService.shared
    //private let taggingService = TaggingService()
    private let textLabelService = TextAnalysisService()
    private let photoLabelService = PhotoLabelService()
    private let translationService = TextTranslationService()
    private let userRepository = UserRepository()
    private let labelingDisposeBag = DisposeBag()
    private let getPhotoUseCase = GetEntityPhotoUseCase.shared
    private let assetNamesRepository = AssetNamesRepository.shared
    private let userClassService = UserClassService.shared
    
    private init() { }
    func create(_ task: TaskModel) -> Observable<TaskModel> {
        let assets = task.photos.map({ ($0.value, task.public) })
        return self.assetNamesRepository.add(assets)
            .flatMap({ _ in
                self.usersDataService.save(task: task)
            })
            .do(onNext: {
                // TODO: move on backend
                self.createLabelsAndSave($0)
            })
    }
    
    func createLabelsAndSave(_ task: TaskModel) {
        let photos = setLabelsTo(photos: task.photos)
        let texts = Observable.just(task.texts)
        //setLabelsTo(textModels: task.texts)
        let bullets = Observable.just(task.bullets)
        //setLabelsTo(textModels: task.bullets)
        return Observable.combineLatest(texts, photos, bullets)
            .do(onNext: { (texts, photos, bullets) in
                task.setup(texts, photos, bullets)
            })
            //.flatMap({ _ in self.taggingService.save(task: task) })
            .flatMap({ _ in self.userClassService.save(task: task) })
            .subscribe(onNext: {
                print("%%%%%%%%")
                print($0)
                print(String(describing: $0["priority"]))
                print("%%%%%%%%")
                if let rawValue = $0["priority"] as? String,
                    let priority = TaskContentPriority(rawValue: rawValue) {
                    task.priority = priority
                    
                    UserClassService.addPriorityDs(priority)
                }
            })
            .disposed(by: labelingDisposeBag)
    }
    
    func save(_ model: TaskModel) -> Observable<TaskModel> {
        return usersDataService.save(task: model)
    }
    
    private func setLabelsTo(textModels: [EntityModel]) -> Observable<[EntityModel]> {
        guard textModels.count > 0 else { return .just([]) }
        return Observable.zip(textModels.map({ model in
            self.textLabelService.execute(text: model.value)
                .map({ labels -> EntityModel in
                    var newModel = model
                    newModel.labels = labels
                    return newModel
                })
                .catchErrorJustReturn(model)
        }))
    }
    
    private func setLabelsTo(photos: [EntityModel]) -> Observable<[EntityModel]> {
        guard photos.count > 0, let uid = userRepository.user?.uid else {
            return .just([])
        }
        
        return Observable.zip(photos.map({ photo in
            self.getPhotoUseCase
                .execute(uid: uid, photoEntityValue: photo.value)
                .flatMap({ image in
                    self.photoLabelService.execute(image: image)
                })
//                .flatMap ({ array -> Observable<[AiLabelModel]> in
//                    guard array.count > 0 else { return .just(array) }
//                    return Observable.zip(array.map { model in
//                        return self.translationService
//                            .execute(text: model.text, from: .en, to: .ru)
//                            .map({
//                                var newModel = model
//                                newModel.text = ($0 ?? model.text).lowercased()
//                                return newModel
//                            })
//                            .catchErrorJustReturn(model)
//                    })
//                })
                .map({ models -> EntityModel in
                    var newPhoto = photo
                    newPhoto.labels = models
                    return newPhoto
                })
                .catchErrorJustReturn(photo)
        }))
    }
}
