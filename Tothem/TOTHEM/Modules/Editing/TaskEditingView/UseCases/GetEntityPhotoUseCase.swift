//
//  GetEntityPhotoUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class GetEntityPhotoUseCase {
    static let shared = GetEntityPhotoUseCase()
    private let picturesRepository = PicturesRepository()
    private let userRepository = UserRepository()
    private let userService = UsersDataService.shared
    private let assetIdsRepository = AssetNamesRepository.shared
    
    private init() { }
    
    func execute(uid: String?, photoEntityValue: String) -> Observable<UIImage> {
        guard let uid = uid ?? userRepository.user?.uid else {
            return .error(GenericError.notAuthorized)
        }
        
        return self.assetIdsRepository
            .assetIdFilename(photoEntityValue)
            .flatMap({
                self.picturesRepository.get(path: uid, name: $0)
            })
            .catchError({ _ in
                self.userService
                    .photoFilename(uid: uid, photoEntityValue: photoEntityValue)
                    .flatMap({
                        self.picturesRepository.get(path: uid, name: $0)
                    })
            })
    }
}
