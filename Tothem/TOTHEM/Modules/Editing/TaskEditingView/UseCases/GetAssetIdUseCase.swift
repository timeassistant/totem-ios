//
//  GetAssetIdUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import Photos
import RxSwift

final class GetAssetIdUseCase {
    static let shared = GetAssetIdUseCase()
    private var assets: [PHAsset] = []
    private var assetIds: [String] = []
    private init() { }
    
    func get(asset: PHAsset) -> Observable<String> {
        guard let index = assets.firstIndex(of: asset) else {
            let assetId = UUID().uuidString
            assets.append(asset)
            assetIds.append(assetId)
            return Observable.just(assetId)
        }
        return Observable.just(assetIds[index])
    }

    func get(assetId: String) -> Observable<PHAsset> {
        guard let index = assetIds.firstIndex(of: assetId) else {
            return Observable.error(RxError.noElements)
        }
        return Observable.just(assets[index])
    }
}
