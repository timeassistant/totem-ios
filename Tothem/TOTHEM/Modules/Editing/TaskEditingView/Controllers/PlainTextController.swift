//
//  TextController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class PlainTextController: CentralSideController {
    weak var titleTextView: EditingTextView?

    override func handle(titleEvent: EditingViewEvent) {
        switch titleEvent {
        case .nextResponder:
            view.editFirstRow(in: 0)
        default:
            break
        }
    }

    override func handleText(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace:
            activeSection = NSNotFound
            titleTextView?.beginEditing(backspaced: true)
        case .nextResponder:
            activeSection = NSNotFound
            titleTextView?.beginEditing(backspaced: false)
        default: break
        }
    }
}
