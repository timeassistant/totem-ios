//
//  ChecklistController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class ChecklistController: AbstractEditingHandler {
    weak var titleTextView: EditingTextView?

    override func beginEditing() {
        beginEditChecklist()
    }

    override func handle(titleEvent: EditingViewEvent) {
        switch titleEvent {
        case .nextResponder:
            beginEditChecklist()
        default:
            break
        }
    }

    override func handle(indexPath: IndexPath, event: EditingViewEvent) {
        switch viewModel.viewModel(at: indexPath).type {
        case .checklist:
            handleChecklist(indexPath: indexPath, event: event)
        default:
            break
        }
    }

    private func handleChecklist(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace:
            activeSection = NSNotFound
            titleTextView?.beginEditing(backspaced: true)
        case .textWillChange: break
        case .nextResponder:
            activeSection = NSNotFound
            titleTextView?.beginEditing(backspaced: false)
        case .startChecklist:
            beginEditChecklist()
        default: break
        }
    }

    private func handleText(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace: editPrev(from: indexPath)
        case .textWillChange: break
        case .nextResponder:
            insertSubtask(indexPath: indexPath)
        case .didBeginEditing:
            beginEditChecklist(becomeFirstResponder: false)
        case .startChecklist:
            beginEditChecklist()
        default: break
        }
    }

    private func handleSubtaskChecklist(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace: editPrev(from: indexPath.section)
        case .textWillChange: break
        case .nextResponder:
            editNext(from: indexPath)
        case .startChecklist:
            view.edit(indexPath: indexPath)
        default: break
        }
    }

    private func handleSubtaskText(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace: editPrev(from: indexPath)
        case .textWillChange: break
        case .nextResponder:
            insertSubtask(indexPath: indexPath)
        case .startChecklist:
            insertSubtask(indexPath: indexPath)
        default: break
        }
    }

    private func beginEditChecklist(becomeFirstResponder: Bool = true) {
        if viewModel.checklistSectionNumberOfItems == 0 {
            viewModel.checklistSectionNumberOfItems = 1
        }

        if becomeFirstResponder {
            activeSection = 0
        }
    }

    private func insertSubtask(indexPath: IndexPath) {
        if indexPath.section > 0,
            viewModel.viewModel(at: indexPath).isTextEmpty {
            editPrev(from: indexPath)
        } else {
            viewModel.insertSubtask(at: indexPath.section + 1)
            view.insert(section: indexPath.section + 1)
            activeSection = indexPath.section + 1
        }
    }
}
