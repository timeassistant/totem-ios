//
//  CentralSideController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class CentralSideController: AbstractEditingHandler {
    override func handle(indexPath: IndexPath, event: EditingViewEvent) {
        switch viewModel.viewModel(at: indexPath).type {
        case .checklist:
            handleChecklist(indexPath: indexPath, event: event)
        case .subtaskChecklist:
            handleSubtaskChecklist(indexPath: indexPath, event: event)
        case .text:
            handleText(indexPath: indexPath, event: event)
        case .subtaskText:
            handleSubtaskText(indexPath: indexPath, event: event)
        default: break
        }
    }

    func handleChecklist(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace: editPrev(from: indexPath)
        case .textWillChange: break
        case .nextResponder:
            edit(next: indexPath)
        case .startChecklist:
            beginEditChecklist()
        default: break
        }
    }

    func handleSubtaskChecklist(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace: editPrev(from: indexPath)
        case .textWillChange: break
        case .nextResponder:
            edit(next: indexPath)
        case .didBeginEditing:
            beginEditChecklist(becomeFirstResponder: false)
        case .didEndEditing: break
        case .startChecklist:
            view.edit(row: indexPath.row, section: indexPath.section)
        default: break
        }
    }

    func handleText(indexPath: IndexPath, event: EditingViewEvent) {
        fatalError("Not implemented")
    }

    func handleSubtaskText(indexPath: IndexPath, event: EditingViewEvent) {
        switch event {
        case .backspace: editPrev(from: indexPath)
        case .textWillChange: break
        case .nextResponder:
            edit(next: indexPath)
        case .didBeginEditing:
            beginEditChecklist(becomeFirstResponder: false)
        case .didEndEditing: break
        case .startChecklist:
            beginEditChecklist()
        default: break
        }
    }

    func beginEditChecklist(becomeFirstResponder: Bool = true) {

    }

    func edit(next indexPath: IndexPath) {
        fatalError("Not implemented")
    }
}
