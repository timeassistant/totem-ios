//
//  EditingViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

protocol EditingViewModel: class {
    var sectionsNumber: Int { get }
    var checklistSectionNumberOfItems: Int { get set }
    var currentEditingSection: Int { get set }

    func numberOfRows(in section: Int) -> Int

    func insertSubtask(at index: Int)

    var indexPathToEdit: IndexPath { get }

    func viewModel(at indexPath: IndexPath) -> EditingTextViewModel
}

protocol TaskContentEditingHandler: class {
    var activeSection: Int { get set }
    func handle(_ view: EditingTextView, event: EditingViewEvent)
    func handle(titleEvent: EditingViewEvent)
}

class AbstractEditingHandler: TaskContentEditingHandler {
    var dispose = DisposeBag()

    var viewModel: EditingViewModel!
    var view: TaskEditingStackView!

    var activeSection: Int = NSNotFound {
        didSet {
            guard activeSection != NSNotFound else { return }

            if activeSection != oldValue {
                viewModel.currentEditingSection = activeSection
                view?.edit(indexPath: viewModel.indexPathToEdit)
            }
        }
    }

    init() {

    }

    func text(at indexPath: IndexPath) -> String {
        fatalError("Not implemented")
    }

    func handle(titleEvent: EditingViewEvent) {
        fatalError("Not implemented")
    }

    func handle(indexPath: IndexPath, event: EditingViewEvent) {
        fatalError("Not implemented")
    }

    func updateContent() {
        fatalError("Not implemented")
    }

    @discardableResult
    func editNext(from indexPath: IndexPath) -> Bool {
        return edit(nextIndexPath: IndexPath(row: indexPath.row + 1, section: indexPath.section))
    }

    @discardableResult
    func edit(nextIndexPath: IndexPath) -> Bool {
        var indexPath = nextIndexPath
        while indexPath.row < viewModel.numberOfRows(in: indexPath.section) {
            guard viewModel.viewModel(at: indexPath).active else {
                indexPath.row += 1
                continue
            }

            activeSection = indexPath.section
            view?.edit(indexPath: indexPath)
            return true
        }

        indexPath.section += 1
        indexPath.row = 0
        if indexPath.section < viewModel.sectionsNumber {
            return edit(nextIndexPath: indexPath)
        }
        return false
    }

    @discardableResult
    func edit(prevIndexPath: IndexPath) -> Bool {
        var indexPath = prevIndexPath
        while indexPath.row >= 0 {
            guard viewModel.viewModel(at: indexPath).active else {
                indexPath.row -= 1
                continue
            }

            view?.edit(indexPath: indexPath)
            return true
        }

        if indexPath.section > 0 {
            indexPath.section -= 1
            indexPath.row = viewModel.numberOfRows(in: indexPath.section) - 1
            edit(prevIndexPath: indexPath)
        }

        return false
    }

    @discardableResult
    func editPrev(from indexPath: IndexPath) -> Bool {
        return edit(prevIndexPath: IndexPath(row: indexPath.row - 1, section: indexPath.section))
    }

    @discardableResult
    func editPrev(from section: Int) -> Bool {
        return edit(prevIndexPath: IndexPath(row: -1, section: section))
    }

    func handle(_ view: EditingTextView, event: EditingViewEvent) {
        guard let path = self.view?.index(of: view) else { return }
        handle(indexPath: path, event: event)
    }

    func beginEditing() {
        view?.edit(indexPath: viewModel.indexPathToEdit)
    }
}
