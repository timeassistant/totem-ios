//
//  TaskFormationController.swift
//  TOTHEM
//
//  Created by r00t on 30.10.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TaskFormationController: UIViewController, XIBLoadable, FormationView, TaskEditingView {

    private lazy var presenter = TaskFormationControllerPresenter(view: self)
    var task: TaskFormationModel
    var didCreatedTask: (() -> Void)?
    var didChangeTaskType: ((TaskFormationModel) -> Void)?
    var willDismissController: (() -> Void)?
    var changeTaskType: ((TaskFormationModel) -> Void)?

    private var itemFormatter: ItemFormatter!

    //rx dispose
    var disposeBag = DisposeBag()

    //OUTLETS
    @IBOutlet var titlePlaceholderLabel: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var titleTextViewHeight: NSLayoutConstraint!
    @IBOutlet var contentViewPlaceholderLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var contentTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sendTaskButton: UIButton!
    @IBOutlet weak var contentContainerView: UIView!
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!

	var additionalHeight: CGFloat = 0

    private let titleViewObserver = TextViewDelegate()
    private let contentViewObserver = TextViewDelegate()

    private var titleText = Variable<String>("")
    private var descriptionText = Variable<String>("")
    private var isEmpty: Observable<Bool>!

    required init(task: TaskFormationModel) {
        self.task = task
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.task = TaskFormationModel(type: .active)
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        isEmpty = Observable.combineLatest(titleText.asObservable(), descriptionText.asObservable()) { !$0.isEmpty || !$1.isEmpty }

        observeKeyboard()
        bind()

        setupUI(with: task.type)
        titleText.value = TaskTransformation.title(task)
        descriptionText.value = TaskTransformation.description(task)
        itemFormatter = ItemFormatter(type: task.type)

        prefillData()
    }

    private func prefillData() {
        self.titleTextView.text = titleText.value
        self.contentTextView.text = descriptionText.value
        self.titlePlaceholderLabel.isHidden = !titleText.value.isEmpty
        self.contentViewPlaceholderLabel.isHidden = !descriptionText.value.isEmpty
    }

    private func setupUI(with type: TimesetType) {
        self.setupUIElement(with: type)

        self.titlePlaceholderLabel.text = "Title text"
        self.contentViewPlaceholderLabel.text = "Content text"
        self.titleTextViewHeight.constant = self.titleTextView.contentSize.height + 15
        self.contentTextViewHeight.constant = self.contentTextView.contentSize.height + 15
        self.titleTextView.becomeFirstResponder()
    }

    private func setupUIElement(with side: TimesetType) {
        self.contentContainerView.backgroundColor = ConfigurationFacade().taskModuleConstants.getTaskBackgroundColorValue(side)
        self.titleTextView.textColor = ConfigurationFacade().taskModuleConstants.getTaskTitleLabelColorValue(side)
        self.contentTextView.textColor = ConfigurationFacade().taskModuleConstants.getTaskDescriptionLabelColorValue(side)

        self.titlePlaceholderLabel.textColor = ConfigurationFacade().taskModuleConstants.getTaskTitleLabelColorValue(side)
        self.contentViewPlaceholderLabel.textColor = ConfigurationFacade().taskModuleConstants.getTaskDescriptionLabelColorValue(side)

        self.sendTaskButton.backgroundColor = ConfigurationFacade().taskModuleConstants.getTaskSendButtonColorValue(side)
        self.sendTaskButton.layer.borderColor =  ConfigurationFacade().taskModuleConstants.getTaskSendButtonBorderColorValue(side).cgColor
        self.sendTaskButton.layer.borderWidth =  ConfigurationFacade().taskModuleConstants.getTaskSendButtonBorderWidthValue(side)

        self.titleTextView.font = ConfigurationFacade().taskModuleConstants.getTaskTitleFontValue(side)
        self.contentTextView.font = ConfigurationFacade().taskModuleConstants.getTaskContentFontValue(side)

        let scaleUpRatio = ConfigurationFacade().taskModuleConstants.getTaskContentScaleValue(side)
        self.sendTaskButton.transform = CGAffineTransform(scaleX: scaleUpRatio, y: scaleUpRatio)

        self.additionalHeight = ConfigurationFacade().taskModuleConstants.getTaskAdditionalHeightValue(side)

        self.contentContainerView .layer.cornerRadius = ConfigurationFacade().taskModuleConstants.getTaskCornerValue(side)
        self.contentContainerView.layer.masksToBounds = true
    }

    private func bind() {
        self.observePlaceholderLabels()
        //self.observerReturnKey()
        self.titleTextView.rx.text
            .orEmpty
            .bind(to: titleText)
            .disposed(by: self.disposeBag)

        self.contentTextView.rx.text
            .orEmpty
            .bind(to: descriptionText)
            .disposed(by: disposeBag)

        isEmpty.bind { (value) in
            UIView.animate(withDuration: 0.3, animations: {
                self.sendTaskButton.alpha = value ? 1 : 0
                self.sendTaskButton.isUserInteractionEnabled = value
            })
            }.disposed(by: self.disposeBag)

        titleTextView.delegate = titleViewObserver
        contentTextView.delegate = contentViewObserver

        titleTextView.rx.observe(CGSize.self, "contentSize")
            .subscribe(onNext: { _ in
                self.titleSizeChanged()
            })
            .disposed(by: self.disposeBag)

        contentTextView.rx.observe(CGSize.self, "contentSize")
            .subscribe(onNext: { _ in
                self.contentSizeChanged()
            })
            .disposed(by: self.disposeBag)
    }

    private var titleReturned = false
    private var contentReturned = false
    private func titleTextDidChange(_ text: String) {
        titlePlaceholderLabel.isHidden = !text.isEmpty
        var task = self.task
        TaskTransformation.update(&task, title: text)
        self.task = task
    }

    private func contentTextDidChange(_ text: String) {
        contentViewPlaceholderLabel.isHidden = !text.isEmpty
        var task = self.task
        TaskTransformation.update(&task, description: text)
        self.task = task
    }

    private func observePlaceholderLabels() {
        titleViewObserver.events
            .flatMap({ self.itemFormatter.title.update(textView: self.titleTextView, with: $0, previouslyReturned: self.contentReturned) })
            .subscribe(onNext: { action in
                self.contentReturned = false
                switch action {
                case .returned:
                    self.titleReturned = true
                case .nextResponder:
                    self.contentTextView.becomeFirstResponder()
                case .textDidChange(let text):
                    self.titleTextDidChange(text)
                case .didBeginEditing:
                    self.titleViewDidBeginEditing()
                }
            }).disposed(by: disposeBag)

        contentViewObserver.events
            .flatMap({ self.itemFormatter.content.update(textView: self.contentTextView, with: $0, previouslyReturned: self.titleReturned) })
            .subscribe(onNext: { action in
                self.titleReturned = false
                switch action {
                case .returned:
                    self.contentReturned = true
                case .nextResponder:
                    self.titleTextView.becomeFirstResponder()
                case .textDidChange(let text):
                    self.contentTextDidChange(text)
                case .didBeginEditing:
                    self.contentViewDidBeginEditing()
                }
            }).disposed(by: disposeBag)
    }

    private func contentViewDidBeginEditing() {
        titleTextView.contentOffset = CGPoint.zero
    }

    private func titleViewDidBeginEditing() {
        contentTextView.contentOffset = CGPoint.zero
    }

    private func contentSizeChanged() {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentTextViewHeight.constant = min(self.contentTextView.textSize.height + 15, self.view.frame.height/4.5)
            self.contentContainerView.layoutIfNeeded()
        })
    }

    private func titleSizeChanged() {
        UIView.animate(withDuration: 0.3, animations: {
            self.titleTextViewHeight.constant = min(self.titleTextView.textSize.height + 15, self.view.frame.height/2.5)
            self.contentContainerView.layoutIfNeeded()
        })
    }

    private func observeKeyboard() {
            KeyboardManager.keyboardHeight()
                .bind(to: self.bottomContainerConstraint.rx.constant.mapObserver {
				$0 + self.additionalHeight
                })
                .disposed(by: self.disposeBag)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !self.contentContainerView.frame.contains((touches.first?.location(in: self.view))!) {
            self.dismiss()
        }
    }

    private var saving = false
    private func save(task: TaskFormationModel) {
        if saving {
            return
        }
        sendTaskButton.isEnabled = false
        saving = true
        presenter.save(task: task) { [weak self] in
            self?.didCreatedTask?()
            self?.dismiss()
            self?.saving = false
        }
    }

    @IBAction func addTaskButton(_ sender: UIButton) {
		let pulse = CASpringAnimation(keyPath: "transform.scale")
		pulse.duration = 0.2
		pulse.fromValue = 1.0
		pulse.toValue = 1.4
		pulse.autoreverses = true
		pulse.initialVelocity = 0.5
		pulse.damping = 1.0
		sendTaskButton.layer.add(pulse, forKey: nil)
        save(task: task)
    }

    @IBAction func processChangeTaskType() {
        self.willDismissController?()
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: {
            self.changeTaskType?(self.task)
        })
    }

    func dismiss() {
        self.willDismissController?()
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}
