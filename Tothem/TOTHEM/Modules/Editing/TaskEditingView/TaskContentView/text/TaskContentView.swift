//
//  TaskContentView.swift
//  TOTHEM
//
//  Created by Sergei Perevoznikov on 26/06/2018.
//  Copyright © 2018 r00t. All rights reserved.
//

import UIKit
import RxSwift
import Photos

enum TaskContentState {
    case compact
    case active
    case inactive
}

class TaskContentView: XibSubview, TaskContentEditingDelegate {

    var dispose = DisposeBag()
    private var contentViewCurrentHeight: Int = 0
    private var contentViewMinimumHeight: CGFloat = 100
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint?
    @IBOutlet weak var contentViewWidth: NSLayoutConstraint?

    var photosView = TaskPhotosView()
    var scrollView = UIScrollView()
    var stackView = TaskEditingStackView()
    var titleView: EditingTextView!
    var titleViewModel: EditingTextViewModel!

    private(set) var type: TimesetType = .center

    private(set) var viewModel: EditingViewModel?
    var activatedView = Variable<UIView>(UIView())
    var contentIsEmpty = BehaviorSubject(value: true)
    var editingPhotos: Bool {
        return type == .left
    }

    private(set) var style: TaskContentStyle! {
        didSet {
            titleView?.set(style: style.titleStyle)
            style.titleStyle.background
                .asObservable()
                .observeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { [weak self] in
                    self?.backgroundColor = $0
                }).disposed(by: dispose)

            layer.masksToBounds = true

            style.cornerRadius
                .asObservable()
                .observeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { [weak self] in
                    self?.layer.cornerRadius = $0
                })
                .disposed(by: self.dispose)
        }
    }

    private(set) var task: TaskModel! {
        didSet {
            titleViewModel?.text.value = task.title
            titleView?.textView.text = task.title

            switch type {
            case .center:
                viewModel = PlainTextViewModel(task: task, type: .center)
            case .right:
                viewModel = ChecklistViewModel(task: task, type: .right)
            case .left:
                viewModel = LeftViewModel(task: task, type: type)
            }
            viewModel?.currentEditingSection = 0
            dataSource = TaskContentViewDataSource(delegate: self, model: viewModel, style: style)
        }
    }

    var dataSource: TaskContentViewDataSource! {
        didSet {
            stackView.dataSource = dataSource
            stackView.resetAllSubviews()
            if let viewModel = viewModel {
                for section in 0..<viewModel.sectionsNumber {
                    stackView.insert(section: section)
                }
            }
        }
    }

    var handler: TaskContentEditingHandler? {
        didSet {
            (handler as? EditingViewController)?.view = stackView
            (handler as? EditingViewController)?.viewModel = viewModel
        }
    }

    var state: TaskContentState {
        set {
            style.state = newValue
        }
        get {
            return style.state
        }
    }

    override func configureView() {
        super.configureView()

        view.translatesAutoresizingMaskIntoConstraints = false
        photosView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(photosView)
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)

        setupTitleView()
        setupConstraints()

        self.scrollView.rx
            .observe(CGSize.self, "contentSize")
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { _ in
                self.updateContentHeight()
            })
            .disposed(by: dispose)

        self.photosView.rx
            .observe(CGSize.self, "contentSize")
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { _ in
                self.updateContentHeight()
            })
            .disposed(by: dispose)

        activatedView
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                guard let path = self?.stackView.index(of: $0) else { return }
                self?.handler?.activeSection = path.section
            })
            .disposed(by: dispose)

        self.titleView
            .events
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] event in
                guard let sself = self else { return }
                if sself.type == .left {
                    sself.handleTitle(event: event)
                } else {
                    sself.handler?.handle(titleEvent: event)
                }
                sself.updateEmptyState()
            })
            .subscribe()
            .disposed(by: dispose)

        photosView.backgroundColor = .clear
    }

    func setup(type: TimesetType, style: TaskContentStyle, task: TaskModel) {
        self.type = type
        self.style = style
        self.task = task

        photosView.isHidden = type != .left
        scrollView.isHidden = type == .left

        updateContentHeight()
        updateEmptyState()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            photosView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            photosView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            photosView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            photosView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            NSLayoutConstraint(item: stackView, attribute: .width, relatedBy: .equal, toItem: scrollView, attribute: .width, multiplier: 1, constant: 0),
            scrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),

            NSLayoutConstraint(item: titleView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0),
            titleView.topAnchor.constraint(equalTo: view.topAnchor),
            titleView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }

    func setupTitleView() {
        titleView = EditingTextView(frame: CGRect(x: 0, y: 0, width: scrollView.bounds.width, height: 50))
        titleViewModel = EditingTextViewModel(.title)

        VisibilityRulesService.shared.visibility(for: .title)
            .startWith(true)
            .map { !$0 }
            .bind(to: titleView.rx.isHidden)
            .disposed(by: self.dispose)

        view.addSubview(titleView)
        titleView.set(model: titleViewModel)
        titleView.formatter = EditingViewType.title.formatter
        titleView.placeholderLabel?.text = "Title"
    }

    private var photosContentSizeHeight: CGFloat = 0
    func updateContentHeight() {
        let scrollSizeHeight = editingPhotos ? photosView.contentSize.height : scrollView.contentSize.height + 20
        let contentHeight = scrollSizeHeight + titleView.bounds.height
        let frame = convert(self.frame, to: UIApplication.shared.windows.first)
        let newContentHeight = Int(max(min(max(contentHeight, contentViewMinimumHeight), frame.maxY - 20 - UIApplication.shared.statusBarFrame.height), contentViewMinimumHeight))
        if newContentHeight != contentViewCurrentHeight {
            contentViewCurrentHeight = newContentHeight
            UIView.animate(withDuration: 0.3) {
                self.contentViewHeight?.constant = CGFloat(newContentHeight)
                self.view.layoutIfNeeded()
            }
        }

        if editingPhotos, photosContentSizeHeight != photosView.contentSize.height {
            photosContentSizeHeight = photosView.contentSize.height
            UIView.animate(withDuration: 0.3) {
                self.photosView.contentOffset = CGPoint(x: 0, y: max(self.photosView.contentSize.height - self.photosView.bounds.height, 0))
            }
        }
    }

    private func handleTitle(event: EditingViewEvent) {
        switch event {
        case .nextResponder:
            titleView.textView.resignFirstResponder()
        default: break
        }
    }

    func handle(_ view: EditingTextView, event: EditingViewEvent) {
        handler?.handle(view, event: event)
        updateEmptyState()
    }

    func add(asset: PHAsset) {
        photosView.add(photo: PhotoModel(asset))
        updateEmptyState()
    }

    func remove(asset: PHAsset) {
        photosView.remove(photo: PhotoModel(asset))
        updateEmptyState()
    }

    func update(photo: PhotoModel) {
        photosView.update(photo: photo)
    }

    func beginEditing(section: Int) {
        if section == NSNotFound {
            switch type {
            case .center:
                titleView.beginEditing(backspaced: false)
            case .right:
                viewModel?.currentEditingSection = 0
                stackView.edit(row: 0, section: 0)
            case .left: break
            }
        } else if section == 0 {
            titleView.beginEditing(backspaced: false)
        } else {
            viewModel?.currentEditingSection = section - 1
            stackView.edit(row: 0, section: section - 1)
        }
    }

    private func updateEmptyState() {
        guard let task = viewModel?.task else { return }
        contentIsEmpty.onNext(titleViewModel.text.value.isEmpty && task.text.isEmpty && task.checklist.isEmpty && photosView.assets.isEmpty)
    }
}
