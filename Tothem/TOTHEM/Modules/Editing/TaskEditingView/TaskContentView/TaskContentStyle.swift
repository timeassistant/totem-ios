//
//  TaskContentStyle.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum TaskContentState {
    case compact
    case active
    case inactive
}

struct TextViewStyle {
    var font = Variable<UIFont>(UIFont.systemFont(ofSize: 14))
    var color = Variable<UIColor>(UIColor.lightGray)
    var background = Variable<UIColor>(UIColor.darkText)
    var separator = SeparatorStyle()
}

struct SeparatorStyle {
    var height = Variable<CGFloat>(0)
    var color = Variable<UIColor>(UIColor.white)
    var offset = Variable<CGFloat>(0)
}

class TaskContentStyle {
    private var constants: TaskModuleConstants {
        return ConfigurationFacade.shared.taskModuleConstants
    }
    private var disposeBag: DisposeBag!
    private var inactiveDisposeBag: DisposeBag!

    var state: TaskContentState {
        didSet {
            guard state != oldValue else { return }

            if state == .compact {
                bindCompactStyle()
            } else {
                bindInactiveStyle()
            }
        }
    }

    var taskType: TimesetType {
        didSet {
            guard taskType != oldValue else { return }

            bindStyle()

            self.constants
                .getTaskCornerValue(taskType)
                .do(onNext: { cornerRadius in
                    self.cornerRadius.value = cornerRadius
                })
                .subscribe()
                .disposed(by: disposeBag)
        }
    }

    var titleStyle: TextViewStyle
    var textStyle: TextViewStyle
    var checklistStyle: TextViewStyle
    var readOnlyStyle: TextViewStyle

    var inactiveStyle: TextViewStyle
    var compactStyle: TextViewStyle

    var cornerRadius = Variable<CGFloat>(0)

    init(type: TimesetType, state: TaskContentState) {
        self.titleStyle = TextViewStyle()
        self.textStyle = TextViewStyle()
        self.checklistStyle = TextViewStyle()
        self.inactiveStyle = TextViewStyle()
        self.compactStyle = TextViewStyle()
        self.readOnlyStyle = TextViewStyle()

        self.taskType = type
        self.state = state

        bindStyle()
        if state == .compact {
            bindCompactStyle()
        } else {
            bindInactiveStyle()
        }

        self.constants
            .getTaskCornerValue(taskType)
            .do(onNext: { cornerRadius in
                self.cornerRadius.value = cornerRadius
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func bindCompactStyle() {
        inactiveDisposeBag = DisposeBag()

        self.compactStyle
            .background
            .asObservable()
            .bind(to: readOnlyStyle.background)
            .disposed(by: inactiveDisposeBag)

        self.compactStyle
            .font
            .asObservable()
            .bind(to: readOnlyStyle.font)
            .disposed(by: inactiveDisposeBag)

        self.compactStyle
            .color
            .asObservable()
            .bind(to: readOnlyStyle.color)
            .disposed(by: inactiveDisposeBag)

        self.compactStyle
            .separator
            .color
            .asObservable()
            .bind(to: readOnlyStyle.separator.color)
            .disposed(by: inactiveDisposeBag)

        self.compactStyle
            .separator
            .height
            .asObservable()
            .bind(to: readOnlyStyle.separator.height)
            .disposed(by: inactiveDisposeBag)

        self.compactStyle
            .separator
            .offset
            .asObservable()
            .bind(to: readOnlyStyle.separator.offset)
            .disposed(by: inactiveDisposeBag)
    }

    private func bindInactiveStyle() {
        inactiveDisposeBag = DisposeBag()

        self.inactiveStyle
            .background
            .asObservable()
            .bind(to: readOnlyStyle.background)
            .disposed(by: inactiveDisposeBag)

        self.inactiveStyle
            .font
            .asObservable()
            .bind(to: readOnlyStyle.font)
            .disposed(by: inactiveDisposeBag)

        self.inactiveStyle
            .color
            .asObservable()
            .bind(to: readOnlyStyle.color)
            .disposed(by: inactiveDisposeBag)

        self.inactiveStyle
            .separator
            .color
            .asObservable()
            .bind(to: readOnlyStyle.separator.color)
            .disposed(by: inactiveDisposeBag)

        self.inactiveStyle
            .separator
            .height
            .asObservable()
            .bind(to: readOnlyStyle.separator.height)
            .disposed(by: inactiveDisposeBag)

        self.inactiveStyle
            .separator
            .offset
            .asObservable()
            .bind(to: readOnlyStyle.separator.offset)
            .disposed(by: inactiveDisposeBag)
    }

    private func bindStyle() {
        disposeBag = DisposeBag()

        //title
        bindTitle()

        //checklist
        bindChecklist()

        //text
        bindText()

        // inactive
        bindInactive()

        //compact
        bindCompact()
    }

    private func bindTitle() {
        self.constants
            .getTaskBackgroundColorValue(taskType)
            .bind(to: titleStyle.background)
            .disposed(by: disposeBag)

        self.constants
            .getTaskTitleFontValue(taskType)
            .bind(to: titleStyle.font)
            .disposed(by: disposeBag)

        self.constants
            .getTaskTitleLabelColorValue(taskType)
            .bind(to: titleStyle.color)
            .disposed(by: disposeBag)
    }

    private func bindChecklist() {
        self.constants
            .getTaskBackgroundColorValue(taskType)
            .bind(to: checklistStyle.background)
            .disposed(by: disposeBag)

        self.constants
            .getTaskChecklistFontValue(taskType)
            .bind(to: checklistStyle.font)
            .disposed(by: disposeBag)

        self.constants
            .getTaskChecklistLabelColorValue(taskType)
            .bind(to: checklistStyle.color)
            .disposed(by: disposeBag)
    }

    private func bindText() {
        self.constants
            .getTaskBackgroundColorValue(taskType)
            .bind(to: textStyle.background)
            .disposed(by: disposeBag)

        self.constants
            .getTaskContentFontValue(taskType)
            .bind(to: textStyle.font)
            .disposed(by: disposeBag)

        self.constants
            .getTaskDescriptionLabelColorValue(taskType)
            .bind(to: textStyle.color)
            .disposed(by: disposeBag)
    }

    private func bindInactive() {
        self.constants
            .getTaskBackgroundColorValue(taskType)
            .bind(to: inactiveStyle.background)
            .disposed(by: disposeBag)

        self.constants
            .getTaskInactiveFontValue(taskType)
            .bind(to: inactiveStyle.font)
            .disposed(by: disposeBag)

        self.constants
            .getTaskChecklistLabelColorValue(taskType)
            .bind(to: inactiveStyle.color)
            .disposed(by: disposeBag)

        self.constants
            .getTaskSeparatorLineHeightValue(taskType)
            .bind(to: inactiveStyle.separator.height)
            .disposed(by: disposeBag)

        self.constants
            .getEditingTaskSeparatorLineColorValue(taskType)
            .bind(to: inactiveStyle.separator.color)
            .disposed(by: disposeBag)

        self.constants
            .getTaskSeparatorLineOffsetValue(taskType)
            .bind(to: inactiveStyle.separator.offset)
            .disposed(by: disposeBag)
    }

    private func bindCompact() {
        self.constants
            .getTaskBackgroundColorValue(taskType)
            .bind(to: compactStyle.background)
            .disposed(by: disposeBag)

        self.constants
            .getTaskCompactFontValue(taskType)
            .bind(to: compactStyle.font)
            .disposed(by: disposeBag)

        self.constants
            .getTaskDescriptionLabelColorValue(taskType)
            .bind(to: compactStyle.color)
            .disposed(by: disposeBag)

        self.constants
            .getTaskSeparatorLineHeightValue(taskType)
            .bind(to: compactStyle.separator.height)
            .disposed(by: disposeBag)

        self.constants
            .getTaskSeparatorLineColorValue(taskType)
            .bind(to: compactStyle.separator.color)
            .disposed(by: disposeBag)

        self.constants
            .getTaskSeparatorLineOffsetValue(taskType)
            .bind(to: compactStyle.separator.offset)
            .disposed(by: disposeBag)
    }
}
