//
//  TaskPhotosContentView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import Photos

final class TaskPhotosContentView: XibSubview, TaskContentViewProtocol {
    var dispose = DisposeBag()
    var events = PublishSubject<TaskContentViewEvent>()
    
    private var contentViewCurrentHeight: CGFloat = 0
    var minHeight: CGFloat = 100
    var maxSize: CGFloat = 300
    @IBOutlet weak var photosView: TaskPhotosView!
    weak var titleView: EditingTextView?
    weak var titleViewModel: EditingTextViewModel?

    private(set) var style: TaskContentStyle!

    var contentIsEmpty = BehaviorSubject(value: true)
    var contentIsEmptyValue: Bool {
        return (try? contentIsEmpty.value()) ?? true
    }

    lazy var contentHeight = BehaviorSubject<CGFloat>(value: 100)
    var contentHeightValue: CGFloat {
        return (try? contentHeight.value()) ?? minHeight
    }

    deinit {
        print("deinit TaskPhotosContentView")
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.photosView.events
            .map({ [weak self] in
                self?.updateEmptyState()
                switch $0 {
                case .remove(let model):
                    return TaskContentViewEvent.remove(model)
                }
            })
            .bind(to: events)
            .disposed(by: dispose)

        self.photosView.rx
            .observe(CGSize.self, "contentSize")
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] _ in
                self?.updateContentHeight()
            })
            .disposed(by: dispose)
    }

    func setup(style: TaskContentStyle, task: TaskModel) {
        self.style = style

        updateContentHeight()
        updateEmptyState()
    }

    private var photosContentSizeHeight: CGFloat = 0
    func updateContentHeight() {
        let height = max(self.photosView.size.height, minHeight)
        if !height.equal(to: contentViewCurrentHeight) {
            contentViewCurrentHeight = height
            contentHeight.on(.next(height))
        }

        if photosContentSizeHeight != photosView.contentSize.height {
            photosContentSizeHeight = photosView.contentSize.height
            UIView.animate(CATransaction.animationDuration()) {
                self.photosView.contentOffset = CGPoint(x: 0, y: max(self.photosView.contentSize.height - self.photosView.bounds.height, 0))
            }
        }
    }

    func handleTitle(event: EditingViewEvent) {
        switch event {
        case .nextResponder:
            titleView?.textView.resignFirstResponder()
        default: break
        }
        updateEmptyState()
    }

    func set(_ models: [EntityModel]) {
        photosView.set(photos: models)
    }

    func add(_ model: EntityModel) {
        photosView.add(photo: model)
        updateEmptyState()
        events.on(.next(.add(model)))
    }

    func remove(_ model: EntityModel) {
        photosView.remove(photo: model)
        updateEmptyState()
        events.on(.next(.remove(model)))
    }

    func update(_ model: EntityModel) {
        photosView.update(photo: model)
    }

    private func updateEmptyState() {
        contentIsEmpty.onNext(photosView.assets.isEmpty)
    }
}
