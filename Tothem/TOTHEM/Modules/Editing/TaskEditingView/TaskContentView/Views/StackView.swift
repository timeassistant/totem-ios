//
//  StackView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class StackView: UIStackView {
    override func layoutSubviews() {
        super.layoutSubviews()
        arrangedSubviews.forEach({ $0.layoutSubviews() })
    }

    func setup(stackView: UIStackView) {
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }

    func resetAllSubviews() {
        arrangedSubviews.forEach({$0.removeFromSuperview()})
    }

    func insert(_ view: UIView, at index: Int) {
        insertArrangedSubview(view, at: index)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
            ])
    }

    func delete(section: Int) {
        arrangedSubviews[section].removeFromSuperview()
    }
}
