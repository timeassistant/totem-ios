//
//  EditingTextViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

class EditingTextViewModel {
    var type: EditingViewType
    private(set) var active: Bool
    var text = BehaviorSubject(value: "")
    var textValue: String {
        return (try? self.text.value()) ?? ""
    }
    var isTextEmpty: Bool {
        return textValue.isEmpty
    }

    var hidden = Variable<Bool>(false)
    var separator = Variable<Bool>(false)
    init(_ type: EditingViewType, active: Bool = true) {
        self.type = type
        self.active = active
    }

    var asString: String? {
        guard !isTextEmpty else { return nil }
        var text = textValue
        if type == .checklist || type == .subtaskChecklist {
            text = text.components(separatedBy: Symbol.newLine.rawValue)
                .map({ " \(Symbol.bullet.rawValue) " + $0 })
                .reduce("", {
                    return $0.isEmpty ? $1 : $0 + Symbol.newLine.rawValue + $1
                })
        }
        return text
    }
}
