//
//  DotsView.swift
//  TOTHEM
//
//  Created by Sergei Perevoznikov on 14/06/2018.
//  Copyright © 2018 r00t. All rights reserved.
//

import UIKit
import RxSwift

class DotsView: StackSubview {
    @IBOutlet weak var dotsLabel: UILabel!
    @IBOutlet weak var leadingContentOffset: NSLayoutConstraint!
    @IBOutlet weak var trailingContentOffset: NSLayoutConstraint!
    @IBOutlet weak var height: NSLayoutConstraint!

    var dispose = DisposeBag()
    var viewModel: EditingTextViewModel!

    override func configureView() {
        super.configureView()
        leadingContentOffset.constant = 10
        trailingContentOffset.constant = 75
        dotsLabel.text = Symbol.dots
    }

    func set(model: EditingTextViewModel) {
        viewModel = model
        model.backgroundColor
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.backgroundColor = $0
            }).disposed(by: dispose)

        model.textColor
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.dotsLabel.textColor = $0
            }).disposed(by: dispose)

        model.font
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.dotsLabel.font = $0
                self?.dotsLabel.sizeToFit()
                self?.height.constant = Symbol.referenceSymbol.size(width: 99, font: $0).height
                self?.dotsLabel.superview?.setNeedsLayout()
            }).disposed(by: dispose)

        model.hidden
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.isHidden = $0
            }).disposed(by: dispose)
    }
}
