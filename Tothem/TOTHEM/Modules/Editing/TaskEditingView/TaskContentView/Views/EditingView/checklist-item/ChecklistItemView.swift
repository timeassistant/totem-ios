//
//  CheckListItemView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class CheckListItemView: EditingTextView {
    @IBOutlet weak var bullets: UIView!
    var bulletFont = UIFont.systemFont(ofSize: 14)
    var bulletColor = UIColor.black
    private var lastLineIsEmpty: Bool = true

    func hideChecklistMark() {
        bullets.isHidden = true
    }

    override func textWillChange(_ text: String) {
        super.textWillChange(text)
        let lines = text.components(separatedBy: Symbol.newLine.rawValue)
        while bullets.subviews.count != lines.count {
            if bullets.subviews.count < lines.count {
                let label = BulletView()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = bulletFont
                label.textColor = bulletColor
                label.text = Symbol.bullet.rawValue
                label.sizeToFit()
                bullets.addSubview(label)
                label.top = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: bullets, attribute: .top, multiplier: 1, constant: 0)
                label.leading = NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: bullets, attribute: .leading, multiplier: 1, constant: 0)
            } else if let last = bullets.subviews.last {
                last.removeFromSuperview()
            }
        }

        var top: CGFloat = 0
        for (index, text) in lines.enumerated() {
            guard let bullet = bullets.subviews[index] as? BulletView else { continue }
            bullet.top.constant = top
            top += size(of: text).height
            bullet.alpha = text.isEmpty ? 0.5 : 1
            bullet.layoutIfNeeded()
        }
    }

    override func set(style: TextViewStyle) {
        super.set(style: style)

        VisibilityRulesService.shared.visibility(for: .checkList)
            .startWith(true)
            .map { !$0 }
            .bind(to: self.view.rx.isHidden)
            .disposed(by: dispose)

        style.color
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] color in
                self?.bulletColor = color
                if let bullets = self?.bullets.subviews as? [BulletView] {
                    bullets.forEach({ $0.textColor = color })
                }
            }).disposed(by: dispose)

        style.font
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] font in
                self?.bulletFont = font
                if let bullets = self?.bullets.subviews as? [BulletView] {
                    bullets.forEach({ $0.font = font })
                }
            }).disposed(by: dispose)
    }
}
