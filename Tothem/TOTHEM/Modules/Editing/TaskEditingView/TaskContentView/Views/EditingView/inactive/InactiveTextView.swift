//
//  InactiveTextView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class InactiveTextView: XibSubview {
    @IBOutlet weak var line1Label: UILabel!
    @IBOutlet weak var line2Label: UILabel!
    @IBOutlet weak var dotsLabel: UILabel!
    @IBOutlet weak var dotsContainerView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lineViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lineViewLeading: NSLayoutConstraint!
    @IBOutlet weak var lineViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var leadingContentOffset: NSLayoutConstraint!
    @IBOutlet weak var trailingContentOffset: NSLayoutConstraint!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!

    var section: Int = 0
    var dispose = DisposeBag()

    override func configureView() {
        super.configureView()
        dotsLabel.text = Symbol.dots

        VisibilityRulesService.shared.visibility(for: .text)
            .startWith(true)
            .map { !$0 }
            .bind(to: self.stackView.rx.isHidden)
            .disposed(by: self.dispose)

        leadingContentOffset.constant = 10
        trailingContentOffset.constant = 75
    }

    class func contentHeight(model: [EditingTextViewModel], style: TextViewStyle) -> CGFloat {
        var height: CGFloat = 0
        let lines = model.text.components(separatedBy: Symbol.newLine.rawValue)
        if lines.count > 0 {
            height += lines[0].size(width: 999, font: style.font.value).height
        }

        if lines.count > 1 {
            height += 5 + lines[1].size(width: 999, font: style.font.value).height
        }

        if lines.count > 2 {
            height += 5 + lines[2].size(width: 999, font: style.font.value).height
        }

        return height
    }

    func set(style: TextViewStyle) {
        style.color
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.line1Label.textColor = $0
                self?.line2Label.textColor = $0
                self?.dotsLabel.textColor = $0
            })
            .disposed(by: dispose)

        style.font
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] font in
                UIView.animate(0) {
                    self?.line1Label.font = font
                    self?.line2Label.font = font
                    self?.dotsLabel.font = font
                }
            }).disposed(by: dispose)

        style.separator.color
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.lineView.backgroundColor = $0
            }).disposed(by: dispose)

        style.separator.height
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.lineViewHeight.constant = $0
            }).disposed(by: dispose)

        style.separator.offset
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.lineViewLeading.constant = $0
                self?.lineViewTrailing.constant = $0
            }).disposed(by: dispose)
    }

    func set(model: EditingTextViewModel) {
        model.text
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                let lines = $0.components(separatedBy: Symbol.newLine.rawValue)
                self?.line1Label.isHidden = lines.count < 1
                self?.line2Label.isHidden = lines.count < 2
                self?.dotsContainerView.isHidden = lines.count < 3

                for (index, line) in lines.enumerated() {
                    if index == 0 { self?.line1Label.text = line }
                    if index == 1 { self?.line2Label.text = line }
                }
            }).disposed(by: dispose)

        model.hidden
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.isHidden = $0
            }).disposed(by: dispose)

        model.separator
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.lineView.isHidden = !$0
            }).disposed(by: dispose)
    }
}
