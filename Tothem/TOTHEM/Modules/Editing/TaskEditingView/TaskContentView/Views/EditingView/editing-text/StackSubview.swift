//
//  XibSubview.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class XibSubview: UIView {

    @IBOutlet var view: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }

    func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        let bundle = Bundle(for: XibSubview.self)
        let xibName = String(describing: type(of: self))
        bundle.loadNibNamed(xibName, owner: self, options: nil)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.fill(in: self)
    }
}

extension UIView {
    func fill(in view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        frame = view.bounds
        view.addSubview(self)
        constraintCenter(y: view)
        constraintHeight(to: view)
        constraintCenter(x: view)
        constraintWidth(to: view)
    }

    @discardableResult
    func constraintWidth(to view: UIView? = nil, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintHeight(to view: UIView? = nil, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintCenter(x to: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: to, attribute: .centerX, multiplier: 1, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintCenter(y to: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: to, attribute: .centerY, multiplier: 1, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintToBottom(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintToTop(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintBottom(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintTop(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = view.topAnchor.constraint(equalTo: topAnchor, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintLeading(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: constant)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    func constraintTrailing(_ view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: constant)
        constraint.isActive = true
        return constraint
    }

    func constraintRemove(_ constraint: NSLayoutConstraint?) {
        guard let constraint = constraint else { return }
        constraint.isActive = false
        removeConstraint(constraint)
    }
}
