//
//  EditingTextView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum EditingViewEvent {
    case none
    case backspace
    case textWillChange(String)
    case nextResponder
    case didBeginEditing
    case didEndEditing
    case contentHeightDidChange
    case startChecklist
}

class EditingTextView: XibSubview {
    var minHeight: CGFloat = 20
    
    var formatter: TextViewFormatter = SingleLineFormatter()
    var events = PublishSubject<EditingViewEvent>()
    var contentHeight = BehaviorSubject<CGFloat>(value: 0)
    var contentHeightValue: CGFloat {
        return (try? contentHeight.value()) ?? minHeight
    }
    
    @IBOutlet weak var leadingContentOffset: NSLayoutConstraint!
    @IBOutlet weak var trailingContentOffset: NSLayoutConstraint!
    @IBOutlet weak var topContentOffset: NSLayoutConstraint!
    @IBOutlet weak var bottomContentOffset: NSLayoutConstraint!

    @IBOutlet weak var textView: UITextView!

    @IBOutlet weak var textViewHeight: NSLayoutConstraint!

    @IBOutlet weak var placeholderLabel: UILabel?
    @IBOutlet weak var activationButton: UIButton?

    private let observer = TextViewDelegate()
    var dispose = DisposeBag()
    var viewModel: EditingTextViewModel!

    deinit {
        print("deinit EditingTextView")
    }

    override func configureView() {
        super.configureView()
        leadingContentOffset.constant = 10
        trailingContentOffset.constant = 75

        textView.textColor = .clear
        placeholderLabel?.textColor = .clear

        textView.delegate = observer
        textView.contentInset = UIEdgeInsets.zero
        textView.textContainerInset = UIEdgeInsets.zero
        textView.textContainer.lineFragmentPadding = 0

        textView.rx.observe(CGPoint.self, "contentOffset")
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] newOffset in
                if newOffset != CGPoint.zero {
                    self?.textView.contentOffset = CGPoint.zero
                }
            })
            .disposed(by: dispose)

        observer.events
            .flatMap({ [unowned self] in
                return self.formatter.update(textView: self.textView, with: $0, previouslyReturned: false)
            })
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] action in
                switch action {
                case .backspace: self?.events.onNext(.backspace)
                case .nextResponder: self?.events.onNext(.nextResponder)
                case .didBeginEditing: self?.events.onNext(.didBeginEditing)
                case .didEndEditing:
                    self?.events.onNext(.didEndEditing)
                case .textWillChange(let text):
                    self?.viewModel.text.on(.next(text))
                    self?.events.onNext(.textWillChange(text))
                case .startChecklist: self?.events.onNext(.startChecklist)
                default: break
                }

            }).disposed(by: dispose)
        placeholderLabel?.text = "Abc"
    }

    func textWillChange(_ text: String) {
        placeholderLabel?.isHidden = !text.isEmpty
        updateHeight(text, with: textView.bounds.width)
    }

    //swiftlint:disable shorthand_operator
    func beginEditing(backspaced: Bool) {
        if !backspaced, formatter.startWithNewLine, let last = textView.text.components(separatedBy: Symbol.newLine.rawValue).last, last != "" {
            textView.text = textView.text + Symbol.newLine.rawValue
            textWillChange(textView.text)
        }
        textView.becomeFirstResponder()
    }

    func prepareForReuse() {
        events = PublishSubject<EditingViewEvent>()
        textView.text = ""
        placeholderLabel?.text = "Abc"
    }

    func size(of text: String) -> CGSize {
        return textView.size(text: text, width: textView.bounds.width)
    }

    private func updateHeight(_ text: String, with width: CGFloat) {
        let min = size(of: Symbol.referenceSymbol).height
        let height = max(size(of: text).height + topContentOffset.constant, min)
        if Int(height) != Int(textViewHeight.constant) {
            let newHeight = max(minHeight, height)
            textViewHeight.constant = newHeight
            textView.superview?.layoutIfNeeded()
            contentHeight.onNext(newHeight)
        }
    }

    func set(style: TextViewStyle) {

        style.color
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.textView.textColor = $0
                self?.placeholderLabel?.textColor = $0
                self?.tintColor = $0
            }).disposed(by: dispose)

        style.font
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                guard let sself = self else { return }
                sself.textView.font = $0
                sself.placeholderLabel?.font = $0
                sself.textWillChange(sself.textView.text)
            }).disposed(by: dispose)
    }

    func set(model: EditingTextViewModel) {
        viewModel = model
        textView.text = model.textValue

        model.text
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.textWillChange($0)
            }).disposed(by: dispose)

        model.hidden
            .asObservable()
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.isHidden = $0
            }).disposed(by: dispose)
    }

    @IBAction func activeTextView() {
        textView.becomeFirstResponder()
    }
}
