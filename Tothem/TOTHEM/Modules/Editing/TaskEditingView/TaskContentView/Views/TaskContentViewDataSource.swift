//
//  TaskContentViewDataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum EditingViewType: Int {
    case title
    case checklist
    case subtaskChecklist
    case text
    case subtaskText
    var formatter: TextViewFormatter {
        switch self {
        case .title:
            return SingleLineFormatter()
        case .text, .subtaskText, .checklist, .subtaskChecklist:
            return TextFormatter()
        }
    }
}

protocol TaskContentEditingDelegate: class {
    var activatedView: Variable<UIView> { get }
    func handle(_ view: EditingTextView, event: EditingViewEvent)
}

class TaskContentViewDataSource {
    private var style: TaskContentStyle
    private weak var delegate: TaskContentEditingDelegate?
    private weak var model: EditingViewModel?

    init(delegate: TaskContentEditingDelegate?, model: EditingViewModel?, style: TaskContentStyle) {
        self.delegate = delegate
        self.style = style
        self.model = model
    }

    func view(at indexPath: IndexPath) -> UIView {
        guard let viewModel = model?.viewModel(at: indexPath)  else { return UIView() }
        if !viewModel.active {
            let view = InactiveTextView()
            view.set(style: style.readOnlyStyle)
            return view
        }
        switch viewModel.type {
        case .title, .text, .subtaskText:
            let view = EditingTextView()
            setup(view: view, indexPath: indexPath)
            view.set(style: style.textStyle)
            return view
        case .checklist, .subtaskChecklist:
            let view = CheckListItemView()
            setup(view: view, indexPath: indexPath)
            view.set(style: style.checklistStyle)
            return view
        }
    }

    private func setup(view: EditingTextView, indexPath: IndexPath) {
        guard let model = model else { return }
        let type = model.viewModel(at: indexPath).type
        view.formatter = type.formatter
        view.events
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] event in
                self?.delegate?.handle(view, event: event)
            })
            .subscribe()
            .disposed(by: view.dispose)

        switch type {
        case .title:
            view.placeholderLabel?.text = "Title"
        case .text:
            view.placeholderLabel?.text = "Text"
        default: break
        }
    }

    func update(view: UIView, at indexPath: IndexPath) {
        guard let viewModel = model?.viewModel(at: indexPath) else { return }
        if let view = view as? EditingTextView {
            view.set(model: viewModel)
        } else if let view = view as? InactiveTextView {
            view.set(model: viewModel)
            if let delegate = delegate {
                view.activateButton
                    .rx.tap
                    .map({ _ in view })
                    .bind(to: delegate.activatedView)
                    .disposed(by: view.dispose)
            }
        }
    }

    func numberOfRows(in section: Int) -> Int {
        return model?.numberOfRows(in: section) ?? 0
    }
}
