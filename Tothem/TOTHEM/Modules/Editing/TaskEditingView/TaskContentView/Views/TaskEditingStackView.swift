//
//  TaskEditingStackView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class TaskEditingStackView: StackView {
    var dataSource: TaskContentViewDataSource! {
        didSet {
            setup(stackView: self)
        }
    }

    func insert(section: Int) {
        let stackView = TaskEditingStackView()
        setup(stackView: stackView)
        insert(stackView, at: section)

        for row in 0..<dataSource.numberOfRows(in: section) {
            insert(row: row, section: section)
        }
    }

    func edit(indexPath: IndexPath, backspaced: Bool = false) {
        edit(row: indexPath.row, section: indexPath.section, backspaced: backspaced)
    }

    func edit(row: Int, section: Int, backspaced: Bool = false) {
        guard let view = arrangedSubviews[section] as? TaskEditingStackView else { return }
        guard let editingView = view.arrangedSubviews[row] as? EditingTextView else { return }
        editingView.beginEditing(backspaced: backspaced)
    }

    func editFirstRow(in section: Int) {
        guard let view = arrangedSubviews[section] as? TaskEditingStackView else { return }
        guard let editingView = view.arrangedSubviews.first(where: { $0 is EditingTextView || $0 is CheckListItemView }) as? EditingTextView else { return }
        editingView.beginEditing(backspaced: false)
    }

    func editLastRow(in section: Int) {
        guard let view = arrangedSubviews[section] as? TaskEditingStackView else { return }
        guard let editingView = view.arrangedSubviews.reversed().first(where: { $0 is EditingTextView || $0 is CheckListItemView }) as? EditingTextView else { return }
        editingView.beginEditing(backspaced: false)
    }

    func insert(row: Int, section: Int) {
        let indexPath = IndexPath(row: row, section: section)
        guard let view = arrangedSubviews[section] as? TaskEditingStackView else { return }
        let editingView = dataSource.view(at: indexPath)
        dataSource.update(view: editingView, at: indexPath)
        view.insert(editingView, at: row)
        view.sizeToFit()
    }

    func insert(indexPath: IndexPath) {
        insert(row: indexPath.row, section: indexPath.section)
    }

    func delete(row: Int, section: Int) {
        guard let view = arrangedSubviews[section] as? TaskEditingStackView else { return }
        view.arrangedSubviews[row].removeFromSuperview()
    }

    func delete(indexPath: IndexPath) {
        delete(row: indexPath.row, section: indexPath.section)
    }

    func reload(section: Int) {
        guard let stack = arrangedSubviews[section] as? TaskEditingStackView else { return }
        for (row, view) in stack.arrangedSubviews.enumerated() {
            let indexPath = IndexPath(row: row, section: section)
            dataSource.update(view: view, at: indexPath)
        }
    }

    func reload(indexPath: IndexPath) {
        guard let stack = arrangedSubviews[indexPath.section] as? TaskEditingStackView else { return }
        dataSource.update(view: stack.arrangedSubviews[indexPath.row], at: indexPath)
    }

    func index(of view: UIView) -> IndexPath? {
        guard let superview = view.superview else { return nil }
        guard let section = arrangedSubviews.firstIndex(of: superview) else { return nil }
        guard let stack = arrangedSubviews[section] as? UIStackView else { return nil }
        guard let row = stack.arrangedSubviews.firstIndex(of: view) else { return nil }
        return IndexPath(row: row, section: section)
    }
}
