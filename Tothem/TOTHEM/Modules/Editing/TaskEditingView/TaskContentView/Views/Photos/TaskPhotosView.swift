//
//  TaskPhotosView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import Photos
import RxSwift

enum TaskPhotoEvent {
    case remove(EntityModel)
}

final class TaskPhotosView: UICollectionView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    private(set) var assets: [EntityModel] = []
    private let columns: CGFloat = 3
    var spacing: CGFloat = 5
    private var nibName = "TaskPhotosCell"
    var disposeBag = DisposeBag()
    var events = PublishSubject<TaskPhotoEvent>()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        register(cellNib, forCellWithReuseIdentifier: reuseId)
        delegate = self
        dataSource = self
    }

    var size: CGSize {
        let width = (bounds.width - spacing * (columns + 1)) / columns
        return CGSize(width: width, height: width)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame.size.height = size.height*1.5 //(bounds.width - spacing * (columns + 1)) / columns
    }

    var cellNib: UINib {
        return UINib(nibName: nibName, bundle: nil)
    }

    var reuseId: String { 
        return nibName
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath)
        if let cell = cell as? TaskPhotosCell {
            addPhoto(to: cell, at: indexPath)
            weak var weakCell = cell
            cell.didPressedDelete = { [weak self] in
                guard let self = self,
                    let asset = self.asset(for: weakCell) else { return }
                self.events.on(.next(.remove(asset)))
            }
        }
        return cell
    }
    
    private func asset(for cell: UICollectionViewCell?) -> EntityModel? {
        if let cell = cell,
            let indexPath = indexPath(for: cell),
            indexPath.row < assets.count {
            return assets[indexPath.row]
        }
        return nil
    }
    
    private func addPhoto(to cell: TaskPhotosCell, at indexPath: IndexPath) {
        let model = assets[indexPath.row]
        weak var cell = cell
        GetEntityPhotoUseCase.shared
            .execute(uid: nil, photoEntityValue: model.value)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] image in
                if let asset = self?.asset(for: cell),
                    model.value == asset.value {
                    cell?.imageView.image = image
                }
            })
            .disposed(by: disposeBag)
    }

    func set(photos: [EntityModel]) {
        assets = photos
        collectionViewLayout.invalidateLayout()
        reloadData()
    }

    func add(photo: EntityModel) {
        if assets.firstIndex(where: { photo.value == $0.value }) == nil {
            assets.append(photo)
            let indexPath = IndexPath(item: assets.count - 1, section: 0)
            performBatchUpdates({
                self.insertItems(at: [indexPath])
            }, completion: nil)
        }
    }

    func remove(photo: EntityModel) {
        if let index = assets.firstIndex(where: { photo.value == $0.value }) {
            assets.remove(at: index)
            performBatchUpdates({
                self.deleteItems(at: [IndexPath(item: index, section: 0)])
            }, completion: nil)
        }
    }

    func update(photo: EntityModel) {
        if let index = assets.firstIndex(where: { photo.value == $0.value }) {
            performBatchUpdates({
                self.reloadItems(at: [IndexPath(item: index, section: 0)])
            }, completion: nil)
        }
    }

    // swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.size
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
    }
}
