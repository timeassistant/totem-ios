//
//  SingleTextController.swift
//  TOTHEM
//
//  Created by Sergei Perevoznikov on 26.03.18.
//  Copyright © 2018 r00t. All rights reserved.
//

import UIKit
import RxSwift

class SingleTextController: UIViewController, XIBLoadable, FormationView, TaskEditingView {

    private lazy var presenter =  TaskFormationControllerPresenter(view: self)

    var task: TaskFormationModel
    var didCreatedTask: (() -> Void)?
    var didChangeTaskType: ((TaskFormationModel) -> Void)?
    var willDismissController: (() -> Void)?
    var changeTaskType: ((TaskFormationModel) -> Void)?

    private var itemFormatter: ItemFormatter!

    //rx dispose
    private var disposeBag = DisposeBag()

    //OUTLETS
    @IBOutlet var titlePlaceholderLabel: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var titleTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sendTaskButton: UIButton!
    @IBOutlet weak var contentContainerView: UIView!
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!

	var additionalHeight: CGFloat = 0
    private var titleMinimumHeight: CGFloat = 0
    private let titleViewObserver = TextViewDelegate()

    private var descriptionText = Variable<String>("")
    private var isEmpty: Observable<Bool>!

    required init(task: TaskFormationModel) {
        self.task = task
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.task = TaskFormationModel(type: .active)
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        isEmpty = descriptionText.asObservable().map { !$0.isEmpty }
        titleMinimumHeight = titleTextViewHeight.constant

        observeKeyboard()
        bind()

        descriptionText.value = TaskTransformation.description(task)
        itemFormatter = ItemFormatter(type: task.type)
        prefillData()
        setupUI(with: task.type)
    }

    private func prefillData() {
        titleTextView.text = descriptionText.value
        titlePlaceholderLabel.isHidden = !descriptionText.value.isEmpty
    }

    private func setupUI(with type: TimesetType) {
        setupUIElement(with: type)
        titlePlaceholderLabel.text = "Title text"
        titleTextView.becomeFirstResponder()
    }

    private func setupUIElement(with side: TimesetType) {
        self.contentContainerView.backgroundColor = ConfigurationFacade().taskModuleConstants.getTaskBackgroundColorValue(side)
        self.titleTextView.textColor = ConfigurationFacade().taskModuleConstants.getTaskTitleLabelColorValue(side)

        self.titlePlaceholderLabel.textColor = ConfigurationFacade().taskModuleConstants.getTaskTitleLabelColorValue(side)

        self.sendTaskButton.backgroundColor = ConfigurationFacade().taskModuleConstants.getTaskSendButtonColorValue(side)
        self.sendTaskButton.layer.borderColor =  ConfigurationFacade().taskModuleConstants.getTaskSendButtonBorderColorValue(side).cgColor
        self.sendTaskButton.layer.borderWidth =  ConfigurationFacade().taskModuleConstants.getTaskSendButtonBorderWidthValue(side)

        self.titleTextView.font = ConfigurationFacade().taskModuleConstants.getTaskTitleFontValue(side)

        let scaleUpRatio = ConfigurationFacade().taskModuleConstants.getTaskContentScaleValue(side)
        self.sendTaskButton.transform = CGAffineTransform(scaleX: scaleUpRatio, y: scaleUpRatio)

        self.additionalHeight = ConfigurationFacade().taskModuleConstants.getTaskAdditionalHeightValue(side)

        self.contentContainerView.layer.cornerRadius = ConfigurationFacade().taskModuleConstants.getTaskCornerValue(side)
        self.contentContainerView.layer.masksToBounds = true
    }

    private func bind() {
        self.observePlaceholderLabels()
        self.titleTextView.rx.text
            .orEmpty
            .bind(to: descriptionText)
            .disposed(by: disposeBag)

        isEmpty.bind { (value) in
            UIView.animate(withDuration: 0.3, animations: {
                self.sendTaskButton.alpha = value ? 1 : 0
                self.sendTaskButton.isUserInteractionEnabled = value
            })
            }.disposed(by: disposeBag)

        titleTextView.delegate = titleViewObserver

        self.titleTextView.rx.observe(CGSize.self, "contentSize")
            .subscribe(onNext: { _ in
                self.titleSizeChanged()
            })
            .disposed(by: disposeBag)
    }

    private func titleTextDidChange(_ text: String) {
        titlePlaceholderLabel.isHidden = !text.isEmpty
        var task = self.task
        TaskTransformation.update(&task, description: text)
        self.task = task
    }

    private func observePlaceholderLabels() {
        titleViewObserver.events
            .flatMap({ self.itemFormatter.title.update(textView: self.titleTextView, with: $0, previouslyReturned: false) })
            .subscribe(onNext: { action in
                switch action {
                case .returned, .nextResponder, .didBeginEditing:
                    break
                case .textDidChange(let text):
                    self.titleTextDidChange(text)
                }
            }).disposed(by: disposeBag)
    }

    private func titleSizeChanged() {
        UIView.animate(withDuration: 0.3, animations: {
            let contentHeight = max(self.titleTextView.textSize.height + 15, self.titleMinimumHeight)
            self.titleTextViewHeight.constant = min(contentHeight, self.view.frame.height / 2)
            self.contentContainerView.layoutIfNeeded()
        })
    }

    private func observeKeyboard() {
            KeyboardManager.keyboardHeight()
                .bind(to: self.bottomContainerConstraint.rx.constant.mapObserver {
				$0 + self.additionalHeight
                })
                .disposed(by: disposeBag)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !self.contentContainerView.frame.contains((touches.first?.location(in: self.view))!) {
            self.dismiss()
        }
    }

    private var saving = false
    private func save(task: TaskFormationModel) {
        if saving {
            return
        }

        sendTaskButton.isEnabled = false
        saving = true
        presenter.save(task: task) { [weak self] in
            self?.didCreatedTask?()
            self?.dismiss()
            self?.saving = false
        }
    }

    @IBAction func addTaskButton(_ sender: UIButton) {
		let pulse = CASpringAnimation(keyPath: "transform.scale")
		pulse.duration = 0.2
		pulse.fromValue = 1.0
		pulse.toValue = 1.4
		pulse.autoreverses = true
		pulse.initialVelocity = 0.5
		pulse.damping = 1.0
		sendTaskButton.layer.add(pulse, forKey: nil)

        save(task: task)
    }

    @IBAction func processChangeTaskType() {
        self.willDismissController?()
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: {
            self.changeTaskType?(self.task)
        })
    }

    func dismiss() {
        self.willDismissController?()
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}
