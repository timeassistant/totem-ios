//
//  Symbol.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

enum Symbol: String {
    case newLine = "\n"
    case bullet = "•"
    case hyphen = "-"
    static var separator = "——————————"
    static var dots = "..."
    static let referenceSymbol = "0"
    var character: Character {
        switch self {
        case .newLine: return self.rawValue.first ?? "\n"
        case .bullet: return self.rawValue.first ?? "•"
        case .hyphen: return self.rawValue.first ?? "-"
        }
    }
}
