//
//  TextViewFormatter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum TaskFormationAction {
    case willChange(String)
    case returned
    case startCheckList
    case didBeginEditing
    case didEndEditing
    case backspace
}

enum TextFormatterAction {
    case backspace
    case returned
    case textWillChange(String)
    case nextResponder
    case didBeginEditing
    case didEndEditing
    case startChecklist
}

protocol TextView: class {
    var value: String { get set }
    var bulletSymbol: String { get }
    var textSize: CGSize { get }
    func removeFromSelectedRange(_ suffix: String)
    func setSelectRangeBefore(_ text: String)
    func setSelectRangeAtTheEnd()
    func insert(_ text: String)
    func insertCheckListItem()
    var selectedRange: NSRange { get set }
    func insertNewLine()
    func removeEmptyCheckListItemBeforeSeletedRange() -> Bool
    func currentLine() -> String
    func insertNewLine(_ times: UInt)
    func removeEmptyLinesFromTheEnd() -> Bool
}

protocol TextViewFormatter {
    func update(textView: TextView, with event: TaskFormationAction, previouslyReturned: Bool) -> Observable<TextFormatterAction>
    var startWithNewLine: Bool { get }
}

extension TextView {
    func insertNewLine() {
        insertNewLine(1)
    }
}

extension String {
    func size(width: CGFloat, font: UIFont) -> CGSize {
        let rectSize = CGSize(width: width, height: CGFloat(Float.greatestFiniteMagnitude))
        return self.boundingRect(with: rectSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil).size
    }
}

extension UITextView: TextView {

    var value: String {
        get {
            return text
        }
        set {
            text = newValue
        }
    }

    var bulletSymbol: String {
        return " \(Symbol.bullet.rawValue) "
    }

    var textSize: CGSize {
        return size(text: self.text, width: contentSize.width)
    }

    func size(text: String, width: CGFloat) -> CGSize {
        return text.size(width: width, font: font!)
    }

    func removeFromSelectedRange(_ suffix: String) {
        var range = selectedRange
        range.location -= suffix.count
        range.length += suffix.count
        let newText = (text as NSString)
            .replacingCharacters(in: range, with: "")
        text = newText
    }

    func setSelectRangeBefore(_ text: String) {
        var range = (self.text as NSString).range(of: text)
        range.length = 0
        range.location -= 1
        selectedRange = range
    }

    func setSelectRangeAtTheEnd() {
        var range = (text as NSString).range(of: text)
        range.location = range.length
        range.length = 0
        selectedRange = range
    }

    func insert(_ text: String) {
        var range = selectedRange
        self.text = ((self.text ?? "") as NSString)
            .replacingCharacters(in: range, with: text)
        range.location += text.count
        selectedRange = range
        scrollRangeToVisible(range)
    }

    func insertCheckListItem() {
        insert(bulletSymbol)
    }

    func insertNewLine(_ times: UInt) {
        [0..<times].forEach { _ in self.insert(Symbol.newLine.rawValue) }
    }

    func removeEmptyCheckListItemBeforeSeletedRange() -> Bool {
        let suffix = "\(Symbol.newLine.rawValue)\(bulletSymbol)"
        let prefix = text.prefix(selectedRange.location)
        if prefix.suffix(suffix.count) == suffix {
            removeFromSelectedRange(suffix)
            return true
        }
        return false
    }

    func currentLine() -> String {
        let text = String((self.text ?? "")
                .prefix(selectedRange.location)
                .reversed())
        let lowerBound = (text as NSString)
            .range(of: Symbol.newLine.rawValue)
            .lowerBound
        return String(text.reversed().suffix(lowerBound))
    }

    func removeEmptyLinesFromTheEnd() -> Bool {
        var changed = false
        var lines = value.components(separatedBy: Symbol.newLine.rawValue)
        while let line = lines.last,
            !line.hasVisibleCharacters {
            lines.removeLast()
                changed = true
        }

        if changed {
            value = lines.reduce("", { result, line in
                return "\(result)\(result.isEmpty ? line : Symbol.newLine.rawValue + line)"
            })
        }

        return changed
    }
}

extension String {
    func firstLine() -> String {
        guard let index = self.firstIndex(where: { $0 == Symbol.newLine.character }) else { return self }
        return String(self[..<index])
    }
}
