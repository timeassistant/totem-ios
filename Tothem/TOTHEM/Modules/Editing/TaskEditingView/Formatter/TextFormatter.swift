//
//  TextFormatter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class TextFormatter: TextViewFormatter {
    var startWithNewLine = true
    func update(textView: TextView, with event: TaskFormationAction, previouslyReturned: Bool) -> Observable<TextFormatterAction> {
        switch event {
        case .returned:
            return returned(textView: textView)
        case .willChange(let newText):
            return Observable.just(.textWillChange(newText))
        case .didBeginEditing:
            return Observable.create { observer in
                observer.onNext(.didBeginEditing)
                if previouslyReturned {
                    textView.setSelectRangeAtTheEnd()
                }
                observer.onCompleted()
                return Disposables.create()
            }
        case .startCheckList:
            return Observable.just(.startChecklist)
        case .backspace:
            return Observable.just(.backspace)
        case .didEndEditing:
            return Observable.create({ observer in
                defer {
                    observer.onCompleted()
                }
                if textView.removeEmptyLinesFromTheEnd() {
                    observer.onNext(.textWillChange(textView.value))
                }
                observer.on(.next(.didEndEditing))
                return Disposables.create()
            })
        }
    }

    private func returned(textView: TextView) -> Observable<TextFormatterAction> {
        return Observable.create({ observer in
            defer {
                observer.onCompleted()
            }

            let suffix = Symbol.newLine.rawValue
            let text = textView.value.prefix(textView.selectedRange.location)
            if text.suffix(suffix.count) == suffix {
                if textView.selectedRange.location == textView.value.count {
                    observer.onNext(.returned)
                    observer.onNext(.nextResponder)
                } else {
                    let end = String(textView.value.suffix(textView.value.count - textView.selectedRange.location - 1)) as NSString

                    textView.removeFromSelectedRange(suffix)
                    observer.onNext(.textWillChange(textView.value))
                    observer.onNext(.returned)

                    let range = end.range(of: Symbol.newLine.rawValue)
                    if range.location != NSNotFound {
                        textView.selectedRange.location = textView.value.count - end.length + range.location
                    }
                }
            } else {
                if textView.value == "" {
                    observer.onNext(.returned)
                    observer.onNext(.nextResponder)
                } else {
                    let text = textView.value.suffix(textView.value.count - textView.selectedRange.location)
                    if text.prefix((suffix + suffix).count) == suffix + suffix ||
                        text == suffix {
                        textView.selectedRange.location += 1
                    } else {
                        textView.insertNewLine()
                        observer.onNext(.textWillChange(textView.value))
                    }
                }
            }

            return Disposables.create()
        })
    }
}
