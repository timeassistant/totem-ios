//
//  TextViewDelegate.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

extension Symbol {
    var formattionAction: TaskFormationAction {
        switch self {
        case .newLine: return .returned
        case .bullet: return .startCheckList
        case .hyphen: return .startCheckList
        }
    }
}

class TextViewDelegate: NSObject, UITextViewDelegate {
    let events = PublishSubject<TaskFormationAction>()

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if text.isEmpty, range.location == 0, range.length == 0 {
            events.on(.next(.backspace))
            return false
        }

        if let symbol = Symbol(rawValue: text) {
            events.on(.next(symbol.formattionAction))
            return false
        }

        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        events.on(.next(.willChange(newText)))
        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        events.on(.next(.didBeginEditing))
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        events.on(.next(.didEndEditing))
    }
}
