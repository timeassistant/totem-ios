//
//  TextFieldDelegate.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class TextFieldDelegate: NSObject, UITextFieldDelegate {
    let events = PublishSubject<TaskFormationAction>()

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let symbol = Symbol(rawValue: string) {
            events.on(.next(symbol.formattionAction))
            return false
        }

        let newText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        events.on(.next(.willChange(newText)))
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        events.on(.next(.didBeginEditing))
    }
}
