//
//  TaskEditingPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import Photos

enum TaskEditingEvent: Event {
    case save(TaskEditingState)
    var key: String {
        switch self {
        case .save(let state):
            switch state {
            case .checklist: return "sr"
            case .description: return "sc"
            case .photos: return "sl"
            }
        }
    }
    
    var classify: Bool {
        return true
    }
}

enum TaskEditingViewState {
    case loading
    case loaded
    case addToContent(EntityModel)
    case removeFromContent(EntityModel)
    case updateContent(EntityModel)
    case saved(TaskModel)
}

enum TaskEditingViewEvent {
    case loaded
    case removeFromGallery(EntityModel)
    case save(TaskModel, TaskEditingState)
}

enum TaskEditingUseCaseResult {
    case loaded
    case photoAdded(EntityModel)
    case photoRemoved(EntityModel)
    case photoUpdated(EntityModel)
    case saved(TaskModel)
}

final class TaskEditingPresenter {
    var state: Observable<TaskEditingViewState>
    var events = PublishSubject<TaskEditingViewEvent>()

    init() {
        let result = events.flatMap { event -> Observable<TaskEditingUseCaseResult> in
            switch event {
            case .loaded:
                return TaskSelectedPhotosUseCase.shared.execute()
            case .save(let model, let state):
                EventsDataManager.shared.add(TaskEditingEvent.save(state))
                return TaskEditingPresenter.saveUseCase(model)
            case .removeFromGallery(let model):
                return TaskSelectedPhotosUseCase.shared.remove(model)
            }
        }

        state = result.scan(TaskEditingViewState.loading) { _, result in
            switch result {
            case .photoRemoved(let model):
                return .removeFromContent(model)
            case .photoAdded(let model):
                return .addToContent(model)
            case .photoUpdated(let model):
                return .updateContent(model)
            case .saved(let model):
                return .saved(model)
            case .loaded:
                return .loaded
            }
        }
    }
}
