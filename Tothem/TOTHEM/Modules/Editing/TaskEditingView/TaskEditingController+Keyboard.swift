//
//  TaskEditingController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//swiftlint:disable all

import UIKit
import RxSwift
import Photos

extension TaskEditingController {
    func observeKeyboard() {
        KeyboardManager.keyboardNotification
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.updateOffset(keyboardNotification: $0)
            })
            .disposed(by: dispose)
    }

    private func updateOffset(keyboardNotification notification: Notification) {
        guard !closing else { return }
        switch notification.name {
        case UIResponder.keyboardWillShowNotification:
            animateKeyboard(notification, to: (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height)
        case UIResponder.keyboardWillHideNotification:
            animateKeyboard(notification, to: nil)
        default:
            break
        }
    }

    private func animateKeyboard(_ notification: Notification, to offset: CGFloat?) {

        var contentViewBottomOffset = self.contentViewBottom.constant
        if let height = offset {
            contentViewBottomOffset = height + self.additionalHeight
        } else {
            contentViewBottomOffset = galleryCollectionView.twoRowsHeight
        }

        if contentViewBottomOffset == self.contentViewBottom.constant {
            return
        }

        guard let userInfo = notification.userInfo else {
            self.contentViewBottom.constant = contentViewBottomOffset
            return
        }

        let defaultCurve = UIView.AnimationOptions.curveEaseOut
        let duration: Double = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0.2
        UIView.animate(withDuration: duration, delay: 0, options: defaultCurve, animations: {
            self.contentViewBottom.constant = contentViewBottomOffset
            self.setPhotoCollectionViewIntialState(bottom: false)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
