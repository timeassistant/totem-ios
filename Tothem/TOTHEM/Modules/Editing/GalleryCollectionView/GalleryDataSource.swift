//
//  GalleryDataSource.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import Photos
import RxSwift

enum GalleryAction {
    case add(PHAsset)
    case remove(PHAsset)
    case fullScreen(CGRect?, Bool)
}

final class GalleryDataSource: NSObject, UICollectionViewDataSource {
    private var disposeBag = DisposeBag()
    
    var fetchResult: PHFetchResult<PHAsset>!
    var repository: PhotoAssetsRepository!
    weak var viewController: UIViewController?
    var selected: [PHAsset] = []
    var actions = PublishSubject<GalleryAction>()
    var size: CGSize?

    var targetFrame: CGRect {
        var targetFrame: CGRect = viewController?.view.bounds ?? .zero
        targetFrame.size.height -= 100
        targetFrame.origin.y = 100
        return targetFrame
    }

    func isVertical(_ collectionView: UICollectionView) -> Bool {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            return layout.scrollDirection == .vertical
        }
        return true
    }

    func canTakePicture(_ collectionView: UICollectionView) -> Bool {
        return isVertical(collectionView)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (fetchResult?.count ?? 0) + (canTakePicture(collectionView) ? 1 : 0)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            return assetCell(collectionView, at: indexPath)
        } else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "AssetCell", for: indexPath)
        }
    }
    
    private func assetCell(_ collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetCell", for: indexPath)
        if let cell = cell as? GalleryAssetCell {
            cell.pan.isEnabled = false
            cell.imageView.contentMode = isVertical(collectionView) ? .scaleAspectFill : .scaleAspectFit
            if canTakePicture(collectionView) {
                if indexPath.item == 0 {
                    cell.populate(UIImage(named: "addPhotoChat")) { }
                } else {
                    setPicture(to: cell, asset: fetchResult.object(at: indexPath.item - 1))
                }
            } else {
                setPicture(to: cell, asset: fetchResult.object(at: indexPath.item))
            }

            if let collectionView = collectionView as? GalleryPanableCollectionView {
                collectionView.configurate(cell: cell)
            }
        }
        return cell
    }
    
    private func setPicture(to cell: GalleryAssetCell, asset: PHAsset) {
        let size = self.size ?? targetFrame.size
        repository.image(from: asset, maxSize: Int(max(size.width, size.height)))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] image in
                self?.populate(to: cell, asset: asset, image: image)
            })
            .disposed(by: disposeBag)
    }
    
    private func populate(to cell: GalleryAssetCell, image: UIImage?) {
        cell.populate(image) {
            
        }
    }
    
    private func populate(to cell: GalleryAssetCell, asset: PHAsset, image: UIImage?) {
        cell.populate(image, selected: selected.firstIndex(of: asset) != nil) { [weak self] in
            guard let sself = self else { return }
            if sself.selected.contains(asset) {
                sself.actions.on(.next(.remove(asset)))
            } else {
                sself.actions.on(.next(.add(asset)))
            }
        }
    }
    
    func indexPath(of asset: PHAsset, collectionView: UICollectionView) -> IndexPath {
        let index = fetchResult.index(of: asset) + (canTakePicture(collectionView) ? 1 : 0)
        return IndexPath(item: index, section: 0)
    }
}
