//
//  GalleryPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import Photos

enum GalleryViewEvent {
    case loaded
    case takePhoto
    case photoPicked(UIImage)
    case selected(PHAsset)
    case unselected(PHAsset)
    case subscribeSelections
}

enum GalleryViewState {
    case loading
    case loaded
    case results(PHFetchResult<PHAsset>)
    case pickNewPhoto
    case newAssetAdded(PHAsset)
    case selected(PHAsset)
    case unselected(PHAsset)
}

enum GalleryUseCaseResult {
    case loaded
    case loading
    case results(PHFetchResult<PHAsset>)
    case cameraAuthorized
    case assetSaved(PHAsset)
    case selected(PHAsset)
    case unselected(PHAsset)
}

final class GalleryPresenter {
    var state: Observable<GalleryViewState>
    var events = PublishSubject<GalleryViewEvent>()
    
    init() {
        let result = events.flatMap { event -> Observable<GalleryUseCaseResult> in
            switch event {
            case .loaded:
                return GalleryAssetsUseCase.shared.execute()
            case .subscribeSelections:
                return GallerySelectedAssetsUseCase.shared.execute()
                    .map({
                        switch $0 {
                        case .add(let asset):
                            return .selected(asset)
                        case .remove(let asset):
                            return .unselected(asset)
                        }
                    })
            case .takePhoto:
                return GalleryTakePhotoUseCase.shared.execute()
            case .photoPicked(let image):
                return GalleryAssetsUseCase.shared.save(image)
            case .selected(let asset):
                return GallerySelectedAssetsUseCase.shared
                    .add(asset)
                    .map({ _ in .loading })
            case .unselected(let asset):
                return GallerySelectedAssetsUseCase.shared
                    .remove(asset)
                    .map({ _ in .loading })
            }
        }
        
        state = result.scan(GalleryViewState.loading) { _, res in
            switch res {
            case .loading: return .loading
            case .loaded: return .loaded
            case .results(let res): return .results(res)
            case .cameraAuthorized: return .pickNewPhoto
            case .assetSaved(let asset): return .newAssetAdded(asset)
            case .selected(let asset): return .selected(asset)
            case .unselected(let asset): return .unselected(asset)
            }
        }
    }
}
