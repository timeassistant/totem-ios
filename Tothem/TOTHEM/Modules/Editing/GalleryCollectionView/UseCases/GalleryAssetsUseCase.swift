//
//  GetGalleryAssetsUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Photos
import RxSwift

final class GalleryAssetsUseCase {
    static let shared = GalleryAssetsUseCase()
    private var repository = PhotoAssetsRepository()
    private var assetsCollection: PHAssetCollection?
    
    func execute() -> Observable<GalleryUseCaseResult> {
        if let collection = assetsCollection {
            return .just(.results(newResults(collection)))
        }
        
        return Observable.create({ observer in
            observer.onNext(.loading)
            self.repository.authorize(photoAccess: { authorized in
                if authorized {
                    self.assetsCollection = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .smartAlbumUserLibrary, options: nil).firstObject
                    if let collection = self.assetsCollection {
                        observer.onNext(.results(self.newResults(collection)))
                    }
                } else {
                    let error = PhotoAssetsRepositoryError.accessDenied
                    observer.onError(error)
                }
                observer.onCompleted()
            })
            return Disposables.create()
        })
    }
    
    func save(_ image: UIImage) -> Observable<GalleryUseCaseResult> {
        guard let collection = assetsCollection else {
            return .error(PhotoAssetsRepositoryError.accessDenied)
        }
        
        return Observable.create({ observer in
            observer.onNext(.loading)
            self.repository.save(image: image, to: collection) { asset, error in
                if let asset = asset {
                    observer.onNext(.assetSaved(asset))
                } else {
                    let error = PhotoAssetsRepositoryError.assetNotFound
                    observer.onError(error)
                }
                observer.onCompleted()
            }
           
            return Disposables.create()
        })
    }
    
    private func newResults(_ collection: PHAssetCollection) -> PHFetchResult<PHAsset> {
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        return PHAsset.fetchAssets(in: collection, options: options)
    }
}
