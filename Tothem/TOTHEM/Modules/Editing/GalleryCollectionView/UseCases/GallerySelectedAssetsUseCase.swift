//
//  GallerySelectedAssetsUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift
import Photos

enum GallerySelectionAction {
    case add(PHAsset)
    case remove(PHAsset)
}

final class GallerySelectedAssetsUseCase {
    static let shared = GallerySelectedAssetsUseCase()
    var assets: [PHAsset] = []
    var events = PublishSubject<GallerySelectionAction>()

    func execute() -> Observable<GallerySelectionAction> {
        assets = []
        return events.asObserver()
    }

    func add(_ asset: PHAsset) -> Observable<Void> {
        return Observable.create({ observer in
            if !self.assets.contains(asset) {
                self.assets.append(asset)
                self.events.onNext(.add(asset))
            }
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        })
    }

    func remove(_ asset: PHAsset) -> Observable<Void> {
        return Observable.create({ observer in
            if let index = self.assets.firstIndex(of: asset) {
                self.assets.remove(at: index)
            }
            self.events.onNext(.remove(asset))
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        })
    }
}
