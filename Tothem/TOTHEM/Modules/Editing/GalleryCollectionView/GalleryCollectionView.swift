//
//  GalleryCollectionView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Photos
import RxSwift

final class GalleryCollectionView: UIView, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    private var disposeBag = DisposeBag()
    private var collectionView: UICollectionView!

    private var repository = PhotoAssetsRepository()
    private var inset: CGFloat = 5
    private var size: CGFloat = 50
    var dataSource = GalleryDataSource()
    var presenter = GalleryPresenter()

    deinit {
        print("deinit GalleryCollectionView")
    }
    
    func set(width: CGFloat) {
        let newSize = max(50, floor((width - (columns + 1) * inset) / columns))
        self.photosViewHeight.constant = fullHeight
        if newSize != size {
            size = newSize
            let scaledSize = UIScreen.main.scale * size
            dataSource.size = CGSize(width: scaledSize, height: scaledSize)
            collectionView.reloadData()
            collectionView.collectionViewLayout.invalidateLayout()
        }
    }

    var fullHeight: CGFloat {
        return (self.viewController?.view.frame.maxY ?? 0) - (self.viewController?.contentView?.frame.height ?? 0)
    }

    var twoRowsHeight: CGFloat {
        return 2 * size + 3 * inset
    }

    private let columns: CGFloat = 3
    @IBOutlet weak var photosViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewController: TaskEditingController? {
        didSet {
            dataSource.viewController = viewController
            collectionView.dataSource = dataSource
            presenter.events.onNext(.loaded)
            presenter.events.onNext(.subscribeSelections)
        }
    }

    var scrollingEnabled: Bool = true {
        didSet {
            collectionView.isScrollEnabled = scrollingEnabled
            collectionView.contentOffset.y = inset * -1
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        UIView.animate(CATransaction.animationDuration()) {
            self.collectionView.layoutSubviews()
        }
    }

    // swiftlint:disable all
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: size, height: size)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return inset
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -5 {
            UIView.animate(CATransaction.animationDuration()) { 
                self.viewController?.setPhotoCollectionViewIntialState()
                self.viewController?.view.layoutIfNeeded()
            }
        } 
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return inset
    }
    // swiftlint:enable all
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            presenter.events.onNext(.takePhoto)
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? GalleryAssetCell,
                let window = UIApplication.shared.windows.first,
                let image = cell.imageView.image {
                let frame = cell.convert(cell.bounds, to: window)
                let fullScreenPhotos = FullScreenPhotosViewController()
                fullScreenPhotos.index = indexPath.item - 1
                fullScreenPhotos.initialFrame = frame
                fullScreenPhotos.image = image
                fullScreenPhotos.dataSource = dataSource

                viewController?.display(photos: fullScreenPhotos)
                dataSource.actions.on(.next(.fullScreen(dataSource.targetFrame, true)))
            }
        }
    }

    func configureView() {
        let layout = UICollectionViewFlowLayout()

        collectionView = UICollectionView(frame: bounds,
                                          collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)

        collectionView.contentInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)

        if let view = collectionView.superview {
            collectionView.fill(in: view)
        }

        collectionView.register(GalleryAssetCell.self, forCellWithReuseIdentifier: "AssetCell")

        dataSource.repository = repository
        collectionView.delegate = self

        self.presenter.state
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] in
                self?.set(state: $0)
            })
            .subscribe()
            .disposed(by: disposeBag)

        self.dataSource.actions
            .map({ action -> GalleryViewEvent? in
                switch action {
                case .add(let asset): return .selected(asset)
                case .remove(let asset): return .unselected(asset)
                default: return nil
                }
            })
            .skipNil()
            .bind(to: presenter.events)
            .disposed(by: disposeBag)
    }
    
    private func set(state: GalleryViewState) {
        switch state {
        case .loaded: break
        case .loading: break
        case .results(let results):
            dataSource.fetchResult = results
            collectionView.reloadData()
        case .pickNewPhoto:
            takePicture()
        case .newAssetAdded(let asset):
            dataSource.actions.onNext(.add(asset))
        case .selected(let asset):
            select(asset)
        case .unselected(let asset):
            deselect(asset)
        }
    }
    
    func reloadData() {
        collectionView.reloadData()
    }
    
    func deselect(_ asset: PHAsset) {
        if let index = dataSource.selected.firstIndex(of: asset) {
            dataSource.selected.remove(at: index)
            let indexPath = dataSource.indexPath(of: asset, collectionView: collectionView)
            collectionView.performBatchUpdates({
                self.collectionView.reloadItems(at: [indexPath])
            }, completion: nil)
        }
    }

    func select(_ asset: PHAsset) {
        if !dataSource.selected.contains(asset) {
            dataSource.selected.append(asset)
            let indexPath = dataSource.indexPath(of: asset, collectionView: collectionView)
            collectionView.performBatchUpdates({
                self.collectionView.reloadItems(at: [indexPath])
            }, completion: nil)
        }
    }
    
    func takePicture() {
        guard let viewController = viewController else { return }
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        viewController.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.originalImage] as? UIImage {
            presenter.events.onNext(.photoPicked(image))
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
