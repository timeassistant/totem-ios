//
//  GalleryAssetCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class GalleryAssetCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    var imageView = UIImageView()
    var checkButton = UIButton(type: .custom)
    private var selectionAction: (() -> Void)?
    private let checkSize: CGFloat = 22
    var pan: UIPanGestureRecognizer!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }

    private func configureView() {
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 4
        addSubview(imageView)

        checkButton.setImage(UIImage(named: "checkOff"), for: .normal)
        checkButton.setImage(UIImage(named: "checkOn"), for: .selected)
        checkButton.translatesAutoresizingMaskIntoConstraints = false
        checkButton.addTarget(self, action: #selector(GalleryAssetCell.selectAsset), for: .touchUpInside)
        checkButton.backgroundColor = .white
        checkButton.layer.masksToBounds = true
        checkButton.layer.cornerRadius = 4
        addSubview(checkButton)

        pan = UIPanGestureRecognizer(target: nil, action: nil)
        pan.delegate = self
        addGestureRecognizer(pan)

        clipsToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = bounds
        checkButton.frame = CGRect(x: bounds.width - checkSize - 5, y: 5, width: checkSize, height: checkSize)
    }

    func populate(_ image: UIImage?, selected: Bool? = nil, action: @escaping () -> Void) {
        guard imageView.image == nil else { return }
        imageView.image = image
        checkButton.isHidden = selected == nil
        checkButton.isSelected = selected ?? false
        selectionAction = action
    }

    @objc func selectAsset() {
        selectionAction?()
    }

    // swiftlint:disable all
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    // swiftlint:enable false
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return abs((pan.velocity(in: pan.view)).y) > abs((pan.velocity(in: pan.view)).x)
    }
}
