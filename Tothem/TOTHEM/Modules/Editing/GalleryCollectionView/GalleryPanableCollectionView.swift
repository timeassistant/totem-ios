//
//  GalleryPanableCollectionView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum PhotoPanAction {
    case changed
    case ended(CGFloat)
}

final class GalleryPanableCollectionView: UICollectionView {
    var actions = PublishSubject<PhotoPanAction>()
    var targetFrame: CGRect = .zero

    func configurate(cell: GalleryAssetCell) {
        cell.pan.isEnabled = true
        cell.pan.removeTarget(nil, action: nil)
        cell.pan.addTarget(self, action: #selector(panHandler(gesture:)))
    }

    var beginPoint = CGPoint.zero
    @objc func panHandler(gesture: UIPanGestureRecognizer) {
        guard let superview = superview else { return }
        switch gesture.state {
        case .began:
            beginPoint = gesture.location(in: superview)
        case .changed:
            let diff = gesture.location(in: superview).y - beginPoint.y
            let centerY = targetFrame.midY + diff
            center = CGPoint(x: targetFrame.midX, y: centerY)
            actions.on(.next(.changed))
        case .ended:
            let diff = gesture.location(in: superview).y - beginPoint.y
            beginPoint = CGPoint.zero
            actions.on(.next(.ended(diff)))
        default: break
        }
    }
}
