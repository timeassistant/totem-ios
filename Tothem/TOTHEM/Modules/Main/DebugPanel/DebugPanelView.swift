//
//  DebugPanelView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class DebugPanelView: XibSubview {
    private let viewModel = DebugPanelViewModel()
    private let disposeBag = DisposeBag()
    @IBOutlet weak var speedValueSlider: UISlider!

    override func configureView() {
        super.configureView()
        configureSpeedData()
    }

    private func configureSpeedData() {
        Observable.combineLatest(
            viewModel.speedMinValue.map { Float($0) },
            viewModel.speedMaxValue.map { Float($0) }
            )
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] data in
                self?.speedValueSlider.minimumValue = data.0
                self?.speedValueSlider.maximumValue = data.1
            })
            .disposed(by: disposeBag)

        speedValueSlider.rx.value
            .skip(1)
            .map { Double($0) }
            .subscribe(onNext: viewModel.setSpeedValue)
            .disposed(by: disposeBag)
    }
}
