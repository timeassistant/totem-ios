//
//  FormulasViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

class FormulasViewController: UIViewController {
    private var bag = DisposeBag()

    var viewModel = FormulasViewModel()

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(FormulaShortTableViewCell.nib, forCellReuseIdentifier: FormulaShortTableViewCell.identifier)

        cancelButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: bag)

        viewModel.formulas.asObservable()
            .bind(to: tableView
                .rx
                .items(cellIdentifier: FormulaShortTableViewCell.identifier,
                       cellType: FormulaShortTableViewCell.self)) { _, formulaKey, cell in
                    cell.key.onNext(formulaKey)
            }
            .disposed(by: bag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let formulaKey = self?.viewModel.formulas.value[indexPath.row] else {
                    return
                }
                self?.tableView.deselectRow(at: indexPath, animated: true)
                self?.viewModel.select.onNext(formulaKey)
            })
            .disposed(by: bag)
    }

}
