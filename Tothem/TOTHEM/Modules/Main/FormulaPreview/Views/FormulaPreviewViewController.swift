//
//  FormulaPreviewViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class FormulaPreviewViewController: UIViewController {

    var disposeBag = DisposeBag()

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!

    var viewModel: FormulaPreviewViewModel

    init(forFormulaKey key: ConfigurationUnitKey) {
        self.viewModel = FormulaPreviewViewModel(forFormulaKey: key, bag: disposeBag)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.viewModel = FormulaPreviewViewModel(forFormulaKey: .circleSize, bag: disposeBag)
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(ShortInfoTableViewCell.nib, forCellReuseIdentifier: ShortInfoTableViewCell.identifier)

        collectionView.register(FormulaPreviewCollectionViewCell.nib, forCellWithReuseIdentifier: FormulaPreviewCollectionViewCell.identifier)

        cancelButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        viewModel.configuratorValues.asObservable()
            .bind(to: collectionView
                .rx
                .items(cellIdentifier: FormulaPreviewCollectionViewCell.identifier,
                       cellType: FormulaPreviewCollectionViewCell.self)) { _, previewData, cell in
                        cell.updateView(forPreviewData: previewData)
                }
            .disposed(by: disposeBag)
        Observable.combineLatest(viewModel.evaluationDetails.asObservable(), DataSourceManager.shared.dataSources)
            .map { previewData, dataSourcesInfo in
                return previewData.map { previewRow -> (row: ShortTableCellInfo, dataRow: DataSourceInfo?) in
                    if let parameterName = ConnectorParameter(rawValue: previewRow.parameterName),
                        let dataSourceRow = dataSourcesInfo.first(where: { $0.name == parameterName }) {
                        return (row: previewRow, dataRow: dataSourceRow)
                    }
                    return (row: previewRow, dataRow: nil)
                }
            }
            .bind(to: tableView
                .rx
                .items(cellIdentifier: ShortInfoTableViewCell.identifier,
                       cellType: ShortInfoTableViewCell.self)) { _, data, cell in
                        guard let dataRow = data.dataRow else { return }
                        cell.setData(data.row, sourceInfo: dataRow)
                }
            .disposed(by: disposeBag)
    }

}
