//
//  EqualizerView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable
class EqualizerView: UIView {

    private enum State {
        case normal
        case critical
        var color: UIColor {
            switch self {
            case .normal: return UIColor.green
            case .critical: return UIColor.red
            }
        }
    }

    private let disposeBag = DisposeBag()

    private let animationDuration: TimeInterval = 1.0 / 10.0

    private let trackHeightToParentHeightRatio: CGFloat = 1.0 / 5.0
    private let pointerWidthToParentWidthRatio: CGFloat = 1.0 / 50.0
    private let pointerHeightToParentHeightRatio: CGFloat = 1.0 / 2.0

    private let labelHeight: CGFloat = 17.0

    private var trackHeight: CGFloat {
        return self.bounds.height * trackHeightToParentHeightRatio
    }
    private var pointerWidth: CGFloat {
        return self.bounds.width * pointerWidthToParentWidthRatio
    }
    private var pointerHeight: CGFloat {
        return self.bounds.height * pointerHeightToParentHeightRatio
    }

    private var track: UIView!
    private var pointer: UIView!
    private var leftEdgeLabel: UILabel!
    private var rightEdgeLabel: UILabel!

    private var trackFrame: CGRect {
        let trackFrameX = pointerWidth / 2.0
        let trackFrameY = (self.bounds.height / 4.0) - (trackHeight / 2.0)
        let trackWidth = self.bounds.width - pointerWidth
        return CGRect(x: trackFrameX, y: trackFrameY, width: trackWidth, height: trackHeight)
    }
    private var pointerFrame: CGRect {
        let pointerFrameX = trackFrame.minX - (pointerWidth / 2) + pointerOffset
        let pointerFrameY = (self.bounds.height / 4.0) - (pointerHeight / 2.0)
        return CGRect(x: pointerFrameX, y: pointerFrameY, width: pointerWidth, height: pointerHeight)
    }
    private var leftEdgeLabelFrame: CGRect {
        let labelY = self.bounds.height - self.labelHeight
        let labelWidth = self.bounds.width / 2.0
        let labelHeight: CGFloat = self.labelHeight
        return CGRect(x: 0, y: labelY, width: labelWidth, height: labelHeight)
    }
    private var rightEdgeLabelFrame: CGRect {
        let labelX = self.bounds.width / 2.0
        let labelY = self.bounds.height - self.labelHeight
        let labelWidth = self.bounds.width / 2.0
        let labelHeight: CGFloat = self.labelHeight
        return CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
    }

    private var pointerOffset: CGFloat {
        let absolutePositionOnTrack = (value.value - minValue) / (maxValue - minValue)
        return trackFrame.width * CGFloat(absolutePositionOnTrack)
    }

    var minValueVariable: Variable<Double> = Variable(0.0)
    var maxValueVariable: Variable<Double> = Variable(1.0)
    private var minValue: Double {
        return minValueVariable.value
    }
    private var maxValue: Double {
        return maxValueVariable.value
    }
    private func getInscribed(value: Double) -> Double {
        if value < self.minValue { return self.minValue }
        if value > self.maxValue { return self.maxValue }
        return value
    }

    var value: Variable<Double> = Variable(0)

    convenience init(minValue: Double, maxValue: Double, frame: CGRect) {
        self.init(frame: frame)

        self.minValueVariable.value = minValue
        self.maxValueVariable.value = maxValue
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
        setupObservers()
        bindVariables()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupSubviews()
        setupObservers()
        bindVariables()
    }

    private func setupSubviews() {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        self.track = trackView
        self.addSubview(track)
        self.pointer = pointerView
        self.addSubview(pointer)
        self.leftEdgeLabel = leftEdgeView
        self.addSubview(leftEdgeLabel)
        self.rightEdgeLabel = rightEdgeView
        self.addSubview(rightEdgeLabel)
    }

    private func setupObservers() {
        let sharedValue = Observable.combineLatest(value.asObservable(), minValueVariable.asObservable(), maxValueVariable.asObservable())
            .map { $0.0 }
            .share()
        sharedValue
            .map { [weak self] in self?.getInscribed(value: $0) }
            .skipNil()
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.updatePointerView()
            })
            .disposed(by: disposeBag)
        sharedValue.asObservable()
            .map { [weak self] in self?.getState(forValue: $0) }
            .skipNil()
            .subscribe(onNext: { [weak self] state in
                guard let strongSelf = self else { return }
                strongSelf.updateTrack(toState: state)
            })
            .disposed(by: disposeBag)
    }

    private func bindVariables() {
        minValueVariable.asObservable()
            .map { String($0.rounded()) }
            .bind(to: leftEdgeLabel.rx.text)
            .disposed(by: disposeBag)
        maxValueVariable.asObservable()
            .map { String($0.rounded()) }
            .bind(to: rightEdgeLabel.rx.text)
            .disposed(by: disposeBag)
    }

    private var trackView: UIView {
        let view = UIView(frame: trackFrame)
        view.backgroundColor = getState(forValue: value.value).color
        return view
    }

    private var pointerView: UIView {
        let view = UIView(frame: pointerFrame)
        view.backgroundColor = UIColor.darkGray
        return view
    }

    private var leftEdgeView: UILabel {
        let label = UILabel(frame: leftEdgeLabelFrame)
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 14.0)
        return label
    }

    private var rightEdgeView: UILabel {
        let label = UILabel(frame: rightEdgeLabelFrame)
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 14.0)
        return label
    }

    private func updatePointerView() {
        UIView.animate(withDuration: animationDuration) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.pointer.frame = strongSelf.pointerFrame
        }
    }

    private func updateTrack(toState state: State) {
        UIView.animate(withDuration: animationDuration) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.track.backgroundColor = state.color
        }
    }

    private func getState(forValue value: Double) -> State {
        let absolutePositionOnTrack = (value - minValue) / (maxValue - minValue)
        if absolutePositionOnTrack > 0.8 {
            return .critical
        } else {
            return .normal
        }
    }

}
