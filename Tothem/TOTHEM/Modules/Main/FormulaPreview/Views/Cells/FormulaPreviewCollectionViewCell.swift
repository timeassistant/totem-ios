//
//  FormulaPreviewCollectionViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum FormulaPreviewVisualElement {
    case circleButton

    func getDefault(parameter: FormulaPreviewVisualElementParameter) -> ParameterValue {
        switch self {
        case .circleButton:
            switch parameter {
            case .size: return ParameterValue.double(7)
            }
        }
    }
}

enum FormulaPreviewVisualElementParameter {
    case size
}

struct FormulaPreviewData {

    var key: ConfigurationUnitKey
    var index: Int
}

class FormulaPreviewCollectionViewCell: UICollectionViewCell {

    private var disposeBag = DisposeBag()

    static let identifier = "FormulaPreviewCollectionViewCell"
    static let nib = UINib(nibName: "FormulaPreviewCollectionViewCell", bundle: nil)

    @IBOutlet weak var infoLabel: UILabel!

    private var data: FormulaPreviewData?

    private var minCircle = UIView()
    private var maxCircle = UIView()
    var circleButton = ButtonPanelComponent<ConfiguredButton>()
    private var originalSize: CGFloat = 60.0

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = UIColor.darkGray
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    func updateView(forPreviewData previewData: FormulaPreviewData?) {

        data = previewData
        guard let previewData = previewData else { return }

        switch previewData.key.previewElementType {
        case .circleButton:
            drawCircle(withData: previewData)
            setupObserversForCircle()
        }
    }

    private func setupObserversForCircle() {

        guard let index = data?.index else { return }
        let sizeShared = ConfigurationFacade.shared
            .buttonPanelCenterModuleConstants
            .testSize(forIndex: index, component: .createButton)
            .share()

        sizeShared.take(1)
            .map { $0.value }
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                let circleSize = CGFloat($0)
                let frame = CGRect(x: (strongSelf.contentView.frame.width / 2) - (circleSize / 2), y: (strongSelf.contentView.frame.height / 2) - (circleSize / 2), width: circleSize, height: circleSize)
                strongSelf.circleButton.component.frame = frame
                strongSelf.circleButton.component.layer.cornerRadius = strongSelf.circleButton.component.frame.width / 2
                strongSelf.originalSize = circleSize
                strongSelf.showInfo(forData: strongSelf.data, value: ParameterValue.double($0))
            }).disposed(by: disposeBag)

        sizeShared.skip(1)
            .throttle(0.25, scheduler: MainScheduler.instance)
            .map { $0.value }
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self else { return }
                let num = CGFloat(value)/strongSelf.originalSize
                let transform = CGAffineTransform.init(scaleX: num, y: num)
                UIView.animate(1, animations: {
                    strongSelf.circleButton.component.transform = transform
                })
            })
            .disposed(by: disposeBag)

        sizeShared.map { $0.minValue }
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                let value = CGFloat($0)
                strongSelf.minCircle.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
                strongSelf.minCircle.frame = CGRect(x: (strongSelf.contentView.frame.width / 2) - (value / 2),
                                                                 y: (strongSelf.contentView.frame.height / 2) - (value / 2),
                                                                 width: value,
                                                                 height: value)
                strongSelf.minCircle.layer.cornerRadius = strongSelf.minCircle.frame.width / 2
            })
            .disposed(by: disposeBag)

        sizeShared.map { $0.maxValue }
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                let value = CGFloat($0)
                strongSelf.maxCircle.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
                strongSelf.maxCircle.frame = CGRect(x: (strongSelf.contentView.frame.width / 2) - (value / 2),
                                                    y: (strongSelf.contentView.frame.height / 2) - (value / 2),
                                                    width: value,
                                                    height: value)
                strongSelf.maxCircle.layer.cornerRadius = strongSelf.maxCircle.frame.width / 2
            })
            .disposed(by: disposeBag)
    }

    private func drawCircle(withData data: FormulaPreviewData) {

        contentView.addSubview(maxCircle)

        self.circleButton.setup { [weak self] circleButton in
            guard let self = self else { return }

            ConfigurationFacade.shared
                .buttonPanelCenterModuleConstants
                .color(.createButton)
                .take(1)
                .subscribe(onNext: {
                    circleButton.backgroundColor = $0
                }).disposed(by: self.disposeBag)

            ConfigurationFacade.shared
                .buttonPanelCenterModuleConstants
                .border(.createButton)
                .take(1)
                .subscribe(onNext: {
                    circleButton.layer.borderWidth = CGFloat($0)
                })
                .disposed(by: self.disposeBag)

            ConfigurationFacade.shared
                .buttonPanelCenterModuleConstants
                .borderColor(.createButton)
                .take(1)
                .subscribe(onNext: {
                    circleButton.layer.borderColor = $0.cgColor
                })
                .disposed(by: self.disposeBag)

            circleButton.clipsToBounds = false
            circleButton.circle.layer.borderColor = UIColor.clear.cgColor

            self.contentView.addSubview(self.circleButton.component)

            self.addSubview(self.minCircle)
        }
    }

    private func showInfo(forData data: FormulaPreviewData?, value: ParameterValue) {
        infoLabel.text = "\(data?.index ?? 0)"
    }

}
