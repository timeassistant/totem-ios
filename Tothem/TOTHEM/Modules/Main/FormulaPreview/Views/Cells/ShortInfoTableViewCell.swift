//
//  ShortInfoTableViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

struct ShortTableCellInfo {
    var parameterName: String
    var parameterValue: ParameterValue
}

class ShortInfoTableViewCell: UITableViewCell {

    static let identifier = "ShortInfoTableViewCell"
    static let nib = UINib(nibName: "ShortInfoTableViewCell", bundle: nil)

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var equalizerView: EqualizerView!

    override func awakeFromNib() {
        super.awakeFromNib()

        equalizerView.backgroundColor = UIColor.white
    }

    func setData(_ data: ShortTableCellInfo, sourceInfo: DataSourceInfo) {

        let value = data.parameterValue.asDouble()

        titleLabel.text = "\(data.parameterName) (\(String(describing: sourceInfo.description)))"
        valueLabel.text = data.parameterValue.asString()

        let decimalMin: Decimal = sourceInfo.getNormalizer(parameter: .min, forPlatform: .ios) ?? 0.0
        let decimalMax: Decimal = sourceInfo.getNormalizer(parameter: .max, forPlatform: .ios) ?? 1.0
        equalizerView.minValueVariable.value = Double(truncating: decimalMin as NSNumber)
        equalizerView.maxValueVariable.value = Double(truncating: decimalMax as NSNumber)
        equalizerView.value.value = value
    }

}
