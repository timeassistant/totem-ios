//
//  FormulaShortTableViewCell.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class FormulaShortTableViewCell: UITableViewCell {

    static let identifier = "FormulaShortTableViewCell"
    static let nib = UINib(nibName: "FormulaShortTableViewCell", bundle: nil)

    var disposeBag = DisposeBag()

    var key = PublishSubject<ConfigurationUnitKey?>()

    var formulaName = BehaviorSubject<String?>(value: "Loading...")
    var formulaAlgorithm = BehaviorSubject<String?>(value: "")

    override func awakeFromNib() {
        super.awakeFromNib()

        self.key
            .subscribe(onNext: { [weak self] key in
                guard let key = key, let strongSelf = self else { return }
                strongSelf.formulaName.onNext(key.readableName)
            })
            .disposed(by: disposeBag)

        self.formulaName
            .subscribe(onNext: { [weak self] in
                self?.textLabel!.text = $0
            })
            .disposed(by: disposeBag)
        
        self.formulaAlgorithm
            .subscribe(onNext: { [weak self] in
                self?.detailTextLabel!.text = $0
            })
            .disposed(by: disposeBag)
    }

}
