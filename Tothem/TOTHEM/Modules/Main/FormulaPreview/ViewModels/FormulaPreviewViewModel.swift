//
//  FormulaPreviewViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class FormulaPreviewViewModel {

    private let disposeBag: DisposeBag

    let dismiss = PublishSubject<()>()

    var formulaKey: ConfigurationUnitKey

    var evaluationDetails = Variable([ShortTableCellInfo]())
    var configuratorValues = Variable([FormulaPreviewData]())
    private func addEvaluationDetails(shortInfo: ShortTableCellInfo) {
        let appendedElementOptionalIndex = evaluationDetails.value
            .firstIndex { $0.parameterName == shortInfo.parameterName }
        if let appendedElementIndex = appendedElementOptionalIndex {
            evaluationDetails.value[appendedElementIndex] = shortInfo
        } else {
            evaluationDetails.value.append(shortInfo)
        }
    }

    private let configurator: ConfigurationUnit

    init(forFormulaKey key: ConfigurationUnitKey, bag: DisposeBag) {

        self.disposeBag = bag

        self.formulaKey = key
        self.configurator = ConfigurationUnit(forKey: key, andIndex: 1)

        configurator.parametersSet.asObservable()
            .throttle(0.25, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (parametersSet) in
                guard let strongSelf = self else { return }
                strongSelf.fillPreviewDataArray(withLength: parametersSet.keys.count)
            })
            .disposed(by: disposeBag)
        configurator.parameterValues
            .throttle(0.25, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (parametersValues) in
                guard let strongSelf = self else { return }
                strongSelf.fillComponentValues(parametersCollector: parametersValues)
            })
            .disposed(by: disposeBag)
    }

    private func fillPreviewDataArray(withLength setLength: Int) {
        if configuratorValues.value.count == setLength {
            return
        }
        configuratorValues.value.removeAll()
        for index in 1...setLength {
            let previewData = FormulaPreviewData(key: self.formulaKey, index: index)
            configuratorValues.value.append(previewData)
        }
    }

    private func fillComponentValues(parametersCollector: FormulaParametersCollector) {

        for (key, value) in parametersCollector.parameters {
            if ConnectorParameter(rawValue: key) == nil { continue }
            addEvaluationDetails(shortInfo: ShortTableCellInfo(parameterName: key, parameterValue: value))
        }
    }
}
