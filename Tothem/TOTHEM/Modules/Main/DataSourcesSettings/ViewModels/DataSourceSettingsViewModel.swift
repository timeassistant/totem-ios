//
//  DataSourceSettingsViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

class DataSourceSettingsViewModel: ViewModelBase {
    let dataSource: Variable<DataSourceDynamic>
    let isEnabled: Observable<Bool>
    let dynamicValue: Observable<Double>
    let minValue: Observable<Double>
    let maxValue: Observable<Double>
    let staticValue: Observable<Double>

    init(forDataSource dataSource: DataSourceDynamic) {
        self.dataSource = Variable(dataSource)
        self.isEnabled = DataSourcesSettingsService.shared.observableIsDynamic(for: dataSource.dataSource)
        self.dynamicValue = dataSource.dataSource.normalizedDeviceBasedValueObservable
            .map { $0.asDouble() }
        self.minValue = DataSourceManager.shared.minValues
            .map { $0[dataSource.dataSource] }
            .skipNil()
            .map { $0.asDouble() }
        self.maxValue = DataSourceManager.shared.maxValues
            .map { $0[dataSource.dataSource] }
            .skipNil()
            .map { $0.asDouble() }

        self.staticValue = Observable.combineLatest(
            DataSourcesSettingsService.shared.observableStaticValue(for: dataSource.dataSource),
            self.minValue)
            .map { $0.0 ?? $0.1 }
    }

    func switchEnabled(to enabled: Bool) {
        DataSourcesSettingsService.shared.setEnabled(dataSource: dataSource.value.dataSource.parameterDomain, to: enabled)
    }

    func setStaticValue(to newValue: Double) {
        DataSourcesSettingsService.shared.setStaticValue(dataSource: dataSource.value.dataSource.rawValue, to: newValue)
    }
}
