//
//  DataSourcesViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class DataSourcesViewController: UIViewController {

    private var disposeBag = DisposeBag()

    var viewModel = DataSourcesViewModel()

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var isDynamicModeEnabled: UISwitch!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "DataSourceDynamicTableViewCell",
                                     bundle: nil),
                               forCellReuseIdentifier: DataSourceDynamicTableViewCell.identifier)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        cancelButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        viewModel.isDynamicModeEnabled
            .take(1)
            .bind(to: isDynamicModeEnabled.rx.isOn)
            .disposed(by: disposeBag)
        isDynamicModeEnabled.rx.value
            .skip(1)
            .subscribe(onNext: { [weak self] enabled in
                self?.viewModel.switchDynamicMode(to: enabled)
            })
            .disposed(by: disposeBag)

        viewModel.dataSources
            .bind(to: tableView.rx.items(cellIdentifier: DataSourceDynamicTableViewCell.identifier)) { [weak self] (_, dataSource, cell) in
                guard let cell = cell as? DataSourceDynamicTableViewCell else { return }
                let dataSourceDomain = dataSource.dataSource.parameterDomain
                cell.titleLabel?.text = dataSource.dataSource.description
                cell.currentValueLabel.text = "Current static value: \(dataSource.currentStaticValue.rounded(toPlaces: 2))"
                cell.switcher.isOn = dataSource.isDynamicModeEnabled
                cell.switcher.rx.isOn
                    .skip(1)
                    .subscribe(onNext: { enabled in
                        self?.viewModel.switchEnabled(dataSource: dataSourceDomain, to: enabled)
                    })
                    .disposed(by: cell.disposeBag)
            }
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(DataSourceDynamic.self)
            .bind(to: viewModel.select)
            .disposed(by: disposeBag)
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.tableView.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: disposeBag)
    }

}
