//
//  DataSourceSettingsViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

class DataSourceSettingsViewController: UIViewController {

    var disposeBag = DisposeBag()

    var viewModel: DataSourceSettingsViewModel

    init(forDataSource dataSource: DataSourceDynamic) {
        self.viewModel = DataSourceSettingsViewModel(forDataSource: dataSource)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("No dataSource passed")
    }

    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var customNavigationItem: UINavigationItem!
    @IBOutlet weak var switcher: UISwitch!

    @IBOutlet weak var staticMaxLabel: UILabel!
    @IBOutlet weak var staticMinLabel: UILabel!
    @IBOutlet weak var staticValueSlider: UISlider!
    @IBOutlet weak var staticCurrentValueLabel: UILabel!

    @IBOutlet weak var dynamicValueSlider: UISlider!
    @IBOutlet weak var dynamicMinLabel: UILabel!
    @IBOutlet weak var dynamicMaxLabel: UILabel!
    @IBOutlet weak var currentDynamicValueLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        doneButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        viewModel.dataSource.asObservable()
            .map { $0.dataSource.description }
            .bind(to: customNavigationItem.rx.title)
            .disposed(by: disposeBag)

        viewModel.isEnabled
            .take(1)
            .bind(to: switcher.rx.isOn)
            .disposed(by: disposeBag)
        switcher.rx.isOn
            .skip(1)
            .subscribe(onNext: { [weak self] enabled in
                self?.viewModel.switchEnabled(to: enabled)
            })
            .disposed(by: disposeBag)

        viewModel.minValue
            .subscribe(onNext: { [weak self] minValue in
                self?.staticMinLabel.text = String(minValue)
                self?.staticValueSlider.minimumValue = Float(minValue)
                self?.dynamicMinLabel.text = String(minValue)
                self?.dynamicValueSlider.minimumValue = Float(minValue)
            })
            .disposed(by: disposeBag)
        viewModel.maxValue
            .subscribe(onNext: { [weak self] maxValue in
                self?.staticMaxLabel.text = String(maxValue)
                self?.staticValueSlider.maximumValue = Float(maxValue)
                self?.dynamicMaxLabel.text = String(maxValue)
                self?.dynamicValueSlider.maximumValue = Float(maxValue)
            })
            .disposed(by: disposeBag)
        viewModel.dynamicValue
            .map { $0.rounded(toPlaces: 2) }
            .subscribe(onNext: { [weak self] value in
                self?.dynamicValueSlider.setValue(Float(value), animated: true)
                self?.currentDynamicValueLabel.text = String(value)
            })
            .disposed(by: disposeBag)
        viewModel.staticValue
            .take(1)
            .map { Float($0) }
            .bind(to: staticValueSlider.rx.value)
            .disposed(by: disposeBag)
        staticValueSlider.rx.value
            .map { Double($0) }
            .map { String($0.rounded(toPlaces: 2)) }
            .bind(to: staticCurrentValueLabel.rx.text)
            .disposed(by: disposeBag)
        staticValueSlider.rx.value
            .skip(1)
            .throttle(0.3, scheduler: MainScheduler.instance)
            .map { Double($0) }
            .subscribe(onNext: { [weak self] newValue in
                self?.viewModel.setStaticValue(to: newValue)
            })
            .disposed(by: disposeBag)
    }

}
