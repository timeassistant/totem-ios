//
//  UploadPictureUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

final class UploadPictureUseCase {
    static let shared = UploadPictureUseCase()
    private let assetNamesRepository = AssetNamesRepository.shared
    private let firebaseRepository = UsersDataService.shared
    private let picturesRepository = PicturesRepository()
    private let userRepository = UserRepository()
    
    func uploadNew() -> Observable<Bool> {
        return assetNamesRepository.state
            .observeOn(MainScheduler.asyncInstance)
            .flatMap({ state -> Observable<Bool> in
                switch state {
                case .new(let assetId, _):
                    return self.upload(assetId: assetId)
                        .map({ _ in true })
                default: return Observable.just(false)
                }
            })
    }
    
    private func upload(assetId: String) -> Observable<Void> {
        guard let path = userRepository.user?.uid else {
            return .error(GenericError.notAuthorized)
        }
        var name = ""
        return self.assetNamesRepository
            .uploading(assetId)
            .flatMap({ _ in
                self.assetNamesRepository
                    .assetIdFilename(assetId)
            })
            .flatMap({ filename -> Observable<UIImage> in
                name = filename
                return self.picturesRepository
                    .getLocal(path: path, name: filename)
            })
            .flatMap({ image in
                self.picturesRepository
                    .put(image: image, path: path, name: name)
            })
            .map({ state -> PhotoModel? in
                switch state {
                case .finished(let size):
                    return PhotoModel(id: assetId, filename: name, size: size)
                default: return nil
                }
            })
            .skipNil()
            .flatMap({ self.firebaseRepository.save($0) })
            .flatMap({ _ in
                self.assetNamesRepository
                    .uploaded(assetId)
            })
            .do(onNext: { _ in
                self.assetNamesRepository.next()
            }, onError: { _ in
                self.assetNamesRepository.next()
            })
    }
}
