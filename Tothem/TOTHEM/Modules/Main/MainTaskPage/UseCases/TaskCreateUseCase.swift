//
//  TaskCreateUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class TaskCreateUseCase {
    static let shared = TaskCreateUseCase()
    weak var creationViewController: TaskEditingController?

    func execute(_ transitions: [CollectionViewCellTransition], on side: TimesetType, position: Double?) {
        var action: TaskViewEditAction?
        
        if side == .center {
            switch UserClassService.shared.choice {
            case .bullet:
                action = .checklist(NSRange(location: 0, length: 0))
            case .media:
                action = .image(0)
            default: break
            }
        }
        
        TaskEditingUseCase.shared
            .execute(transitions, on: side, action: action, position: position, changeListSideLocked: action != nil)

        creationViewController = TaskEditingUseCase.shared.editingController
    }
}
