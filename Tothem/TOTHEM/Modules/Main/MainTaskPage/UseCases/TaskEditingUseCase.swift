//
//  TaskEditingUseCase.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class TaskEditingUseCase {
    static let shared = TaskEditingUseCase()
    weak var editingController: TaskEditingController?
    var sides: [TimesetType] = []
    
    func execute(_ transitions: [CollectionViewCellTransition], on side: TimesetType, action: TaskViewEditAction? = nil, position: Double? = nil, changeListSideLocked: Bool = false) {

        var editingAction = TaskViewEditAction.image(0)
        if action == nil {
            switch side {
            case .center:
                editingAction = .title(NSRange(location: 0, length: 0))
            case .right:
                editingAction = .checklist(NSRange(location: 0, length: 0))
            default: break
            }
        } else {
            editingAction = action!
        }

        let controller = TaskEditingController(editingAction: editingAction)
        controller.position = position
        controller.transitions = transitions
        controller.side = side
        controller.window = UIApplication.shared.keyWindow
        controller.canExitFromPreview = action == nil
        controller.changeListSideLocked = changeListSideLocked

        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve

        editingController = controller

        UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: false, completion: nil)
    }
}

//        ListEditingUseCase.shared
//            .execute(model, transitions: transitions, side: side, action: action)
//            .subscribeOn(MainScheduler.instance)
//            .do(onNext: {
//                switch $0 {
//                case .willAppear: break
//
//                case .didAppear:
//                    self.rightController?.releaseContent()
//                    self.leftController?.releaseContent()
//                    self.centerController?.releaseContent()
//                default: break
//                }
//            })
//            .subscribe()
//            .disposed(by: self.editingDisposeBag)

//        scrollView.delegate = nil
//
//        ListEditingUseCase.shared
//            .execute(model, transitions: transitions, side: side, action: action)
//            .bind(to: editingEvents)
//            .disposed(by: editingDisposeBag)
//
//        editingEvents
//            .asObserver()
//            .observeOn(MainScheduler.instance)
//            .do(onNext: { [weak self] in
//
//                self?.leftController?.handleEditing(event: $0)
//                self?.rightController?.handleEditing(event: $0)
//                self?.centerController?.handleEditing(event: $0)
//
//                switch $0 {
//                case .didDismiss:
//                    self?.scrollView.delegate = self
//                    self?.editingDisposeBag = DisposeBag()
//                case .updated(let model):
//                    guard let self = self else { return }
//                    self.model = model
//                    self.setInitialPreviewFrames()
//                default: break
//                }
//            })
//            .subscribe()
//            .disposed(by: editingDisposeBag)
//
//        ChangeSideUseCase.shared.change
//            .asObserver()
//            .observeOn(MainScheduler.instance)
//            .do(onNext: { [weak self] in
//                self?.side = $0.0
//                self?.setOffset(to: $0.0)
//            })
//            .subscribe()
//            .disposed(by: editingDisposeBag)
