//
//  MainTaskViewController+ButtonPanel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxGesture

extension MainTaskViewController {
    func buttonPanelChangeModule(to module: ButtonPanelModuleBehaviour, progress: CGFloat) {
        let shiftProgress = Progress(value: progress)
        let to = module.frame
        let from = buttonPanelCurrentModule.frame
        buttonPanelTop?.constant = value(from.minY, to: to.minY, progress: progress)
        buttonPanelLeading?.constant = value(from.minX, to: to.minX, progress: progress)
        buttonPanelHeight?.constant = value(from.height, to: to.height, progress: progress)
        buttonPanelWidth?.constant = value(from.width, to: to.width, progress: progress)
        
        switch shiftProgress.value {
        case Progress.max:
            buttonPanelCurrentModule = module
            buttonPanelUpdatePosition(module: module)
            if !module.isHidden {
                UIView.animate(CATransaction.animationDuration()) {
                    self.buttonPanelCurrentModule.appearance
                        .components.horizontalLine
                        .component.alpha = 1
                }
            }
        case Progress.min:
            if !module.isHidden {
                UIView.animate(CATransaction.animationDuration()) {
                    self.buttonPanelCurrentModule.appearance
                        .components.horizontalLine
                        .component.alpha = 1
                }
            }

        default:
            UIView.animate(CATransaction.animationDuration()) {
                self.buttonPanelCurrentModule.appearance
                    .components.horizontalLine
                    .component.alpha = 0
            }
        }
    }

    func buttonPanelUpdatePosition(side: TimesetType) {
        guard side == timesetManger.engineService.stateService.currentState else { return }
        buttonPanelUpdatePosition(module: buttonPanelCurrentModule)
    }

    private func buttonPanelUpdatePosition(module: ButtonPanelModuleBehaviour) {
        guard !buttonPanel.isPanning else { return }
        buttonPanelTop?.constant = module.frame.minY
        buttonPanelHeight?.constant = module.frame.height
        buttonPanelWidth?.constant = module.frame.width
        buttonPanelLeading?.constant = module.frame.minX
        view.layoutIfNeeded()
    }

    private func value(_ from: CGFloat, to: CGFloat, progress: CGFloat) -> CGFloat {
        let shiftProgress = Progress(value: progress)
        return to > from ? max(from, to * shiftProgress.value) :
            min(from, max(to, from * shiftProgress.reversedValue))
    }
}

extension MainTaskViewController: ButtonPanelDelegate {
    func buttonPanelBeganPanning(gesture: UIPanGestureRecognizer) {
        guard let module = buttonPanelCurrentModule else { return }
        buttonPanelBeginPanningFrame = module.frame
        buttonPanelBeginPanningLocation = gesture.location(in: view)
        let view = module.appearance.components.horizontalLine.component
        timesetManger.activeListViewController.insertionBegin(with: view)
    }

    func buttonPanelChangedPanning(gesture: UIPanGestureRecognizer) {
        buttonPanelTop?.constant = buttonPanelBeginPanningFrame.minY + gesture.location(in: view).y - buttonPanelBeginPanningLocation.y
        guard let module = buttonPanelCurrentModule else { return }
        let view = module.appearance.components.horizontalLine.component
        timesetManger.activeListViewController.insertionChanged(with: view)
    }

    func buttonPanelEndedPanning(gesture: UIPanGestureRecognizer) {
        defer {
            buttonPanelUpdatePosition(module: buttonPanelCurrentModule)
        }

        guard let module = buttonPanelCurrentModule else { return }
        let view = module.appearance.components.horizontalLine.component
        timesetManger.activeListViewController.insertionEnded(with: view)
    }

    func didInteract(with event: ComponentEventType, componentType: Components.ComponentsElements) {
        if componentType == .createButton(value: nil) && event == .touchInside {

            let side = timesetManger.engineService.stateService.currentState
            
            ConfigurationFacade.shared
                .hapticParameters
                .getHapticCreateButton(side)

            timesetManger.activeListViewController.previewEmtyCell()
        }
    }
}
