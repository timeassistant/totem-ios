//
//  MainTaskViewController+Creation.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

extension MainTaskViewController {
    func create(on side: TimesetType, position: Double?) {
        var transitions: [CollectionViewCellTransition] = []

        if let model = timesetManger.centerStack.controller.emptyModel {
            let transition = CollectionViewCellTransition(side: .center, model: model, controller: timesetManger.centerStack.collection)
            transitions.append(transition)
        }

        if let model = timesetManger.leftStack.controller.emptyModel {
            let transition = CollectionViewCellTransition(side: .left, model: model, controller: timesetManger.leftStack.collection)
            transitions.append(transition)
        }

        if let model = timesetManger.rightStack.controller.emptyModel {
            let transition = CollectionViewCellTransition(side: .right, model: model, controller: timesetManger.rightStack.collection)
            transitions.append(transition)
        }

        TaskCreateUseCase.shared
            .execute(transitions, on: side, position: position)

        TaskCreateUseCase.shared
            .creationViewController?
            .events.asObserver()
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: handle)
            .subscribe()
            .disposed(by: dispose)
    }

    private func handle(event: TaskEditingControllerEvent) {
        guard let controller = TaskCreateUseCase.shared
            .creationViewController else { return }
        switch event {
        case .exit:
            controller.exitToList()
        case .dismiss:
            statusBarHidden = false
            controller.dismiss(animated: false, completion: {
                self.timesetManger.centerStack.controller.set(state: .previewDismissed)
                self.timesetManger.leftStack.controller.set(state: .previewDismissed)
                self.timesetManger.rightStack.controller.set(state: .previewDismissed)
            })
        default: break
        }
    }
}
