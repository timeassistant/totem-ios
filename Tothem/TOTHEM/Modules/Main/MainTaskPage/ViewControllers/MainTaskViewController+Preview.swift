//
//  MainTaskViewController+Preview.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

enum PreviewEvent: Event {
    case edit(TimesetType)
    var key: String {
        switch self {
        case .edit(let side):
            return "e" + side.eventKey
        }
    }
    
    var classify: Bool {
        return false
    }
}

extension MainTaskViewController {
    func preview(_ model: CollectionCellModel, on side: TimesetType) {
        var transitions: [CollectionViewCellTransition] = []
        let sides = GetTaskSidesUseCase.shared.execute(model)

        if sides.contains(.center) {
            var transition = CollectionViewCellTransition(side: .center, model: model, controller: timesetManger.centerStack.collection)
            transition.dataManager = TasksPreviewDataManager.shared
            transitions.append(transition)
        }

        if sides.contains(.left) {
            var transition = CollectionViewCellTransition(side: .left, model: model, controller: timesetManger.leftStack.collection)
            transition.dataManager = TasksPreviewDataManager.shared
            transitions.append(transition)
        }

        if sides.contains(.right) {
            var transition = CollectionViewCellTransition(side: .right, model: model, controller: timesetManger.rightStack.collection)
            transition.dataManager = TasksPreviewDataManager.shared
            transitions.append(transition)
        }

        preview(on: side, transitions: transitions)
    }

    func preview(on side: TimesetType, transitions: [CollectionViewCellTransition]) {
        //self.statusBarHidden = true
        TaskPreviewUseCase.shared
            .execute(side: side, transitions: transitions)

        TaskPreviewUseCase.shared
            .previewController?.events
            .asObserver()
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] in
                self?.handle($0)
            })
            .subscribe()
            .disposed(by: dispose)
    }

    private func handle(_ event: PreviewControllerEvent) {
        guard let controller = TaskPreviewUseCase.shared.previewController else { return }

        switch event {
        case .edit(let action, let side):
            EventsDataManager.shared.add(PreviewEvent.edit(side))
            TaskPreviewUseCase.shared.action = (action, side)
            handle(.dismiss)
        case .exit:
            controller.exitToList()
        case .dismiss:
            controller.dismiss()
            
            if let (action, side) = TaskPreviewUseCase.shared.action {
                TaskPreviewUseCase.shared.action = nil
                var transitions: [CollectionViewCellTransition] = []
                if let transition = controller.leftController.transition {
                    transitions.append(transition)
                } else if let model = timesetManger.leftStack.controller
                    .emptyModel {
                    var transition = CollectionViewCellTransition(side: .left, model: model, controller: timesetManger.leftStack.collection)
                    transition.dataManager = TasksPreviewDataManager.shared
                    transitions.append(transition)
                }

                if let transition = controller.centerController.transition {
                    transitions.append(transition)
                } else if let model = timesetManger.centerStack.controller
                    .emptyModel {
                    var transition = CollectionViewCellTransition(side: .center, model: model, controller: timesetManger.centerStack.collection)
                    transition.dataManager = TasksPreviewDataManager.shared
                    transitions.append(transition)
                }

                if let transition = controller.rightController.transition {
                    transitions.append(transition)
                } else if let model = timesetManger.rightStack.controller
                    .emptyModel {
                    var transition = CollectionViewCellTransition(side: .right, model: model, controller: timesetManger.rightStack.collection)
                    transition.dataManager = TasksPreviewDataManager.shared
                    transitions.append(transition)
                }

                edit(transitions, action: action, on: side, fromPreview: true)
            } else {
                statusBarHidden = false
            }
        case .previewPhoto(let photos, let index):
            let viewController = PhotosPreviewController()
            viewController.photos = photos
            viewController.openAtIndex = index
            controller.present(viewController, animated: true, completion: nil)
        }
    }
}
