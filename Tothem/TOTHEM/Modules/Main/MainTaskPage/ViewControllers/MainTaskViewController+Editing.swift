//
//  MainTaskViewController+Editing.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

extension MainTaskViewController {
    func edit(_ model: CollectionCellModel, on side: TimesetType) {

        var transitions: [CollectionViewCellTransition] = []
        let sides = GetTaskSidesUseCase.shared.execute(model)

        if sides.contains(.center) {
            let transition = CollectionViewCellTransition(side: .center, model: model, controller: timesetManger.centerStack.collection)
            transitions.append(transition)
        } else if let model = timesetManger.centerStack.controller
            .emptyModel {
            let transition = CollectionViewCellTransition(side: .center, model: model, controller: timesetManger.centerStack.collection)
            transitions.append(transition)
        }

        if sides.contains(.left) {
            let transition = CollectionViewCellTransition(side: .left, model: model, controller: timesetManger.leftStack.collection)
            transitions.append(transition)
        } else if let model = timesetManger.leftStack.controller.emptyModel {
            let transition = CollectionViewCellTransition(side: .left, model: model, controller: timesetManger.leftStack.collection)
            transitions.append(transition)
        }

        if sides.contains(.right) {
            let transition = CollectionViewCellTransition(side: .right, model: model, controller: timesetManger.leftStack.collection)
            transitions.append(transition)
        } else if let model = timesetManger.leftStack.controller.emptyModel {
            let transition = CollectionViewCellTransition(side: .right, model: model, controller: timesetManger.rightStack.collection)
            transitions.append(transition)
        }

        edit(transitions, action: nil, on: side, fromPreview: false)
    }

    func edit(_ transitions: [CollectionViewCellTransition], action: TaskViewEditAction?, on side: TimesetType, fromPreview: Bool) {

        TaskEditingUseCase.shared
            .execute(transitions, on: side, action: action)

        TaskEditingUseCase.shared.sides = transitions.map({ $0.side })

        TaskEditingUseCase.shared
            .editingController?
            .events
            .asObserver()
            .subscribeOn(MainScheduler.asyncInstance)
            .do(onNext: { [weak self] in
                self?.handle(event: $0, fromPreview: fromPreview)
            })
            .subscribe()
            .disposed(by: dispose)
    }

    private func handle(event: TaskEditingControllerEvent, fromPreview: Bool) {
        //guard let controller = TaskEditingUseCase.shared
        //    .editingController else { return }
        switch event {
        case .exit:
            //controller.exitToList()
            handle(event: .dismiss, fromPreview: fromPreview)
        case .dismiss:
            dismissEditingController(fromPreview: fromPreview)
        default: break
        }
    }

    private func dismissEditingController(fromPreview: Bool) {
        guard let editingController = TaskEditingUseCase.shared
            .editingController else { return }

        let side = editingController.side
        TaskEditingUseCase.shared.editingController = nil

        if fromPreview {
            editingController.dismiss(animated: false, completion: nil)
            editingController.transitions.forEach {
                if $0.model.viewModel.isEmpty {
                    $0.exit()
                }
            }

            preview(on: side, transitions: editingController.transitions.filter({
                !$0.model.viewModel.isEmpty
            }))
        } else {
            editingController.transitions.forEach {
                $0.exit()
            }

            timesetManger.centerStack.controller.set(state: .previewDismissed)
            timesetManger.leftStack.controller.set(state: .previewDismissed)
            timesetManger.rightStack.controller.set(state: .previewDismissed)
            editingController.dismiss(animated: false, completion: nil)

            statusBarHidden = false
        }
    }
}
