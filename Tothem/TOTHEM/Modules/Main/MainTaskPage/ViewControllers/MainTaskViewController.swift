//
//  ViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxGesture

final class MainTaskViewController: UIViewController {
    //Rx Disposition
    var dispose = DisposeBag()

    //VIEW MODEL
    var viewModel: MainTaskViewControllerViewModelProtocol = MainTaskViewControllerViewModel()

    // OUTLETS & VIEWS
    var timesetManger: TimelineManagerView!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet weak var backgroundBlur: UIVisualEffectView!
    @IBOutlet weak var backgroundContrastView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var debugLabel: UILabel?
    
    var buttonPanel: ButtonPanel!
    var profileTransition: ProfileTransition!

    var statusBarHidden: Bool = false {
        didSet {
            var height = UIApplication.shared.statusBarFrame.height
            height = height == 0 ? 20 : height
            topConstraint.constant = statusBarHidden ? 0 : height

            UIView.animate(withDuration: CATransaction.animationDuration(), delay: 0.2, options: [], animations: {
                self.statusBarBackground.alpha = self.statusBarHidden ? 0 : 1
                self.view.layoutSubviews()
                self.setNeedsStatusBarAppearanceUpdate()
            }, completion: nil)
        }
    }

    private lazy var centerPanelBehaviour = ButtonPanelModule(buttonPanel, side: .center)
    private lazy var leftPanelBehaviour = ButtonPanelModule(buttonPanel, side: .left)
    private lazy var rightPanelBehaviour = ButtonPanelModule(buttonPanel, side: .right)

	// Status Bar
	var statusBarBackground: UIView!

    var buttonPanelBeginPanningFrame: CGRect = .zero
    var buttonPanelBeginPanningLocation: CGPoint = .zero
    var buttonPanelLeading: NSLayoutConstraint?
    var buttonPanelTop: NSLayoutConstraint?
    var buttonPanelWidth: NSLayoutConstraint?
    var buttonPanelHeight: NSLayoutConstraint?

    @IBOutlet var contentView: UIView?
    @IBOutlet var debugPanelView: UIView?
    
    let displayDebugPanel = DataSourcesSettingsService.shared
        .observableIsDynamic(for: .walkingSpeed)

    private var presenter = MainTaskPresenter()
    // Current Module which is using
    var buttonPanelCurrentModule: ButtonPanelModuleBehaviour! {
        didSet {
            buttonPanelCurrentModule.updateAppearence()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepare()
		self.backgroundSetup()

        statusBarHidden = false

		ConfigurationFacade.shared
            .backgroundConstants
            .getStatusbarColorsValue(.center)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: {
                self.statusBarBackground.backgroundColor = $0
            })
            .disposed(by: self.dispose)

        self.presenter.state
            .observeOn(MainScheduler.asyncInstance)
            .do(onNext: {
                switch $0 {
                case .activityIndicatorVisible: UIApplication.shared
                    .isNetworkActivityIndicatorVisible  = true
                case .activityIndicatorHidden: UIApplication.shared
                    .isNetworkActivityIndicatorVisible  = false
                }
            })
            .subscribe()
            .disposed(by: dispose)

        presenter.events.on(.next(.viewDidLoad))

        if let view = debugPanelView {
            self.displayDebugPanel
                .bind(to: view.rx.isHidden)
                .disposed(by: dispose)
        }
    }

    private func prepare() {
        self.prepareUI()
        self.bind()
    }

    private func prepareUI() {
        self.setupButtonPanel()
        self.setupTimesetView()
		self.preSetupStatusBar()
    }

    private func bind() {
        self.timesetManger.engineService.stateService
            .state
            .asObservable()
            .bind(to: self.viewModel.timesetManagerState)
            .disposed(by: self.dispose)

        self.timesetManger.engineService
            .isChangingPosition
            .subscribe(onNext: changeButtonPannelModuleType)
            .disposed(by: self.dispose)

        self.timesetManger.engineService.stateService
            .preview
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: {
                self.centerPanelBehaviour.isHidden = $0
                self.leftPanelBehaviour.isHidden = $0
                self.rightPanelBehaviour.isHidden = $0
            })
            .disposed(by: self.dispose)

        self.timesetManger
            .centerStack
            .controller
            .createButtonFrame
            .subscribe(onNext: {
                self.centerPanelBehaviour.frame = $0
                self.buttonPanelUpdatePosition(side: .center)
            })
            .disposed(by: self.dispose)

        self.timesetManger
            .leftStack
            .controller
            .createButtonFrame
            .subscribe(onNext: {
                self.leftPanelBehaviour.frame = $0
                self.buttonPanelUpdatePosition(side: .left)
            })
            .disposed(by: self.dispose)

        self.timesetManger
            .rightStack
            .controller
            .createButtonFrame
            .subscribe(onNext: {
                self.rightPanelBehaviour.frame = $0
                self.buttonPanelUpdatePosition(side: .right)
            })
            .disposed(by: self.dispose)

        self.buttonPanel
            .components
            .profileContainerView.component.rx
            .tapGesture()
            .skip(1)
            .map { _ in () }
            .subscribe(self.viewModel.openProfileController)
            .disposed(by: self.dispose)

		self.timesetManger
            .engineService
            .backgroundColorShiftValue
            .subscribe(onNext: self.backgroundColorFade)
            .disposed(by: self.dispose)

		self.timesetManger
            .engineService
            .positionBackgroundAlphaColor
            .subscribe(onNext: self.backgroundContrastSetup)
            .disposed(by: self.dispose)
        
        let value = UserClassService.shared.userClass.asObservable()
        let energy = UserClassService.shared.userEnergy.asObservable()
        let form = UserClassService.shared.userForm.asObservable()
        
        Observable.combineLatest(value, energy, form)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { userClass, energy, form in
                let text = """
                subject: \(userClass.subject > 0 ? "right" : "left") \(userClass.subject.rounded(toPlaces: 3))
                unique: \(userClass.unique > 0 ? "alpha": "beta") \(userClass.unique.rounded(toPlaces: 3))
                creative: \(userClass.creative.rounded(toPlaces: 3))
                
                
                energy: \(energy)
                form: \(form)
                """
                self.debugLabel?.text = text
            })
            .disposed(by: self.dispose)
    }

	private func backgroundColorFade(_ shift: CGFloat) {
		backgroundContrastView.alpha = shift
	}

	private func backgroundContrastSetup(_ position: TimesetType) {
		ConfigurationFacade.shared
            .backgroundConstants
            .getContrastLayerColorValue(position)
            .asObservable()
            .subscribe(onNext: {
                self.backgroundContrastView.backgroundColor = $0
            })
            .disposed(by: dispose)

        VisibilityRulesService.shared
            .visibility(for: .coverLayer)
            .startWith(true)
            .map { !$0 }
            .bind(to: backgroundContrastView.rx.isHidden)
            .disposed(by: dispose)
	}

    private func changeButtonPannelModuleType(shift: ScrollShift) {
        switch shift {
        case .center(progress: let progress):
            let isRightSide = backgroundImageView.frame.origin.x + backgroundImageView.frame.width/2 > view.frame.width/2
            let currentTranformation: CGFloat = isRightSide ? 20 : -20
            let backgroundTransform = currentTranformation * (1 - progress)
            backgroundImageView.transform = CGAffineTransform(translationX: backgroundTransform, y: 0)

            buttonPanelChangeModule(to: centerPanelBehaviour, progress: progress)
        case .left(progress: let progress):
            backgroundImageView.transform = CGAffineTransform(translationX: -20 * progress, y: 0)
            buttonPanelChangeModule(to: leftPanelBehaviour, progress: progress)
        case .right(progress: let progress):
            backgroundImageView.transform = CGAffineTransform(translationX: 20 * progress, y: 0)
            buttonPanelChangeModule(to: rightPanelBehaviour, progress: progress)
        }
    }

    // MARK: Setup Methods

    private func setupButtonPanel() {
        buttonPanel = ButtonPanel()
        buttonPanel.delegate = self
        contentView?.addSubview(buttonPanel)
        
        buttonPanelLeading = contentView?.constraintLeading(buttonPanel)
        buttonPanelTop = contentView?.constraintTop(buttonPanel)
        buttonPanelWidth = buttonPanel.constraintWidth(constant: centerPanelBehaviour.frame.width)
        buttonPanelHeight = buttonPanel.constraintHeight(constant: CGFloat(kCreateButtonSize))
        buttonPanelCurrentModule = centerPanelBehaviour
    }

	private func preSetupStatusBar() {
		self.statusBarBackground = UIView(frame: UIApplication.shared.statusBarFrame)
		self.view.addSubview(self.statusBarBackground)
	}

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }

    override var prefersStatusBarHidden: Bool {
        return self.statusBarHidden
    }

    private func backgroundSetup() {
		self.preSetupStatusBar()

		ConfigurationFacade.shared
            .backgroundConstants
            .getStatusbarColorsValue(.center)
            .subscribe(onNext: {
                self.statusBarBackground.backgroundColor = $0
            })
            .disposed(by: dispose)

		ConfigurationFacade.shared
            .backgroundConstants
            .getContrastLayerColorValue(.center)
            .subscribe(onNext: {
                self.backgroundContrastView.backgroundColor = $0
            })
            .disposed(by: dispose)

		ConfigurationFacade.shared
            .backgroundConstants
            .getBlurAlpha()
            .subscribe(onNext: {
                self.backgroundBlur.alpha = $0
            })
            .disposed(by: dispose)

		ConfigurationFacade.shared
            .backgroundConstants
            .getBlurType()
            .subscribe(onNext: {
                self.backgroundBlur.effect = UIBlurEffect(style: $0)
            })
            .disposed(by: self.dispose)
	}

    private lazy var adaptationPresenter = TimesetPresenter()
    private func setupTimesetView() {
        let side = UserClassService.objectDs
        self.timesetManger = TimelineManagerView(frame: view.bounds, state: side)

        if let contentView = contentView {
            timesetManger.fill(in: contentView)
            contentView.bringSubviewToFront(buttonPanel)
        }

        // TODO: enable stabilization on main screen
//        adaptationPresenter.state?
//            .observeOn(MainScheduler.asyncInstance)
//            .subscribe(onNext: { [weak self] _ in
//                self?.setBias($0)
//            })
//            .disposed(by: dispose)
        
        
    }

    private func setBias(_ model: TimesetParametersModel) {
        let animated = (try? SettingsRepository.shared.isAnimated.value()) ?? true
        let bias = model.bias
        
        let preview = try? self.timesetManger.engineService.stateService
            .preview.value()
        
        UIView.animate(animated ? 0.3 : 0) {
            let transform = preview == false ? CGAffineTransform(translationX: CGFloat(bias.x), y: CGFloat(bias.y)) : .identity
            self.view.transform = transform
            self.timesetManger.engineService.set(bias: CGFloat(bias.x))
        }
    }
    
    func displayButtonPanel() {
        UIView.animate(CATransaction.animationDuration()) {
            self.buttonPanel.components.createButton.component.alpha = 1
        }
    }
}

extension MainTaskViewController: ProfileTransitionFrom {
    var userImage: UIView {
        return self.buttonPanel.components.profileContainerView.component
    }
}
