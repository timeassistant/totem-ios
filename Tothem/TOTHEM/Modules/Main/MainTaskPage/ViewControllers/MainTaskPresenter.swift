//
//  MainTaskPresenter.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import RxSwift

enum MainTaskViewState {
    case activityIndicatorVisible
    case activityIndicatorHidden
}

enum MainTaskViewEvent {
    case viewDidLoad
}

enum MainTaskUseCaseResult {
    case photoUploading
    case photoUploaded
}

final class MainTaskPresenter {
    var state: Observable<MainTaskViewState>
    var events = PublishSubject<MainTaskViewEvent>()

    init() {
        let result = events.flatMap { event -> Observable<MainTaskUseCaseResult> in
            switch event {
            case .viewDidLoad:
                return UploadPictureUseCase.shared
                    .uploadNew()
                    .map({ $0 ? .photoUploading : .photoUploaded })
            }
        }

        state = result.scan(.activityIndicatorHidden) { _, result in
            switch result {
            case .photoUploading: return .activityIndicatorVisible
            case .photoUploaded: return .activityIndicatorHidden
            }
        }
    }
}
