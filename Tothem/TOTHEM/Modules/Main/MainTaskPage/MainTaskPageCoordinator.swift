//
//  MainTaskPageCoordinator.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import UIKit
import RxSwift

class MainTaskPageCoordinator: CoordinatorProtocol {
    var initialController = StoryboardScene.Main.mainTaskViewController.instantiate()
    var navigationController: UINavigationController!

    required init() {
        self.bind()
    }

    func bind() {
        self.initialController
            .viewModel
            .openProfileController
            .observeOn(MainScheduler.asyncInstance)
            .skip(1)
            .subscribe(onNext: openUserProfileController)
            .disposed(by: initialController.dispose)
    }

    func openUserProfileController() {
        initialController.profileTransition = ProfileTransition()
        let userProfileControler = UserProfileController()
        userProfileControler.transitioningDelegate = initialController.profileTransition
        userProfileControler.viewModel
            .login
            .do(onNext: openLogin)
            .subscribe()
            .disposed(by: initialController.dispose)
        userProfileControler.viewModel
            .openFormulasList
            .do(onNext: openFormulasList)
            .subscribe()
            .disposed(by: initialController.dispose)
        userProfileControler.viewModel
            .openDataSourcesSettings
            .do(onNext: openDataSourcesList)
            .subscribe()
            .disposed(by: initialController.dispose)
        userProfileControler.viewModel
            .openDesignTestCase
            .do(onNext: openDesignTestCase)
            .subscribe()
            .disposed(by: initialController.dispose)

        initialController.present(userProfileControler, animated: true, completion: nil)
    }

    func openFormulasList() {
        let controller = FormulasViewController()
        initialController.presentedViewController?.present(controller, animated: true, completion: nil)
        controller.viewModel
            .dismiss
            .do(onNext: { controller.dismiss(animated: true, completion: nil) })
            .subscribe()
            .disposed(by: initialController.dispose)
        controller.viewModel
            .select
            .do(onNext: { self.preview(from: controller, formula: $0) })
            .subscribe()
            .disposed(by: initialController.dispose)
    }

    func openDataSourcesList() {
        let controller = DataSourcesViewController()
        initialController.presentedViewController?.present(controller, animated: true, completion: nil)
        controller.viewModel
            .dismiss
            .do(onNext: { controller.dismiss(animated: true, completion: nil) })
            .subscribe()
            .disposed(by: initialController.dispose)
        controller.viewModel
            .select
            .do(onNext: { self.preview(from: controller, dataSource: $0) })
            .subscribe()
            .disposed(by: initialController.dispose)
    }

    func openDesignTestCase(for testCase: DesignModeTestCase) {
        let controller: UIViewController
        let viewModel: ViewModelBase
        switch testCase {
        case .geolocation:
            let geolocationViewController = GeolocationRawPresentationViewController()
            controller = geolocationViewController
            viewModel = geolocationViewController.viewModel
        case .geolocationDeviation:
            let deviationViewController = GeolocationDeviationViewController()
            controller = deviationViewController
            viewModel = deviationViewController.viewModel
        case .geolocationTrial:
            let trialViewController = GeolocationTrialViewController()
            controller = trialViewController
            viewModel = trialViewController.viewModel
        case .brightness:
            let brightnessViewController = LightRawPresentationViewController()
            controller = brightnessViewController
            viewModel = brightnessViewController.viewModel
        case .brightnessDual:
            let brightnessViewController = LightDualPresentationViewController()
            controller = brightnessViewController
            viewModel = brightnessViewController.viewModel
        case .brightnessTrial:
            let brightnessViewController = LightTrialPresentationViewController()
            controller = brightnessViewController
            viewModel = brightnessViewController.viewModel
        case .stabilizationXY:
            let viewController = StabilizationXYViewController()
            controller = viewController
            viewModel = viewController.viewModel
        default:
            return
        }

        weak var viewController = controller
        initialController.presentedViewController?.present(controller, animated: true, completion: nil)
        
        viewModel
            .dismiss
            .do(onNext: {
                viewController?.dismiss(animated: true, completion: nil)
            })
            .subscribe()
            .disposed(by: initialController.dispose)
    }

    func preview(from controller: UIViewController, formula: ConfigurationUnitKey) {
        let previewController = FormulaPreviewViewController(forFormulaKey: formula)
        previewController
            .viewModel
            .dismiss
            .do(onNext: {
                controller.dismiss(animated: true, completion: nil)
            })
            .subscribe()
            .disposed(by: previewController.disposeBag)

        controller.present(previewController, animated: true, completion: nil)
    }

    func preview(from controller: UIViewController, dataSource: DataSourceDynamic) {
        let previewController = DataSourceSettingsViewController(forDataSource: dataSource)
        previewController.viewModel
            .dismiss
            .do(onNext: {
                controller.dismiss(animated: true, completion: nil)
            })
            .subscribe()
            .disposed(by: previewController.disposeBag)
        controller.present(previewController, animated: true, completion: nil)
    }

    func openLogin() {
        let authCoordinator = StoryboardScene.Authentication.authStartViewController.instantiate()
        initialController.presentedViewController?.present(authCoordinator, animated: true, completion: nil)
    }
}
