//
//  GeolocationDeviationViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

class GeolocationTrialViewModel: ViewModelBase {
    private let configurator = MovingConfigurator.shared

    private let minCircleSize = Observable<Number>.just(50)
    private let originalCircleSize = Observable<Number>.just(150)
    private let maxCircleSize = Observable<Number>.just(250)

    var isManual: Observable<Bool> {
        return configurator.isManual
    }

    var walkingSpeedValue: Observable<Number> {
        return configurator.walkingSpeedValue
    }

    var walkingSpeedMinValue: Observable<Number> {
        return configurator.walkingSpeedMinValue
    }

    var walkingSpeedMaxValue: Observable<Number> {
        return configurator.walkingSpeedMaxValue
    }
    
    var drivingSpeedValue: Observable<Number> {
        return configurator.drivingSpeedValue
    }

    var drivingSpeedMinValue: Observable<Number> {
        return configurator.drivingSpeedMinValue
    }

    var drivingSpeedMaxValue: Observable<Number> {
        return configurator.drivingSpeedMaxValue
    }
    
    var activeDataSource: Observable<ConnectorParameter> {
        return configurator.activeDataSource
    }

    var straightScale: Observable<Number> {
        return Observable.combineLatest(originalCircleSize, maxCircleSize)
            .map({ RangeParameter(min: $0.0, max: $0.1) })
            .flatMap(configurator.normalize)
    }

    var oppositeScale: Observable<Number> {
        return Observable.combineLatest(originalCircleSize, minCircleSize)
            .map({ RangeParameter(min: $0.0, max: $0.1) })
            .flatMap(configurator.normalize)
    }

    var normalized: Observable<Number> {
        return configurator.normalizedValue.map({ $0.rounded(toPlaces: 2) })
    }

    func switchManual(to manual: Bool) {
        DataSourcesSettingsService.shared.setEnabled(dataSource: ConnectorParameter.walkingSpeed.parameterDomain, to: !manual)
    }
    
    func setStaticWalkingSpeedValue(to newValue: Number) {
        DataSourcesSettingsService.shared.setStaticValue(dataSource: ConnectorParameter.walkingSpeed.rawValue, to: newValue)
    }
    
    func setStaticDrivingSpeedValue(to newValue: Number) {
        DataSourcesSettingsService.shared.setStaticValue(dataSource: ConnectorParameter.drivingSpeed.rawValue, to: newValue)
    }
}
