//
//  GeolocationDeviationViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

class GeolocationTrialViewController: UIViewController {

    var disposeBag = DisposeBag()

    private let minCircleSize: Double = 50.0
    private let originalCircleSize: Double = 150.0
    private let maxCircleSize: Double = 250.0

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var rawDataLabel: UILabel!
    @IBOutlet weak var normalizedDataValue: UILabel!
    @IBOutlet weak var manualSwitch: UISwitch!

    @IBOutlet weak var minScaleCircle: UIView!
    @IBOutlet weak var oneFourthScaleCircle: UIView!
    @IBOutlet weak var oneSecondScaleCircle: UIView!
    @IBOutlet weak var threeFourthScaleCircle: UIView!
    @IBOutlet weak var maxScaleCircle: UIView!

    @IBOutlet weak var straightDynamicCircle: UIView!
    @IBOutlet weak var straightDynamicCircleWidth: NSLayoutConstraint!
    @IBOutlet weak var straightDynamicCircleHeight: NSLayoutConstraint!

    @IBOutlet weak var oppositeDynamicCircle: UIView!
    @IBOutlet weak var oppositeDynamicCircleWidth: NSLayoutConstraint!
    @IBOutlet weak var oppositeDynamicCircleHeight: NSLayoutConstraint!

    @IBOutlet weak var staticCircle: UIView!
    
    @IBOutlet weak var walkingSpeedValueSlider: UISlider!
    @IBOutlet weak var minWalkingSpeedLabel: UILabel!
    @IBOutlet weak var maxWalkingSpeedLabel: UILabel!

    @IBOutlet weak var drivingSpeedValueSlider: UISlider!
    @IBOutlet weak var minDrivingSpeedLabel: UILabel!
    @IBOutlet weak var maxDrivingSpeedLabel: UILabel!

    private let dynamicCircleRestColor = UIColor(red: 78/255, green: 217/255, blue: 162/255, alpha: 1.0)
    private let dynamicCircleStraightDeviationColor = UIColor(red: 204/255, green: 69/255, blue: 54/255, alpha: 1.0)
    private let dynamicCircleOppositeDeviationColor = UIColor(red: 78/255, green: 156/255, blue: 214/255, alpha: 1.0)

    let viewModel: GeolocationTrialViewModel

    init() {
        self.viewModel = GeolocationTrialViewModel()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("No dataSource passed")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureScaleCircles()
        
        backButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        viewModel.isManual
            .take(1)
            .bind(to: manualSwitch.rx.isOn)
            .disposed(by: disposeBag)

        manualSwitch.rx.isOn
            .skip(1)
            .subscribe(onNext: { [weak self] enabled in
                self?.viewModel.switchManual(to: enabled)
            })
            .disposed(by: disposeBag)

        Observable.combineLatest(
            viewModel.isManual,
            viewModel.activeDataSource
            )
            .subscribe(onNext: { [weak self] (isManual, source) in
                self?.setThumbForWalkingSpeedSlider(isActive: isManual && (source == .walkingSpeed))
                self?.setThumbForDrivingSpeedSlider(isActive: isManual && (source == .drivingSpeed))
            })
            .disposed(by: disposeBag)

        configureSpeedValueLabel()
        configureWalkingSpeedData()
        configureDrivingSpeedData()
        configureCircleAndNormalizedValue()
    }

    private func setThumbForWalkingSpeedSlider(isActive: Bool) {
        guard let thumbImage = UIImage(named: isActive ? "walkingPerson" : "walkingPersonGrayBackground") else { return }
        walkingSpeedValueSlider.setThumbImage(thumbImage, for: .normal)
        walkingSpeedValueSlider.setThumbImage(thumbImage, for: .highlighted)
    }

    private func setThumbForDrivingSpeedSlider(isActive: Bool) {
        guard let thumbImage = UIImage(named: isActive ? "drivenCar" : "drivenCarGrayBackground") else { return }
        drivingSpeedValueSlider.setThumbImage(thumbImage, for: .normal)
        drivingSpeedValueSlider.setThumbImage(thumbImage, for: .highlighted)
    }

    private func configureScaleCircles() {
        let edgeValueCircleColor = UIColor.white
        let mediumCircleColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1.0)
        let scaleCircleColor = UIColor(red: 60/255, green: 60/255, blue: 60/255, alpha: 1.0)

        minScaleCircle.becomeRounded(borderWidth: 1, borderColor: edgeValueCircleColor.cgColor)
        oneFourthScaleCircle.becomeRounded(borderWidth: 1, borderColor: scaleCircleColor.cgColor)
        oneSecondScaleCircle.becomeRounded(borderWidth: 1, borderColor: mediumCircleColor.cgColor)
        threeFourthScaleCircle.becomeRounded(borderWidth: 1, borderColor: scaleCircleColor.cgColor)
        maxScaleCircle.becomeRounded(borderWidth: 1, borderColor: edgeValueCircleColor.cgColor)
        straightDynamicCircle.becomeRounded(borderWidth: 3, borderColor: dynamicCircleRestColor.cgColor)
        oppositeDynamicCircle.becomeRounded(borderWidth: 3, borderColor: dynamicCircleRestColor.cgColor)
        staticCircle.becomeRounded(borderWidth: 3, borderColor: dynamicCircleRestColor.cgColor)
    }

    private func configureWalkingSpeedData() {
        viewModel.isManual
            .bind(to: walkingSpeedValueSlider.rx.isEnabled)
            .disposed(by: disposeBag)

        let initialSliderValueObservable = viewModel.walkingSpeedValue
            .take(1)
            .map { Float($0) }

        let viewModelSliderValueObservable = Observable.combineLatest(
            viewModel.isManual,
            viewModel.walkingSpeedValue
            )
            .filter { !$0.0 }
            .map { Float($0.1) }

        let manualSliderValueObservable = Observable.combineLatest(
            viewModel.isManual,
            walkingSpeedValueSlider.rx.value
            )
            .skip(1)
            .filter { $0.0 }
            .throttle(0.3, scheduler: MainScheduler.instance)
            .map { Double($0.1) }

        let currentValueObservable = Observable.merge(
            initialSliderValueObservable,
            viewModelSliderValueObservable,
            manualSliderValueObservable.map { Float($0) }
        )

        Observable.combineLatest(
            viewModel.walkingSpeedMinValue.map { Float($0) },
            viewModel.walkingSpeedMaxValue.map { Float($0) }
            )
            .withLatestFrom(currentValueObservable) { (data, currentValue) -> (Float, Float, Float) in
                return (min: data.0, max: data.1, current: currentValue)
            }
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] data in
                self?.walkingSpeedValueSlider.minimumValue = data.0
                self?.minWalkingSpeedLabel.text = "\(Double(data.0).rounded(toPlaces: 1))"
                self?.walkingSpeedValueSlider.maximumValue = data.1
                self?.maxWalkingSpeedLabel.text = "\(Double(data.1).rounded(toPlaces: 1))"
                self?.walkingSpeedValueSlider.value = data.2
            })
            .disposed(by: disposeBag)

        Observable.combineLatest(
            viewModel.isManual,
            viewModel.walkingSpeedValue
            )
            .filter { !$0.0 }
            .map { Float($0.1) }
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: walkingSpeedValueSlider.rx.value)
            .disposed(by: disposeBag)

        manualSliderValueObservable
            .distinctUntilChanged {
                $0.rounded(toPlaces: 2) == $1.rounded(toPlaces: 2)
            }
            .skip(1)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] newValue in
                self?.viewModel.setStaticWalkingSpeedValue(to: newValue)
                self?.adaptDrivingSpeedSliderToWalkingModeManually()
            })
            .disposed(by: disposeBag)
    }

    private func configureDrivingSpeedData() {
        viewModel.isManual
            .bind(to: drivingSpeedValueSlider.rx.isEnabled)
            .disposed(by: disposeBag)
        let initialSliderValueObservable = viewModel.drivingSpeedValue
            .take(1)
            .map { Float($0) }
        let viewModelSliderValueObservable = Observable.combineLatest(viewModel.isManual, viewModel.drivingSpeedValue)
            .filter { !$0.0 }
            .map { Float($0.1) }
        let manualSliderValueObservable = Observable.combineLatest(viewModel.isManual, drivingSpeedValueSlider.rx.value)
            .skip(1)
            .filter { $0.0 }
            .throttle(0.3, scheduler: MainScheduler.instance)
            .map { Double($0.1) }

        let currentValueObservable = Observable.merge(
            initialSliderValueObservable,
            viewModelSliderValueObservable,
            manualSliderValueObservable.map { Float($0) }
        )

        Observable.combineLatest(
            viewModel.drivingSpeedMinValue.map { Float($0) },
            viewModel.drivingSpeedMaxValue.map { Float($0) }
            )
            .withLatestFrom(currentValueObservable) { (data, currentValue) -> (Float, Float, Float) in
                return (min: data.0, max: data.1, current: currentValue)
            }
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] data in
                self?.drivingSpeedValueSlider.minimumValue = data.0
                self?.minDrivingSpeedLabel.text = "\(Double(data.0).rounded(toPlaces: 1))"
                self?.drivingSpeedValueSlider.maximumValue = data.1
                self?.maxDrivingSpeedLabel.text = "\(Double(data.1).rounded(toPlaces: 1))"
                self?.drivingSpeedValueSlider.value = data.2
            })
            .disposed(by: disposeBag)

        Observable.combineLatest(viewModel.isManual, viewModel.drivingSpeedValue)
            .filter { !$0.0 }
            .map { Float($0.1) }
            .bind(to: drivingSpeedValueSlider.rx.value)
            .disposed(by: disposeBag)

        manualSliderValueObservable
            .distinctUntilChanged { $0.rounded(toPlaces: 2) == $1.rounded(toPlaces: 2) }
            .skip(1)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] newValue in
                self?.viewModel.setStaticDrivingSpeedValue(to: newValue)
                self?.adaptWalkingSpeedSliderToDrivingModeManually()
            })
            .disposed(by: disposeBag)
    }

    private func adaptWalkingSpeedSliderToDrivingModeManually() {
        walkingSpeedValueSlider.setValue(walkingSpeedValueSlider.minimumValue, animated: true)
    }

    private func adaptDrivingSpeedSliderToWalkingModeManually() {
        drivingSpeedValueSlider.setValue(drivingSpeedValueSlider.minimumValue, animated: true)
    }

    private func configureSpeedValueLabel() {
        let manuallySetInitialValueObservable = viewModel.isManual
            .take(1)
            .withLatestFrom(viewModel.activeDataSource)
            .flatMapLatest { [weak self] source -> Observable<Double> in
                guard let strongSelf = self else { return Observable.never() }
                if source == .walkingSpeed {
                    return strongSelf.viewModel.walkingSpeedValue.take(1)
                } else {
                    return strongSelf.viewModel.drivingSpeedValue.take(1)
                }
        }
        let dynamicValueObservable = Observable.merge(viewModel.walkingSpeedValue, viewModel.drivingSpeedValue)
            .skip(2)

        Observable.merge(manuallySetInitialValueObservable, dynamicValueObservable)
            .map { "Speed: \($0.rounded(toPlaces: 2))" }
            .bind(to: rawDataLabel.rx.text)
            .disposed(by: disposeBag)
    }

    private func setStraight(_ doubleSize: Double) {
        DispatchQueue.main.async {
            let size = CGFloat(doubleSize)
            UIView.animate(withDuration: CATransaction.animationDuration(), delay: 0.0, options: [UIView.AnimationOptions.curveLinear], animations: {
                self.straightDynamicCircleWidth.constant = size
                self.straightDynamicCircleHeight.constant = size
                self.view.layoutIfNeeded()
                self.straightDynamicCircle.layer.cornerRadius = size / 2
                self.straightDynamicCircle.layer.borderColor = (size == 150)
                    ? self.dynamicCircleRestColor.cgColor
                    : self.dynamicCircleStraightDeviationColor.cgColor
            }, completion: nil)
        }
    }

    private func setOpposite(_ doubleSize: Double) {
        DispatchQueue.main.async {
            let size = CGFloat(doubleSize)
            UIView.animate(withDuration: CATransaction.animationDuration(), delay: 0.0, options: [UIView.AnimationOptions.curveLinear], animations: {
                self.oppositeDynamicCircleWidth.constant = size
                self.oppositeDynamicCircleHeight.constant = size
                self.view.layoutIfNeeded()
                self.oppositeDynamicCircle.layer.cornerRadius = size / 2

                self.oppositeDynamicCircle.layer.borderColor = (size == 150)
                    ? self.dynamicCircleRestColor.cgColor
                    : self.dynamicCircleOppositeDeviationColor.cgColor
            }, completion: nil)
        }
    }

    private func configureCircleAndNormalizedValue() {
        self.viewModel
            .straightScale
            .subscribe(onNext: setStraight)
            .disposed(by: disposeBag)

        self.viewModel
            .oppositeScale
            .subscribe(onNext: setOpposite)
            .disposed(by: disposeBag)

        self.viewModel
            .normalized
            .map { "Normalized: \($0)" }
            .bind(to: normalizedDataValue.rx.text)
            .disposed(by: disposeBag)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
