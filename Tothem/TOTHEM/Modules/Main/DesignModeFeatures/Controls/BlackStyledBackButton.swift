//
//  BlackStyledBackButton.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

class BlackStyledBackButton: UIButton {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var fillerView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("BlackStyledBackButton", owner: self, options: nil)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.fill(in: self)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        fillerView.layer.borderWidth = 2
        fillerView.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
        fillerView.layer.cornerRadius = contentView.frame.width / 2
    }

}
