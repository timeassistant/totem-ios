//
//  GeolocationDeviationViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

class GeolocationDeviationViewModel: ViewModelBase {

    private let walkingDataSource: ConnectorParameter
    private let drivingDataSource: ConnectorParameter

    let isManual: Observable<Bool>

    let walkingSpeedValue: Observable<Double>
    let walkingSpeedMinValue: Observable<Double>
    let walkingSpeedMaxValue: Observable<Double>

    let drivingSpeedValue: Observable<Double>
    let drivingSpeedMinValue: Observable<Double>
    let drivingSpeedMaxValue: Observable<Double>

    let typeLastlyChanhed: Observable<String>

    override init() {
        let walkingSpeedDataSource = ConnectorParameter.walkingSpeed

        self.isManual = DataSourcesSettingsService.shared.observableIsDynamic(for: walkingSpeedDataSource)
            .map { !$0 }

        self.walkingDataSource = walkingSpeedDataSource
        self.walkingSpeedValue = walkingSpeedDataSource.normalizedValueObservable
            .map { $0.asDouble() }
        self.walkingSpeedMinValue = DataSourceManager.shared.minValues
            .map { $0[walkingSpeedDataSource] }
            .skipNil()
            .map { $0.asDouble() }
        self.walkingSpeedMaxValue = DataSourceManager.shared.maxValues
            .map { $0[walkingSpeedDataSource] }
            .skipNil()
            .map { $0.asDouble() }

        let drivingSpeedDataSource = ConnectorParameter.drivingSpeed
        self.drivingDataSource = drivingSpeedDataSource
        self.drivingSpeedValue = drivingSpeedDataSource.normalizedValueObservable
            .map { $0.asDouble() }
        self.drivingSpeedMinValue = DataSourceManager.shared.minValues
            .map { $0[drivingSpeedDataSource] }
            .skipNil()
            .map { $0.asDouble() }
        self.drivingSpeedMaxValue = DataSourceManager.shared.maxValues
            .map { $0[drivingSpeedDataSource] }
            .skipNil()
            .map { $0.asDouble() }

        self.typeLastlyChanhed = DataSourcesSettingsService.shared.observableGeolocationLastlyChanged()
    }

    func switchManual(to manual: Bool) {
        DataSourcesSettingsService.shared.setEnabled(dataSource: walkingDataSource.parameterDomain,
                                                     to: !manual)
    }

    func setStaticWalkingSpeedValue(to newValue: Double) {
        DataSourcesSettingsService.shared.setStaticValue(dataSource: walkingDataSource.rawValue,
                                                         to: newValue)
    }

    func setStaticDrivingSpeedValue(to newValue: Double) {
        DataSourcesSettingsService.shared.setStaticValue(dataSource: drivingDataSource.rawValue,
                                                         to: newValue)
    }
}
