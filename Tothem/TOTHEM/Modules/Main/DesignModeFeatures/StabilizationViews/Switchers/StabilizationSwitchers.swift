//
//  StabilizationSwitchers.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class StabilizationSwitchers: XibSubview {
    let disposeBag = DisposeBag()

    @IBOutlet weak var syncButton: UIButton!
    @IBOutlet weak var xButton: UIButton!
    @IBOutlet weak var yButton: UIButton!

    @IBOutlet weak var linearButton: UIButton!
    @IBOutlet weak var parabolicButton: UIButton!

    @IBOutlet weak var animatedSwitch: UISwitch!

    override func configureView() {
        super.configureView()

        Observable.combineLatest(
            SettingsRepository.shared.isRoll,
            SettingsRepository.shared.isPitch,
            SettingsRepository.shared.isYaw)
            .map({ !$0 && !$1 && !$2 })
            .take(1)
            .bind(to: syncButton.rx.isSelected)
            .disposed(by: disposeBag)

        SettingsRepository.shared.isRoll
            .take(1)
            .bind(to: xButton.rx.isSelected)
            .disposed(by: disposeBag)

        SettingsRepository.shared.isPitch
            .take(1)
            .bind(to: yButton.rx.isSelected)
            .disposed(by: disposeBag)

        SettingsRepository.shared.function
            .take(1)
            .map({ $0 == 1 })
            .bind(to: linearButton.rx.isSelected)
            .disposed(by: disposeBag)

        SettingsRepository.shared.function
            .take(1)
            .map({ $0 == 0 })
            .bind(to: parabolicButton.rx.isSelected)
            .disposed(by: disposeBag)

        SettingsRepository.shared.isAnimated
            .take(1)
            .bind(to: animatedSwitch.rx.isOn)
            .disposed(by: disposeBag)

        animatedSwitch.rx.isOn
            .subscribe(onNext: {
                SettingsRepository.shared.isAnimated.on(.next($0))
            })
            .disposed(by: disposeBag)

    }

    @IBAction func syncButtonTapped(_ sender: Any) {
        guard !syncButton.isSelected else { return }
        syncButton.isSelected = true
        xButton.isSelected = false
        yButton.isSelected = false

        SettingsRepository.shared.isRoll.on(.next(false))
        SettingsRepository.shared.isPitch.on(.next(false))
        SettingsRepository.shared.isYaw.on(.next(false))
    }

    @IBAction func xButtonTapped(_ sender: Any) {
        guard !xButton.isSelected else { return }
        syncButton.isSelected = false
        xButton.isSelected = true
        yButton.isSelected = false

        SettingsRepository.shared.isRoll.on(.next(true))
        SettingsRepository.shared.isPitch.on(.next(false))
        SettingsRepository.shared.isYaw.on(.next(false))
    }

    @IBAction func yButtonTapped(_ sender: Any) {
        guard !yButton.isSelected else { return }
        syncButton.isSelected = false
        xButton.isSelected = false
        yButton.isSelected = true

        SettingsRepository.shared.isRoll.on(.next(false))
        SettingsRepository.shared.isPitch.on(.next(true))
        SettingsRepository.shared.isYaw.on(.next(false))
    }

    @IBAction func linearButtonTapped(_ sender: Any) {
        guard !linearButton.isSelected else { return }
        linearButton.isSelected = true
        parabolicButton.isSelected = false
        SettingsRepository.shared.function.on(.next(1))
    }

    @IBAction func parabolicButtonTapped(_ sender: Any) {
        guard !parabolicButton.isSelected else { return }
        linearButton.isSelected = false
        parabolicButton.isSelected = true
        SettingsRepository.shared.function.on(.next(0))
    }

}
