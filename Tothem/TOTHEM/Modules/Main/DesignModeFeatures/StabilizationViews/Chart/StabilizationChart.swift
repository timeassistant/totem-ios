//
//  StabilizationChart.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

final class StabilizationChart: XibSubview, ChartViewDataSource {
    @IBOutlet weak var amplifierLabel: UILabel!
    @IBOutlet weak var dampingLabel: UILabel!
    @IBOutlet weak var reactionLabel: UILabel!
    @IBOutlet weak var biasYRaw: UILabel!
    @IBOutlet weak var biasXRaw: UILabel!

    @IBOutlet weak var positionerXView: UIView!
    @IBOutlet weak var positionerYView: UIView!

    @IBOutlet weak var chartView: ChartView!
    
    private var parameters: StabilizationParameters = .default
    private var function: StabilizationFunction = .parabolic

    override func configureView() {
        super.configureView()
        chartView.dataSource = self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        dy = 1 - chartView.bounds.height / 2
        asixXdy = 1 - chartView.bounds.height / 2
        chartView.setNeedsDisplay()
    }

    func setParameters(_ value: StabilizationParameters) {
        parameters = value
        chartView.setNeedsDisplay()
    }

    func setFunction(_ value: StabilizationFunction) {
        function = value
        chartView.setNeedsDisplay()
    }

    func setBias(_ bias: (Vector3, Vector3)) {
        biasXRaw.text = "\(Int(CGFloat(bias.1.x)))"
        biasYRaw.text = "\(Int(CGFloat(bias.1.y)))"
        positionerXView.transform = CGAffineTransform(translationX: CGFloat(bias.0.x) * scale, y: 0)
        positionerYView.transform = CGAffineTransform(translationX: CGFloat(bias.0.y) * scale, y: 0)
    }

    func set(amplifier: CGFloat) {
        amplifierLabel.text = "\(amplifier.rounded(toPlaces: 2))"
        chartView.setNeedsDisplay()
    }

    func set(damping: CGFloat) {
        dampingLabel.text = "\(damping.rounded(toPlaces: 2))"
        chartView.setNeedsDisplay()
    }

    func set(reaction: CGFloat) {
        reactionLabel.text = "\(reaction.rounded(toPlaces: 2))"
        chartView.setNeedsDisplay()
    }
    
    // MARK: Chart Data Source

    var dx: CGFloat = 0
    var dy: CGFloat = -100
    var asixYdx: CGFloat = 0
    var asixXdy: CGFloat = -100

    var scale: CGFloat {
        return chartView.bounds.width / 2
    }

    var min: CGFloat {
        return -1 * scale
    }

    var max: CGFloat {
        return 1 * scale
    }

    var step: CGFloat = 1

    func y(x: CGFloat) -> CGFloat {
        let params = parameters
        let x = Double(x / scale)
        let y = CGFloat(StabilizationConfigurator.shared.normalize(x, params, function))
        return abs(y * scale)
    }
}
