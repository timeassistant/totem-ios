//
//  StabilizationXYViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class StabilizationXYViewController: UIViewController {
    var disposeBag = DisposeBag()

    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!

    @IBOutlet weak var maxView: UIView!
    @IBOutlet weak var maxViewHeight: NSLayoutConstraint!
    @IBOutlet weak var maxViewWidth: NSLayoutConstraint!

    @IBOutlet weak var chart: StabilizationChart!
    @IBOutlet weak var chartX: NSLayoutConstraint!
    @IBOutlet weak var chartY: NSLayoutConstraint!

    @IBOutlet weak var switchers: StabilizationSwitchers!
    @IBOutlet weak var switchersX: NSLayoutConstraint!
    @IBOutlet weak var switchersY: NSLayoutConstraint!

    @IBOutlet weak var sliders: StabilizationSliders!
    @IBOutlet weak var slidersX: NSLayoutConstraint!
    @IBOutlet weak var slidersY: NSLayoutConstraint!

    let viewModel: StabilizationXYViewModel

    init() {
        self.viewModel = StabilizationXYViewModel()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("No dataSource passed")
    }

    deinit {
        print("deinit")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        switchersX.constant = UIScreen.main.bounds.width / 2 * -1
        switchersY.constant = UIScreen.main.bounds.height / 2
        slidersX.constant = 2 * UIScreen.main.bounds.width / 3
        slidersY.constant = 2 * UIScreen.main.bounds.width / 3
        chartY.constant = UIScreen.main.bounds.height / 2  * -1

        view.rx.panGesture()
            .subscribe(onNext: { [weak self] gesture in
                self?.pan(gesture)
            })
            .disposed(by: disposeBag)

        backButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        [maxView, view1, view2]
            .forEach {
            $0?.layer.masksToBounds = false
            $0?.layer.cornerRadius = 10
        }

        maxView.layer.borderWidth = 1

        viewModel.parameters
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.chart.setParameters($0)
            })
            .disposed(by: disposeBag)

        viewModel.function
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.chart.setFunction($0)
            })
            .disposed(by: disposeBag)

        viewModel.adaptation.background_color
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.setBackground($0)
            })
            .disposed(by: disposeBag)

       viewModel.timesetAdaptation.maxBias
        .observeOn(MainScheduler.asyncInstance)
        .subscribe(onNext: { [weak self] in
            self?.setMax($0)
        })
        .disposed(by: disposeBag)

        viewModel.timesetAdaptation.bias
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.setBias($0)
            })
            .disposed(by: disposeBag)

        setupAmplifier()
        setupDamping()
        setupReaction()
    }

    private func setBackground(_ color: UIColor) {
        maxView.layer.borderColor = color.cgColor
        view1.backgroundColor = color
        view2.backgroundColor = color
    }

    private func setMax(_ bias: CGRect) {
        let size = contentView.bounds.size
        maxViewHeight.constant = bias.maxY * 2 + size.height
        maxViewWidth.constant = bias.maxX * 2 + size.width
    }

    private func setupAmplifier() {
        viewModel.amplifier
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sliders.amplifierSlider.value = Float($0)
                self?.chart.set(amplifier: CGFloat($0))
            })
            .disposed(by: disposeBag)

        sliders.amplifierSlider.rx.value.asObservable()
            .map({ Double($0) })
            .skip(1)
            .subscribe(onNext: { [weak self] in
                self?.viewModel.set(amplifier: $0)
            })
            .disposed(by: disposeBag)
    }

    private func setupDamping() {
        viewModel.damping
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sliders.dampingSlider.value = Float($0)
                self?.chart.set(damping: CGFloat($0))
            })
            .disposed(by: disposeBag)

        sliders.dampingSlider.rx.value.asObservable()
            .map({ Double($0) })
            .skip(1)
            .subscribe(onNext: { [weak self] in
                self?.viewModel.set(damping: $0)
            })
            .disposed(by: disposeBag)
    }

    private func setupReaction() {
        viewModel.reaction
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.sliders.reactionSlider.value = Float($0)
                self?.chart.set(reaction: CGFloat($0))
            })
            .disposed(by: disposeBag)

        sliders.reactionSlider.rx.value.asObservable()
            .map({ Double($0) })
            .skip(1)
            .subscribe(onNext: { [weak self] in
                self?.viewModel.set(reaction: $0)
            })
            .disposed(by: disposeBag)
    }

    private func setBias(_ bias: (Vector3, Vector3)) {
        let animated = (try? SettingsRepository.shared.isAnimated.value()) ?? true

        UIView.animate(withDuration: animated ? CATransaction.animationDuration() : 0, animations: {
            self.chart.setBias(bias)
            self.contentView.transform = CGAffineTransform(translationX: CGFloat(bias.1.x), y: CGFloat(bias.1.y))
        })
    }

    var panningView: UIView?
    var initialCenter: CGPoint = .zero

    private func pan(_ gesture: UIPanGestureRecognizer) {
        if gesture.state == .began {
            panningView = nil
            let location = gesture.location(in: view)
            if switchers.frame.contains(location) {
                panningView = switchers
                initialCenter.x = switchersX.constant
                initialCenter.y = switchersY.constant
            } else if sliders.frame.contains(location) {
                panningView = sliders
                initialCenter.x = slidersX.constant
                initialCenter.y = slidersY.constant
            } else if chart.frame.contains(location) {
                panningView = chart
                initialCenter.x = chartX.constant
                initialCenter.y = chartY.constant
            }
            return
        }

        guard let panningView = panningView else { return }

        if gesture.state == .changed {
            let tr = gesture.translation(in: view)
            panningView.transform = CGAffineTransform(translationX: tr.x, y: tr.y)
        }

        if gesture.state == .ended {
            let tr = gesture.translation(in: view)
            panningView.transform = .identity
            if panningView == chart {
                chartX.constant += tr.x
                chartY.constant += tr.y
            } else if panningView == switchers {
                switchersX.constant += tr.x
                switchersY.constant += tr.y
            } else if panningView == sliders {
                slidersX.constant += tr.x
                slidersY.constant += tr.y
            }
        }
    }
}
