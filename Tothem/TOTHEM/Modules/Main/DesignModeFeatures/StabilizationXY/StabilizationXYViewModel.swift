//
//  StabilizationXYViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift

final class StabilizationXYViewModel: ViewModelBase {
    let adaptation = CellAdaptation(side: .center, checked: false)
    let timesetAdaptation = TimesetAdaptation.shared
    var parameters: Observable<StabilizationParameters>!
    var function: Observable<StabilizationFunction>!

    var reaction: Observable<Number>!
    var amplifier: Observable<Number>!
    var damping: Observable<Number>!
    let settings = PublishSubject<()>()

    override init() {
        super.init()

        function = SettingsRepository.shared.function
            .map({ StabilizationFunction(rawValue: $0) ?? .parabolic })

        parameters = Observable.combineLatest(
            SettingsRepository.shared.isRoll,
            SettingsRepository.shared.isPitch,
            SettingsRepository.shared.isYaw)
            .flatMap({ (roll, pitch, yaw) -> Observable<StabilizationParameters> in
                if roll {
                    return SettingsRepository.shared.attitude_roll
                } else if pitch {
                    return SettingsRepository.shared.attitude_pitch
                } else if yaw {
                    return SettingsRepository.shared.attitude_yaw
                } else {
                    return SettingsRepository.shared.attitude
                }
            })

        reaction = parameters.map({ $0.reaction })
        amplifier = parameters.map({ $0.amplifier })
        damping = parameters.map({ $0.damping })
    }

    func set(reaction: Double) {
        let value = reaction
        if let isX = try? SettingsRepository.shared.isRoll.value(),
            isX, var params = try? SettingsRepository.shared
            .attitude_roll.value() {
            params.reaction = value
            SettingsRepository.shared.attitude_roll.on(.next(params))
        } else if let isY = try? SettingsRepository.shared.isPitch.value(), isY, var params = try? SettingsRepository.shared
            .attitude_pitch.value() {
            params.reaction = value
            SettingsRepository.shared.attitude_pitch.on(.next(params))
        } else if let isZ = try? SettingsRepository.shared.isYaw.value(),
            isZ, var params = try? SettingsRepository.shared
            .attitude_yaw.value() {
            params.reaction = value
            SettingsRepository.shared.attitude_yaw.on(.next(params))
        } else if var params = try? SettingsRepository.shared
            .attitude.value() {
            params.reaction = value
            SettingsRepository.shared.attitude.on(.next(params))
        }
    }

    func set(amplifier: Double) {
        let value = amplifier
        if let isX = try? SettingsRepository.shared.isRoll.value(),
            isX, var params = try? SettingsRepository.shared
            .attitude_roll.value() {
            params.amplifier = value
            SettingsRepository.shared.attitude_roll.on(.next(params))
        } else if let isY = try? SettingsRepository.shared.isPitch.value(), isY, var params = try? SettingsRepository.shared
                .attitude_pitch.value() {
            params.amplifier = value
            SettingsRepository.shared.attitude_pitch.on(.next(params))
        } else if let isZ = try? SettingsRepository.shared.isYaw.value(),
            isZ, var params = try? SettingsRepository.shared
            .attitude_yaw.value() {
            params.amplifier = value
            SettingsRepository.shared.attitude_yaw.on(.next(params))
        } else if var params = try? SettingsRepository.shared
            .attitude.value() {
            params.amplifier = value
            SettingsRepository.shared.attitude.on(.next(params))
        }
    }

    func set(damping: Double) {
        let value = damping
        if let isX = try? SettingsRepository.shared.isRoll.value(),
            isX, var params = try? SettingsRepository.shared
            .attitude_roll.value() {
            params.damping = value
            SettingsRepository.shared.attitude_roll.on(.next(params))
        } else if let isY = try? SettingsRepository.shared.isPitch.value(), isY, var params = try? SettingsRepository.shared
            .attitude_pitch.value() {
            params.damping = value
            SettingsRepository.shared.attitude_pitch.on(.next(params))
        } else if let isZ = try? SettingsRepository.shared.isYaw.value(),
            isZ, var params = try? SettingsRepository.shared
            .attitude_yaw.value() {
            params.damping = value
            SettingsRepository.shared.attitude_yaw.on(.next(params))
        } else if var params = try? SettingsRepository.shared
            .attitude.value() {
            params.damping = value
            SettingsRepository.shared.attitude.on(.next(params))
        }
    }
}
