//
//  LightRawPresentationViewModel.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift

class LightRawPresentationViewModel: ViewModelBase {

    private let lightDataSource: ConnectorParameter

    let isManual: Observable<Bool>

    let lightValue: Observable<Double>
    let lightMinValue: Observable<Double>
    let lightMaxValue: Observable<Double>

    override init() {
        let lightDataSource = ConnectorParameter.ambientLightBrightness

        self.isManual = DataSourcesSettingsService.shared.observableIsDynamic(for: lightDataSource)
            .map { !$0 }

        self.lightDataSource = lightDataSource
        self.lightValue = lightDataSource.normalizedValueObservable
            .map { $0.asDouble() }
        self.lightMinValue = DataSourceManager.shared.minValues
            .map { $0[lightDataSource] }
            .skipNil()
            .map { $0.asDouble() }
        self.lightMaxValue = DataSourceManager.shared.maxValues
            .map { $0[lightDataSource] }
            .skipNil()
            .map { $0.asDouble() }
    }

    func switchManual(to manual: Bool) {
        DataSourcesSettingsService.shared.setEnabled(dataSource: lightDataSource.parameterDomain,
                                                     to: !manual)
    }

    func setStaticLightValue(to newValue: Double) {
        DataSourcesSettingsService.shared.setStaticValue(dataSource: lightDataSource.rawValue,
                                                         to: newValue)
    }
}
