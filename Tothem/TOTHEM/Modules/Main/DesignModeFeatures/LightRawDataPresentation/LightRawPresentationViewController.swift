//
//  LightRawPresentationViewController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

class LightRawPresentationViewController: UIViewController {

    var disposeBag = DisposeBag()

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var rawDataLabel: UILabel!
    @IBOutlet weak var normalizedDataValue: UILabel!
    @IBOutlet weak var manualSwitch: UISwitch!

    @IBOutlet weak var lightCircle: UIView!

    @IBOutlet weak var lightValueSlider: UISlider!
    @IBOutlet weak var minBrightnessLabel: UILabel!
    @IBOutlet weak var maxBrightnessLabel: UILabel!
    
    let viewModel: LightRawPresentationViewModel

    init() {
        self.viewModel = LightRawPresentationViewModel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("No dataSource passed")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        lightCircle.becomeRounded(borderWidth: 0, borderColor: UIColor.clear.cgColor)

        backButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        viewModel.isManual
            .take(1)
            .bind(to: manualSwitch.rx.isOn)
            .disposed(by: disposeBag)
        manualSwitch.rx.isOn
            .skip(1)
            .subscribe(onNext: { [weak self] enabled in
                self?.viewModel.switchManual(to: enabled)
            })
            .disposed(by: disposeBag)

        configureBrightnessValueLabel()
        configureCircleAndNormalizedValue()
        configureLightData()
    }

    private func configureLightData() {
        viewModel.isManual
            .bind(to: lightValueSlider.rx.isEnabled)
            .disposed(by: disposeBag)
        let initialSliderValueObservable = viewModel.lightValue
            .take(1)
            .map { Float($0) }
        let viewModelSliderValueObservable = Observable.combineLatest(viewModel.isManual, viewModel.lightValue)
            .filter { !$0.0 }
            .map { Float($0.1) }
        let manualSliderValueObservable = Observable.combineLatest(viewModel.isManual, lightValueSlider.rx.value)
            .skip(1)
            .filter { $0.0 }
            .throttle(0.3, scheduler: MainScheduler.instance)
            .map { Double($0.1) }
        let currentValueObservable = Observable.merge(initialSliderValueObservable, viewModelSliderValueObservable,
                                                      manualSliderValueObservable.map { Float($0) })
        Observable.combineLatest(viewModel.lightMinValue.map { Float($0) },
                                 viewModel.lightMaxValue.map { Float($0) })
            .withLatestFrom(currentValueObservable) { (data, currentValue) -> (Float, Float, Float) in
                return (min: data.0, max: data.1, current: currentValue)
            }
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] data in
                self?.lightValueSlider.minimumValue = data.0
                self?.minBrightnessLabel.text = "\(Double(data.0).rounded(toPlaces: 1))"
                self?.lightValueSlider.maximumValue = data.1
                self?.maxBrightnessLabel.text = "\(Double(data.1).rounded(toPlaces: 1))"
                self?.lightValueSlider.value = data.2
            })
            .disposed(by: disposeBag)
        Observable.combineLatest(viewModel.isManual, viewModel.lightValue)
            .filter { !$0.0 }
            .map { Float($0.1) }
            .bind(to: lightValueSlider.rx.value)
            .disposed(by: disposeBag)
        manualSliderValueObservable
            .distinctUntilChanged { $0.rounded(toPlaces: 2) == $1.rounded(toPlaces: 2) }
            .skip(1)
            .subscribe(onNext: { [weak self] newValue in
                self?.viewModel.setStaticLightValue(to: newValue)
            })
            .disposed(by: disposeBag)
    }

    private func configureBrightnessValueLabel() {
        viewModel.lightValue
            .map {
                "Brightness: \($0.rounded(toPlaces: 2))" }
            .bind(to: rawDataLabel.rx.text)
            .disposed(by: disposeBag)
    }

    private func configureCircleAndNormalizedValue() {
        let composedLightInfo = Observable.combineLatest(viewModel.lightMinValue,
                                                         viewModel.lightMaxValue,
                                                         viewModel.lightValue)
        let complexLightValueObservable = composedLightInfo
            .throttle(0.3, scheduler: MainScheduler.instance)
            .share(replay: 1, scope: .forever)
        complexLightValueObservable
            .map { UIColor(white: CGFloat($0.2), alpha: 1.0) }
            .subscribe(onNext: { [weak self] color in
                guard let strongSelf = self else { return }
                UIView.animate(withDuration: CATransaction.animationDuration(), delay: 0.0, options: [UIView.AnimationOptions.curveLinear], animations: {
                    strongSelf.lightCircle.layer.backgroundColor = color.cgColor
                }, completion: nil)
            })
            .disposed(by: disposeBag)
        complexLightValueObservable
            .map { $0.2 }
            .map { "Normalized: \($0.rounded(toPlaces: 2))" }
            .bind(to: normalizedDataValue.rx.text)
            .disposed(by: disposeBag)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
