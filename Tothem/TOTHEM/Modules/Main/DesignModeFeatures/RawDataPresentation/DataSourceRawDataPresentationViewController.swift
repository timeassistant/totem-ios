//
//  DataSourceRawDataPresentationViewController.swift
//  TOTHEM
//
//  Created by Eugen Briukhachyov on 8/21/18.
//  Copyright © 2018 r00t. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DataSourceRawDataViewController: UIViewController {

    var disposeBag = DisposeBag()

    private let originalCircleSize: Double = 50.0
    private let maxCircleSize: Double = 250.0

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var rawDataLabel: UILabel!
    @IBOutlet weak var manualSwitch: UISwitch!

    @IBOutlet weak var minScaleCircle: UIView! {
        didSet {
            minScaleCircle.layer.borderWidth = 2
            minScaleCircle.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
            minScaleCircle.layer.cornerRadius = minScaleCircle.frame.width / 2
        }
    }
    @IBOutlet weak var oneFourthScaleCircle: UIView! {
        didSet {
            oneFourthScaleCircle.layer.borderWidth = 2
            oneFourthScaleCircle.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
            oneFourthScaleCircle.layer.cornerRadius = oneFourthScaleCircle.frame.width / 2
        }
    }
    @IBOutlet weak var oneSecondScaleCircle: UIView! {
        didSet {
            oneSecondScaleCircle.layer.borderWidth = 2
            oneSecondScaleCircle.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
            oneSecondScaleCircle.layer.cornerRadius = oneSecondScaleCircle.frame.width / 2
        }
    }
    @IBOutlet weak var maxScaleCircle: UIView! {
        didSet {
            maxScaleCircle.layer.borderWidth = 2
            maxScaleCircle.layer.borderColor = UIColor(red: 95/255, green: 95/255, blue: 95/255, alpha: 1.0).cgColor
            maxScaleCircle.layer.cornerRadius = maxScaleCircle.frame.width / 2
        }
    }
    @IBOutlet weak var dynamicCircle: UIView! {
        didSet {
            dynamicCircle.layer.borderWidth = 1
            dynamicCircle.layer.borderColor = UIColor.red.cgColor
            dynamicCircle.layer.cornerRadius = dynamicCircle.frame.width / 2
        }
    }
    @IBOutlet weak var manualValueSlider: UISlider!
    @IBOutlet weak var minValueLabel: UILabel!
    @IBOutlet weak var maxValueLabel: UILabel!

    let viewModel: DataSourceRawDataPresentationViewModel

    init(forDataSource dataSource: ConnectorParameter) {
        self.viewModel = DataSourceRawDataPresentationViewModel(withDataSource: dataSource)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("No dataSource passed")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        backButton.rx.tap
            .bind(to: viewModel.dismiss)
            .disposed(by: disposeBag)

        viewModel.isManual
            .take(1)
            .bind(to: manualSwitch.rx.isOn)
            .disposed(by: disposeBag)
        manualSwitch.rx.isOn
            .skip(1)
            .subscribe(onNext: { [weak self] enabled in
                self?.viewModel.switchManual(to: enabled)
            })
            .disposed(by: disposeBag)
        viewModel.value
            .map { $0.rounded(toPlaces: 2) }
            .map { "RAW DATA: \($0)" }
            .bind(to: rawDataLabel.rx.text)
            .disposed(by: disposeBag)
        Observable.combineLatest(viewModel.minValue, viewModel.maxValue, viewModel.value)
            .throttle(0.3, scheduler: MainScheduler.instance)
            .map { [weak self] data -> Double? in
                guard let strongSelf = self else { return nil }
                let dynamicScaleLength = data.1 - data.0
                let staticScaleLength = strongSelf.maxCircleSize - strongSelf.originalCircleSize
                let scaleTranslationFactor = staticScaleLength / dynamicScaleLength
                let dynamicMinTranslated = data.0 * scaleTranslationFactor
                let dynamicScaleOffset = strongSelf.originalCircleSize - dynamicMinTranslated
                let translatedValue = data.2 * scaleTranslationFactor + dynamicScaleOffset
                let scaleFactor = translatedValue / strongSelf.originalCircleSize
                return scaleFactor
            }
            .skipNil()
            .map { CGFloat($0) }
            .subscribe(onNext: { [weak self] scaleFactor in
                guard let strongSelf = self else { return }
                let transform = CGAffineTransform.init(scaleX: scaleFactor, y: scaleFactor)
                UIView.animate(withDuration: 1, animations: {
                    strongSelf.dynamicCircle.transform = transform
                })
            })
            .disposed(by: disposeBag)
        viewModel.isManual
            .bind(to: manualValueSlider.rx.isEnabled)
            .disposed(by: disposeBag)
        viewModel.minValue
            .map { "\($0.rounded(toPlaces: 1))" }
            .bind(to: minValueLabel.rx.text)
            .disposed(by: disposeBag)
        viewModel.minValue
            .map { Float($0) }
            .subscribe(onNext: { [weak self] minValue in
                self?.manualValueSlider.minimumValue = minValue
            })
            .disposed(by: disposeBag)
        viewModel.maxValue
            .map { Float($0) }
            .subscribe(onNext: { [weak self] maxValue in
                self?.manualValueSlider.maximumValue = maxValue
            })
            .disposed(by: disposeBag)
        viewModel.maxValue
            .map { "\($0.rounded(toPlaces: 1))" }
            .bind(to: maxValueLabel.rx.text)
            .disposed(by: disposeBag)
        Observable.combineLatest(viewModel.isManual, viewModel.value)
            .filter { !$0.0 }
            .map { Float($0.1) }
            .bind(to: manualValueSlider.rx.value)
            .disposed(by: disposeBag)
        let manualSliderSubscription = manualValueSlider.rx.value
        Observable.combineLatest(viewModel.isManual, manualSliderSubscription)
            .throttle(0.3, scheduler: MainScheduler.instance)
            .filter { $0.0 }
            .map { Double($0.1) }
            .distinctUntilChanged { $0.rounded(toPlaces: 2) == $1.rounded(toPlaces: 2) }
            .skip(1)
            .subscribe(onNext: { [weak self] newValue in
                self?.viewModel.setStaticValue(to: newValue)
            })
            .disposed(by: disposeBag)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
