//
//  ChartView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol ChartViewDataSource: class {
    var min: CGFloat { get }
    var max: CGFloat { get }
    func y(x: CGFloat) -> CGFloat
    var dx: CGFloat { get }
    var dy: CGFloat { get }
    var asixXdy: CGFloat { get }
    var asixYdx: CGFloat { get }
}

final class ChartView: UIView {
    weak var dataSource: ChartViewDataSource?

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        drawChart()
        drawX()
        drawY()
    }

    private func drawX() {
        let aPath = UIBezierPath()

        aPath.move(to: CGPoint(x: 0, y: bounds.height / 2 + (dataSource?.asixXdy ?? 0) * -1))
        aPath.addLine(to: CGPoint(x: bounds.width, y: bounds.height / 2 + (dataSource?.asixXdy ?? 0) * -1))

        UIColor.green.withAlphaComponent(0.2).set()
        aPath.stroke()
    }

    private func drawY() {
        let aPath = UIBezierPath()

        aPath.move(to: CGPoint(x: bounds.width / 2, y: 0))
        aPath.addLine(to: CGPoint(x: bounds.width / 2, y: bounds.height))

        UIColor.green.withAlphaComponent(0.2).set()
        aPath.stroke()
    }

    private func drawChart() {
        guard let dataSource = dataSource else { return }

        let aPath = UIBezierPath()

        let x = self.x(dataSource.min)
        let y = self.y(dataSource.y(x: dataSource.min))

        aPath.move(to: CGPoint(x: x, y: y))

        for x in stride(from: dataSource.min + 1, to: dataSource.max + 1, by: 1) {
            let y = self.y(dataSource.y(x: x))
            aPath.addLine(to: CGPoint(x: self.x(x), y: y))
        }

        UIColor.red.set()
        aPath.stroke()
    }

    private func x(_ x: CGFloat) -> CGFloat {
        return bounds.midX + x + (dataSource?.dx ?? 0)
    }

    private func y(_ y: CGFloat) -> CGFloat {
        return bounds.midY - (dataSource?.dy ?? 0) + y * -1
    }
}
