//
//  UserProfileController.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

enum DesignModeTestCase {
    case geolocation
    case geolocationDeviation
    case geolocationTrial
    case brightness
    case brightnessDual
    case brightnessTrial
    case dump(title: String)
    case rawDataPresentation(dataSource: ConnectorParameter)
    case stabilizationXY

    var title: String {
        switch self {
        case .geolocation:
            return "SIZE :: MONO"
        case .geolocationDeviation:
            return "SIZE :: DUAL"
        case .geolocationTrial:
            return "SIZE :: TRIAL"
        case .brightness:
            return "COLOR :: MONO"
        case .brightnessDual:
            return "COLOR :: DUAL"
        case .brightnessTrial:
            return "COLOR :: TRIAL"
        case .stabilizationXY:
            return "STABILIZATION XY :: DUAL"
        case .dump(let title):
            return title
        case .rawDataPresentation(let dataSource):
            return "\(dataSource.parameterDomain) :: PRESENTATION"
        }
    }
}

class UserProfileController: UIViewController {
    private var bag = DisposeBag()
    var viewModel = UserProfileViewModel()
    init() {
        super.init(nibName: String(describing: UserProfileController.self), bundle: nil)
        self.modalPresentationStyle = .overFullScreen
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var circleMask: ProfileMaskView!
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet var changeProfileButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var userImageViewTwo: UIImageView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var formulasButton: UIButton!
    @IBOutlet weak var dataSourcesButton: UIButton!

    private let designModeTestCases = [
        DesignModeTestCase.stabilizationXY,
        DesignModeTestCase.geolocation,
        DesignModeTestCase.geolocationDeviation,
        DesignModeTestCase.geolocationTrial,
        DesignModeTestCase.brightness,
        DesignModeTestCase.brightnessDual,
        DesignModeTestCase.brightnessTrial
    ]

    // LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepare()
    }

    private func prepare() {
        self.setupMask()
        infoTableView.delegate = self
        infoTableView.dataSource = self
        userImageViewTwo.layer.borderWidth = 2
        userImageViewTwo.layer.borderColor = UIColor.white.cgColor

        self.viewModel
            .areFormulasVisible
            .map { !$0 }
            .subscribe(onNext: { [weak self] in
                self?.formulasButton.isHidden = $0
            })
            .disposed(by: bag)
        
        self.viewModel
            .areFormulasVisible
            .map { !$0 }
            .subscribe(onNext: { [weak self] in
                self?.dataSourcesButton.isHidden = $0
            })
            .disposed(by: bag)

        settingsButton.rx.tap
            .bind(to: viewModel.openSettingsController)
            .disposed(by: bag)
        loginButton.rx.tap
            .bind(to: viewModel.login)
            .disposed(by: bag)
        formulasButton.rx.tap
            .bind(to: viewModel.openFormulasList)
            .disposed(by: bag)
        dataSourcesButton.rx.tap
            .bind(to: viewModel.openDataSourcesSettings)
            .disposed(by: bag)
    }

    private func setupMask() {
        self.circleMask = ProfileMaskView()
        self.circleMask.makeCornersRounded()
        self.view.insertSubview(self.circleMask, at: 1)
        self.backgroundImage.mask = self.circleMask
    }

    @IBAction func changeProfileButton(_ sender: UIButton) {
		ConfigurationFacade.shared.hapticParameters.getHapticButton()
        _ = AuthManager.shared.logout()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard view.window != nil else { return }
        
        guard let circleMaskTouch = touches.first?.location(in: self.circleMask).y
            else {
            return
        }
        guard circleMaskTouch > self.circleMask.frame.height ||
            circleMaskTouch < 0
            else {
            return
        }
        self.dismissController()
    }

     var isDissmisBegin = false

    private func dismissController() {
        self.isDissmisBegin = true
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.defaultPosition(for: [self.userImageViewTwo, self.infoTableView, self.changeProfileButton, self.circleMask])
        }, completion: { _ in
            self.dismiss(animated: true, completion: nil)
        })
    }
}

extension UserProfileController {
    func defaultPosition(for views: [UIView]) {
        views.forEach { $0.transform = CGAffineTransform.identity }
    }

    func changePosition(for views: [UIView], with progress: Progress) {
        guard !isDissmisBegin else {
            return
        }
        let shift = (self.circleMask.maxYOffset * 2) * progress.value
        views.forEach {if (shift < self.circleMask.maxYOffset) || $0 == self.circleMask {
            $0.transform = CGAffineTransform(translationX: 0, y: -shift)}}
    }
}

extension UserProfileController: ProfileTransitionTo {
    var userImage: UIView {
        return self.userImageViewTwo
    }

    var maskView: UIView {
        return self.circleMask
    }

    var backgroundOpacityLevel: CGFloat {
        return 0.4
    }

    var animatableViews: [UIView] {
        return [self.infoTableView, self.changeProfileButton]
    }
}

//EXAMPLE
extension UserProfileController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return designModeTestCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let testCase = designModeTestCases[indexPath.row]
        let title = testCase.title

        cell.backgroundColor = UIColor.clear

        let label = UILabel()
        label.text = title
        label.textAlignment = .center
        label.textColor = UIColor.white

        cell.addSubview(label)

        label.fill(in: cell)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height / 10
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let testCase = designModeTestCases[indexPath.row]
        viewModel.openDesignTestCase.onNext(testCase)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView.contentSize.height > scrollView.frame.height * 1.5 else { return }
        let progress = scrollView.contentOffset.y / (scrollView.contentSize.height - scrollView.frame.height)
        print(progress)
        self.changePosition(for: [self.userImageViewTwo, self.infoTableView, self.changeProfileButton, self.circleMask], with: Progress(value: CGFloat(progress), constraints: false))
    }

}
