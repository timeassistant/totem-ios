//
//  UserProfileMaskView.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol ChangableCircleView {
    var maxYOffset: CGFloat { get }
    var circleSide: CGFloat { get }
    func setNeedsFrame()
    var calulatedFrame: CGRect { get }
}

extension ChangableCircleView where Self: UIView {
    var calulatedFrame: CGRect {
        return CGRect(x: ((UIScreen.main.bounds.width / 2) - (self.circleSide / 2)),
                      y: ((UIScreen.main.bounds.height / 2) - (self.circleSide / 2)) + self.maxYOffset,
                      width: self.circleSide,
                      height: self.circleSide)
    }

    func makeCornersRounded() {
        self.clipsToBounds = false
        self.layer.cornerRadius = self.circleSide / 2
    }

    func setNeedsFrame() {
        self.frame = self.calulatedFrame
    }
}

// CIRCLE VIEW

class ProfileMaskView: UIView, ChangableCircleView {

    var maxYOffset: CGFloat {
        return UIScreen.main.bounds.height * 0.1
    }

    var circleSide: CGFloat {
        return UIScreen.main.bounds.height
    }

    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        self.setNeedsFrame()
        self.backgroundColor = UIColor.blue
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
