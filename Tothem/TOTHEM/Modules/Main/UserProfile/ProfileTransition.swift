//
//  FadeTransitioning.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

protocol ProfileTransitionProtocol {
    var userImage: UIView { get }
}

protocol ProfileTransitionFrom: ProfileTransitionProtocol {

}

protocol ProfileTransitionTo: ProfileTransitionProtocol {
    var maskView: UIView { get }
    var backgroundOpacityLevel: CGFloat { get }
    var animatableViews: [UIView] { get }
}

class ProfileTransition: NSObject, UIViewControllerAnimatedTransitioning {

    enum TransitionType {
        case present
        case dismiss
    }

    var type: TransitionType = .present

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let present = (type == .present)
        let conteinerView = transitionContext.containerView
        let fromVC = transitionContext.viewController(forKey: .from)!
        let toVC = transitionContext.viewController(forKey: .to)!
        toVC.view.frame = UIScreen.main.bounds

        let endTransition: () -> Void = {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        toVC.view.layoutIfNeeded()

        switch present {

        case true:
            [toVC.view].forEach { conteinerView.addSubview($0) }
            toVC.view.backgroundColor = toVC.view.backgroundColor?.withAlphaComponent(0)
            let (transitionViewTo, transitionViewFrom) = ((toVC as? ProfileTransitionTo), ((fromVC as? UINavigationController)?.topViewController as? ProfileTransitionFrom))

            let calculation = self.transitionAttributes(from: transitionViewFrom!, to: transitionViewTo!)

            transitionViewTo?.maskView.transform = CGAffineTransform(translationX: -calculation.maskTransitionX, y: -calculation.maskTransitionY).scaledBy(x: calculation.scaleFactorMask, y: calculation.scaleFactorMask)
            transitionViewTo?.userImage.transform = CGAffineTransform(translationX: -calculation.imageTransitionX, y: -calculation.imageTransitionY).scaledBy(x: calculation.scaleFactorImage, y: calculation.scaleFactorImage)

            transitionViewFrom?.userImage.isHidden = true
            transitionViewTo?.animatableViews.forEach { $0.alpha = 0 }

            UIView.animate(withDuration: 0.7,
                           delay: 0,
                           usingSpringWithDamping: 0.6,
                           initialSpringVelocity: 0.8,
                           options: .curveEaseOut,
                           animations: {
                            toVC.view.backgroundColor = toVC.view.backgroundColor?.withAlphaComponent(transitionViewTo?.backgroundOpacityLevel ?? 0.4)
                            transitionViewTo?.animatableViews.forEach { $0.alpha = 1 }
                            transitionViewTo?.maskView.transform = CGAffineTransform.identity
                            transitionViewTo?.userImage.transform = CGAffineTransform.identity
            }, completion: { _ in endTransition()
            })
        case false:
            if fromVC.modalPresentationStyle != .overFullScreen {
                [toVC.view].forEach { conteinerView.insertSubview($0, belowSubview: fromVC.view)}
            }

            let (transitionViewFrom, transitionViewTo) = (((toVC as? UINavigationController)?.topViewController as? ProfileTransitionFrom), (fromVC as? ProfileTransitionTo))

            let calculation = self.transitionAttributes(from: transitionViewFrom!, to: transitionViewTo!)

            UIView.animate(withDuration: 0.5,
                           animations: {
                fromVC.view.backgroundColor = fromVC.view.backgroundColor?.withAlphaComponent(0)
                transitionViewTo?.animatableViews.forEach { $0.alpha = 0 }
                transitionViewTo?.maskView.transform = CGAffineTransform(translationX: -calculation.maskTransitionX, y: -calculation.maskTransitionY).scaledBy(x: calculation.scaleFactorMask, y: calculation.scaleFactorMask)
                transitionViewTo?.userImage.transform = CGAffineTransform(translationX: -calculation.imageTransitionX, y: -calculation.imageTransitionY).scaledBy(x: calculation.scaleFactorImage, y: calculation.scaleFactorImage)

            }, completion: { _ in
                transitionViewFrom?.userImage.isHidden = false
                endTransition()
            })

        }
    }

    private func transitionAttributes(from view: ProfileTransitionFrom, to: ProfileTransitionTo)
        -> (scaleFactorMask: CGFloat, scaleFactorImage: CGFloat, maskTransitionY: CGFloat, maskTransitionX: CGFloat, imageTransitionY: CGFloat, imageTransitionX: CGFloat) {
        let viewImageViewFromFrame = view.userImage.superview?.convert(view.userImage.frame, to: UIView(frame: UIScreen.main.bounds)) ?? CGRect.zero

        let scaleFactorMask = viewImageViewFromFrame.height / to.maskView.frame.height

        let scaleFactorImage = viewImageViewFromFrame.height / to.userImage.frame.height

        let maskTransitionY = (to.maskView.frame.height / 2) + (to.maskView.frame.origin.y - (viewImageViewFromFrame.origin.y + viewImageViewFromFrame.height / 2))

        let maskTransitionX = (to.maskView.frame.width / 2) + (to.maskView.frame.origin.x - (viewImageViewFromFrame.origin.x + viewImageViewFromFrame.width / 2))

        let imageTransitionY = (to.userImage.frame.height / 2) + (to.userImage.frame.origin.y - (viewImageViewFromFrame.origin.y + viewImageViewFromFrame.height / 2))

        let imageTransitionX = (to.userImage.frame.width / 2) + (to.userImage.frame.origin.x - (viewImageViewFromFrame.origin.x + viewImageViewFromFrame.width / 2))

        return (scaleFactorMask: scaleFactorMask,
                scaleFactorImage: scaleFactorImage,
                maskTransitionY: maskTransitionY,
                maskTransitionX: maskTransitionX,
                imageTransitionY: imageTransitionY,
                imageTransitionX: imageTransitionX)
    }

}

// MARK: - UIViewControllerTransitioningDelegate
extension ProfileTransition: UIViewControllerTransitioningDelegate {
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.type = .present
        return self
    }

    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.type = .dismiss
        return self
    }
}
