//
//  TextLabellingServiceTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
 

import XCTest
import RxSwift
@testable import TOTHEM

final class TextLabelServiceTests: XCTestCase {
    private var disposeBag: DisposeBag = DisposeBag()
    private var service: TextAnalysisService!
    override func setUp() {
        super.setUp()
        service = TextAnalysisService()
    }
    
    func test_Example1() {
        let exp = expectation(description: "text_expectation")
        
        let text = "Сухой корм произведен для стерилизованных кошек и кастрированных котов с учетом их сниженной потребности в калорийности пищи."
        
        service.execute(text: text)
            .subscribe(onNext: {
                XCTAssert($0.contains(where: { $0.text == "корм cухой" }))
                exp.fulfill()
            }, onError: { _ in
                exp.fulfill()
            })
            .disposed(by: disposeBag)
        
        wait(for: [exp], timeout: 100)
    }
    
    func test_Example2() {
        let exp = expectation(description: "text_expectation")
        
        let text = "Сухой корм для стерилизованных кошек и кастрированных котов с учетом их сниженной потребности в калорийности пищи."
        
        service.execute(text: text)
            .subscribe(onNext: {
                XCTAssert($0.contains(where: { $0.text == "корм cухой" }))
                exp.fulfill()
            }, onError: { _ in
                exp.fulfill()
            })
            .disposed(by: disposeBag)
        
        wait(for: [exp], timeout: 100)
    }
    
    func test_Example3() {
        let exp = expectation(description: "text_expectation")
        
        let text = "Для стерилизованных кошек и кастрированных котов с учетом их сниженной потребности в калорийности пищи произведен сухой корм."
        
        service.execute(text: text)
            .subscribe(onNext: {
                XCTAssert($0.contains(where: { $0.text == "корм cухой" }))
                exp.fulfill()
            }, onError: { _ in
                exp.fulfill()
            })
            .disposed(by: disposeBag)
        
        wait(for: [exp], timeout: 100)
    }
}
