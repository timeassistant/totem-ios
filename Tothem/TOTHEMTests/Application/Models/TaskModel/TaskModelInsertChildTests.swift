//
//  TaskModelInsertChildTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class TaskModelInsertChildTests: XCTestCase {
    private var utils: JSONUtils!
    private var order = ["text1", "text2", "text3", "photo1", "photo2", "photo3", "bullet1", "bullet2", "bullet3"]
    private var model: TaskModel!

    override func setUp() {
        super.setUp()
        utils = JSONUtils(bundle: Bundle(for: type(of: self)))
        let json: JSONObject = try! utils.read(from: "task-model-moving")
        model = TaskModel(json)
    }

    func test_Should_InsertChild_DoNotChangeOrder() {
        model.insertChild("text3", after: "text1")
        XCTAssertEqual(model.order, order)
    }

    func test_Should_Text1HasChildText3_If_InsertText3AsChildToText1() {
        model.insertChild("text3", after: "text1")
        XCTAssertEqual(model.items["text1"]?.child, ["text3"])
    }

    func test_Should_Text2HasChildText3_If_InsertText3AsChildToText2FromText1() {
        model.insertChild("text3", after: "text1")
        model.insertChild("text3", after: "text2")
        XCTAssertEqual(model.items["text2"]?.child, ["text3"])
    }

    func test_Should_Text3HasNoChild_If_InsertText3AsChildToText2FromText1() {
        model.insertChild("text3", after: "text1")
        model.insertChild("text3", after: "text2")
        XCTAssertEqual(model.items["text1"]?.child, [])
    }

    func test_Should_Text1HasChildB3T3_If_InsertAsChildAfterText1() {
        model.insertChild("text3", after: "text1")
        model.insertChild("bullet3", after: "text1")
        XCTAssertEqual(model.items["text1"]?.child, ["bullet3", "text3"])
    }

    func test_Should_Text1HasChildT3B3_If_InsertAsChildAfterT3() {
        model.insertChild("text3", after: "text1")
        model.insertChild("bullet3", after: "text3")
        XCTAssertEqual(model.items["text1"]?.child, ["text3", "bullet3"])
    }

    func test_Should_Text1HasChildT3B3_If_InsertAsChildAfterT3FromT2() {
        model.insertChild("text3", after: "text1")
        model.insertChild("bullet3", after: "text2")
        model.insertChild("bullet3", after: "text3")
        XCTAssertEqual(model.items["text1"]?.child, ["text3", "bullet3"])
    }

    func test_Should_InsertAllChild_If_InsertWithChilds() {
        model.insertChild("text3", after: "text1")
        model.insertChild("bullet3", after: "text3")
        model.insertChild("text2", after: "bullet1")
        model.insertChild("bullet2", after: "bullet1")
        model.insertChild("bullet1", after: "text3")
        XCTAssertEqual(model.items["text1"]?.child, ["text3", "bullet1", "bullet2", "text2",  "bullet3"])
    }

    func test_Should_InsertOneChild_If_InsertToAnother() {
        model.insertChild("text3", after: "text1")
        model.insertChild("bullet3", after: "text3")
        model.insertChild("text2", after: "bullet1")
        model.insertChild("bullet2", after: "bullet1")
        model.insertChild("bullet2", after: "text3")
        XCTAssertEqual(model.items["text1"]?.child, ["text3", "bullet2", "bullet3"])
    }

    func test_Should_InsertOneChildAsRoot_If_InsertFromChild() {
        model.insertChild("bullet3", after: "text1")
        model.insertChild("bullet2", after: "text1")
        model.insertChild("text3", after: "text1")
        model.insertChild("text2", after: "text1")
        model.insert("bullet2", after: "text1")
        XCTAssertEqual(model.items["text1"]?.child, ["text2", "text3", "bullet3"])
    }

    func test_Should_HasCorrectOrderB2AfterT1_If_InsertFromChild() {
        model.insertChild("bullet3", after: "text1")
        model.insertChild("bullet2", after: "text1")
        model.insertChild("text3", after: "text1")
        model.insertChild("text2", after: "text1")
        model.insert("bullet2", after: "text1")
        order.rearrange(from: 7, to: 1)
        XCTAssertEqual(model.order, order)
    }
}
