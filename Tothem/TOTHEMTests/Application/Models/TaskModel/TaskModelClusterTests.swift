//
//  TaskModelClusterTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class TaskModelClusterTests: XCTestCase {
    private var utils: JSONUtils!
    private var order: [String] = []
    private var model: TaskModel!

    override func setUp() {
        super.setUp()
        utils = JSONUtils(bundle: Bundle(for: type(of: self)))
        let json: JSONObject = try! utils.read(from: "task-model-cluster")
        model = TaskModel(json)
    }

    override func tearDown() {
        EntityModel.newId = {
            return UUID().uuidString
        }
    }

    func test_Should_MockingParsed_If_AllDataCorrect() {
        XCTAssertEqual(model.id, "task model id")
    }

    func test_Should_ReturnOrderedText_If_CallDescriptionText() {
        XCTAssertEqual(model.descriptionText, "")
    }

    func test_Should_ReturnOrderedText_If_CallChecklistText() {
        XCTAssertEqual(model.checklistText, "")
    }

    func test_Should_ParseOrder_If_ReadTaskModelUpdating() {
        XCTAssertEqual(model.order, order)
    }

    // MARK: - Global rule of clusterization
    func test_Should_InsertBulletAsChildOnCenter_If_BulletCreatedAfterText() {
        let newTextId = UUID().uuidString
        EntityModel.newId = {
            return newTextId
        }
        model.update(texts: UUID().uuidString, side: .center)
        
        let newBulletId = UUID().uuidString
        EntityModel.newId = {
            return newBulletId
        }
        model.update(bullets: UUID().uuidString, side: .center)
        XCTAssertEqual(model.items[newTextId]?.child, [])
    }
    
    func test_Should_InsertTextAsChildOnRight_If_TextCreatedAfterBullet() {
        let entities = [UUID().uuidString, UUID().uuidString]
        let strings = [UUID().uuidString, UUID().uuidString]
        
        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: strings[0], side: .right)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(texts: strings[1], side: .right)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [entities[1]])
    }
    
    func test_Should_InsertTextAsChildOnLeft_If_TextCreatedAfterPhoto() {
        let entities = [UUID().uuidString, UUID().uuidString]
        let strings = [UUID().uuidString, UUID().uuidString]
        
        EntityModel.newId = {
            return entities[0]
        }
        model.update(photos: [EntityModel(.photo, value: strings[0])], side: .left)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(texts: strings[1], side: .left)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [entities[1]])
    }
    
    func test_Should_NotInsertPhotoAsChildOnCenter_If_PhotoCreatedAfterText() {
        let entities = [UUID().uuidString, UUID().uuidString]
        let strings = [UUID().uuidString, UUID().uuidString]
        
        EntityModel.newId = {
            return entities[0]
        }
        model.update(texts: strings[0], side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(photos: [EntityModel(.photo, value: strings[1])], side: .center)
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    func test_Should_NotInsertEntitiesAsChildOnCenter_If_CreatedAfterText() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString, UUID().uuidString]
        let strings = [UUID().uuidString, UUID().uuidString, UUID().uuidString, UUID().uuidString]
        
        var photos: [EntityModel] = []
        EntityModel.newId = {
            return entities[0]
        }
        model.update(texts: strings[0], side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        photos.append(EntityModel(.photo, value: strings[1]))
        model.update(photos: photos, side: .center)
        
        EntityModel.newId = {
            return entities[2]
        }
        model.update(bullets: strings[2], side: .center)
        
        EntityModel.newId = {
            return entities[3]
        }
        photos.append(EntityModel(.photo, value: strings[3]))
        model.update(photos: photos, side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    func test_Should_NotInsertBulletAsChildInLastModelOnCenter_If_BulletCreatedAfterLastText() {
        let entities = [UUID().uuidString, UUID().uuidString]
        let strings = [UUID().uuidString, UUID().uuidString]
        
        model.update(texts: strings[0], side: .center)
        
        EntityModel.newId = {
            return entities[0]
        }
        model.update(texts: "\(strings[0])\n\(strings[1])", side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: strings[0], side: .center)
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    func test_Should_NotInsertTextAsChildOnCenter_If_TextCreatedAfterBullet() {
        let entities = [UUID().uuidString, UUID().uuidString]
        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(texts: UUID().uuidString, side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    func test_Should_NotInsertBulletAsChildOnCenter_If_BulletCreatedAfterText() {
        let entities = [UUID().uuidString, UUID().uuidString]
        EntityModel.newId = {
            return entities[0]
        }
        model.update(texts: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    func test_Should_NotInsertPhotoAsChildToTextOnCenter_If_PhotoCreatedAfterTextAndPhoto() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        EntityModel.newId = {
            return entities[0]
        }
        model.update(texts: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[2]
        }
        model.update(photos: [EntityModel(.photo, value: UUID().uuidString)], side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    func test_Should_NotInsertPhotoAsChildToBulletOnCenter_If_PhotoCreatedAfterTextAndPhoto() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        EntityModel.newId = {
            return entities[0]
        }
        model.update(texts: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[2]
        }
        model.update(photos: [EntityModel(.photo, value: UUID().uuidString)], side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [])
    }
    
    // MARK: - Local rule of clusterization
    func test_Should_InsertBulletAsChildOnCenter_If_BulletCreatedAfterPhoto() {
        let entities = [UUID().uuidString, UUID().uuidString]
        let strings = [UUID().uuidString]
        EntityModel.newId = {
            return entities[0]
        }
        model.update(photos: [EntityModel(.photo, value: strings[0])], side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: strings[0], side: .center)
        XCTAssertEqual(model.items[entities[0]]?.child, [entities[1]])
    }
    
    func test_Should_InsertPhotoAsChildOnCenter_If_PhotoCreatedAfterBullet() {
        let entities = [UUID().uuidString, UUID().uuidString]
        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(photos: [EntityModel(.photo, value: UUID().uuidString)], side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [entities[1]])
    }
    
    func test_Should_InsertPhotosAsChildOnCenter_If_CreatedAfterBullet() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        
        var photos: [EntityModel] = []
        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        photos.append(EntityModel(.photo, value: UUID().uuidString))
        model.update(photos: photos, side: .center)
        
        EntityModel.newId = {
            return entities[2]
        }
        photos.append(EntityModel(.photo, value: UUID().uuidString))
        model.update(photos: photos, side: .center)
        
        XCTAssertEqual(model.items[entities[0]]?.child, [entities[1], entities[2]])
    }
    
    func test_Should_InsertPhotosAsChildOnCenter_If_CreatedAfterLastBullet() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        let bullets = [UUID().uuidString, UUID().uuidString]
        
        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: bullets[0], side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: bullets[0] + "\n" + bullets[1], side: .center)
        
        EntityModel.newId = {
            return entities[2]
        }
        model.update(photos: [EntityModel(.photo, value: UUID().uuidString)], side: .center)
        
        XCTAssertEqual(model.items[entities[1]]?.child, [entities[2]])
    }
    
    func test_Should_InsertBulletsAsChildOnCenter_If_CreatedAfterLastPhoto() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        var photos: [EntityModel] = []
        
        EntityModel.newId = {
            return entities[0]
        }
        photos.append(EntityModel(.photo, value: UUID().uuidString))
        model.update(photos: photos, side: .center)
        
        EntityModel.newId = {
            return entities[1]
        }
        photos.append(EntityModel(.photo, value: UUID().uuidString))
        model.update(photos: photos, side: .center)
        
        EntityModel.newId = {
            return entities[2]
        }
        model.update(bullets: UUID().uuidString, side: .center)
        
        XCTAssertEqual(model.items[entities[1]]?.child, [entities[2]])
    }
}
