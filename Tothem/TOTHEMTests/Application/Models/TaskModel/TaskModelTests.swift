//
//  TaskModelTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class TaskModelTests: XCTestCase {
    private var utils: JSONUtils!

    override func setUp() {
        super.setUp()
        utils = JSONUtils(bundle: Bundle(for: type(of: self)))
    }

    func test_Should_ParseTaskId_If_IdPresented() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        XCTAssertEqual(TaskModel(json).id, "task model center 1 id")
    }

    func test_Should_ParseTaskTitle_If_TitlePresented() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        XCTAssertEqual(TaskModel(json).title, "task model center 1 title")
    }

    func test_ShouldNot_ParseTaskTitleFromLeftModel_If_TitlePresentedFromCenter() {
        let json: JSONObject = try! utils.read(from: "task-model-left-1")
        XCTAssertNotEqual(TaskModel(json).title, "task model 1")
    }

    func test_Should_ParseTaskTypeCenter_If_TypePresented() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        XCTAssertEqual(TaskModel(json).side, TimesetType.center)
    }

    func test_Should_ParseTaskTypeLeft_If_TypePresented() {
        let json: JSONObject = try! utils.read(from: "task-model-left-1")
        XCTAssertEqual(TaskModel(json).side, TimesetType.left)
    }

    func test_Should_CenterBeChecked_If_CheckedTrue() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        XCTAssertEqual(TaskModel(json).checked, true)
    }

    func test_Should_LeftBeNotChecked_If_CheckedFalse() {
        let json: JSONObject = try! utils.read(from: "task-model-left-1")
        XCTAssertEqual(TaskModel(json).checked, false)
    }

    func test_Should_RightBeNotChecked_If_CheckedNotPresented() {
        let json: JSONObject = try! utils.read(from: "task-model-right-1")
        XCTAssertEqual(TaskModel(json).checked, false)
    }

    func test_Should_CenterBeCombined_If_ModeCombined() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        XCTAssertEqual(TaskModel(json).mode, TaskModelMode.combined)
    }

    func test_Should_LeftBeSplitted_If_ModeSplitted() {
        let json: JSONObject = try! utils.read(from: "task-model-left-1")
        XCTAssertEqual(TaskModel(json).mode, TaskModelMode.splitted)
    }

    func test_Should_RightBeBeSplitted_If_ModeNotPresented() {
        let json: JSONObject = try! utils.read(from: "task-model-right-1")
        XCTAssertEqual(TaskModel(json).mode, TaskModelMode.splitted)
    }

    func test_Should_SetCenterOrder_If_Presented() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        XCTAssertEqual(TaskModel(json).order, ["text1", "text2", "text3"])
    }

    func test_Should_ReturnCenterTexts_If_OrderPreseted() {
        let json: JSONObject = try! utils.read(from: "task-model-center-1")
        let model = TaskModel(json)
        let texts = model.texts.map({ $0.id })
        XCTAssertEqual(texts, model.order)
    }

    func test_Should_ReturnLeftPhotos_If_OrderNotPreseted() {
        let json: JSONObject = try! utils.read(from: "task-model-left-1")
        let model = TaskModel(json)
        let photos = model.photos.map({ $0.id })
        XCTAssertEqual(photos, ["photo1", "photo2", "photo3"])
    }

    func test_Should_ReturnRightBullets_If_OrderNotPreseted() {
        let json: JSONObject = try! utils.read(from: "task-model-right-1")
        let model = TaskModel(json)
        let photos = model.bullets.map({ $0.id })
        XCTAssertEqual(photos, ["bullet1", "bullet2", "bullet3"])
    }

}
