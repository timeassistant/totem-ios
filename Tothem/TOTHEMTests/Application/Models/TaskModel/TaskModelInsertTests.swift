//
//  TaskModelInsertTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class TaskModelInsertTests: XCTestCase {
    private var utils: JSONUtils!
    private var order = ["text1", "text2", "text3", "photo1", "photo2", "photo3", "bullet1", "bullet2", "bullet3"]
    private var model: TaskModel!

    override func setUp() {
        super.setUp()
        utils = JSONUtils(bundle: Bundle(for: type(of: self)))
        let json: JSONObject = try! utils.read(from: "task-model-moving")
        model = TaskModel(json)
    }

    func test_Should_ParsedOrderBeCorrect_If_Check() {
        XCTAssertEqual(model.order, order)
    }

    func test_Should_Text3BeAfterText1_If_InsertText3AfterText1() {
        model.insert("text3", after: "text1")
        order.rearrange(from: 2, to: 1)
        XCTAssertEqual(model.order, order)
    }

    func test_Should_Text1BeAfterText3_If_InsertText1AfterText3() {
        model.insert("text1", after: "text3")
        order.rearrange(from: 0, to: 2)
        XCTAssertEqual(model.order, order)
    }

    func test_Should_Text3BeFirst_If_InsertText3AfterNil() {
        model.insert("text3", after: nil)
        order.rearrange(from: 2, to: 0)
        XCTAssertEqual(model.order, order)
    }

    func test_Should_Text3BeAfterBullet3_If_InsertText3AfterBullet3() {
        model.insert("text3", after: "bullet3")
        order.rearrange(from: 2, to: 8)
        XCTAssertEqual(model.order, order)
    }
}
