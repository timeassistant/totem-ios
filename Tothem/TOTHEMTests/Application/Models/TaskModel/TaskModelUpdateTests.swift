//
//  TaskModelUpdateTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class TaskModelUpdateTests: XCTestCase {
    private var utils: JSONUtils!
    private var order = ["text1", "text2", "text3", "photo1", "photo2", "photo3", "bullet1", "bullet2", "bullet3"]
    private var model: TaskModel!

    override func setUp() {
        super.setUp()
        utils = JSONUtils(bundle: Bundle(for: type(of: self)))
        let json: JSONObject = try! utils.read(from: "task-model-updating")
        model = TaskModel(json)
    }

    override func tearDown() {
        EntityModel.newId = {
            return UUID().uuidString
        }
    }

    func test_Should_MockingParsed_If_AllDataCorrect() {
        XCTAssertEqual(model.id, "task model id")
    }

    func test_Should_ReturnOrderedText_If_CallDescriptionText() {
        XCTAssertEqual(model.descriptionText, "task text 1\ntask text 2\ntask text 3")
    }

    func test_Should_ReturnOrderedText_If_CallChecklistText() {
        XCTAssertEqual(model.checklistText, "task bullet 1\ntask bullet 2\ntask bullet 3")
    }

    func test_Should_ParseOrder_If_ReadTaskModelUpdating() {
        XCTAssertEqual(model.order, order)
    }

    func test_Should_ModelChecklistTextBeEqualBulletsWithOrder_If_UseHelper() {
        let checklist = EditingTextHelper.text(from: model.bullets)
        XCTAssertEqual(checklist, model.checklistText)
    }

    func test_Should_ModelTextsTextBeEqualBulletsWithOrder_If_UseHelper() {
        let description = EditingTextHelper.text(from: model.texts)
        XCTAssertEqual(description, model.descriptionText)
    }

    func test_Should_InsertIdToOrderAt0_If_InsertTextLineAt0() {
        var items = model.texts
        items.insert(EntityModel(.text, value: UUID().uuidString), at: 0)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 0)
        model.update(texts: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAt1_If_InsertTextLineAt1() {
        var items = model.texts
        items.insert(EntityModel(.text, value: UUID().uuidString), at: 1)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 1)
        model.update(texts: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAt2_If_InsertTextLineAt2() {
        var items = model.texts
        items.insert(EntityModel(.text, value: UUID().uuidString), at: 2)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 2)
        model.update(texts: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAtTheEnd_If_InsertTextLineAt3() {
        var items = model.texts
        items.insert(EntityModel(.text, value: UUID().uuidString), at: 3)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.append(newId)
        model.update(texts: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAt6_If_InsertBulletLineAt0() {
        var items = model.bullets
        items.insert(EntityModel(.bullet, value: UUID().uuidString), at: 0)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 6)
        model.update(bullets: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAt7_If_InsertBulletLineAt1() {
        var items = model.bullets
        items.insert(EntityModel(.bullet, value: UUID().uuidString), at: 1)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 7)
        model.update(bullets: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAt8_If_InsertBulletLineAt2() {
        var items = model.bullets
        items.insert(EntityModel(.bullet, value: UUID().uuidString), at: 2)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 8)
        model.update(bullets: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_InsertIdToOrderAt9_If_InsertBulletLineAt3() {
        var items = model.bullets
        items.insert(EntityModel(.bullet, value: UUID().uuidString), at: 3)

        let newId = UUID().uuidString
        EntityModel.newId = {
            return newId
        }

        order.insert(newId, at: 9)
        model.update(bullets: EditingTextHelper.text(from: items))
        XCTAssertEqual(order, model.order)
    }

    func test_Should_MakeCenterOrder_If_AddBulletBullet() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        let bullets = [UUID().uuidString, UUID().uuidString]
        
        model.items = [:]
        model.order = []

        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: bullets[0])

        EntityModel.newId = {
            return entities[1]
        }
        model.update(bullets: "\(bullets[0])\n")

        EntityModel.newId = {
            return entities[2]
        }
        model.update(bullets: "\(bullets[0])\n\(bullets[1])")
        XCTAssertEqual([entities[0], entities[2]], model.order)
    }

    func test_Should_MakeCenterOrder_If_AddBulletTextBullet() {
        let entities = [UUID().uuidString, UUID().uuidString, UUID().uuidString, UUID().uuidString]
        let texts = [UUID().uuidString, UUID().uuidString, UUID().uuidString]
        
        model.items = [:]
        model.order = []

        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: texts[0])

        EntityModel.newId = {
            return entities[1]
        }
        model.update(texts: texts[1])

        EntityModel.newId = {
            return entities[2]
        }
        model.update(bullets: "\(texts[0])\n")

        EntityModel.newId = {
            return entities[3]
        }
        model.update(bullets: "\(texts[0])\n\(texts[2])")
        XCTAssertEqual([entities[0], entities[1], entities[3]], model.order)
    }

    func test_Should_MakeCenterOrder_If_AddBulletTextBulletBulletTextBullet() {
        var entities: [String] = []
        var texts: [String] = []
        var bullets: [String] = []
        for _ in stride(from: 0, to: 9, by: 1) {
            entities.append(UUID().uuidString)
            texts.append(UUID().uuidString)
            bullets.append(UUID().uuidString)
        }
        
        model.items = [:]
        model.order = []

        EntityModel.newId = {
            return entities[0]
        }
        model.update(bullets: bullets[0])

        EntityModel.newId = {
            return entities[1]
        }
        model.update(texts: texts[0])

        EntityModel.newId = {
            return entities[2]
        }
        model.update(bullets: "\(bullets[0])\n")

        EntityModel.newId = {
            return entities[3]
        }
        model.update(bullets: "\(bullets[0])\n\(bullets[1])")
        
        EntityModel.newId = {
            return entities[4]
        }
        model.update(bullets: "\(bullets[0])\n\(bullets[2])")

        EntityModel.newId = {
            return entities[5]
        }
        model.update(bullets: "\(bullets[0])\n\(bullets[2])\n")

        EntityModel.newId = {
            return entities[6]
        }
        model.update(texts: "\(texts[0])\n\(texts[1])")

        EntityModel.newId = {
            return entities[7]
        }
        model.update(bullets: "\(bullets[0])\n\(bullets[2])\n\(bullets[3])")
        
        EntityModel.newId = {
            return entities[8]
        }
        model.update(bullets: "\(bullets[0])\n\(bullets[2])\n\(bullets[3])\n\(bullets[4])")

        XCTAssertEqual([entities[0], entities[1], entities[3], entities[6], entities[7], entities[8]], model.order)
    }
}
