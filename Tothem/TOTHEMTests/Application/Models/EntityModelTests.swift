//
//  EntityModelTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class EntityModelTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    func test_Should_SetId_If_PassJsonWithId() {
        let json: JSONObject = ["id": UUID().uuidString]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.id, json["id"] as? String)
    }
    
    func test_Should_SetRandomId_If_PassNilJson() {
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: nil)
        XCTAssertEqual(entity.id.isEmpty, false)
    }
    
    func test_Should_SetValue_If_PassJsonWithAssetId() {
        let value = UUID().uuidString
        let json: JSONObject = ["asset_id": value]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.value, value)
    }
    
    func test_Should_SetChecked_If_PassJsonWithChecked() {
        let json: JSONObject = ["checked": true]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.checked, true)
    }
    
    func test_Should_SetChecked_If_PassJsonWithCheckedTrueString() {
        let json: JSONObject = ["checked": "true"]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.checked, true)
    }
    
    func test_Should_SetChecked_If_PassJsonWithCheckedFalseString() {
        let json: JSONObject = ["checked": "false"]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.checked, false)
    }
    
    func test_Should_SetCheckedFalse_If_PassNotValueInJson() {
        let json: JSONObject = ["id": UUID().uuidString]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.checked, false)
    }
    
    func test_Should_SetValue_If_PassValue() {
        let value = UUID().uuidString
        let entity = EntityModel(.photo, value: value)
        XCTAssertEqual(entity.value, value)
    }
    
    func test_Should_ReturnJsonWithId_If_PassJsonWithId() {
        let json: JSONObject = ["id": UUID().uuidString]
        let entity = EntityModel(.photo, mid: nil, uid: nil, json: json)
        XCTAssertEqual(entity.json["id"] as? String, json["id"] as? String)
    }
}
