//
//  PriorityTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class EditingTextHelperTests: XCTestCase {
    var models: [EntityModel]!
    var order: [String]!
    
    override func setUp() {
        super.setUp()
        models = []
        
        for _ in stride(from: 0, to: 4, by: 1) {
            models.append(EntityModel(.text, value: UUID().uuidString))
        }
        
        order = models.map({ $0.id }).shuffled()
    }

    func test_Should_ModelsCountBe4_If_StrideFrom0To4() {
        XCTAssert(models.count == 4)
    }
    
    func test_Should_ReturnReducedText_If_UseReduceByAddingNewLine() {
        let text = models.reduce("", { result, item in
            return result.isEmpty ? item.value : result + Symbol.newLine.rawValue + item.value
        })
        XCTAssertEqual(EditingTextHelper.text(from: models), text)
    }
    
    func test_Should_ReturnOrderedText_If_CallTextWithOrder() {
        var newModels: [EntityModel] = []
        for id in order {
            for model in models where model.id == id {
                newModels.append(model)
            }
        }
        let text = EditingTextHelper.text(from: newModels)
        XCTAssertEqual(EditingTextHelper.text(models, order: order), text)
    }

    func test_Should_HasSameNumberOfItems_If_ChangeFirstLine() {
        let text = UUID().uuidString + Symbol.newLine.rawValue + EditingTextHelper.text(Array(models.suffix(from: 1)), order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        XCTAssertEqual(newModels.count, models.count)
    }

    func test_Should_UpdateFirstItem_If_ChangeFirstLine() {
        let newFirst = UUID().uuidString
        let text = newFirst + Symbol.newLine.rawValue + EditingTextHelper.text(Array(models.suffix(from: 1)), order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        XCTAssertEqual(newModels[0].value, newFirst)
    }

    func test_Should_RemoveFirstItem_If_RemoveFirstLine() {
        let suffix = Array(models.suffix(from: 1))
        let text = EditingTextHelper.text(suffix, order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        let orderedSuffix = EditingTextHelper.ordered(suffix, order: order)
        XCTAssertEqual(newModels, orderedSuffix)
    }

    func test_Should_InsertFirstItem_If_InsertFirstLine() {
        let newFirst = UUID().uuidString
        let text = newFirst + Symbol.newLine.rawValue + EditingTextHelper.text(models, order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        XCTAssertEqual(newModels[0].value, newFirst)
    }

    func test_Should_IncrementNumberOfItems_If_InsertFirstLine() {
        let newFirst = UUID().uuidString
        let text = newFirst + Symbol.newLine.rawValue + EditingTextHelper.text(models, order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        XCTAssertEqual(newModels.count, models.count + 1)
    }

    func test_Should_NotInsertFirstItemEmpty_If_InsertNewLine() {
        let text = Symbol.newLine.rawValue + EditingTextHelper.text(models, order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        XCTAssertEqual(newModels[0].value, models.first(where: { $0.id == order[0] })?.value)
    }

    func test_Should_UpdateFirstItem_If_ChangeFirstLine1() {
        let newFirst = UUID().uuidString
        let text = newFirst + Symbol.newLine.rawValue + EditingTextHelper.text(Array(models.suffix(from: 2)), order: order)
        let newModels = EditingTextHelper.updated(models, order: order, type: .text, text: text)
        XCTAssertEqual(newModels[0].value, newFirst)
    }
}
