//
//  CellParametersModelHelper.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
@testable import TOTHEM

extension CellParametersModel {
    static var center: CellParametersModel {
        var parameters = CellParametersModel.default
        parameters.insets = .init(top: 0, left: 20, bottom: 0, right: 20)
        parameters.separator = 20
        parameters.separator_color = .clear
        parameters.background_color = UIColor(hue: 120/360.0, saturation: 35/100.0, brightness: 50 / 100.0, alpha: 90/100.0)
        return parameters
    }
}
