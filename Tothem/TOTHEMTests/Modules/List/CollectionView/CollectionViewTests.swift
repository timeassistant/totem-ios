//
//  CollectionViewTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift
import FBSnapshotTestCase
@testable import TOTHEM

final class CollectionViewTests: FBSnapshotTestCase {
    var view: CollectionView!
    var topParameters = CellParametersModel.default
    var bottomParameters = CellParametersModel.default
    private var utils: JSONUtils!
    
    override func setUp() {
        super.setUp()
        CreateCollectionCellsUseCase.imageLoader = { _, _ in
            return Observable.empty()
        }

        utils = JSONUtils(bundle: Bundle(for: type(of: self)))

        let controller = TasksViewController()
        controller.loadViewIfNeeded()
        //view = controller.bottomCollectionView
        view.backgroundColor = .white
        view.collectionDataSource.parameters = CellParametersModel.center
        view.parameters = CellParametersModel.center
        view.set([])
    }

    func task(from file: String) -> TaskModel {
        let json: JSONObject = try! utils.read(from: file)
        return TaskModel(json)
    }

//    func test_CollectionView() {
//        let model = task(from: "collection-view-task-model-center-2")
//        let (_, bottom) = CreateTaskCollectionsUseCase.create(.center, [model])
//        
//        view.set(CreateCollectionCellsUseCase.create(bottom, .center))
//        FBSnapshotVerifyView(view)
//    }
}
