//
//  CenterCollectionViewControllerTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift
import FBSnapshotTestCase
@testable import TOTHEM

final class CenterCollectionViewControllerTests: CollectionViewControllerTests {
    override func setUp() {
        topParameters = CellParametersModel.center
        bottomParameters = CellParametersModel.center

        super.setUp()
    }

    private func mockedTasks() -> [TaskModel] {
        var models = [task(from: "center-controller-task-model-center-2")]
        for index in stride(from: 0, to: 10, by: 1) {
            let model = task(from: "center-controller-task-model-center-1")
            model.title = "index \(index)"
            models.append(model)
        }
        return models
    }
    
//    func test_CenterCollectionViewControllerBottom() {
//        let model = task(from: "center-controller-task-model-center-1")
//        controller.set(state: .update(.center, [model], UserClass(subject: 1, unique: 1, creative: 1)))
//        FBSnapshotVerifyView(controller.view)
//    }
//    
//    func test_CenterCollectionViewControllerTop() {
//        let model = task(from: "center-controller-task-model-center-2")
//        controller.set(state: .update(.center, [model], UserClass(subject: 1, unique: 1, creative: 1)))
//        FBSnapshotVerifyView(controller.view)
//    }
//    
//    func test_CenterCollectionViewControllerMultipleCells() {
//        controller.set(state: .update(.center, mockedTasks(), UserClass(subject: 1, unique: 1, creative: 1)))
//        FBSnapshotVerifyView(controller.view)
//    }
}
