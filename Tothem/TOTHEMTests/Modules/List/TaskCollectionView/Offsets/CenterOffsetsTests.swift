//
//  CenterOffsetsTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
@testable import TOTHEM

final class CenterOffsetsTests: XCTestCase {
    private var offsets: CollectionViewControllerOffsets!
    private var height: CGFloat = 600
    private var min: CGFloat = 60
    
    private func new(top: CGFloat, bottom: CGFloat) -> CollectionViewControllerOffsets {
        return ListViewScrollOffsets(height: height, min: min, top: top, bottom: bottom)
    }
    
    func test_Should_CalculateMiddle_If_Top100EmptyBottomEmpty() {
        offsets = new(top: 100, bottom: 0)
        XCTAssertEqual(offsets.middle, height / 2)
    }
    
    func test_Should_CalculateMiddle_If_Top400EmptyBottomEmpty() {
        let value: CGFloat = 400
        offsets = new(top: value, bottom: 0)
        XCTAssertEqual(offsets.middle, value)
    }
    
    func test_Should_CalculateMiddle_If_Top700EmptyBottomEmpty() {
        let value: CGFloat = 700
        offsets = new(top: value, bottom: 0)
        XCTAssertEqual(offsets.middle, height - 3 * min / 2)
    }
    
    func test_Should_CalculateTopInset_If_Top100EmptyBottomEmpty() {
        offsets = new(top: 100, bottom: 0)
        XCTAssertEqual(offsets.topInsets.bottom, height * (1 - 0.5))
    }
    
    func test_Should_CalculateTopInset_If_Top400EmptyBottomEmpty() {
        offsets = new(top: 400, bottom: 0)
        XCTAssertEqual(offsets.topInsets.bottom, offsets.middle / 2)
    }
    
    func test_Should_CalculateTopInset_If_Top700EmptyBottomEmpty() {
        offsets = new(top: 700, bottom: 0)
        XCTAssertTrue(offsets.topInsets.bottom.equal(to: height * (1 - 0.85)))
    }
    
    func test_Should_CalculateBottomInset_If_Top100EmptyBottomEmpty() {
        offsets = new(top: 100, bottom: 0)
        XCTAssertEqual(offsets.bottomInsets.top, height * 0.5)
    }
    
    func test_Should_CalculateBottomInset_If_Top400EmptyBottomEmpty() {
        offsets = new(top: 400, bottom: 0)
        XCTAssertEqual(offsets.bottomInsets.top, 400)
    }
    
    func test_Should_CalculateBottomInset_If_Top700EmptyBottomEmpty() {
        offsets = new(top: 700, bottom: 0)
        XCTAssertEqual(offsets.bottomInsets.top, height * 0.85)
    }
    
    func test_Should_CalculateMiddle_If_Bottom100EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 100)
        XCTAssertEqual(offsets.middle, min)
    }
    
    func test_Should_CalculateMiddle_If_Bottom400EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 400)
        XCTAssertEqual(offsets.middle, min)
    }
    
    func test_Should_CalculateMiddle_If_Bottom700EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 700)
        XCTAssertEqual(offsets.middle, min)
    }
    
    func test_Should_CalculateTopInset_If_Bottom100EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 100)
        XCTAssertEqual(offsets.topInsets.bottom, 0)
    }
    
    func test_Should_CalculateTopInset_If_Bottom400EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 400)
        XCTAssertEqual(offsets.topInsets.bottom, 0)
    }
    
    func test_Should_CalculateTopInset_If_Bottom700EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 700)
        XCTAssertEqual(offsets.topInsets.bottom, 0)
    }
    
    func test_Should_CalculateBottomInset_If_Bottom100EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 100)
        XCTAssertEqual(offsets.bottomInsets.top, min)
    }
    
    func test_Should_CalculateBottomInset_If_Bottom400EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 400)
        XCTAssertEqual(offsets.bottomInsets.top, min)
    }
    
    func test_Should_CalculateBottomInset_If_Bottom700EmptyTopEmpty() {
        offsets = new(top: 0, bottom: 700)
        XCTAssertEqual(offsets.bottomInsets.top, min)
    }
}
