//
//  LeftCollectionViewCellTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift
import FBSnapshotTestCase
@testable import TOTHEM

final class CenterCollectionViewCellTests: FBSnapshotTestCase {
    private var cell: CenterCollectionViewCell!
    private var imageLoader: ((String?, String) -> Observable<UIImage>)!
    private var model: CollectionCellModel!
    private var viewModel: CenterCollectionViewCellViewModel!
    private let bundle = Bundle(for: CenterCollectionViewCellTests.self)
    
    override func setUp() {
        super.setUp()
        
        cell = CenterCollectionViewCell.xib()
        
        imageLoader = { _, file in
            if let image = UIImage(contentsOfFile: file) {
                return BehaviorSubject(value: image)
            } else {
                return Observable.empty()
            }
        }

        viewModel = CenterCollectionViewCellViewModel()
        model = CollectionCellModel(reuseId: UUID().uuidString, viewModel: viewModel)
    }
    
    func test_CenterCollectionViewCell() {
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_Checked() {
        viewModel.checked = true
        viewModel.isEmpty = false
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_ShortTitle() {
        let task = TaskModel()
        task.title = "Short title"
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_LongTitle() {
        let task = TaskModel()
        task.title = "Long Long Long Long Long Long title"
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_OneBullet() {
        let task = TaskModel()
        task.title = "One bullet"
        
        ["bullet 1"].forEach {
            let entity = EntityModel(.bullet, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TwoBullets() {
        let task = TaskModel()
        task.title = "Two bullets"
        
        ["bullet 1", "bullet 2"].forEach {
            let entity = EntityModel(.bullet, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TitleOnePhoto() {
        let task = TaskModel()
        task.title = "One photo"
        
        ["Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TitleTwoPhotos() {
        let task = TaskModel()
        task.title = "Two photo"
        
        ["Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TitleThreePhotos() {
        let task = TaskModel()
        task.title = "Two photo"
        
        ["Photo3", "Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TitlePhotosBullets() {
        let task = TaskModel()
        task.title = "Title photos and bullets"
        
        ["Photo4", "Photo3", "Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        ["bullet 1", "bullet 2", "bullet 3"].forEach {
            let entity = EntityModel(.bullet, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TitleTextPhotosBullets() {
        let task = TaskModel()
        task.title = "Title text photos and bullets"
        
        ["Photo2", "Photo1", "Photo4", "Photo3"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        ["bullet 1", "bullet 2", "bullet 3"].forEach {
            let entity = EntityModel(.bullet, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        ["text 1", "text 2", "text 3"].forEach {
            let entity = EntityModel(.text, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TextPhotosBullets() {
        let task = TaskModel()
        
        ["Photo3", "Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        ["bullet 1", "bullet 2", "bullet 3"].forEach {
            let entity = EntityModel(.bullet, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        ["text 1", "text 2", "text 3"].forEach {
            let entity = EntityModel(.text, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_Text() {
        let task = TaskModel()
        task.title = "Title and text with no media"
        
        ["text 1", "text 2", "text 3"].forEach {
            let entity = EntityModel(.text, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_CenterCollectionViewCell_TitleText() {
        let task = TaskModel()
        
        ["text 1", "text 2", "text 3"].forEach {
            let entity = EntityModel(.text, value: $0)
            task.order.append(entity.id)
            task.items[entity.id] = entity
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
}
