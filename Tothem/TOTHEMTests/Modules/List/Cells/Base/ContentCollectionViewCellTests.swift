//
//  ContentCollectionViewCellTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import FBSnapshotTestCase
@testable import TOTHEM
import RxSwift

final class ContentCollectionViewCellTests: FBSnapshotTestCase {
    private var cell: ContentCollectionViewCell!
    
    override func setUp() {
        super.setUp()
        let frame = CGRect(x: 0, y: 0, width: 350, height: 100)
        cell = ContentCollectionViewCell(frame: frame)
    }
    
    func test_ContentCollectionViewCell_ClearBackground() {
        FBSnapshotVerifyView(cell)
    }
    
    func test_ContentCollectionViewCell_RedBackground() {
        cell.apply(color: .green)
        FBSnapshotVerifyView(cell)
    }
    
    func test_ContentCollectionViewCell_Corner() {
        cell.apply(color: .blue)
        let model = CollectionCellModel(reuseId: UUID().uuidString, viewModel: CollectionCellViewModel(BehaviorSubject<CGFloat>(value: 20)))
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
}
