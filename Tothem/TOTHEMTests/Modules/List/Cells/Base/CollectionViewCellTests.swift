//
//  CollectionViewCellTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import FBSnapshotTestCase
@testable import TOTHEM

final class CollectionViewCellTests: FBSnapshotTestCase {
    private var cell: CollectionViewCell!
    
    override func setUp() {
        super.setUp()
        let frame = CGRect(x: 0, y: 0, width: 350, height: 100)
        cell = CollectionViewCell(frame: frame)
    }
    
    func test_CollectionViewCell_ClearBackground() {
        FBSnapshotVerifyView(cell)
    }
    
    func test_CollectionViewCell_RedBackground() {
        cell.apply(color: .red)
        FBSnapshotVerifyView(cell)
    }
}
