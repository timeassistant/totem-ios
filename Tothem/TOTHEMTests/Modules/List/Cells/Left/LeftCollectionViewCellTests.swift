//
//  LeftCollectionViewCellTests.swift
//  Copyright © 2019 Visera. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RxSwift
import FBSnapshotTestCase
@testable import TOTHEM

final class LeftCollectionViewCellTests: FBSnapshotTestCase {
    private var cell: LeftCollectionViewCell!
    private var imageLoader: ((String?, String) -> Observable<UIImage>)!
    private var model: CollectionCellModel!
    private var viewModel: LeftCollectionViewCellViewModel!
    
    override func setUp() {
        super.setUp()
        cell = LeftCollectionViewCell.xib()
        imageLoader = { _, file in
            if let image = UIImage(contentsOfFile: file) {
                return BehaviorSubject(value: image)
            } else {
                return Observable.empty()
            }
        }

        viewModel = LeftCollectionViewCellViewModel()
        model = CollectionCellModel(reuseId: UUID().uuidString, viewModel: viewModel)
    }
    
    func test_LeftCollectionViewCell() {
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_LeftCollectionViewCell_ShortTitle() {
        let task = TaskModel()
        task.title = "Short title"
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_LeftCollectionViewCell_LongTitle() {
        let task = TaskModel()
        task.title = "Long Long Long Long Long Long title"
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_LeftCollectionViewCell_TitleOnePhoto() {
        let task = TaskModel()
        task.title = "One photo"
        
        let bundle = Bundle(for: LeftCollectionViewCellTests.self)
        ["Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_LeftCollectionViewCell_TitleTwoPhotos() {
        let task = TaskModel()
        task.title = "Two photo"
        
        let bundle = Bundle(for: LeftCollectionViewCellTests.self)
        ["Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_LeftCollectionViewCell_TitleThreePhotos() {
        let task = TaskModel()
        task.title = "Two photo"
        
        let bundle = Bundle(for: LeftCollectionViewCellTests.self)
        ["Photo3", "Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
    
    func test_LeftCollectionViewCell_TitleFourPhotos() {
        let task = TaskModel()
        task.title = "Two photo"
        
        let bundle = Bundle(for: LeftCollectionViewCellTests.self)
        ["Photo4", "Photo3", "Photo2", "Photo1"].forEach {
            if let url = bundle.path(forResource: $0, ofType: "png") {
                let entity = EntityModel(.photo, value: url)
                task.order.append(entity.id)
                task.items[entity.id] = entity
            }
        }
        
        viewModel.set(task: task, imageLoader: imageLoader)
        model.viewModel = viewModel
        cell.set(model)
        FBSnapshotVerifyView(cell)
    }
}
