# Visera Totem

Repository for the new version of the [Totem] iOS App. This app is fully written in Swift 5 and makes use of xib files for the UI.

![appicon](/Resources/app_icon.png)

## Requirements

- iOS 11.0 SDK and Xcode 10.2 or upper

- [SwiftLint](https://github.com/realm/SwiftLint)

- [Cocoapods](https://cocoapods.org)

The app is made following style conventions by using this tool, for installation with homebrew use the following command: ```brew install swiftlint```

## Can be used (optional install)

- [Swiftgen](https://github.com/SwiftGen/SwiftGen)

Additional tool for generating access paths to view controllers in storyboards

## To run the app in xcode

### Internal

Run from the repo root ```git submodule init & git submodule update``` to check and download the submodule with app settings (private repo).

Go to TOTHEM project folder and run ```bundle exec fastlane setup_env``` to copy env files into fastlane folder.

Run ```bundle exec fastlane setup_app --dev``` to copy Info.plist files for App and Google services.

Run ```pod update``` to install all pods dependencies

### External

Create a free account on firebase and create two projects. The first one for users data and the second one for parameters.

In the first project Authorization section enable login with email. Setup Realtime database, keep privacy setting as it is.
In the second project setup Realtime database, set privacy settings to readonly mode:
```
{
  "rules": {
    ".read": true,
    ".write": false
  }
}
```

Import interface parameters in the Realtime database from the file:

http://bit.ly/2N7WzYl

## Contributor guidelines

### Creating a pull request
* All pull requests must be associated with a specific Issue. If an issue doesn't exist please first create it.

### Swift style
* Swift code should generally follow the conventions listed at https://github.com/raywenderlich/swift-style-guide.
  * Exception: we use 4-space indentation instead of 2.
  * This is a loose standard. We do our best to follow this style

### Whitespace
* New code should not contain any trailing whitespace.
* We recommend enabling both the "Automatically trim trailing whitespace" and "Including whitespace-only lines" preferences in Xcode (under Text Editing).
* <code>git rebase --whitespace=fix</code> can also be used to remove whitespace from your commits before issuing a pull request.

### Commits
* Each commit should have a single clear purpose. If a commit contains multiple unrelated changes, those changes should be split into separate commits.
* If a commit requires another commit to build properly, those commits should be squashed.
* Follow-up commits for any review comments should be squashed. Do not include "Fixed PR comments", merge commits, or other "temporary" commits in pull requests.

## Git flow

Our git flow can be described in this steps:

- master will be our main branch

- features will branch from master and merge into master always by PR

- release branches will come from master, have production configuration and small fixes. The release tags will be made here

- hotfixes will branch from the last release tag. When finished, a new version tag will be made and then merged into master

![gitflow](/Resources/git-flow.png)

## Branch naming

Currently we have the current branch naming: ```prefix/business-area/short-description```

* Complete Bitrise build, it goes to testflight and tested and lint checked:

-  ```hotfix```: Fixes a production bug.

-  ```release```: Build from master with all the closed features for a certain release.

-  ```feature```: A certain complete feature.

-  ```fix```: Bugfix.

-  ```test```: Add tests to a certain piece of code.

-  ```refactor```: Refactors a certain piece of code.

-  ```documentation```: Adds documentation.

Business areas:

-  ```timeline```

-  ```preview```

-  ```editing```

-  ```creation```

-  ```adaptation```

-  ```backend```

## Architecture

To be specified

## Libraries

To be specified

## Additional tools

### [Bitrise CI](https://bitrise.io)

We use Bitrise to build the app and submit it to TestFlight

### [Fastlane](https://fastlane.tools)

We use Fastlane for signing and building the app

### [Slack](https://slack.com)

We use Slack for communication and starting builds in Bitrise
